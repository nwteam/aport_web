/*
 * list-contact.js 
 */

function goTypePage(no) {
  document.getElementById("hid_message_type").value = no;
  document.getElementById("frm_message").action = "list-contact.php";
  document.getElementById("frm_message").submit();
}

function goMessage(no) {
  document.getElementById("no").value = no;
  document.getElementById("frm_message").action = "detail-contact.php";;
  document.getElementById("frm_message").submit();
}

function goDelete(no) {
  if (confirm("削除処理を行います。宜しいですか？")) {
    document.getElementById("no").value = no;
    document.getElementById("frm_message").action = "del-contact.php";
    document.getElementById("frm_message").submit();
  }
}

function goMessagePage(no) {
  document.getElementById("page").value = no;
  document.getElementById("frm_message").action = "list-contact.php";
  document.getElementById("frm_message").submit();
}