/*
 * reg-project.js 
 */

var INPUT_MSG_INSERT = "プロジェクトを登録します。宜しいですか？";
var INPUT_MSG_IMAGE = "イメージファイルを選択してください";

$(function(){

});

function goSubmit() {
  if ($("#frm_project").validationEngine('validate')) {
    if (!document.getElementById("myFile").value) {
      document.getElementById("myFile").focus();
      alert(INPUT_MSG_IMAGE);
      return false;
    } else {
      document.getElementById("hid_image").value = "Y";
    }

    var isYn = confirm(INPUT_MSG_INSERT);
    if (isYn) {
      document.getElementById("hid_send").value = "Y";
      return true;
    } else {
      return false;
    }
  } else {
  	return false;
  }
}

