/*
 * index.js
 */

function goProject(no) {
  document.getElementById("hid_project_no").value = no;
  document.getElementById("frm_project").action = "detail-project.php?p_no="+no;
  document.getElementById("frm_project").submit();
}

function goSearch(no) {
  document.getElementById("cmb_category").value = no;
  document.getElementById("frm_project").action = "list-project.php";
  document.getElementById("frm_project").submit();
}

function goRanking(no) {
  document.getElementById("hid_ranking").value = no;
  document.getElementById("frm_project").action = "index.php";
  document.getElementById("frm_project").submit();
}

function goSupport(no) {
  document.getElementById("no").value = no;
  document.getElementById("frm_project").action = "book-support.php";
  document.getElementById("frm_project").submit();
}