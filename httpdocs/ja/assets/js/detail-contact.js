/*
 * detail-contact.js 
 */

function goSend(no) {
  document.getElementById("no").value = no;
  document.getElementById("frm_message").action = "send-contact.php";;
  document.getElementById("frm_message").submit();
}

function goDelete(no) {
  if (confirm("削除処理を行います。宜しいですか？")) {
    document.getElementById("no").value = no;
    document.getElementById("frm_message").action = "del-contact.php";
    document.getElementById("frm_message").submit();
  }
}
