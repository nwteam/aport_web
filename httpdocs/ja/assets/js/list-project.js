/*
 * list-project.js 
 */

$(function(){

});

function goSearch() {
  if ($("#frm_project").validationEngine('validate')) {
    document.getElementById("frm_project").submit();
  }
}

function goProject(no) {
  if ($("#frm_project").validationEngine('validate')) {
    document.getElementById("hid_page_list").value = no;
    document.getElementById("frm_project").submit();
  }
}

function compareNum(field, rules, i, options){
  if ($("#txt_wish_price_to").val() != "") {
    if (parseInt(field.val()) > parseInt($("#txt_wish_price_to").val())) {
      // this allows to use i18 for the error msgs
      return "正しい数字を入力してください";
    }
  }
}

function goProjectPage(no) {
  document.getElementById("page").value = no;
  document.getElementById("frm_project").action = "list-project.php";
  document.getElementById("frm_project").submit();
}

function goProjectDetail(no) {
  document.getElementById("frm_project").action = "detail-project.php?p_no="+no;
  document.getElementById("hid_project_no").value = no;
  document.getElementById("frm_project").submit();
}

function goSupport(no) {
  document.getElementById("no").value = no;
  document.getElementById("frm_project").action = "book-support.php";
  document.getElementById("frm_project").submit();
}