$.extend(validatePrompt, {
    project_type:{
        onFocus:"",
        succeed:"",
        isNull:"※必須項目です",
        error:""
    },
    public_title:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※40文字以内で入力してください。",
            badFormat:""
        }
    },
    thanks_msg:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※1000文字以内で入力してください。",
            badFormat:""
        }
    },
    facebook_page:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※300文字以内で入力してください。",
            badFormat:""
        }
    },
    twitter_account:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※300文字以内で入力してください。",
            badFormat:""
        }
    },
    blog_url_1:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※300文字以内で入力してください。",
            badFormat:""
        }
    },
    blog_url_2:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※300文字以内で入力してください。",
            badFormat:""
        }
    },
    blog_url_3:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※300文字以内で入力してください。",
            badFormat:""
        }
    },
    full_name:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※45文字以内で入力してください。",
            badFormat:""
        }
    },
    introduce:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※300文字以内で入力してください。",
            badFormat:""
        }
    },
    bank_name_txt:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※45文字以内で入力してください。",
            badFormat:""
        }
    },
    branch_bank:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※45文字以内で入力してください。",
            badFormat:""
        }
    },
    card_owner:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※45文字以内で入力してください。",
            badFormat:""
        }
    },
    wish_price:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※半角数字20桁以内で入力してください。",
            badFormat:"※半角数字で入力してください"
        }
    },
    branch_bank_code:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※半角数字35桁以内で入力してください。",
            badFormat:"※半角数字で入力してください"
        }
    },
    card_number:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※半角数字35桁以内で入力してください。",
            badFormat:"※半角数字で入力してください"
        }
    },
    movie_type:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:""
    },
    movie_url:{
        onFocus:"",
        succeed:"",
        isNull:"",
        error:{
            badLength:"※255桁以内で入力してください。",
            badFormat:"※正確なURLで入力してください。例：https://youtu.be/xxxxxx"
        }
    }
});

$.extend(validateFunction, {
    project_type:function(option) {
		var bool = (option.value == -1);
        if (bool) {
            validateSettings.isNull.run(option, "");
        }
        else {
            validateSettings.succeed.run(option);
        }
    },
    public_title:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 80);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    movie_url:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 255);
        var format = validateRules.isUrl(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    thanks_msg:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 300);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    facebook_page:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 300);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    twitter_account:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 300);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    blog_url_1:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 300);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    blog_url_2:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 300);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    blog_url_3:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 300);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    full_name:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 45);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    introduce:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 300);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    bank_name_txt:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 45);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    branch_bank:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 45);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    card_owner:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 45);
        var format = validateRules.isPublicTitle(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    wish_price:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 20);
        var format = validateRules.fullNumberName(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    branch_bank_code:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 35);
        var format = validateRules.fullNumberName(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    card_number:function(option) {
        var length = validateRules.betweenLength(option.value.replace(/[^\x00-\xff]/g, "**"), 1, 35);
        var format = validateRules.fullNumberName(option.value);
        if (!length) {
            validateSettings.error.run(option, option.prompts.error.badLength);
        }
        else {
            if (!format) {
                validateSettings.error.run(option, option.prompts.error.badFormat);
            } else {
                validateSettings.succeed.run(option);
            }
        }
    },
    FORM_validate:function() {
		$("#project_type").jdValidate(validatePrompt.project_type, validateFunction.project_type);
		$("#public_title").jdValidate(validatePrompt.public_title, validateFunction.public_title);
		$("#wish_price").jdValidate(validatePrompt.wish_price, validateFunction.wish_price);
		$("#movie_type").jdValidate(validatePrompt.movie_type, validateFunction.movie_type);
		$("#movie_url").jdValidate(validatePrompt.movie_url, validateFunction.movie_url);
		$("#category_no").jdValidate(validatePrompt.category_no, validateFunction.category_no);
		$("#thanks_msg").jdValidate(validatePrompt.thanks_msg, validateFunction.thanks_msg);
		$("#facebook_page").jdValidate(validatePrompt.facebook_page, validateFunction.facebook_page);
		$("#twitter_account").jdValidate(validatePrompt.twitter_account, validateFunction.twitter_account);
		$("#blog_url_1").jdValidate(validatePrompt.blog_url_1, validateFunction.blog_url_1);
		$("#blog_url_2").jdValidate(validatePrompt.blog_url_2, validateFunction.blog_url_2);
		$("#blog_url_3").jdValidate(validatePrompt.blog_url_3, validateFunction.blog_url_3);
		$("#full_name").jdValidate(validatePrompt.full_name, validateFunction.full_name);
		$("#introduce").jdValidate(validatePrompt.introduce, validateFunction.introduce);
		$("#bank_name").jdValidate(validatePrompt.bank_name, validateFunction.bank_name);
		$("#bank_name_txt").jdValidate(validatePrompt.bank_name_txt, validateFunction.bank_name_txt);
		$("#branch_bank").jdValidate(validatePrompt.branch_bank, validateFunction.branch_bank);
		$("#branch_bank_code").jdValidate(validatePrompt.branch_bank_code, validateFunction.branch_bank_code);
		$("#card_type").jdValidate(validatePrompt.card_type, validateFunction.card_type);
		$("#card_number").jdValidate(validatePrompt.card_number, validateFunction.card_number);
		$("#card_owner").jdValidate(validatePrompt.card_owner, validateFunction.card_owner);
        return validateFunction.FORM_submit(["#project_type","#public_title","#wish_price","#movie_type","#movie_url","#category_no","#project_record_date","#thanks_msg","#return_min_1","#return_min_2","#return_min_3","#return_min_4","#return_min_5","#return_title_1","#return_title_2","#return_title_3","#return_title_4","#return_title_5","#return_text_1","#return_text_2","#return_text_3","#return_text_4","#return_text_5","#max_count_1","#max_count_2","#max_count_3","#max_count_4","#max_count_5","#facebook_page","#twitter_account","#blog_url_1","#blog_url_2","#blog_url_3","#full_name","#introduce","#bank_name","#bank_name_txt","#branch_bank","#branch_bank","#branch_bank_code","#card_type","#card_number","#card_owner"]);
    }
});

//默认下用户名框获得焦点
//setTimeout(function() {
//    $("#project_type").get(0).focus();
//}, 0);

$("#project_type").jdValidate(validatePrompt.project_type, validateFunction.project_type);
$("#public_title").jdValidate(validatePrompt.public_title, validateFunction.public_title);
$("#wish_price").jdValidate(validatePrompt.wish_price, validateFunction.wish_price);
$("#movie_type").jdValidate(validatePrompt.movie_type, validateFunction.movie_type);
$("#movie_url").jdValidate(validatePrompt.movie_url, validateFunction.movie_url);
$("#category_no").jdValidate(validatePrompt.category_no, validateFunction.category_no);
$("#thanks_msg").jdValidate(validatePrompt.thanks_msg, validateFunction.thanks_msg);
$("#facebook_page").jdValidate(validatePrompt.facebook_page, validateFunction.facebook_page);
$("#twitter_account").jdValidate(validatePrompt.twitter_account, validateFunction.twitter_account);
$("#blog_url_1").jdValidate(validatePrompt.blog_url_1, validateFunction.blog_url_1);
$("#blog_url_2").jdValidate(validatePrompt.blog_url_2, validateFunction.blog_url_2);
$("#blog_url_3").jdValidate(validatePrompt.blog_url_3, validateFunction.blog_url_3);
$("#full_name").jdValidate(validatePrompt.full_name, validateFunction.full_name);
$("#introduce").jdValidate(validatePrompt.introduce, validateFunction.introduce);
$("#bank_name").jdValidate(validatePrompt.bank_name, validateFunction.bank_name);
$("#bank_name_txt").jdValidate(validatePrompt.bank_name_txt, validateFunction.bank_name_txt);
$("#branch_bank").jdValidate(validatePrompt.branch_bank, validateFunction.branch_bank);
$("#branch_bank_code").jdValidate(validatePrompt.branch_bank_code, validateFunction.branch_bank_code);
$("#card_type").jdValidate(validatePrompt.card_type, validateFunction.card_type);
$("#card_number").jdValidate(validatePrompt.card_number, validateFunction.card_number);
$("#card_owner").jdValidate(validatePrompt.card_owner, validateFunction.card_owner);
