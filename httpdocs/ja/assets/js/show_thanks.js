	$(function(){
		$("#submit").click(function(){
			var email = $("#email_input").val();
			if (!email.match(/^([a-zA-Z0-9])+([a-zA-Z0-9¥._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9¥._-]+)+$/)){
				jAlert("メールアドレスの形式が間違っています。メールアドレスを正しく入力してください", "メールアドレスをご確認ください");
				return false;
			}else{
				$("#formID").hide();
				$("#thankyou").fadeIn();
			}
		});
	});