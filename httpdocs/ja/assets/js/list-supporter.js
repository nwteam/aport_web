/*
 * list-supporter.js 
 */

function goMemberPage(no) {
  document.getElementById("hid_member_no").value = no;
  document.getElementById("frm_member").action = "detail-member.php";
  document.getElementById("frm_member").submit();
}

function goSupportPage(project_no, invest_no) {
  document.getElementById("no").value = project_no;
  document.getElementById("hid_support_no").value = invest_no;
  document.getElementById("frm_member").action = "book-support.php";
  document.getElementById("frm_member").submit();
}

function goSupportAdmin(project_no) {
  var check_num = document.getElementById("frm_member").chk_sel.length;
  var rtn = false;
  if (check_num>1) {
    for (var i=0; i<check_num; i++) {
      if (document.getElementById("frm_member").chk_sel[i].checked) {
      	rtn = true;
      }
    }
  } else {
    if (document.getElementById("frm_member").chk_sel.checked) {
      rtn = true;
    }
  }

  if (!rtn) {
    alert("該当項目を選択してください。");
    return;
  } else {
  	if (confirm("出資確定処理を行います。宜しいですか？")) {
      document.getElementById("frm_member").action = "book-send-admin.php";
      document.getElementById("frm_member").submit();
    }
  }
}

function chkSelect(num) {
  var check_num = document.getElementById("frm_member").chk_sel.length;
  if (check_num>1) {
    for (var i=0; i<check_num; i++) {
      if (parseInt(num) != i) {
        document.getElementById("frm_member").chk_sel[i].checked = false;
      }
    }
  }
}