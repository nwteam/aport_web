/*
 * upt-member-project.js
 */

var INPUT_MSG_UPDATE = "会員詳細情報を変更します。宜しいですか？";
var INPUT_MSG_IMAGE = "イメージファイルを選択してください";
var INPUT_MSG_ZIP = "郵便番号を確認してください。";
var INPUT_MSG_ADD = "住所は全角のみ入力してください。";
var INPUT_MSG_TEL = "形式を確認してください。";

$(function(){
	$("#txt_birth_date").datepicker({
//		minDate: '-0d',
		dateFormat: "yy/mm/dd",
		showMonthAfterYear: false,
		showButtonPanel: true,

	});

});

function goSubmit() {
  if ($("#frm_member").validationEngine('validate')) {

    var strZipcode = document.getElementById("txt_zipcode").value;
    var strAdd2 = document.getElementById("txt_add_2").value;
    var strAdd3 = document.getElementById("txt_add_3").value;
    var strTel = document.getElementById("txt_tel").value;
    var strMobileTel = document.getElementById("txt_mobile_tel").value;
    var strFax = document.getElementById("txt_fax").value;
    var strBirth = document.getElementById("txt_birth_date").value;

    if (strZipcode != "" && !chkZipcode(strZipcode)) {
      document.getElementById("txt_zipcode").focus();
      alert(INPUT_MSG_ZIP);
      return false;
    }

    if (strAdd2 != "" && !chkZenkaku(strAdd2)) {
      document.getElementById("txt_add_2").focus();
      alert(INPUT_MSG_ADD);
      return false;
    }

    if (strAdd3 != "" && !chkZenkaku(strAdd3)) {
      document.getElementById("txt_add_3").focus();
      alert(INPUT_MSG_ADD);
      return false;
    }

    if (strTel != "" && !chkTel(strTel)) {
      document.getElementById("txt_tel").focus();
      alert(INPUT_MSG_TEL);
      return false;
    }

    if (strMobileTel != "" && !chkTel(strMobileTel)) {
      document.getElementById("txt_mobile_tel").focus();
      alert(INPUT_MSG_TEL);
      return false;
    }

    if (strFax != "" && !chkTel(strFax)) {
      document.getElementById("txt_fax").focus();
      alert(INPUT_MSG_TEL);
      return false;
    }

    if (strBirth != "" && !chkDateFormat(strBirth)) {
      document.getElementById("txt_birth_date").focus();
      alert(INPUT_MSG_TEL);
      return false;
    }

    if (!document.getElementById("myFile").value && !document.getElementById("hid_img_ok").value) {
      document.getElementById("myFile").focus();
      alert(INPUT_MSG_IMAGE);
      return false;
    }
    var isYn = confirm(INPUT_MSG_UPDATE);
    if (isYn) {
      document.getElementById("hid_send").value = "Y";
      if (document.getElementById("myFile").value) {
        document.getElementById("hid_image_1").value = "Y";
      }
      /*
      if (document.getElementById("myFile_1").value) {
        document.getElementById("hid_image_2").value = "Y";
      }
      if (document.getElementById("myFile_2").value) {
        document.getElementById("hid_image_3").value = "Y";
      }
      */
      return true;
    } else {
      return false;
    }
  } else {
  	return false;
  }
}

