/*
 * rank-project.js 
 */

function goProjectList(no) {
  document.getElementById("hid_page_list").value = no;
  document.getElementById("hid_arrange").value = "";
  document.getElementById("hid_yyyymm").value = "";
  document.getElementById("page").value = "";
  document.getElementById("frm_project").action = "rank-project.php";
  document.getElementById("frm_project").submit();

}

function goArrangeList(no) {
  document.getElementById("hid_arrange").value = no;
  document.getElementById("frm_project").action = "rank-project.php";
  document.getElementById("frm_project").submit();
}

function goMonthList(yyyymm) {
  document.getElementById("hid_yyyymm").value = yyyymm;
  document.getElementById("frm_project").action = "rank-project.php";
  document.getElementById("frm_project").submit();
}

function goProjectPage(no) {
  document.getElementById("page").value = no;
  document.getElementById("frm_project").action = "rank-project.php";
  document.getElementById("frm_project").submit();
}