/*
 * support-project.js 
 */

function goProjectPage(no) {
  document.getElementById("page").value = no;
  document.getElementById("frm_project").action = "support-project.php";
  document.getElementById("frm_project").submit();
}

function goProjectDetail(no) {
  document.getElementById("frm_project").action = "detail-project.php?p_no="+no;
  document.getElementById("hid_project_no").value = no;
  document.getElementById("frm_project").submit();
}