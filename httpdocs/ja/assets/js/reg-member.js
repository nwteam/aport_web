/*
 * reg-member.js
 */

var INPUT_MSG_INSERT = "会員詳細情報を登録します。宜しいですか？";
//var INPUT_MSG_IMAGE = "イメージファイルを選択してください";
var INPUT_MSG_KANA = "フリガナは、「カタカナ」のみで入力してください。";

$(function(){

});

function goSubmit() {
  if ($("#frm_member").validationEngine('validate')) {

    var strKana1 = document.getElementById("txt_kana_1").value;
    var strKana2 = document.getElementById("txt_kana_2").value;

    if (!FuriganaCheck(strKana1)) {
      document.getElementById("txt_kana_1").focus();
      alert(INPUT_MSG_KANA);
      return false;
    }
    if (!FuriganaCheck(strKana2)) {
      document.getElementById("txt_kana_2").focus();
      alert(INPUT_MSG_KANA);
      return false;
    }

    var isYn = confirm(INPUT_MSG_INSERT);
    if (isYn) {
      document.getElementById("hid_send").value = "Y";
      if (document.getElementById("myFile").value) {
        document.getElementById("hid_image_1").value = "Y";
      }
      if (document.getElementById("myFile_1").value) {
        document.getElementById("hid_image_2").value = "Y";
      }
      if (document.getElementById("myFile_2").value) {
        document.getElementById("hid_image_3").value = "Y";
      }
      return true;
    } else {
      return false;
    }
  } else {
  	return false;
  }
}
