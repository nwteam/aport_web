//マウスオン
$(function(){
     $('img').hover(function(){
        $(this).attr('src', $(this).attr('src').replace('_off', '_on'));
          }, function(){
             if (!$(this).hasClass('currentPage')) {
             $(this).attr('src', $(this).attr('src').replace('_on', '_off'));
        }
   });
});
//スクロール
$(function(){
	$('a[href^=#]').click(function(){
		var speed = 300;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});



//アコーディオン	
    //$(window).resize(function() {
//        var $g = $('#navigation');
//        if ($(window).width() <= 640) {
//            $g.hide();
//        } else {
//            $g.show();
//        }
//    });
    $(function(){
        var $g = $('#navigation');
        var $btn = $('#menu');
        $btn.on('click', function() {
            $g.not(':animated').slideToggle(400, function() {
                $btn.toggleClass('act');
            });
        });
    });
