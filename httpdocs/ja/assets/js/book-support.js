/*
 * book-support.js
 */

var INPUT_MSG_BOOK = "サポート処理を行います。宜しいですか？";
var INPUT_MSG_DELETE = "サポートを削除します。宜しいですか？";
var MAX_AMOUNT = "100000";

$(function(){
});

function goSubmit() {

  if ($("#frm_project").validationEngine('validate')) {

    var tmp_attain_amount = document.getElementById("hid_attain_amount").value;
    var tmp_invest_amount = document.getElementById("txt_invest_amount").value;
/*    tmp_present_min = new Array();

    for(cnt=1;cnt<=9;cnt++){
      	if(document.getElementById("hid_present" + cnt + "_min") != null){
    		tmp_present_min[cnt] = document.getElementById("hid_present" + cnt + "_min").value;
    	}
    }
*/

    var tmp_present_no = "";
    var tmp_present_min = 0;
    for(cnt=0;cnt<=8;cnt++){
    	if (document.getElementById("frm_project").rdo_present_no[cnt]!=undefined){
    		if (document.getElementById("frm_project").rdo_present_no[cnt].checked) {
    		    tmp_present_no = document.getElementById("frm_project").rdo_present_no[cnt].value;
    		    tmp_present_min=document.getElementById("hid_present" + tmp_present_no + "_min").value
    		}
    	}
    }
     //if (parseInt(tmp_invest_amount)>parseInt(tmp_attain_amount)) {
    //  alert("サポート金額が目標サポート金額を超えています。");
    //  return false;
    //}
    //alert(tmp_present_no);
    //alert(tmp_invest_amount);
    //alert(tmp_present3_min);

    if (parseInt(tmp_invest_amount)>parseInt(MAX_AMOUNT)) {
      //alert("サポート金額が10万円を超えています。");
	    if (!confirm("サポート金額が10万円を超えていますが、よろしいですか？")){
	    	return false;
	    }
    }
    if ( parseInt(tmp_present_min)>parseInt(tmp_invest_amount)) {
      alert("お返し選択がサポート金額を超えています。");
      return false;
    }

    var isYn = confirm(INPUT_MSG_BOOK);
    if (isYn) {
      document.getElementById("hid_send").value = "Y";
      document.getElementById("frm_project").action = "book-send.php";
      document.getElementById("frm_project").submit();
      return true;
    } else {
      return false;
    }
  } else {
  	return false;
  }
}

function goMemberPage(no) {
  document.getElementById("hid_member_no").value = no;
  document.getElementById("frm_project").action = "detail-member.php";
  document.getElementById("frm_project").submit();
}

function goSupportDelete(no) {
  var isYn = confirm(INPUT_MSG_DELETE);
  if (isYn) {
    document.getElementById("hid_support_no").value = no;
    document.getElementById("frm_project").action = "del-support.php";
    document.getElementById("frm_project").submit();
  }
}

function goProjectDetail() {
  document.getElementById("frm_project").action = "detail-project.php?p_no="+no;
  document.getElementById("frm_project").submit();
}

function goSupportList(no) {
	  document.getElementById("hid_support_no").value = no;
	  document.getElementById("frm_project").action = "list-supporter.php";
	  document.getElementById("frm_project").submit();
}