/**
 * @namespace APORT
 */
var APORT = APORT || {};

APORT.common = {

	/**
	 * ページ最上部までスクロール
	 */
	scrollTop: function () {
		$('html, body').animate({
			scrollTop: 0
		}, 450);
	},

	scrollTopFade: function() {
		var scrollpx = 200,
			scrollBottom = $(window).scrollTop() + $(window).height(),
			footerHeight = $('.footer').find('.footer-inner').offset().top,
			scrollStop = scrollBottom - footerHeight + 20,
			$fadeTarget = $('#jsi-scroll-top-fade');

		if ($(window).scrollTop() > scrollpx) {
			$fadeTarget.fadeIn();
		} else {
			$fadeTarget.fadeOut();
		}

		if(scrollBottom >= footerHeight ) {
			$($fadeTarget).css('bottom', scrollStop);
		} else {
			$($fadeTarget).css('bottom', '');
		}
	},

	/**
	 * 指定のページ内リンクまでスクロール
	 */
	scrollToInternalLink: function (target) {
		var $target = $(target),
			link = $target.attr('href'),
			$linkInternalTarget = $(link);
			$hdHeight = $('.hd').height();
			$linkInternalTargetPosition = ($linkInternalTarget.offset().top) - $hdHeight;

		$('body, html').stop().animate({
			scrollTop: $linkInternalTargetPosition
		});
	},

	/**
	 * ドロップダウンメニュー
	 */
	dropdown: (function () {

		return {

			headerNavToggle: function (targetArea,closeArea) {
				var $dropdownArea = $(targetArea),
					$closeArea =  $(closeArea);

				if ($dropdownArea.hasClass('jsc-op')) {
					this.hide($dropdownArea);
				}else {
					this.show($dropdownArea);
					this.hide($closeArea);
				}
			},

			loginMenuToggle: function (targetArea) {

				var $toggleArea = $(targetArea);
				if ($toggleArea.hasClass('jsc-op')) {
					this.hide($toggleArea);
				}else {
					this.show($toggleArea);
				}
			},

			/**
			 * ドロップダウンメニューを表示する
			 */
			show: function (target) {
				target.addClass('jsc-op');
			},

			/**
			 * ドロップダウンメニューを非表示にする
			 */
			hide: function (target) {
				target.removeClass('jsc-op');
			}
		};
	})(),

	localNav: function (target) {
		var $target = $('.jsc-local-nav-category'),
			addclassName = "local-nav-category-open";


		if($target.hasClass(addclassName)) {
			$target.removeClass(addclassName);
		}else {
			$target.addClass(addclassName);
			$target.addClass(addclassName);
		}
	},
	/**
	 * gifアニメ
	 */
	// gifAnimate: (function () {
	// 	var now = (new Date()).getTime(),
	// 		$target = $('.jsc-gif-animate'),
	// 		$src = $target.attr('src');
	// 		$target.attr('src', $src + '?' + now);
	// 		$('.jsc-gif-animate').removeClass('jsc-dn');
	// })(),
	gifAnimate: (function () {
	    var now = (new Date()).getTime(),
	        $target = $('.jsc-gif-animate').eq(0),
	        $src = $target.attr('src');
	        $target.attr('src', $src + '?' + now);
	        $('.jsc-gif-animate').removeClass('jsc-dn');

	        $target2 = $('.jsc-gif-animate').eq(1),
	        $src2 = $target2.attr('src');
	        $target2.attr('src', $src2 + '?' + now);

	})(),

	toggleMortion: (function (targetArea) {

		return {
			toggle: function (targetArea) {
				var $dropdownArea = $(targetArea);

				if ($dropdownArea.hasClass('jsc-op')) {
					$dropdownArea.removeClass('jsc-op');
				}else {
					$dropdownArea.addClass('jsc-op');
				};
			},
			toggleAddClass : function (target, addclassName) {
				var $target = $(target);

				if ($target.hasClass(addclassName)) {
					$target.removeClass(addclassName);
				}else {
					$target.addClass(addclassName);
				};
			},
			toggleProjectDetail: function (btnOpen, btnClose, target) {
				var $target = $(target),
					$btnOpen = $(btnOpen),
					$btnClose = $(btnClose);

				this.toggle($target);

				if ($btnOpen.hasClass('jsc-dn')) {
					$btnOpen.removeClass('jsc-dn');
					$btnClose.addClass('jsc-dn');
				}else {
					$btnOpen.addClass('jsc-dn');
					$btnClose.removeClass('jsc-dn');
				};
			},
			togglePresenter: function (open, close, dot, inner) {
				var $open = $(open),
					$close = $(close),
					$dot = $(dot),
					$inner = $(inner);

				if ($inner.hasClass('jsc-dn')) {
					$dot.addClass('jsc-dn');
					$open.addClass('jsc-dn');
					$inner.removeClass('jsc-dn');
					$close.removeClass('jsc-dn');
				}else {
					$dot.removeClass('jsc-dn');
					$open.removeClass('jsc-dn');
					$inner.addClass('jsc-dn');
					$close.addClass('jsc-dn');
				};
			}
		}
	})(),

	 /**
	 * radioBtn
	 * @class
	 */
	 radioBtn: function (th) {
	 	var $target = $(th).parent(),
	 		$targetlen = $(".jsc-ticketlist");
	 		$(".jsc-ticketlist > li").removeClass('checked');
	 		$target.addClass('checked');
			$(".jsc-ticketlist > li").find("input").removeAttr("checked","");
			$target.find("input").attr("checked","");


	 }
}


$('#jsi-link-scroll-top').on('click', function (e) {
	e.preventDefault();
	APORT.common.scrollTop();
});

$('.jsc-link-scroll').on('click', function (e) {
	e.preventDefault();
	APORT.common.scrollToInternalLink(this);
});

$('#jsi-header-mymenu-toggle').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.dropdown.headerNavToggle("#jsi-header-mymenu","#jsi-header-g-nav");
});

$('#jsi-header-g-nav-toggle').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.dropdown.headerNavToggle("#jsi-header-g-nav","#jsi-header-mymenu");
});

$('#jsi-header-g-nav-login-menu-toggle').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.dropdown.loginMenuToggle("#jsi-header-g-nav-login-menu");
});

$('.jsc-local-nav-category-toggle').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.localNav();
});

$('.jsc-sub-ticketlist-fund-toggle').on('click', function (e) {
	e.preventDefault();
	var toggleTarget = $(this).parent().find(".jsc-sub-ticketlist-fund-inner");
	APORT.common.toggleMortion.toggle(toggleTarget);
	APORT.common.toggleMortion.toggleAddClass(this,"sub-ticketlist-fund-open");
});

$('.jsc-btn-project-detail-open').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.toggleMortion.toggleProjectDetail(".jsc-btn-project-detail-open", ".jsc-btn-project-detail-close", ".jsc-project-description");
});
$('.jsc-btn-project-detail-close').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.toggleMortion.toggleProjectDetail(".jsc-btn-project-detail-close", ".jsc-btn-project-detail-open", ".jsc-project-description");
});


$('.jsc-sub-presenter-detail-more-open').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.toggleMortion.togglePresenter(".jsc-sub-presenter-detail-more-open", ".jsc-sub-presenter-detail-more-close", ".jsc-sub-presenter-detail-dot", ".jsc-sub-presenter-detail-inner");
});
$('.jsc-sub-presenter-detail-more-close').on('click', "a", function (e) {
	e.preventDefault();
	APORT.common.toggleMortion.togglePresenter(".jsc-sub-presenter-detail-more-close",".jsc-sub-presenter-detail-more-open", ".jsc-sub-presenter-detail-dot", ".jsc-sub-presenter-detail-inner");
});


$('.jsc-ticketlist > li label').on('click', function (e) {
	APORT.common.radioBtn(this);
});

$(window).on("scroll", function() {
	APORT.common.scrollTopFade();
});
