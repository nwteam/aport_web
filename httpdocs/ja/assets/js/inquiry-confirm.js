/*
 * inquiry-confirm.js 
 */

function goBack() {
  document.getElementById("hid_send").value = "Y";
  document.getElementById("frm_inquiry").action = "inquiry.php";
  document.getElementById("frm_inquiry").submit();
}

function goSubmit() {
  if (confirm("お問い合わせ内容を送信します。宜しいですか？")) {
    document.getElementById("hid_send").value = "Y";
    document.getElementById("frm_inquiry").action = "inquiry-complete.php";
    document.getElementById("frm_inquiry").submit();
  }
}