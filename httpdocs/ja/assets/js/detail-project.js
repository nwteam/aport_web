/*
 * detail-project.js 
 */

function goCommentPage(no) {
  document.getElementById("page").value = no;
  document.getElementById("frm_project").action = "detail-project.php?p_no="+no;
  document.getElementById("frm_project").submit();
}

function goCommentInsert() {
  if (!document.getElementById("txt_comment").value) {
    alert('コメントを入力してください。');
    return;
  }

  if (confirm("コメントを投稿します。宜しいですか？")) {
  //document.getElementById("no").value = no;
    document.getElementById("frm_project").action = "reg-comment.php";
    document.getElementById("frm_project").submit();
  }
}

function goCommentDelete(no) {
  if (confirm("コメントを削除します。宜しいですか？")) {
    document.getElementById("no").value = no;
    document.getElementById("frm_project").action = "del-comment.php";
    document.getElementById("frm_project").submit();
  }
}

function goUpdateProject(no) {
  if (confirm("プロジェクト変更画面に遷移します。宜しいですか？")) {
    document.getElementById("no").value = no;
    document.getElementById("frm_project").action = "upt-project.php";
    document.getElementById("frm_project").submit();
  }
}

function goUpdateDelete(no) {
  if (confirm("プロジェクトを削除します。宜しいですか？")) {
    document.getElementById("no").value = no;
    document.getElementById("frm_project").action = "del-project.php";
    document.getElementById("frm_project").submit();
  }
}

function goSupport(no) {
  document.getElementById("no").value = no;
  document.getElementById("frm_project").action = "book-support.php";
  document.getElementById("frm_project").submit();
}

function goSupportList() {
  document.getElementById("frm_project").action = "list-supporter.php";
  document.getElementById("frm_project").submit();
}

function goMemberPage(no) {
  document.getElementById("hid_member_no").value = no;
  document.getElementById("frm_project").action = "detail-member.php";
  document.getElementById("frm_project").submit();
}