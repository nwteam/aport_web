$(function(){
    var watchScrollTop = function(){
        var h = $(".header").height() + $(".page-ttl-wrapper").height();
        var t = parseInt( $(".body").css('padding-top') );
        var h2= h + $(".body").height() + t;
        var fixedClassPC = '.illust';
        var illustHeight = $( fixedClassPC ).height();
        var passedHeader = false;
        return function ( _scrollTop ) {
            if( _scrollTop < h2 - illustHeight - t ){
                $( fixedClassPC ).css({
                    marginTop: 0
                });
            }else{
                $( fixedClassPC ).css({
                    marginTop: -( _scrollTop + illustHeight + t - h2 )
                });
            };
            if( _scrollTop < h ){
                if( !passedHeader )return false;
                $( fixedClassPC ).css({
                    position: 'static',
                    top: ''
                });
                passedHeader = !passedHeader;
            }else{
                if( passedHeader )return false;
                $( fixedClassPC ).css({
                    position: 'fixed',
                    top: t
                });
                passedHeader = !passedHeader;
            };
        };
    };
    $(window).on('orientationchange load', function() {
        var ua = window.navigator.userAgent.toLowerCase();
        $( '.illust' ).css({
            position: 'static',
            top: '',
            marginTop: 0
        });
        $(window).off('scroll.fixed');
        if(/iphone|android/.test(ua) || (/ipad/.test(ua) && Math.abs(window.orientation) !== 90 )){
            return false;
        };
        var func = watchScrollTop();
        $(window).on('scroll.fixed', function() {
            var currentScrollTop = $(window).scrollTop();
            func( currentScrollTop );
        });
    });
});
