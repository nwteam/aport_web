/*
 * reg-project.js
 */

var INPUT_MSG_INSERT = "プロジェクトを登録します。宜しいですか？";
var INPUT_MSG_IMAGE = "イメージファイルを選択してください";
var INPUT_MSG_DATE = "日付を確認してください";
var INPUT_MSG_PRICE = "金額を確認してください";
var INPUT_MSG_TEXT = "内容を確認してください";

$(function(){
	$("#txt_project_start_date").datepicker({
//		minDate: '-0d',
		dateFormat: "yy/mm/dd",
		showMonthAfterYear: false,
		showButtonPanel: true,

	});

});

function goSubmit() {
  if ($("#frm_project").validationEngine('validate')) {
    if (!document.getElementById("myFile").value) {
      document.getElementById("myFile").focus();
      alert(INPUT_MSG_IMAGE);
      return false;
    } else {
      document.getElementById("hid_image").value = "Y";
    }

    if (!chkDate(document.getElementById("txt_project_start_date").value)) {
      document.getElementById("txt_project_start_date").focus();
      alert(INPUT_MSG_DATE);
      return false;
    }

    var p_amt = document.getElementById("txt_wish_price").value;
    var p_1_amt = document.getElementById("txt_present_1_min").value;
    var p_1_txt = document.getElementById("txt_present_1_text").value;
/*
    var p_2_amt = document.getElementById("txt_present_2_min").value;
    var p_2_txt = document.getElementById("txt_present_2_text").value;
    var p_3_amt = document.getElementById("txt_present_3_min").value;
    var p_3_txt = document.getElementById("txt_present_3_text").value;
    var p_4_amt = document.getElementById("txt_present_4_min").value;
    var p_4_txt = document.getElementById("txt_present_4_text").value;
    var p_5_amt = document.getElementById("txt_present_5_min").value;
    var p_5_txt = document.getElementById("txt_present_5_text").value;
    var p_6_amt = document.getElementById("txt_present_6_min").value;
    var p_6_txt = document.getElementById("txt_present_6_text").value;
    var p_7_amt = document.getElementById("txt_present_7_min").value;
    var p_7_txt = document.getElementById("txt_present_7_text").value;
    var p_8_amt = document.getElementById("txt_present_8_min").value;
    var p_8_txt = document.getElementById("txt_present_8_text").value;
    var p_9_amt = document.getElementById("txt_present_9_min").value;
    var p_9_txt = document.getElementById("txt_present_9_text").value;
*/

    if ((p_1_amt) && parseInt(p_amt) < parseInt(p_1_amt)) {
      document.getElementById("txt_present_1_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

/*
    if ((p_2_amt) && !(p_2_txt)) {
      document.getElementById("txt_present_2_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_2_amt) && (p_2_txt)) {
      document.getElementById("txt_present_2_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_2_amt) && parseInt(p_2_amt) <= parseInt(p_1_amt)) {
      document.getElementById("txt_present_2_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

    if ((p_3_amt) && !(p_2_amt)) {
      document.getElementById("txt_present_2_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_3_amt) && !(p_3_txt)) {
      document.getElementById("txt_present_3_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_3_amt) && (p_3_txt)) {
      document.getElementById("txt_present_3_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_3_amt) && parseInt(p_3_amt) <= parseInt(p_2_amt)) {
      document.getElementById("txt_present_3_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

    if ((p_4_amt) && !(p_3_amt)) {
      document.getElementById("txt_present_3_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_4_amt) && !(p_4_txt)) {
      document.getElementById("txt_present_4_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_4_amt) && (p_4_txt)) {
      document.getElementById("txt_present_4_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_4_amt) && parseInt(p_4_amt) <= parseInt(p_3_amt)) {
      document.getElementById("txt_present_4_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

    if ((p_5_amt) && !(p_4_amt)) {
      document.getElementById("txt_present_4_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_5_amt) && !(p_5_txt)) {
      document.getElementById("txt_present_5_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_5_amt) && (p_5_txt)) {
      document.getElementById("txt_present_5_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_5_amt) && parseInt(p_5_amt) <= parseInt(p_4_amt)) {
      document.getElementById("txt_present_5_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

    if ((p_6_amt) && !(p_5_amt)) {
      document.getElementById("txt_present_5_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_6_amt) && !(p_6_txt)) {
      document.getElementById("txt_present_6_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_6_amt) && (p_6_txt)) {
      document.getElementById("txt_present_6_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_6_amt) && parseInt(p_6_amt) <= parseInt(p_5_amt)) {
      document.getElementById("txt_present_6_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

    if ((p_7_amt) && !(p_6_amt)) {
      document.getElementById("txt_present_6_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_7_amt) && !(p_7_txt)) {
      document.getElementById("txt_present_7_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_7_amt) && (p_7_txt)) {
      document.getElementById("txt_present_7_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_7_amt) && parseInt(p_7_amt) <= parseInt(p_6_amt)) {
      document.getElementById("txt_present_7_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

    if ((p_8_amt) && !(p_7_amt)) {
      document.getElementById("txt_present_7_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_8_amt) && !(p_8_txt)) {
      document.getElementById("txt_present_8_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_8_amt) && (p_8_txt)) {
      document.getElementById("txt_present_8_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_8_amt) && parseInt(p_8_amt) <= parseInt(p_7_amt)) {
      document.getElementById("txt_present_8_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }

    if ((p_9_amt) && !(p_8_amt)) {
      document.getElementById("txt_present_8_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_9_amt) && !(p_9_txt)) {
      document.getElementById("txt_present_9_text").focus();
      alert(INPUT_MSG_TEXT);
      return false;
    }
    if (!(p_9_amt) && (p_9_txt)) {
      document.getElementById("txt_present_9_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
    if ((p_9_amt) && parseInt(p_9_amt) <= parseInt(p_8_amt)) {
      document.getElementById("txt_present_9_min").focus();
      alert(INPUT_MSG_PRICE);
      return false;
    }
*/
    var isYn = confirm(INPUT_MSG_INSERT);
    if (isYn) {
      document.getElementById("hid_send").value = "Y";
      return true;
    } else {
      return false;
    }
  } else {
  	return false;
  }
}

