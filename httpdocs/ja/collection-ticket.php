<?php
	$lang="ja";
	require_once('../../config/common-config.php');
	require_once(SITE_PATH.'config/common-require.php');

	$agent=getAgentType();
	require_once(SITE_PATH.'/code/collection-ticket-code.php');

	$smarty->assign("lang", $lang);
	$smarty->display($lang.$agent."/collection-ticket.tpl");

?>
