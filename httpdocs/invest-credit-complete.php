<?php

ini_set( 'display_errors', 1 );
	print "クレジット決済成功処理です";
	$result=$_GET;
	//idを分割
	$id_arr = explode("-", $result[sendid]);	//プロジェクトNo,支援No,会員No,言語コード
	$sendpoint=$result[sendpoint];
	$result_data=$result[result];
	$lang=$id_arr[3];
	$clientip=$result[clientip];
	$auth_no=$result[sendid];

	print $result_data;

	require_once('../config/common-config.php');
	require_once('../config/common-require.php');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	$commonDao = new CommonDao();


	$note="**** クレジットカード決済結果処理 ".date("Y-m-d H:i:s")." ****\n";
	$note.= "type:".$result[sendpoint]." id:". $result[sendid]." result:".$result[result]."\n";


	if(count($id_arr)!=4){
		$note.= "コード番号が正しくありません";
		write_log($note);
		exit;

	}
	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);


	//支援情報取得
	$query  = " select *  from sf_invest where no = {$id_arr[1]}";
//	print $query."<br>";
	$ret = mysql_query("set names utf8");
  	$ret = mysql_query($query, $connect);
	$i_data   = mysql_fetch_array($ret);
//	print_r_with_pre($i_data);

	  if(!is_array($i_data)){
		$note.= "支援情報が見つかりませんでした";
		write_log($note);
		exit;
	  }

	//支援コース情報取得
	$query  = " select *  from sf_prj_present where present_no = {$i_data[present_no]}";
//	print $query."<br>";
	$ret = mysql_query("set names utf8");
  	$ret = mysql_query($query, $connect);
	$pre_data   = mysql_fetch_array($ret);

	  if(!is_array($pre_data)){
		$note.= "プレゼント情報が見つかりませんでした";
		write_log($note);
		exit;
	  }

	//プロジェクト情報取得

	  $query  = " select p.*,d.project_name  from sf_project p
		inner join sf_project_detail d on p.no=d.project_no and d.lang='$db_lang'
		where p.no = {$id_arr[0]}";

	print $query."<br>";
	$ret = mysql_query("set names utf8");
  	$ret = mysql_query($query, $connect);
	$p_data   = mysql_fetch_array($ret);


	  if(!is_array($p_data)){
		$note.= "プロジェクト情報が見つかりませんでした";
		write_log($note);
		exit;
	  }

	//ユーザー情報取得
	$query  = " select *  from sf_member where user_no = {$id_arr[2]}";
//	print $query."<br>";
	$ret = mysql_query("set names utf8");
  	$ret = mysql_query($query, $connect);
	$m_data   = mysql_fetch_array($ret);
	//print_r_with_pre($m_data);

	  if(!is_array($m_data)){
		$note.= "会員情報が見つかりませんでした";
		write_log($note);
		exit;
	  }
	if($sendpoint==1){	//ユーザ　クレジット認証結果
		if($result_data=="ok"){	//認証OK
			print "OK";

		    //トランザクションをはじめる準備
		    $Query = "set autocommit = 0";
		    mysql_query( $Query, $connect );

		    //トランザクション開始
		    $Query = "begin";
		    mysql_query( $Query, $connect );

			//支援ステータス更新
			$data1[status]=1;
			$data1[clientip]=$clientip;
			$data1[auth_no]=$auth_no;
			$where1[no]=$id_arr[1];

		    $sql=$commonDao->MakeUpdateSQL("sf_invest",$data1,$where1);
		//	print $sql."<br>";

			$ret = mysql_query("set names utf8");
			$ret = mysql_query($sql, $connect);
			if( $ret === false ){
		        //ロールバック
		        $Query = "rollback";
		        mysql_query( $Query, $connect );
				$note.= "支援情報更新失敗\n";
				$note.=$sql;
				write_log($note);
				mysql_close($connect);
				exit;
			}
			$i_data[auth_no]=$auth_no;
			//支援金額総額・支援者数更新
			$summary=$p_data[now_summary]+$i_data[invest_amount];
			$supporter=$p_data[now_supporter]+1;

			$data2[now_summary]=$summary;
			$data2[now_supporter]=$supporter;
		    $where2[no]=$id_arr[0];

		    $sql=$commonDao->MakeUpdateSQL("sf_project",$data2,$where2);
			//print $sql."<br>";

			$ret = mysql_query("set names utf8");
			$ret = mysql_query($sql, $connect);
			if( $ret === false ){
		        //ロールバック
		        $Query = "rollback";
		        mysql_query( $Query, $connect );
				$note.= "プロジェクト情報更新失敗\n";
				$note.=$sql;
				write_log($note);
				mysql_close($connect);
				exit;
			}
			 //コミット
	        $Query = "commit";
	        mysql_query( $Query, $connect );


			$note.="支援登録完了 ";
			//メール送信
			$smarty->assign("m_data", $m_data);
			$smarty->assign("p_data", $p_data);
			$smarty->assign("i_data", $i_data);
			$smarty->assign("pre_data", $pre_data);

			$mail_subject=getMailSubject(6);
			print $mail_subject."<br>";
			$subject = "[" . $str_site_title . "]".$mail_subject;
			$mailBody = $smarty->fetch($lang."/mail/m-invest-project-complete.tpl");
			//$mailfrom="From:" .mb_encode_mimeheader(MAIL_FROM_NAME) ."<".MAIL_FROM.">";
			send_mail($m_data[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

		}else{
			print "NG";
			$note.="認証NG ";
			//支援ステータス更新
			$data1[status]=92;
			$data1[auth_no]=$clientip;
			$where1[no]=$id_arr[1];

		    $sql=$commonDao->MakeUpdateSQL("sf_invest",$data1,$where1);
		//	print $sql."<br>";

			$ret = mysql_query("set names utf8");
			$ret = mysql_query($sql, $connect);

			$i_data[auth_no]=$auth_no;

			//メール送信
			$smarty->assign("m_data", $m_data);
			$smarty->assign("p_data", $p_data);
			$smarty->assign("i_data", $i_data);
			$smarty->assign("pre_data", $pre_data);

			$mail_subject=getMailSubject(7);
			$subject = "[" . $str_site_title . "]".$mail_subject;
			$mailBody = $smarty->fetch($lang."/mail/m-invest-project-err.tpl");
			//$mailfrom="From:" .mb_encode_mimeheader(MAIL_FROM_NAME) ."<".MAIL_FROM.">";
			send_mail($m_data[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
		}

	}elseif($sendpoint==2){	//運営　決済結果
		$note.="**** これは決済の結果です ****\n";


	}else{
		$note.="決済タイプ判別不能 ";

	}
		write_log($note);

		mysql_close($connect);
	print "おわりました<br>";
	print '<a href="/ja/index.php" >トップページへ</a>';

?>


