<?php
class ProjectDAO extends DAOBase {

	/**
	 * プロジェクト 基本情報更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData1($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_project set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			$name_dt=trim($item[public_title]);
			$name_dt=htmlspecialchars(addslashes($name_dt));

			$sql="update sf_project_detail set project_name='".$name_dt."' where project_no=".$item[no];
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}

	/**
	 * プロジェクト リターン更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData2($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_project set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);
			
			for($r=1;$r<31;$r++){
				if ($item['return_min'][$r]!=""){
					if($item['present_num'][$r]!=""){
						$sql="update sf_prj_present set 
									min=".$item['return_min'][$r].", 
									invest_limit=".$item['max_count'][$r].", 
									title='".$item['return_title'][$r]."', 
									text='".$item['return_text'][$r]."', 
									return_img='".$item['new_return_img'][$r]."', 
									return_post_flg='".$item['return_post_flg'][$r]."', 
									return_year='".$item['return_year'][$r]."', 
									return_month='".$item['return_month'][$r]."' 
									where present_no=".$item['present_num'][$r] ;
						$this->executeUpdate($sql);
					}else{
						$sql="insert into sf_prj_present (project_no,lang,min,invest_limit,title,text,return_img,return_post_flg,return_year,return_month) Values (
									'".$item[no]."', 
									'ja', 
									".$item['return_min'][$r].", 
									'".$item['max_count'][$r]."', 
									'".htmlspecialchars(addslashes($item['return_title'][$r]))."', 
									'".htmlspecialchars(addslashes($item['return_text'][$r]))."', 
									'".$item['new_return_img'][$r]."', 
									'".$item['return_post_flg'][$r]."', 
									'".$item['return_year'][$r]."', 
									'".$item['return_month'][$r]."')";
						$this->executeUpdate($sql);
					}
				}
			}
			//支援のステータスの更新
			if($item[invest_status]){
				$sql="update sf_invest set status='{$item[invest_status]}'
					 where project_no='{$item[no]}' and status>=1 and status<=3";
				$ret=$commonDao->run_sql($sql);
			}
			// コミット
			$this->db->commit();
		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}
	/**
	 * プロジェクト 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData3($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_project set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			$project_dt=trim($item[project_text]);
			$project_dt=htmlspecialchars(addslashes($project_dt));

			$sql="update sf_project_detail set project_text='".$project_dt."' where project_no=".$item[no];
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}
	/**
	 * プロジェクト 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData4($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();
		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}
		$ins=implode(",",$tmp1);
		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}
		$sql="update sf_project set $ins $where";
		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);
			// コミット
			$this->db->commit();
		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}
	/**
	 * プロジェクト 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData5($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();
		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}
		$ins=implode(",",$tmp1);
		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}
		$sql="update sf_project set $ins $where";
		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);
			//銀行情報
			if ($item[bank_name_txt]!=""){
				$bank_name=$item[bank_name_txt];
				
			}else{
				$bank_name=$item[bank_name];
			}
			$introduce_dt=trim($item[introduce]);
			$introduce_dt=htmlspecialchars(addslashes($introduce_dt));
			$sql="update sf_member set 
						profile_img='".$item[new_profile_img]."', 
						full_name='".$item[full_name]."', 
						profile='".$introduce_dt."', 
						bank_name='".$bank_name."', 
						branch_bank='".$item[branch_bank]."', 
						branch_bank_code=".$item[branch_bank_code].", 
						card_type='".$item[card_type]."',
						card_number=".$item[card_number].",
						card_owner='".$item[card_owner]."' 
						where user_no=".$item[user_no] ;
			$this->executeUpdate($sql);
			// コミット
			$this->db->commit();
		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}
	/**
	 * プロジェクト 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData6($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_project set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);
			// コミット
			$this->db->commit();
		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}
	/**
	 * アップデータ情報 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData7($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_update_msg set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}
	/**
	 * メール読む処理 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData8($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_message set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){
			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}
		return true;
	}
	/**
	 * プロジェクト 更新処理
	 * @param $fi:フィールド名配列
	 *        $dt:値配列
	 *        $item:フォームデータ
	 * @return
	 */
	public function upItemData($fi,$dt,$wfi,$wdt,$item=array()) {
		$commonDao = new CommonDAO();

		if(is_array($fi)){
	           	for ($i=0;$i<count($fi);$i++){
					$va=trim($dt[$i]);
					$tmp1[]=$fi[$i]."='".htmlspecialchars($va, ENT_QUOTES)."'";
		    	}
		}else if($fi){
			$dt=trim($dt);
			$dt=htmlspecialchars(addslashes($dt));
			$tmp1[]=$fi."='".$dt."'";
		}

		$ins=implode(",",$tmp1);

		if(is_array($wfi)){
		    $tmp=array();
	            for ($i=0;$i<count($wfi);$i++){
		    	$tmp[]=$wfi[$i]."='".$wdt[$i]."'";
		    }
		    $where=" where ".implode(" and ",$tmp);
		}else if($wfi){
			$where=" where ".$wfi."='".$wdt."'";
		}

		$sql="update sf_project set $ins $where";

		$this->db->beginTransaction();
		try {
			// 実行
			$this->executeUpdate($sql);

			$name_dt=trim($item[public_title]);
			$name_dt=htmlspecialchars(addslashes($name_dt));
			$project_dt=trim($item[project_text]);
			$project_dt=htmlspecialchars(addslashes($project_dt));
/*
			$sql_detail = "SELECT * FROM sf_project_detail where project_no=".$item[no];
			$project_detail_src = $this->executeQuery($sql);
			$detail_count=count($project_detail_src);

			if($detail_count=="0"){
				$ikey[]="project_no";
				$ival[]=$item[no];
				$ikey[]="lang";
				$ival[]="ja";
				$ikey[]="project_text";
				$ival[]=$item[project_text];
				$ikey[]="project_name";
				$ival[]=$item[public_title];
				$ikey[]="create_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("sf_project_detail",$ikey,$ival);
			}else{
*/
				$sql="update sf_project_detail set project_name='".$name_dt."',project_text='".$project_dt."' where project_no=".$item[no];
				$this->executeUpdate($sql);
//			}

			//プロフィール画像
			if ($item[bank_name_txt]!=""){
				$bank_name=$item[bank_name_txt];
				
			}else{
				$bank_name=$item[bank_name];
			}
			//echo $bank_name;
			//echo "111=".$item[bank_name_txt];die;
			$introduce_dt=trim($item[introduce]);
			$introduce_dt=htmlspecialchars(addslashes($introduce_dt));
			$sql="update sf_member set 
						profile_img='".$item[new_profile_img]."', 
						full_name='".$item[full_name]."', 
						profile='".$introduce_dt."', 
						bank_name='".$bank_name."', 
						branch_bank='".$item[branch_bank]."', 
						branch_bank_code=".$item[branch_bank_code].", 
						card_type='".$item[card_type]."',
						card_number=".$item[card_number].",
						card_owner='".$item[card_owner]."' 
						where user_no=".$item[user_no] ;
			$this->executeUpdate($sql);
			
			for($r=1;$r<6;$r++){
				if ($item['return_min'][$r]!=""){
					if($item['present_num'][$r]!=""){
						//プロフィール画像
						$sql="update sf_prj_present set 
									min=".$item['return_min'][$r].", 
									invest_limit=".$item['max_count'][$r].", 
									title='".$item['return_title'][$r]."', 
									text='".$item['return_text'][$r]."', 
									return_img='".$item['new_return_img'][$r]."', 
									return_post_flg='".$item['return_post_flg'][$r]."', 
									return_year='".$item['return_year'][$r]."', 
									return_month='".$item['return_month'][$r]."' 
									where present_no=".$item['present_num'][$r] ;
						$this->executeUpdate($sql);
					}else{
						//プロフィール画像
						$sql="insert into sf_prj_present (project_no,lang,min,invest_limit,title,text,return_img,return_post_flg,return_year,return_month) Values (
									'".$item[no]."', 
									'ja', 
									".$item['return_min'][$r].", 
									'".$item['max_count'][$r]."', 
									'".htmlspecialchars(addslashes($item['return_title'][$r]))."', 
									'".htmlspecialchars(addslashes($item['return_text'][$r]))."', 
									'".$item['new_return_img'][$r]."', 
									'".$item['return_post_flg'][$r]."', 
									'".$item['return_year'][$r]."', 
									'".$item['return_month'][$r]."')";
						$this->executeUpdate($sql);
					}
				}
			}
			//支援のステータスの更新
			if($item[invest_status]){
				$sql="update sf_invest set status='{$item[invest_status]}'
					 where project_no='{$item[no]}' and status>=1 and status<=3";
				$ret=$commonDao->run_sql($sql);
			}

			// コミット
			$this->db->commit();

			//$this->db->beginTransaction();
			//if($item[new_cover_img]){
			//	$cover_img_name=URL_IMG_PROJECT.$item[no]."_sn_".$item[new_cover_img];
			//	$sql="update sf_project set cover_img='{$cover_img_name}' where no={$item[no]}";
			//	$ret=$commonDao->run_sql($sql);
			//}
			//// コミット
			//$this->db->commit();
		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to insert 'Project'." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

	/**
	 * プロジェクト詳細説明の更新

	 */
	public function updateProjectDetail($data,$project_no,$lang) {
		$sql="update sf_project_detail set project_text='".$data."' where project_no=".$project_no." and lang='".$lang."'";

		//print $sql."<br>";
		try {
			$this->db->beginTransaction();

			// 実行
			//$this->executeUpdate($sql,$this->get_db());
			$this->executeUpdate($sql);

			// コミット
			$this->db->commit();

		}catch(Exception $e){

			$this->addMessage(SYSTEM_MESSAGE_ERROR, "Failed to Update." . $e);
			$this->db->rollBack();
			return false;
		}

		return true;

	}

	/**
	 * カテゴリ プルダウン用の配列取得

	 */

	public function getCategoryOptionArr() {
		$sql = "SELECT
		            no,category_name
				FROM
					sf_category
				WHERE del_flg=0
			 ";

		$ret = $this->executeQuery($sql);

		$Arr[""]="選択してください";
		if($ret){
			foreach($ret as $data){
				$Arr[$data[no]]=$data[category_name];
			}
		}
		return $Arr;
	}

}
?>