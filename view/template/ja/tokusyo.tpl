{include file="{$lang}/sf-head.tpl"}
	<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
</head>
<body>
<div class="wrapper">
{include file="{$lang}/sf-header.tpl"}
<div class="page-ttl-wrapper" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">特定商取引法の表記</h2>
	</div>
</div>
<div class="body">
	<div class="container container-offset cf">
		<div class="col-main">
			<div class="col-inner">
				<dl class="list-line">
					<dt class="ttl-blue">事業者</dt>
					<dd>株式会社朝日新聞社メディアラボ</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">運営責任者</dt>
					<dd>堀江　隆</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">所在地</dt>
					<dd>&#12306;104-8011　東京都中央区築地５丁目３番２号</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">電話番号</dt>
					<dd>03-6869-9001</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">問い合わせ</dt>
					<dd><a href="mailto:info@a-port-mail.com">info@a-port-mail.com</a></dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">販売価格</dt>
					<dd>各プロジェクトにある「リターン」をご覧ください。全て税込表記となっております。</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">支払い方法</dt>
					<dd>ソフトバンク・ペイメント・サービスの提供する決済システムを利用しておりクレジットカードによる支払いとなっております。銀行振込については、「実行確約型」及び、目標金額に到達した「達成時実行型（All or Nothing型）」でのみ取扱いできるものとします。</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">支払い時期</dt>
					<dd>
						<dl>
							<dt class="dt-inner">達成時実行型（All or Nothing型）方式</dt>
							<dd class="dd-inner">各プロジェクトの目標額到達時点（お客様による資金提供誓約後、各プロジェクトが予め設定した目標額に調達金額が到達した時点。ただし、資金提供の目標額に達しなかった場合は決済されません。）</dd>
							<dt class="dt-inner">実行確約型方式</dt>
							<dd class="dd-inner">お客様がプロジェクトの資金提供をご誓約頂いたタイミングで決済が実行されます。</dd>
						</dl>
					</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">引き渡し時期</dt>
					<dd>各プロジェクトの資金提供募集終了日に代金支払いが確認できた時点で完了するものとします。<br /><br />
						※支援者へのリターンの引き渡し時期（権利の移転時期、役務の提供時期など）は、各プロジェクトの該当ページに掲載します。</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">商品の代金以外の料金</dt>
					<dd>特にありません。</dd>
				</dl>
				<dl class="list-line">
					<dt class="ttl-blue">商品の返品およびキャンセルについて</dt>
					<dd class="numbers">（１）支援者がプロジェクトの趣旨に賛同し、「この内容で応援する」ボタンを押すことにより支払い手続を行った後のキャンセルについては、その翌日を起算日として７日以内に当社所定の手続に従って行わない限り、効力を生じないものとします。ただし、既にプロジェクトが成立し決済されている場合はキャンセルできません。</dd>
					<dd class="numbers">（２）前項の規定に合致した場合であってもプロジェクトの募集期間が残り８日未満であった場合のキャンセルはできないものとし、仮に手続が行われたとしても、キャンセルの効力は生じないものとします。また、「達成時実行型（All or Nothing型）」において、プロジェクトが募集期間中に目標金額に達した場合、それ以前に支払い手続がなされたものはキャンセルできません。</dd>
					<dd class="numbers">（３）クラウドファンディングの性質上、支援者へのリターンは受注生産品となりますので、支援者が行った支払い手続につき決済が完了した時点以降のキャンセル、リターンの返品または返金はできません。</dd>
				</dl>
			</div>
		</div><!--/col-main-->

		{include file="{$lang}/other-sub.tpl"}

		<ol class="breadcrumb cf">
			<li><a href="/">サイトトップ</a></li>
			<li>特定商取引法の表記</li>
		</ol>
	</div><!--/container-->
</div><!--/body-->
</div>
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
