{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<!--LV1 wrapper Start -->
<div class="wrapper">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="upDateEdit" class="clearfix">
			<!--LV4 tab Start -->
			{include file="{$lang}/publish-title.tpl"}
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			{if $err_msg.top}
					<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
			{/if}
      <div id="updateArea">
				<h3>メールを送る</h3>
        <dl class="clearfix">
          <dt>宛先（必須）</dt>
          <dd>
            <select name="to_user_id">
              <option value="{$supporter_info.user_no}" {if $input_data.to_user_id==$supporter_info.user_no}selected{/if}>{$supporter_info.member_name}＜{$supporter_info.email}＞</option>
              <option value="all" {if $input_data.to_user_id=="all"}selected{/if}>全体支援者へ</option>
            </select>
          </dd>
          <dt>件名（必須）</dt>
          <dd>
            <input id="message_title" name="message_title" type="text" value="{$input_data.message_title}"/>
						{if $err_msg.message_title}
							<span class="red" style="color:red;">※{$err_msg.message_title}</span><br />
						{/if}
          </dd>
          <dt>本文</dt>
          <dd class="htmlEditor">
            <textarea cols="80" id="message_body" name="message_body" rows="10">{$input_data.message_body}</textarea>
						{if $err_msg.message_body}
							<span class="red" style="color:red;">※{$err_msg.message_body}</span><br />
						{/if}
        </dl>
				<input id="msg_id" name="msg_id" type="hidden" value="{$input_data.msg_id}">
        <div class="actions">
          <input name="sbm_back" type="submit" value="戻る" />
          <input name="sbm_update" type="submit" value="確認する" />
        </div>
        <!--/#fragment-1  -->
			</form>
			</div>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
