	{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<div id="wrapper" class="clearfix">
	<div id="myPage">
		{include file="{$lang}/mypage-profile.tpl"}</a>
		<!-- #myProfile -->
		{include file="{$lang}/mypage-side.tpl"}</a>
		<!-- #myPageSide -->
		<div id="myPageContents">
			<div id="myProfileEdit">
			<h2>プロフィール設定</h2>
			<form accept-charset="UTF-8" action="" method="post" enctype="multipart/form-data">
				{if $err_msg.top}
					<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
				{/if}
				{if $finish_msg}
					<p class="update">{$finish_msg}</p>
				{/if}
				<dl>
					<dt>お名前 <span class="red">(必須)</span></dt>
					<dd>姓：
						<input class="half" id="name_1" name="name_1" type="text" value="{$input_data.name_1}" />
						　名：
						<input class="half" id="name_2" name="name_2" type="text" value="{$input_data.name_2}" />
						{if $err_msg.name_1}
							<p class="error" style="color:red;">※{$err_msg.name_1}</p>
						{/if}
						{if $err_msg.name_2}
							<p class="error" style="color:red;">※{$err_msg.name_2}</p>
						{/if}
					</dd>
					<dt>ニックネーム</dt>
					<dd>
						<input id="nickname" name="nickname" type="text" value="{$input_data.nickname}"/>
						<input id="org_nickname" name="org_nickname" type="hidden" value="{$input_data.org_nickname}"/>
						<br />
						<p class="mT10">
							<input name="ml_flg" type="hidden" value="0" />
							<input id="ml_flg" name="ml_flg" type="checkbox" value="1" {if $input_data.ml_flg=="1"} checked="checked" {/if}/>
							表示名に使う<span class="note">（あなたの名前がニックネームで表示されます）</span>
						</p>
						{if $err_msg.nickname}
							<p class="error" style="color:red;">※{$err_msg.nickname}</p>
						{/if}
					</dd>
					</dd>
					<dt>現在地</dt>
					<dd>
						<select id="add_1" name="add_1">
							{html_options  options=$array_area selected=$input_data.add_1 }
						</select>
					</dd>
					<dt>WEBサイトURL</dt>
					<dd>
						<input id="hp_url" name="hp_url" type="text" value="{$input_data.hp_url}"/>
						<p class="note">※半角英数字</p>
						{if $err_msg.hp_url}
							<p class="error" style="color:red;">※{$err_msg.hp_url}</p>
						{/if}
					</dd>
					<!--
					<dt>facebookアカウント</dt>
					<dd><a href="/profile/social_setting">+ 連携する</a></a></dd>
					-->
					<dt>自己紹介</dt>
					<dd>
						<textarea cols="4" id="profile" maxlength="300" name="profile" rows="10">{$input_data.profile}</textarea>
						<p class="note">※300文字以内</p>
						{if $err_msg.profile}
							<p class="errorTitle" style="color:red;">※{$err_msg.profile}</p>
						{/if}
					</dd>
					<dt>プロフィール画像</dt>
					<dd>
						{if $smarty.session.TMP_FUP13!=""}
							<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP13}" width=200 height=200 />
						{elseif $input_data.profile_img!=""}
							<img src="{$smarty.const.C_IMG_ACTRESS}{$input_data.profile_img}" width=200 height=200 />
							<input type="hidden" name="profile_img" value="{$input_data.profile_img}" />
						{/if}
						<p><input type="file" name="up_file3" value="1" size="40" class="" aria-invalid="false" style="border:none;"/></p>
						<p>プロフィール画像（200×200px）を設定してください。</p>
						{if $err_msg.profile_img}
							<p class="errorTitle" style="color:red;">※{$err_msg.profile_img}</p>
						{/if}
					</dd>
				</dl>
				<br>
				<div class="actions">
					<input name="sbm_update" type="submit" value="変更を保存" />
				</div>
			</form>
			</div>
		</div>
		<!-- #myPageContents --> 
	</div>
	<!-- #myPage --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
