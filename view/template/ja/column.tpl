{include file="{$lang}/sf-head.tpl"}
<link href="{$smarty.const.C_ASSETS}css/common.css" media="screen" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/column.css" media="screen" rel="stylesheet" />
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}lib/html5shiv-printshiv-776c428db58d65d46e807ac934e8a1a3.js"></script>
<script src="{$smarty.const.C_ASSETS}lib/respond-448013aff8d268bb6b82475310302815.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper">
{include file="{$lang}/sf-header.tpl"}
    <div class="column-main-wrapper">
        <h2 class="column-main-ttl"><img src="{$smarty.const.C_ASSETS}images/column/ttl-column-main.png" alt="A-port OFFICIAL COLUMN"></h2>
    </div>
    <div class="body">
        <div class="container container-column cf">
            <div class="col-main">
                <ul class="column-list">
                	{foreach name=outer item=con from=$result_list}
                    <li>
			{if $con.cover_img!=''}
				<div class="column-thum"><a href="column-detail.php?id={$con.id}"><img src="{$con.cover_img}" alt="{$con.title}"></a></div>
			{/if}
                        <div class="column-status cf">
                            <p class="column-date roboto-b">{$con.insert_time}</p>
                            <p class="column-category"> <span class="{$con.category_class}">{$con.category_name}</span> </p>
                        </div>
                        <h3 class="column-title">{$con.title}</h3>
                        <p class="column-description">
                        <p>{$con.contents_limit}</p>
                        </p>
                        <div class="column-more-btn"><a href="column-detail.php?id={$con.id}">続きを読む</a></div>
                    </li>
                    {/foreach}
                </ul>
            </div>
            
            {include file="{$lang}/column-side.tpl"}
        </div>
    </div>

    <div class="pagination-wrapper  pagination-sp-hide">
        <ul class="pagination roboto-b cf">
		<!--{$paging_before nofilter}{$paging_str nofilter}{$paging_next nofilter}-->
		{column_page countpage=$total_page nowpage=$nowpage}
        </ul>
    </div>
</div>
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
