{include file="{$lang}/sf-head.tpl"}

<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<script type='text/javascript'>
{literal}
function set_ticket_amount() {
	$('.selectTicket').text($(this).attr('data-amount'));
	$('#ticketSelect').find('.checked').removeClass('checked');
	$(".ticketMarkList").remove();
	$(this).parent().before('<ul class="ticketMarkList"><li><span class="selectTicketMark">このリターンを選択しています。</span></li></ul>');
	$(this).parents('li').first().addClass('checked');
}
function set_payment_select() {
	$('#paymentSelect').find('.checked').removeClass('checked');
	$(this).parents('li').first().addClass('checked');
{/literal}
{if $project_info.project_type!="1"||$project_info.now_summary+$project_info.admin_summary>$project_info.wish_price}
{literal}
	if ($("input[name=payment_method][type='radio']:checked").val()=="payeasy"){
		$('#attention').toggle();
	}else{
		$('#attention').css('display','none'); 
	}
{/literal}
{/if}
{literal}
}
$(function() {
	$('.selectTicket').text($('.ticket_rd:checked').attr('data-amount'));
	$('.ticket_rd:checked').parents('li').first().addClass('checked');
	$('.ticket_rd:checked').parent().before('<ul class="ticketMarkList"><li><span class="selectTicketMark">このリターンを選択しています。</span></li></ul>');
	$('.payment_select:checked').parents('li').first().addClass('checked');

	$('.ticket_rd').change(set_ticket_amount);
	$('.payment_select').change(set_payment_select);
});
{/literal}
</script>

</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div id="wrapper" class="clearfix">
	<ul id="step">
		<li class="active">1. 応援リターンの選択 </li>
		<li>2. 決済情報の入力 </li>
		<li>3. 応援内容の確認 </li>
		<li>4. 完了<span>（応援コメントを寄せよう）</span> </li>
	</ul>
	<div id="projectStart">
		<div id="projectContents">
			<h2>プロジェクトを応援してサポーターになる</h2>
			<form action="{$pagelink_collection_ticket}" method="post">
				<input type="hidden" id="no" name="no" value="{$project_info.no}" />
				{if $err_msg1.top}
					<p class="errorTitle" style="color:red;">※{$err_msg1.top}</p>
				{/if}
				<div id="collectAmount" class="section">
					<h3>応援リターンの選択</h3>
					<p class="pageDescription">リターンを選択してください。リターンは応援額ごとに選択でき、それぞれ特典が違います。</p>
					<ul id="ticketSelect">
					{if $err_msg1.present_no}
						<p class="error" style="color:red;">※{$err_msg1.present_no}</p>
					{/if}
					{foreach from=$present_info item="present" name="loop"}
						<li>
						<label for="ticket{$present.present_no}">
							<dl>
							<dt>
								<input {if $present.invest_limit<$present.invest_cnt}disabled{elseif $selected_present_no==$present.present_no} checked="checked" {/if} class="rd ticket_rd" data-amount="{$present.min|number_format}" id="ticket{$present.present_no}" name="present_no" type="radio" value="{$present.present_no}" />
								{$present.min|number_format}円 リターン </dt>
							<dd>
								<ul>
									<li>{$present.text|nl2br nofilter}</li>
								</ul>
							</dd>
							</dl>
						</label>
						</li>
						{if $present.invest_limit<$present.invest_cnt}
							<dd class="courseEnd">※ このコースは限定数量に達したため完売いたしました。</dd>
						{/if}
					{/foreach}
					</ul>
					<h3>さらに応援する</h3>
					<div id="moreFunding" class="formSection">
					    <p class="fundingInput"> (リターン)<span class="selectTicket"></span>円 +
					      <input class="fundingInputSet" id="amount_plus" maxlength="9" name="amount_plus" size="20" type="text" value="{if $input_data.amount_plus!=''}{$input_data.amount_plus}{else}0{/if}" />
					      円 </p>
							{if $err_msg1.amount_plus}
								<p class="error" style="color:red;">※{$err_msg1.amount_plus}</p>
							{/if}
					    <p class="caution">こちらの入力欄に金額をご入力いただくと、リターン額以上の金額で応援することができます。追加のご支援はプロジェクトへの大きな支援になります！</p>
					    <p class="note">※半角数字でご記入ください。</p>
					</div>
				</div>
				<!-- #collectAmount -->

				<h3>決済方法選択</h3>
				{if $err_msg1.payment_method}
						<p class="error" style="color:red;">{$err_msg1.payment_method}</p>
				{/if}
				{if $err_msg1.payment_method1}
						<p class="error" style="color:red;">{$err_msg1.payment_method1}</p>
						<p class="error" style="color:red;">{$err_msg1.payment_method2}</p>
						<p class="error" style="color:red;">{$err_msg1.payment_method3}</p>
						<p class="error" style="color:red;">{$err_msg1.payment_method4}</p>
						<p class="error" style="color:red;">{$err_msg1.payment_method5}</p>
						<p class="error" style="color:red;">{$err_msg1.payment_method6}</p>
						<p class="error" style="color:red;">{$err_msg1.payment_method7}</p>
				{/if}
				<ul id="paymentSelect">
				  <li class="paySelect">
				    <label>
				    <dl>
				      <dt>
								{if $project_info.project_type!="1"||$project_info.now_summary+$project_info.admin_summary>$project_info.wish_price}
									<input class="rd payment_select" id="m_credit" name="payment_method" type="radio" value="Credit" {if $input_data.payment_method=="Credit"}checked{/if} />
								{else}
									<input class="rd payment_select" id="m_credit" name="payment_method" type="radio" value="Credit" checked />
								{/if}
								<span class="iconCredit">クレジットカード</span> 
							</dt>
				      <dd><img alt="「VISA」「MasterCard」" src="{$smarty.const.C_ASSETS}images/logo_visa.png" /></dd>
				    </dl>
				    </label>
				  </li>
					{if $project_info.project_type!="1"||$project_info.now_summary+$project_info.admin_summary>$project_info.wish_price}
						<li class="paySelect">
							<label>
							<dl>
								<dt>
									<input class="rd payment_select" id="m_cvs" name="payment_method" type="radio" value="cvs" {if $input_data.payment_method=="cvs"}checked{/if} />
									<span class="iconConvenience">コンビニ</span> </dt>
								<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni.png" alt="コンビニ"></dd>
							</dl>
							</label>
						</li>
						<li class="paySelect">
							<label>
							<dl>
								<dt>
									<input class="rd payment_select" id="m_payeasy" name="payment_method" type="radio" value="payeasy" {if $input_data.payment_method=="payeasy"}checked{/if} />
									<span class="iconPayeasy">Pay-easy</span> </dt>
								<dd><img src="{$smarty.const.C_ASSETS}images/logo_payeasy.png" alt="Pay-easy"></dd>
							</dl>
							</label>
						</li>
					</ul>
					<div class="" {if $input_data.payment_method!="payeasy"}style="display:none;"{/if} id="attention">
						<p class="note">ゆうちょ銀行・みずほ銀行・三井住友銀行・三菱東京UFJ銀行・りそな銀行<br>
							<a href="http://www.econtext.jp/customer/pay_payeasy/list.php" target="_blank" style="color:#05a1c6;">その他の取り扱い銀行はこちら></a><br>
							※振込手数料はサポーターの方にご負担頂きます。<br>
							※一部金融機関での振込手続きにおいて、キャッシュカードが必要になる場合があります。<br>
							※10万円以上の場合は各行のネットバンク払いをご利用ください。<br>
							※ペイジー（銀行振込）選択の場合、10万円以下は銀行ATMでのお振込みも可能です。<br>
							10万円以上は各行のネットバンキングからお振込み下さい。</p>
					</div>
				{/if}
				<p class="actions">
					<input class="btnLv01" name="sbm_send" type="submit" value="次へ" />
				</p>
			</form>
		</div>
		<!-- #projectContnets -->

	{include file="{$lang}/collection-side.tpl"}

	</div>
	<!-- #projectStart --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
