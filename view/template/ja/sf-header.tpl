<!-- START facebook js sdk -->
<div id="fb-root"></div>
{literal}
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- /END facebook js sdk -->
{/literal}
<header class="header">
	<div class="header-navbar">
		<div class="header-tag-line">
		  	<p class="header-logo-a-port"><a href="/"><img src="{$smarty.const.C_ASSETS}images/common/logo-a-port.png" alt="a-port"></a></p>
		  	<h1 class="header-keywords">A-portは朝日新聞社のクラウドファンディングサイトです</h1>
	  	</div>
		<nav id="jsi-header-mymenu" class="header-mymenu-wrapper">
			<ul  class="header-mymenu cf">
				 {if $current_user.user_no}
				      <li class="header-mymenu-mypage loggedIn"><a href="{$pagelink_mypage}">マイページ</a></li>
				      <li class="header-mymenu-login"><a href="{$pagelink_logout}">ログアウト</a></li>
				 {else}
				     <li class="header-mymenu-login"><a href="{$pagelink_login}">ログイン</a></li>
				     <li class="header-mymenu-signup"><a href="{$pagelink_register}">新規登録</a></li>
				 {/if}
			</ul>
		</nav>
		<div class="header-toggle-wrapper cf">
			<ul class="header-toggle cf">
				<li id="jsi-header-mymenu-toggle" class="header-mymenu-toggle"><a href="javascript:void(0);"><img src="{$smarty.const.C_ASSETS}images/sp/btn-mymenu-toggle.png" alt="mymenu"></a></li>
				<li id="jsi-header-g-nav-toggle" class="header-g-nav-toggle"><a href="javascript:void(0);"><img src="{$smarty.const.C_ASSETS}images/sp/btn-gnavi-toggle.png" alt="g-nav"></a></li>
				<li class="header-g-nav-toggle fbLike-sp"><div class="fb-like" expr:data-href="data:post.url" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></a></li>
			</ul>
		</div>
	</div>
	<nav id="jsi-header-g-nav" class="header-g-nav-wrapper cf">
		<ul class="header-g-nav-projects cf">
			<li class="header-search-project"><a href="{$pagelink_list_project}"><img src="{$smarty.const.C_ASSETS}images/pc/header-search-project.png" alt="プロジェクトを探す"></a></li>
			<li class="header-start-project"><a href="{$pagelink_start}"><img src="{$smarty.const.C_ASSETS}images/pc/header-start-project.png" alt="プロジェクトをはじめる"></a></li>
		</ul>
		<ul class="header-g-nav cf">
			<li class="header-g-nav-about"><a href="{$pagelink_about}"><img src="{$smarty.const.C_ASSETS}images/pc/header-g-nav-about.png" alt="A-portについて"></a></li>
			<li class="header-g-nav-crowd-funding"><a href="{$pagelink_guide}"><img src="{$smarty.const.C_ASSETS}images/pc/header-g-nav-crowd-funding.png" alt="クラウドファンディングとは？"></a></li>
			<li class="header-g-nav-column"><a href="{$pagelink_column}"><img src="{$smarty.const.C_ASSETS}images/pc/header-g-nav-column.png" alt="コラム"></a></li>
			<li class="header-g-nav-fbLike"><div class="fb-like" expr:data-href="data:post.url" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></a></li>
			<li class="header-g-nav-question"><a href="{$pagelink_faq}">よくあるご質問</a></li>
			<li class="header-g-nav-terms"><a href="{$pagelink_usage_project}">利用規約</a></li>
			<li class="header-g-nav-privacy"><a href="{$pagelink_privacypolicy}">プライバシーポリシー</a></li>
			<li class="header-g-nav-tks"><a href="{$pagelink_tokusyo}">特定商取引法の表記</a></li>
			<li class="header-g-nav-inquiry"><a href="{$pagelink_inquiry}">お問い合わせ</a></li>
		</ul>
		<!-- <div class="header-g-nav-fbLike"><div class="fb-like" data-href="https://a-port.asahi.com/" data-layout="button" data-action="like" data-show-faces="false" data-share="false" data-colorscheme="light"></div></a></div> -->
	</nav>
</header>