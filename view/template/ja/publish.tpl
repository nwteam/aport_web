{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />

</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			{include file="{$lang}/publish-title.tpl"}
			<ul class="tab clearfix mb15">
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">支援者の一覧</a></li>
				<li><a href="{$pagelink_publish_update}?p_no={$project_info.no}">アップデート</a></li>
				<li><a href="{$pagelink_publish_mail}?p_no={$project_info.no}">メール</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
      <div id="fragment-1" class="clearfix">
        <h3>支援者の一覧</h3>
        <!--<p class="draftingUtilityBtn"><a href="#">一覧をCSVでダウンロード</a></p>-->
        <table class="draftingTable">
          <tbody>
            <tr>
              <th colspan="2" scope="col" class="collectorName">支援者名</th>
              <th scope="col" class="cheer">応援額</th>
              <th scope="col" class="return">リターン</th>
              <th scope="col" class="cheerDate">応援日</th>
              <th scope="col" class="collectorMemo">メモ（<a href="#">編集する</a>）</th>
              <th scope="col" class="collectorMail">メール</th>
            </tr>
						{foreach name=outer item=con from=$comment_list}
            <tr>
						{if $con.profile_img!=""}
              <td class="collectorThumb"><img src="{$smarty.const.C_IMG_ACTRESS}{$con.profile_img}" alt="{$con.member_name}"/></td>
						{else}
              <td class="collectorThumb"><img src="{$smarty.const.C_ASSETS}images/projects/collector-thum-noimage.png" alt="{$con.member_name}"/></td>
						{/if}
              <td class="collectorName">{$con.member_name}</td>
              <td class="cheer">{$con.invest_amount|number_format}円</td>
              <td class="return">{$con.ps_title}</td>
              <td class="cheerDate">{$con.create_date|date_format:"%Y/%m/%d"}</td>
              <td class="collectorMemo"><a href="#">{$con.memo|nl2br nofilter}</a></td>
							{if $con.unread_cnt=="0"||$con.unread_cnt==""}
								<td class="collectorMail"><a href="{$pagelink_publish_add}?p_no={$project_info.no}&m_id={$con.user_no}"><img src="{$smarty.const.C_ASSETS}images/icon_mail.png" width="20" height="14" alt="mail"/>(0)</a></td>
							{else}
								<td class="collectorMail"><a href="{$pagelink_publish_add}?p_no={$project_info.no}&m_id={$con.user_no}"><img src="{$smarty.const.C_ASSETS}images/icon_mail_active.png" width="20" height="14" alt="mail"/>({$con.unread_cnt})</a></td>
							{/if}
            </tr>
						{/foreach}
          </tbody>
        </table>
      </div>
      <!--/#fragment-1  -->
      <p class="mypageBack"><a href="{$pagelink_mypage}">&lt;&nbsp;マイページへ戻る</a></p>
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
