{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/start.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.C_ASSETS}js/lib/colorbox/colorbox.css" />
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/form.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
	{include file="{$lang}/sf-header.tpl"}

<div class="wrapper">
<div class="page-ttl-wrapper" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">プロジェクトの申し込みをする</h2>
	</div>
</div>

<div class="body">
	<div class="container container-single">
		<div class="project-new-step step3">step3</div>
		<div class="col-inner">
			<h3 class="h3"><span>Step.3</span>完了</h3>
			<div class="project-new-done-wrapper">
				<p class="project-new-done-message">プロジェクトの申し込みシートのご提出が完了いたしました。<br>
                申し込みシートについては、次のステップにお進み頂く方には、<br>
                その後の掲載迄のステップと合わせまして、<br>
                7営業日以内にこちらから再度ご連絡させていただきます。</p>
				<div class="form-submit-btn">
					<a href="/">トップに戻る</a>
				</div>
			</div>
		</div>
	</div>
    <div class="step-wrap">
      <h3><img src="{$smarty.const.C_ASSETS}images/start/step_tit.png" alt="初めてでも安心、5つのステップ"></h3>
      <div class="step-inner">
        <!--PC用表示-->
        <ol class="pc-blc">
          <li><img class="step01" src="{$smarty.const.C_ASSETS}images/start/step01.gif" alt="(1)プロジェクトを申し込む
          「プロジェクトを申し込む」ボタンを 押すと出てくる申請フォームに、 プロジェクトの内容をご記入下さい。"></li>
          <li><img class="step02" src="{$smarty.const.C_ASSETS}images/start/step02.gif" alt="(2)アイディアを練る
          審査通過後、プロジェクトページの 作成に入ります。 スタッフも相談に応じますので、 ブラッシュアップしていきましょう。">
          <!--point1-->
           <a class="popup" href="{$smarty.const.C_ASSETS}popup/step2-1.html"><img class="point01" src="{$smarty.const.C_ASSETS}images/start/point01.png" alt="「欲しい！」「支援したい！」と 思わせる魅力的なリターンを 設定しよう"></a>
          <!--point2-->
         <a class="popup" href="{$smarty.const.C_ASSETS}popup/step2-2.html"><img class="point02" src="{$smarty.const.C_ASSETS}images/start/point02.png" alt="文章、写真、動画で あなたの想いを伝えよう"></a>
          <!--point3-->
         <a class="popup" href="{$smarty.const.C_ASSETS}popup/step2-3.html"><img class="point03" src="{$smarty.const.C_ASSETS}images/start/point03.png" alt="目標金額は慎重に 設定しよう"></a></li>
          <li><img class="step03" src="{$smarty.const.C_ASSETS}images/start/step03.gif" alt="(3)資金調達スタート＆PR
          資金調達スタートです。 目標金額達成に向けて、 プロジェクトを積極的に発信し、 多くの人を巻き込みましょう。">
          <!--point4-->
          <a class="popup" href="{$smarty.const.C_ASSETS}popup/step3-1.html"><img class="point04" src="{$smarty.const.C_ASSETS}images/start/point04.png" alt="プロジェクトを公開したら、SNSをフル活用してPRしよう"></a>
          <!--point5-->
          <a class="popup" href="{$smarty.const.C_ASSETS}popup/step3-3.html"><img class="point05" src="{$smarty.const.C_ASSETS}images/start/point05.png" alt="活動報告は、A-portでも定期的に発信しよう"></a>
          <!--point6-->
           <a class="popup" href="{$smarty.const.C_ASSETS}popup/step3-2.html"><img class="point06" src="{$smarty.const.C_ASSETS}images/start/point06.png" alt="身近な人を 巻き込むことから はじめよう"></a>
          </li>
          <li><img class="step04" src="{$smarty.const.C_ASSETS}images/start/step04.gif" alt="(4)募集終了
          プロジェクトが成立したら、 まずは支援者にお礼メッセージを 送りましょう。 そして、実行に移していきましょう。"></li>
          <li><img class="step05" src="{$smarty.const.C_ASSETS}images/start/step05.gif" alt="(5)実行＆リターン送付
          プロジェクト実行の経過を随時、 支援者に報告しましょう。 約束のリターンの送付が終わったら、 プロジェクト終了です。"></li>
        </ol>
        <!--PCここまで-->
        <!--SP用表示-->
        <div class="sp-blc">
          <p><img class="step01" src="{$smarty.const.C_ASSETS}images/start/step01_sp.gif" alt="(1)プロジェクトを申し込む
          「プロジェクトを申し込む」ボタンを 押すと出てくる申請フォームに、 プロジェクトの内容をご記入下さい。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg01.png" alt=""></p>
          <p><img class="step02" src="{$smarty.const.C_ASSETS}images/start/step02_sp.gif" alt="(2)アイディアを練る
          審査通過後、プロジェクトページの 作成に入ります。 スタッフも相談に応じますので、 ブラッシュアップしていきましょう。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg02.png" alt=""></p>
          <dl class="acMenu">
            <dt> <img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">「欲しい！」「支援したい！」と思わせる<br>
              魅力的なリターンを設定しよう</dt>
            <dd>プロジェクトに共感してもらうことも大切ですが、「これが欲しい」と思っていただける<span class="ylw">魅力的なリターンの設定</span>も必要不可欠です。リターンは、「モノ」以外にも、関係者限定イベントの参加や、支援者の名前を公式サイトなどに掲載するといった無形のモノもあります。<span class="ylw">特別な魅力があるものほど支援者の心をつかむ</span>ことができます。金額に応じた複数種類の魅力的なリターンをつくってみましょう。<br />
				事務局も随時相談に乗ります。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg03.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">文章、写真、動画で
              あなたの想いを伝えよう</dt>
            <dd>クラウドファンディングは、<span class="ylw">いかに多くの方々の共感を得られることが出来るか</span>が、プロジェクトの成功を左右します。あなたの想い、プロジェクトの概要や背景について、正確にわかりやすく文章で伝えましょう。そして、さらに「素敵」と思っていただけるよう、<span class="ylw">写真、動画を丁寧に織り込んでいくことも大切です。</span><br />
				見出しや文章校正などは事務局でもサポートいたします。写真・動画撮影でお困りの場合は、専門業者などご紹介できますのでご相談ください。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg04.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">目標金額は慎重に設定しよう</dt>
            <dd>プロジェクトを実行するために必要な経費を丁寧に計算してみましょう。<br />
				見積もりをとることはもちろんのこと、リターンの発送費、場合によっては対応する人の「人件費」が必要になります。<br />
				<span class="ylw">支援者の理解が得られる目標金額</span>を設定してください。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg05.png" alt=""></p>
          <p><img class="step03" src="{$smarty.const.C_ASSETS}images/start/step03_sp.gif" alt="(3)資金調達スタート＆PR
          資金調達スタートです。 目標金額達成に向けて、 プロジェクトを積極的に発信し、 多くの人を巻き込みましょう。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg06.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">プロジェクトを公開したら、
              SNSをフル活用してPRしよう</dt>
            <dd>SNSを上手に活用し、<span class="ylw">積極的にあなたのプロジェクトをPRしましょう。</span>あなたの熱意、魅力的なリターン、プロジェクトの進捗状況などを定期的に発信することを心がけましょう。投稿へのコメントや支援者へのお礼も忘れずに・・・。<br />
				さらに、<span class="ylw">仲間と一緒にイベントなどを開催して盛り上げていくことも有効な手段</span>です。私たちも、あなたのプロジェクトを広めるために最大限支援させて頂きます。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg07.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">身近な人を巻き込むことから
              はじめよう</dt>
            <dd>まずは、あなたのプロジェクトの仲間に<span class="ylw">身近な人・・家族、友人、知人を巻き込むことが大切です。</span>家族や友人などの共感が得られないプロジェクトが、まったくの他人から賛同を得られることは難しいかもしれません。<br />
				プロジェクトへの想いを身近な人に地道に伝え、<span class="ylw">共感の輪をどんどん広げていく</span>ことを心がけましょう。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg08.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">活動報告は、A-portでも
              定期的に発信しよう</dt>
            <dd>CFサイトでの<span class="ylw">活動報告も定期的に実施して、支援者とのつながりも大事にしましょう。</span>支援者は、あなたの報告を受けて、さらにあなたのプロジェクトを発信し、プロジェクト終了後も、あなたのサポーターであり続けてくれるかもしれません。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg09.png" alt=""></p>
          <p><img class="step04" src="{$smarty.const.C_ASSETS}images/start/step04_sp.gif" alt="(4)募集終了
          プロジェクトが成立したら、 まずは支援者にお礼メッセージを 送りましょう。 そして、実行に移していきましょう。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg10.png" alt=""></p>
          <p><img class="step05" src="{$smarty.const.C_ASSETS}images/start/step05_sp.gif" alt="(5)実行＆リターン送付
          プロジェクト実行の経過を随時、 支援者に報告しましょう。 約束のリターンの送付が終わったら、 プロジェクト終了です。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg11.png" alt=""></p>
        </div>
        <!--SPここまで-->
      </div>
      <!--/step-inner-->
      <div class="mind">
        <h3>起案者の心構え</h3>
        <ul>
          <li>「欲しい！」「支援したい！」と思わせる魅力的なリターンを設定しよう</li>
          <li>文章、写真、動画であなたの想いを伝えよう</li>
          <li>目標金額は慎重に設定しよう</li>
          <li>プロジェクトを公開したら、SNSをフル活用しよう</li>
          <li>身近な人を巻き込むことからはじめよう</li>
          <li>活動報告は、A-portでも定期的に発信しよう</li>
        </ul>
        <p>※プロジェクトの成功率を高める秘訣についての資料は、実際にプロジェクトを立ち上げることが決まった方々に別途、差し上げております。</p>
      </div>
      <!--/mind-->
    </div>
	<!--/step-wrap-->


</div>
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/jquery.rollover.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/colorbox/jquery.colorbox-min.js"></script>
{literal}
<script>
jQuery(document).ready(function($) {
  $('ol.pc-blc a img').rollover();
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $(".popup").colorbox({
    iframe:true,
    width:"740px",
    height:"490px"
  });
});
</script>
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
