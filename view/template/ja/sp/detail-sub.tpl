		<div class="col-sub">
			<div class="col-inner">
				<ul class="sub-project-status">
					<li class="sub-project-status-collector">
						<p class="sub-project-status-label">支援者</p>
						<p class="sub-project-status-var"><span>{$total_count}</span>人</p>
					</li>
					<li class="sub-project-status-last">
						<p class="sub-project-status-label">残り期間</p>
						<p class="sub-project-status-var"><span>{$item.diff_in_days}</span>日</p>
					</li>
					<li class="sub-project-status-fund">
						<p class="sub-project-status-label">集まっている金額</p>
						<p class="sub-project-status-var"><span>{$item.now_summary|number_format}</span>円</p>
						<p class="sub-project-status-target">目標金額：<span>{$item.wish_price|number_format}</span>円</p>
					</li>
					<li class="sub-project-status-per">
						<p class="sub-project-status-label">達成率<span>{$item.percent}</span>%</p>
						<div class="progress">
							<div class="gauge" style="width:100%;">
								<div class="gauge2" {if $item.percent<100}style="width:{$item.percent}%;"{else}style="width:100%;"{/if}></div>
							</div>
						</div>
					</li>
				</ul>
				{if $item.percent<100}
					{if $item.project_type=="1"}
						<p class="project-notes">このプロジェクトでは、<span class="">{$item.invest_limit|date_format:"%Y年%m月%d日"}23:59</span>までに目標に到達した場合のみ、ファンディングが実行されます。</p>
					{else}
						<p class="project-notes">このプロジェクトでは、目標到達に関わらず、<span class="">{$item.invest_limit|date_format:"%Y年%m月%d日"}23:59</span>までに集まった金額がファンディングされます。</p>
					{/if}
				{else}
					<p class="project-notes"><span class="">{$item.invest_limit|date_format:"%Y年%m月%d日"}23:59</span>に終了しました。</p>
				{/if}
					{if $item.in_invest_flg}
						{if $current_user.user_no}
							{if $current_user.user_no!=$item.user_no}
								<div class="btn-project-invest">
									<a href="{$pagelink_collection_ticket}?no={$item.no}">プロジェクトを応援する</a>
								</div>
							{/if}
						{else}
							<div class="btn-project-invest">
								<a href="{$pagelink_login}">応援をするにはログインが必要です</a>
							</div>
						{/if}
					{else}
						<div class="btn-project-invest">
							<a href="#">応援期間終了</a>
						</div>
					{/if}
			</div>
			<div class="col-inner">
				<h4 class="sub-presenter-ttl">実行者</h4>
				<div class="sub-presenter-profile cf">
					<div class="sub-presenter-thum"><img src="{$smarty.const.C_IMG_ACTRESS}{$item.profile_img}" alt="実行者イメージ"></div>
					<div class="sub-presenter-meta">
						<p class="sub-presenter-name">{$item.full_name}</p>
						<p class="sub-presenter-location">{$array_area[$actress.area]}</p>
					</div>
				</div>
				<div class="sub-presenter-link">
					<a href="{$item.blog_url_1}" target="_blank">{$item.blog_url_1}</a>
				</div>
				<div class="sub-presenter-detail">
					{mb_substr($item.profile,0,20,'utf-8')}
					<span class="jsc-sub-presenter-detail-dot">...</span>
					<span class="jsc-sub-presenter-detail-inner jsc-dn">
					{mb_substr($item.profile,21,1000,'utf-8')}
					</span>
					<div class="sub-presenter-detail-more-open jsc-sub-presenter-detail-more-open"><a href="javascript:void(0);">続きを見る</a></div>
					<div class="sub-presenter-detail-more-close jsc-sub-presenter-detail-more-close jsc-dn"><a href="javascript:void(0);">閉じる</a></div>
				</div>
				{if $current_user.user_no!=$item.user_no}
					{if $current_user.user_no}
						<div class="btn-presenter-question">
							<a href="{$pagelink_mypage_sendmsg}?p_no={$item.no}&m_id={$item.user_no}">質問や意見をメールする</a>
						</div>
					{else}
						<div class="btn-presenter-question">
							<a href="{$pagelink_login}">質問や意見をメールする</a>
						</div>
					{/if}
				{/if}
			</div>
			<div class="sub-ticketlist-wrapper">
				<ul class="sub-ticketlist">
				{foreach from=$present_list item="present" name="loop"}
					{if $present.invest_limit>=$present.invest_cnt}
					<li>
					{else}
					<li class="soldout">
					{/if}
						{if $present.invest_limit>=$present.invest_cnt}
							{if $present.invest_limit=="9999"||$present.invest_limit=="0"||$present.invest_limit==""}
							{else}
								<span class="limitedTicket">残り{$present.invest_limit}枚</span>
							{/if}
						{else}
							<span class="soldoutTicket">SOLD OUT</span>
						{/if}
						<p class="sub-ticketlist-fund jsc-sub-ticketlist-fund-toggle"><span>{$present.min|number_format}</span>円</p>
						<div class="sub-ticketlist-inner jsc-sub-ticketlist-fund-inner">
							<p class="returnTitle">{$present.title}</p>
							<p class="sub-ticketlist-return-ttl">リターン</p>
							{if $present.return_img!=""}
							<p class="sub-ticketlist-img"><img src="{$smarty.const.C_IMG_PROJECT}{$present.return_img}" alt="" width=254 height=180></p>
							{/if}
							<ul class="sub-ticketlist-return">
								{$present.text|nl2br nofilter}
							</ul>
							<p class="sub-ticketlist-collector">
								<span class="sub-ticketlist-collector-label">サポーターの数</span>
								<span class="sub-ticketlist-collector-var">{$present.invest_cnt}</span>人
							</p>
							<p class="sub-ticketlist-delivery">
								お届け予定：<span class="sub-ticketlist-delivery-var">{$present.return_year}年{$present.return_month}月上旬</span>
							</p>
							<div class="project-invest-box">
								<div class="btn-project-invest">
								{if $item.in_invest_flg}
									{if $current_user.user_no}
										{if $current_user.user_no!=$item.user_no}
											<a href="{$pagelink_collection_ticket}?no={$item.no}&present_no={$present.present_no}">プロジェクトを応援する</a>
										{/if}
									{else}
										<a href="{$pagelink_login}">応援をするにはログインが必要です</a>
									{/if}
								{else}
									<a href="#">応援期間終了</a>
								{/if}
								</div>
							</div>
						</div>
					</li>
				{/foreach}
				</ul>
			</div>
		</div><!--/col-sub-->