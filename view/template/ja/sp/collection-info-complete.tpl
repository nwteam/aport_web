{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<script type='text/javascript'>
{literal}

{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div id="wrapper">
	<ul id="step">
		<li>1. 応援リターンの選択 </li>
		<li>2. 決済情報の入力 </li>
		<li>3. 応援内容の確認 </li>
		<li class="active">4. 完了<span>（応援コメントを寄せよう）</span> </li>
	</ul>
	<div id="projectStart">
		<div id="projectContents">
			<h2>完了</h2>
			{if $payment_method=="cvs"}
				<h3>購入申し込みを受け付けました</h3>
				<div id="due" class="attentionBox">
					<h4>お支払い期日：{$payment_term|date_format:"%m月%d日"}（{$array_week[$payment_term|date_format:"%w"]}）{$payment_term|date_format:"%H時%M分"}まで</h4>
					<h5>【注意】お支払い期日を過ぎますと申し込まれた支援内容はキャンセルとなりますのでご注意ください。</h5>
					<p>登録メールアドレス宛に、コンビニ決済でのお支払方法に関するメールを送信いたしました。<br>コンビニ決済でのお支払い完了後、支援完了メールが届きます。</p>
					<ul class="note">
						<li>※ 迷惑メールフォルダに自動振分されて届くことがありますので一度ご確認下さい。</li>
						<li>※ メールが届きましたら必ず開封して中を確認してください。返金時に確認のとれるメールアドレスをご用意してください。</li>
					</ul>
				</div>
					<dl class="confirmList">
						<dt>支払い金額</dt>
						<dd>{$total_amount|number_format}円（税込）</dd>
					</dl>
					<dl class="confirmList">
						<dt>支払い受付番号</dt>
						<dd>{$receipt_no}</dd>
					</dl>
					<dl class="confirmList">
						<dt>電話番号</dt>
						<dd>{substr_replace($ks_tel,'****',-4)}</dd>
					</dl>
					<dl class="confirmList">
						<dt>支払い方法</dt>
						{if $convenience=="00002"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni01.png" alt="FamilyMart"/></dd>
						{/if}
						{if $convenience=="00004"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni02.png" alt="サークルK"/></dd>
						{/if}
						{if $convenience=="00001"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni03.png" alt="LAWSON"/></dd>
						{/if}
						{if $convenience=="00005"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni04.png" alt="MINI STOP"/></dd>
						{/if}
						{if $convenience=="00008"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni05.png" alt="Seicomart"/></dd>
						{/if}
					</dl>
					<div class="attentionBox"> 
						<p class="attention">【重要】<strong>入金が確認されるまでは、仮登録</strong>となります。</p>
						<p class="note">【重要】限定枚数のあるリターンは、<strong>{$payment_term|date_format:"%m月%d日"}（{$array_week[$payment_term|date_format:"%w"]}）{$payment_term|date_format:"%H時%M分"}まで予約状態</strong>となります。</p>
					</div>
			{/if}
			{if $payment_method=="payeasy"}
				<h3>購入申し込みを受け付けました</h3>
				<div id="due" class="attentionBox">
					<h4>お支払い期日：{$payment_term|date_format:"%m月%d日"}（{$array_week[$payment_term|date_format:"%w"]}）{$payment_term|date_format:"%H時%M分"}まで</h4>
					<h5>【注意】お支払い期日を過ぎますと申し込まれた支援内容はキャンセルとなりますのでご注意下さい。</h5>
					<p>登録メールアドレス宛に、ペイジー(銀行振込)でのお支払方法に関するメールを送信いたしました。<br>ペイジー(銀行振込)でのお支払い完了後、支援完了メールが届きます。</p>
					<ul class="note">
						<li>※ 迷惑メールフォルダに自動振分されて届くことがありますので一度ご確認ください。</li>
						<li>※ メールが届きましたら必ず開封して内容を確認してください。返金時に確認のとれるメールアドレスをご用意してください。</li>
						<li>※ 銀行ATMから１０万円を超える現金の振り込みはできないため、キャッシュカードでお支払いいただくか、ネットバンキングをご利用ください。</li>
						<li>※ ネットバンキングのお支払い方法についても、支援完了メールでご確認ください。</li>
					</ul>
				</div>
					<dl class="confirmList">
						<dt>支払い金額</dt>
						<dd>{$total_amount|number_format}円（税込）</dd>
					</dl>
					<dl class="confirmList">
						<dt>支払い受付番号</dt>
						<dd>{$receipt_no}</dd>
					</dl>
					<dl class="confirmList">
						<dt>電話番号</dt>
						<dd>{substr_replace($ks_tel,'****',-4)}</dd>
					</dl>
					<dl class="confirmList">
						<dt>支払い方法</dt>
						<dd><img src="{$smarty.const.C_ASSETS}images/logo_payeasy.png" alt="ペイジー"/></dd>
					</dl>
			{/if}
			<h3>応援コメントを寄せよう！</h3>
			<!--<p>支援が完了しました。このプロジェクトの魅力をfacebookやTwitterでみんなに伝えて、支援の輪を広げましょう！</p>-->
			<div id="forPresenter">
			 <h4>実行者からのコメント</h4>
			 <p class="presenterThumb"><img src="{$smarty.const.C_IMG_ACTRESS}{$item.profile_img}" alt="実行者イメージ"></p>
			 <p class="presenterComment">実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント実行者からのコメント</p>
			</div>
			
			<div id="forSupporter">
			 <h4>実行者に応援コメントを寄せよう！</h4>
			 <ul class="note">
				<li>※ コメントは<a href="detail-project-supporter.php?p_no={$item.no}">支援者一覧ページ</a>に公開されます。個人情報等を記載しないようお願い致します。</li>
				<li>※ 絵文字は入力できません。</li>
				<li>※ コメントは支払いが確認されてからの反映となります。</li>
			 </ul>
			 <p class="supporterThumb"><img src="{$smarty.const.C_IMG_ACTRESS}{$member_info.profile_img}" alt="サポーターイメージ"></p>
			 <form accept-charset="UTF-8" action="" method="post">
			 <p class="supporterComment"><textarea name="supporter_comment"></textarea></p>
			 <!--<ul id="snsLink">
				<li><label>
									<input type="checkbox" name="" value="facebook" />
									<span class="fb">facebook</span>に投稿</label></li>
									<li><label>
									<input type="checkbox" name="" value="Twitter" />
									<span class="tw">Twitter</span>に投稿</label></li>
			 </ul>-->
					<p class="actions">
						<input class="btnLv01" name="sbm_update" type="submit" value="投稿する" />
					</p>
			</form>
			</div>
		</div>
		<!-- #projectContnets -->
		<div class="side">
			<div id="projectSideSummary" class="pjtItem pcCon">
				<div class="pjtImage"><a href="{$pagelink_detail_project}{$item.hope_url}"><img alt="{$item.title}" src="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}" /></a></div>
				<div class="pjtTitle">{$item.title}</div>
				<ul class="pjtTip">
					<li class="presenter">{$item.full_name}</li>
				</ul>
			</div>
		</div>
		<!--.side--> 
		
	</div>
	<!-- #projectStart --> 
</div>
<!--/#wrapper-->
	{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
