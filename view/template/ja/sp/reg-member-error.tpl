{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/signin.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<div id="wrapper">
<div id="signIN">
	<h2><strong>会員登録</strong></h2>
	<h4 style="color:red;"><strong>
	{if $msgFlg==1}
		メール記載の認証URLをもう一度クリックしてください。<br />
		クリックしてもうまく表示されない場合は、コピー＆ペーストでブラウザでアクセスしてください。<br />
		または、登録の期限が切れたために、仮登録データが削除されている可能性があります。<br />
		たいへん申し訳ございませんが、もう一度仮登録からやり直してください。
	{elseif $msgFlg==2}
		会員の本登録は完了しています。
	{elseif $msgFlg==3}
		登録の期限が切れました。<br />たいへん申し訳ございませんが、もう一度仮登録からやり直してください。
	{elseif $msgFlg==4}
		登録時にエラーが発生しました。<br />たいへん申し訳ございませんが、もう一度仮登録からやり直してください。
	{else}
		不正アクセスエラー
	{/if}
	</strong>
	</h4>
	{if $msgFlg==2}
	</br>
	<p id="membershipMessage" ><a href="{$pagelink_login}"><input class="btnLogin" name="sbm_login" id="btnLogin" type="button" value="ログインする" /></a></p>
	{else}
	<p class="LnkArw"><a href="/"><span>トップページに戻る</span></a></p>
	{/if}
</div>
<!--/#contents--> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
