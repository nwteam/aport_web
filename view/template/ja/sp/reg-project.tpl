{include file="{$lang}/sf-head.tpl"}

<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/form.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div class="wrapper">
<div class="page-ttl-wrapper" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">プロジェクトの申し込みをする</h2>
	</div>
</div>

<div class="body">
	<div class="container container-single">
		<div class="project-new-step step1">step1</div>
		<div class="col-inner">
			<h3 class="h3"><span>Step.1</span>お申し込みシート記入</h3>
			<form id="frm_project" name="frm_project" method="post" action="{$pagelink_reg_project}#frm_project">
				<div class="error-message-wrapper">
					<ul class="error-message">
					{if $err_msg.top}
						<li>※{$err_msg.top}</li>
					{/if}
					</ul>
				</div>
				<div class="input-form-wrapper">
					<ul class="input-form">
						<li>
							<dl class="input-form-inner input-applicant cf">
								<dt>お名前</dt>
								<dd>
									{if $err_msg.name}
										<p class="errorTitle" style="color:red;">※{$err_msg.name}</p>
									{/if}
									<input type="text" name="name" id="name"  value="{$input_data.name}" class="form-control">
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-email cf">
								<dt>メールアドレス</dt>
								<dd>
									{if $err_msg.email}
										<p class="errorTitle" style="color:red;">※{$err_msg.email}</p>
									{/if}
									<input type="text" name="email" id="email" value="{$input_data.email}" class="form-control">
									<span class="caution">※半角英数</span>
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-facebook cf">
								<dt>Facebookアカウント<span class="atn-txt">（任意）</span></dt>
								<dd>
									{if $err_msg.fb_url}
										<p class="errorTitle" style="color:red;">※{$err_msg.fb_url}</p>
									{/if}
									<input type="text" name="fb_url" id="fb_url"  value="{$input_data.fb_url}"  class="form-control">
									<span class="caution">※https://www.facebook.com/id、またはユーザーネーム</span>
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-name cf">
								<dt>プロジェクト名</dt>
								<dd>
									{if $err_msg.project_name}
										<p class="errorTitle" style="color:red;">※{$err_msg.project_name}</p>
									{/if}
									<input type="text" id="project_name" name="project_name" value="{$input_data.project_name}" class="form-control">
									<span class="caution">※60文字以内</span>
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-category-id cf">
								<dt>カテゴリ名</dt>
								<dd>
									{if $err_msg.category_no}
										<p class="errorTitle" style="color:red;">※{$err_msg.category_no}</p>
									{/if}
								<select class="fmselect" name="category_no" >
									{html_options  options=$array_category selected=$input_data.category_no }
								</select>
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-description cf">
								<dt>プロジェクト概要</dt>
								<dd>
									{if $err_msg.project_text}
										<p class="errorTitle" style="color:red;">※{$err_msg.project_text}</p>
									{/if}
									<textarea id="project_text" name="project_text" rows="15" class="form-control">
{if ($input_data.project_text=='')}
▼プロジェクト内容について、ご記入ください。
・起案者のプロフィール
・プロジェクトで実現したい夢や目標
・実現したい時期
・必要な資金とその使い道

（例）
私は○○県△△市のレザークラフト（革細工）職人です。
最近発注が多いのが、「ＩＤカード」や「ＩＣカード」のケース。
カードを忘れてオフィスビルに入ることができず、取りに帰ったという話をよく聞きます。
そこで、アプリを制作するA社と一緒に、カードが持ち主の身体から一定距離離れるとスマートフォンのアラームが鳴る仕組みを搭載した丈夫でデザイン性の高い多機能カードケースを作りたいと思っています。
来年末には販売を予定していますが、生産するのに必要な資金約200万円を集めたいと思っています。
{else}
{$input_data.project_text}
{/if}
									</textarea>
									<span class="caution">※1000文字以内</span>
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-goal-amount cf">
								<dt>想定目標金額</dt>
								<dd>
									{if $err_msg.wish_price}
										<p class="errorTitle" style="color:red;">※{$err_msg.wish_price}</p>
									{/if}
									<input type="text" name="wish_price" id="wish_price" value="{$input_data.wish_price}" class="form-control"><span class="en">円</span><span class="caution">※半角数字</span>
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-ticket-description cf">
								<dt>想定リターン</dt>
								<dd>
									{if $err_msg.comment_text}
										<p class="errorTitle" style="color:red;">※{$err_msg.comment_text}</p>
									{/if}
									<textarea id="comment_text" name="comment_text" rows="15" class="form-control">
{if ($input_data.comment_text=='')}
▼支援してくれたユーザーへの、金額に応じたリターン内容をご記入ください。

（例）
・500円...支援へのお礼メッセージ
・3000円...支援へのお礼メッセージ＋商品１点送付
・5000円...支援へのお礼メッセージ＋商品２点
・10000円...支援へのお礼メッセージ＋商品２点送付＋革のコインケース２点送付。
{else}
{$input_data.comment_text}
{/if}
									</textarea>
									<span class="caution">※箇条書きでご記入ください(1000文字以内)</span>
								</dd>
							</dl>
						</li>
					</ul>
					<div class="check-term">
						<label for="checkboxfield002">
							<input type="checkbox" name="rulesParticipant" value="1" {if $input_data.rulesParticipant}checked="checked" {/if} class="checkbox">
							<span><a href="{$pagelink_usage_project}" target="_blank" >利用規約</a>に同意する</span>
							{if $err_msg.rulesParticipant}
								<p class="errorTitle" style="color:red;">※利用規約を同意してください。</p>
							{/if}
						</label>
					</div>
					<div class="form-submit-wraper">
						<div class="form-submit-btn">
							<input type="submit" name="commit" value="確認画面へ">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
