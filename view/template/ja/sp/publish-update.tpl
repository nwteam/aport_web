{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />

</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			{include file="{$lang}/publish-title.tpl"}
			<ul class="tab clearfix mb15">
				<li><a href="{$pagelink_publish}?p_no={$project_info.no}">支援者の一覧</a></li>
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">アップデート</a></li>
				<li><a href="{$pagelink_publish_mail}?p_no={$project_info.no}">メール</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
      <div id="fragment-2">
        <h3>アップデート</h3>
        <p class="draftingUtilityBtn"><a href="{$pagelink_publish_update_add}?p_no={$project_info.no}">新規作成</a></p>
        <table class="upDateTable">
          <tbody>
            <tr>
              <th colspan="2" scope="col" class="upDateTitle">タイトル</th>
              <th scope="col" class="upDateDate">投稿日</th>
              <th scope="col" class="upDateScale">公開範囲</th>
              <th scope="col" class="upDateEdit">設定</th>
            </tr>
						{assign var=i value=$update_cnt}
						{foreach name=outer item=con from=$update_list}
            <tr>
              <td class="vol">Vol.{$i}</td>
              <td class="tL">{$con.update_title}</td>
              <td class="upDateDate">{$con.create_date|date_format:"%Y/%m/%d"}</td>
							{if $con.update_type=="1"}
								<td class="upDateScale">支援者限定</td>
							{else}
								<td class="upDateScale">全員に公開</td>
							{/if}
              <td class="upDateEdit"><a href="{$pagelink_publish_update_add}?p_no={$project_info.no}&u_id={$con.update_id}">設定</a></td>
            </tr>
						{$i=$i-1}
						{/foreach}
          </tbody>
        </table>
      </div>
      <!--/#fragment-2  -->
      <p class="mypageBack"><a href="{$pagelink_mypage}">&lt;&nbsp;マイページへ戻る</a></p>
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
