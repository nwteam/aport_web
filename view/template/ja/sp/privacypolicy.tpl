{include file="{$lang}/sf-head.tpl"}
	<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
</head>
<body>
<div class="wrapper">
{include file="{$lang}/sf-header.tpl"}
<div class="page-ttl-wrapper"  style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">プライバシーポリシー</h2>
	</div>
</div>
<div class="body">
	<div class="container container-offset cf">
		<div class="col-main">
			<div class="col-inner">
				<section class="clause">
					<h3 class="project-description-ttl pt20">
						A-portでの個人情報の取扱いについて
					</h3>
                    <p class="fs12 pt10 pb5">当社は、個人情報保護法の全面施行にあわせ、「個人情報保護方針」、「報道・著述目的で取り扱う個人情報の保護方針」も策定しました。個人情報に関して適用される法令や規範を順守するとともにこれら方針に従って個人情報を適切に取扱います。</p>
                    <p class="fs12 pb5"><a href="http://www.asahi.com/shimbun/reference/privacypolicy.html" target="_blank">朝日新聞社「個人情報について」のページへ</a></p>
                    <p class="fs12 pb10">また、A-portでの個人情報の取扱いについては、下記の通りとします。</p>
					<ol class="list-style-first">
						<li>当社は、個人情報を、<a href="http://www.asahi.com/shimbun/reference/privacypolicy01.html" target="_blank">『個人情報保護方針』</a>に従って厳正に管理し、適切に取り扱います。</li>
						<li>当社は、個人情報を、次の各号に定める範囲内で利用します。また、当社は、あらかじめ当該個人情報の当事者の同意を得た場合、その範囲内で個人情報を利用することがあります。
							<ol class="list-style-inner">
								<li>（１）A-portのサービス運営、提供（会員登録・支援・解約手続き、A-port会員の認証、問い合わせやトラブルへの対応、顧客管理、A-port利用料金の請求・決済、各種通知・連絡を含みます）</li>
								<li>（２）A-portのサービス向上、改良、カスタマイズに役立てるための調査・アンケートの実施、分析</li>
								<li>（３）A-portの利用状況や属性等に応じた新たなA-portサービスの開発、研究</li>
								<li>（４）A-portに企画を掲載する起案者、広告主、提携企業等の商品・サービス等に関するご案内</li>
								<li>（５）当社または当社グループ会社が発行・運営する商品・サービス等（新聞、出版物、デジタルメディア、文化事業、イベント、セミナーなどを含みます。以下同じ）のご案内</li>
								<li>（６）当社が運営する商品・サービス等の改善のための調査・アンケートの実施、分析</li>
								<li>（７）上記各号に付随する業務</li>
							</ol>
						</li>
						<li>当社は、前項に定める業務の全部または一部を、業務提携先である株式会社モーションギャラリー（以下、「提携先」といいます）をはじめとする第三者（以下「委託先」といいます）に委託することがあるものとし、委託業務遂行のために、提携先および委託先に個人情報の取り扱いを委託することがあります。この場合、当社は提携先に対し、委託業務の遂行に必要な範囲内でのみ個人情報を開示して取り扱わせるとともに、個人情報の厳正な管理及び取り扱いを義務付けます。</li>
						<li>当社はA-port会員の登録情報について、個人を特定できない統計的数値として処理した上でA-portの広告主を含む第三者に提供することがあります。</li>
						<li>A-port会員が、A-portのサービスを通じて第三者と取引関係を持ったことにより、当該第三者が取得した個人情報の取り扱いについて、当社は責任を負いません。</li>
						<li>当社は、解約・解除等の事由を問わず、A-port退会後も、A-port利用料金の請求履歴など、法令上保管の必要のある情報を当該法令に従い一定期間保管する他、A-portの運営に必要な範囲内で当該A-port会員に関する個人情報を保有し、『個人情報保護方針』に従って取り扱うものとします。</li>
					</ol>
				</section>
			</div>
		</div><!--/col-main-->
		{include file="{$lang}/other-sub.tpl"}
		<ol class="breadcrumb cf">
			<li><a href="/">サイトトップ</a></li>
			<li>プライバシーポリシー</li>
		</ol>
	</div><!--/container-->
</div><!--/body-->
</div>
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
