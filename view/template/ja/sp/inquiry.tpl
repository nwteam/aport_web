{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/form.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div id="wrapper" class="clearfix">
<div class="page-ttl-wrapper" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">お問い合わせ</h2>
	</div>
</div>

<div class="body">
	<div class="container container-single">
		<div class="col-inner">
			<p class="read">各種お問い合わせは、下記フォームよりお願いいたします。</p>
			<form action="" method="post">
				<div class="input-form-wrapper">
					<ul class="input-form">
						<li>
							<dl class="input-form-inner input-name cf">
								<dt>お名前</dt>
								<dd>
									<input type="text" name="name" id="name" value="{$input_data.name}" class="form-control">
									{if $err_msg.name}
										<p class="error" style="color:red;">※{$err_msg.name}</p>
									{/if}
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-email cf">
								<dt>メールアドレス</dt>
								<dd>
									<input type="text" name="email" id="email" value="{$input_data.email}" class="form-control">
									<span class="caution">※半角英数</span>
									{if $err_msg.email}
										<p class="error" style="color:red;">※{$err_msg.email}</p>
									{/if}
								</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-description cf">
								<dt>お問い合わせ内容</dt>
								<dd>
									<textarea name="message" id="message"  rows="15" class="form-control">{$input_data.message}</textarea>
									<span class="caution">※1000文字以内</span>
									{if $err_msg.message}
										<p class="error" style="color:red;">※{$err_msg.message}</p>
									{/if}
								</dd>
							</dl>
						</li>
					</ul>
					<div class="check-term">
						<label for="checkboxfield002">
							<input type="checkbox" name="rulesParticipant" id="checkboxfield002" value="1" class="checkbox">
							<span><a href="{$pagelink_privacypolicy}">プライバシーポリシー</a>に同意する</span>
						</label>
						{if $err_msg.rulesParticipant}
							</br><span class="error" style="color:red;">※{$err_msg.rulesParticipant}</span>
						{/if}
					</div>
					<div class="form-submit-wraper">
						<div class="form-submit-btn">
							<input type="submit" value="次に進む" id="sendForm" name="send" onClick="return confirm('入力した内容を送信します。よろしいですか？');">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
