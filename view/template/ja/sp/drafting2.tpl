{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
<style type="text/css">
{literal}
.hide{display:none;}.invisible{visibility:hidden;}.overflow{overflow:hidden;}
.clear{display:block;clear:both;height:0;overflow:hidden;}

/* formbox */
#drafting .succeed{background:url(assets/images/check_ok.gif) no-repeat -105px 0;}
#drafting .focus{color:#999;line-height:22px;*line-height:20px;}
#drafting .null,#drafting .error{color:red;line-height:22px;*line-height:20px;}
{/literal}
</style>

</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul class="tab clearfix mb15">
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting1.php?p_no={$input_data.no}">基本情報</a></li>
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">リターン内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting3.php?p_no={$input_data.no}">プロジェクト内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting4.php?p_no={$input_data.no}" >SNSとPR</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting5.php?p_no={$input_data.no}" >起案者情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting6.php?p_no={$input_data.no}">プレビュー</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
			<!--LV5 fragment-2 Start -->
			<div id="fragment-2">
				<div class="draftingLeft">
					{if $err_msg.top}
							<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
					{/if}
					{for $i=1;$i<31;$i++}
						{if $i<31&&$i!=1}
							<p id="add{$i}" class="addReturn" >＋ Returnを追加</p>
						{/if}
						{if $i>1}
							{if $input_data.return_min.$i!=""}
								<div id="return{$i}" style="display:inline;">
							{else}
								<div id="return{$i}" style="display:none;">
							{/if}
						{/if}
						<h3><span>{$i}</span>{$i}つめのリターン</h3>
						<table>
							<tbody>
								<tr>
									<th scope="row">リターン金額（半角数字）</th>
									<td>
									<input id="return_min_{$i}" name="return_min[{$i}]" size="35" type="text" value="{$input_data.return_min.$i}"/>円（税込）
									<label id="return_min_{$i}_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="return_min_{$i}_error"></label>
									{if $err_msg.return_min.$i}
											<p class="errorTitle" style="color:red;">※{$err_msg.return_min.$i}</p>
									{/if}
									</td>
								</tr>
								<tr>
									<th scope="row">リターンタイトル（40文字以内）</th>
									<td>
									<input id="return_title_{$i}" name="return_title[{$i}]" size="35" type="text"  value="{$input_data.return_title.$i}" maxlength="40" />
									<label id="return_title_{$i}_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="return_title_{$i}_error"></label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターン内容</th>
									<td>
									<textarea cols="50" id="return_text_{$i}" maxlength="300" name="return_text[{$i}]" rows="10">{$input_data.return_text.$i}</textarea>
									<label id="return_text_{$i}_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="return_text_{$i}_error"></label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターンの郵送<br><span class="cautionMini">「必要」を選択すると支援者の住所情報を取得します</span></th>
									<td>
										<label><input type="radio" name="return_post_flg[{$i}]" value="1" {if $input_data.return_post_flg.$i=='1'}checked="checked"{else}{/if}/>必要</label>
										<label><input type="radio" name="return_post_flg[{$i}]" value="0" {if $input_data.return_post_flg.$i=='0'}checked="checked"{else}{/if}/>不必要</label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターンのお届け予定</th>
									<td>
										<select name="return_year[{$i}]">{html_options options=$array_post_year selected=$input_data.return_year.$i }</select>年
										<select name="return_month[{$i}]">{html_options options=$array_post_month selected=$input_data.return_month.$i }</select>月
									</td>
								</tr>
								<tr>
									<th scope="row">リターンの最大数（半角数字）</th>
									<td>
										<input id="max_count_{$i}" name="max_count[{$i}]" size="35"  maxlength="4" type="text" {if $input_data.max_count.$i==""||$input_data.max_count.$i==0}value="9999"{else}value="{$input_data.max_count.$i}"{/if}/>個</br>
										<span class="cautionMini">※限度数を上限なしに設定する場合は、「9999」と入力してください</span>
										<label id="max_count_{$i}_succeed" class="blank"></label>
										<span class="clear"></span>
										<label id="max_count_{$i}_error"></label>
										{if $err_msg.max_count.$i}
												<p class="errorTitle" style="color:red;">※{$err_msg.max_count.$i}</p>
										{/if}
									</td>
								</tr>
								<tr>
									<th scope="row">リターン画像<br>（280×165pxで3MB以下のJPEG,PNG,GIFファイル）</th>
									<td>
									{if $smarty.session.TMP_FUP.$i!=""}
										<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP.$i}" width=280 height=165 />
									{elseif $input_data.return_img.$i!=""}
										<img src="{$smarty.const.C_IMG_PROJECT}{$input_data.return_img.$i}" width=280 height=165 />
										<input type="hidden" name="new_return_img[{$i}]" value="{$input_data.return_img.$i}" />
									{/if}
									<p><input type="file" name="upload_file{$i}" size="40" aria-invalid="false" /></p>
									{if $err_msg.upload_file.$i}
											<p class="errorTitle" style="color:red;">※{$err_msg.upload_file.$i}</p>
									{/if}
									</td>
								</tr>
							</tbody>
						</table>
					{/for}
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					<ul class="formStep">
						<li class="formPrev"><a href="{$smarty.const.ROOT_URL}ja/drafting1.php?p_no={$input_data.no}">戻る</a></li>
						<li class="formNext"><input id="next_f2" value="次へ" type="submit" name="d2" style="float:right;"/>
						<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-2  -->
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
<script type='text/javascript'>
{literal}
$(function() {
	$('#add2').click(function(){$(this).next('#return2').slideToggle();});
	$('#add3').click(function(){$(this).next('#return3').slideToggle();});
	$('#add4').click(function(){$(this).next('#return4').slideToggle();});
	$('#add5').click(function(){$(this).next('#return5').slideToggle();});
	$('#add6').click(function(){$(this).next('#return6').slideToggle();});
	$('#add7').click(function(){$(this).next('#return7').slideToggle();});
	$('#add8').click(function(){$(this).next('#return8').slideToggle();});
	$('#add9').click(function(){$(this).next('#return9').slideToggle();});
	$('#add10').click(function(){$(this).next('#return10').slideToggle();});
	$('#add11').click(function(){$(this).next('#return11').slideToggle();});
	$('#add12').click(function(){$(this).next('#return12').slideToggle();});
	$('#add13').click(function(){$(this).next('#return13').slideToggle();});
	$('#add14').click(function(){$(this).next('#return14').slideToggle();});
	$('#add15').click(function(){$(this).next('#return15').slideToggle();});
	$('#add16').click(function(){$(this).next('#return16').slideToggle();});
	$('#add17').click(function(){$(this).next('#return17').slideToggle();});
	$('#add18').click(function(){$(this).next('#return18').slideToggle();});
	$('#add19').click(function(){$(this).next('#return19').slideToggle();});
	$('#add20').click(function(){$(this).next('#return20').slideToggle();});
	$('#add21').click(function(){$(this).next('#return21').slideToggle();});
	$('#add22').click(function(){$(this).next('#return22').slideToggle();});
	$('#add23').click(function(){$(this).next('#return23').slideToggle();});
	$('#add24').click(function(){$(this).next('#return24').slideToggle();});
	$('#add25').click(function(){$(this).next('#return25').slideToggle();});
	$('#add26').click(function(){$(this).next('#return26').slideToggle();});
	$('#add27').click(function(){$(this).next('#return27').slideToggle();});
	$('#add28').click(function(){$(this).next('#return28').slideToggle();});
	$('#add29').click(function(){$(this).next('#return29').slideToggle();});
	$('#add30').click(function(){$(this).next('#return30').slideToggle();});
});
{/literal}
</script>
{include file="{$lang}/sf-footer.tpl"}

<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.form.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
