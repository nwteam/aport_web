	<div class="draftingRight">
	  <h3>プロジェクト成功のノウハウ</h3>
	  <p>下記のTipsを参考にして、プロジェクトを成功させよう！</p>
	  <ul class="project-list-index">
		<li>
		  <div class="project-img"> <a href="/projects/tabeyomanabo"> <img src="https://mg-img.s3.amazonaws.com/projects/tabeyomanabo/f50329d2-6851-4784-9b8b-372df95cd6b8" alt="子どもの居場所『おいしい塾　たべよ・まなぼ』"> </a></div>
		  <div class="project-box">
			<p class="project-title"> <a href="/projects/tabeyomanabo"> 温かいご飯と学習サポートを無料で。福島に子どもたちの居場所「塾」を作りたい </a> </p>
			<ul class="project-tip cf">
			  <li class="project-tip-category" style="width:120px;"> <a href="categories/Community">コミュニティ</a> </li>
			  <li class="project-tip-presenter"> Marik… </li>
			</ul>
			<div class="project-summary ">
			  <div class="progress">
				<div class="gauge" style="width:43%;"> </div>
			  </div>
			  <ul class="project-status">
				<li class="project-status-fund">
				  <p class="project-status-label">集まった金額</p>
				  <p class="project-status-var"> <span>281,000</span>円</p>
				</li>
				<li class="project-status-per">
				  <p class="project-status-label">達成率</p>
				  <p class="project-status-var"><span>43</span>%</p>
				</li>
				<!-- あと〜日 or 終了後 -->
				<li class="project-status-last">
				  <p class="project-status-label">あと</p>
				  <p class="project-status-var"><span>49</span>日</p>
				</li>
			  </ul>
			</div>
		  </div>
		</li>
		<li>
		  <div class="project-img"> <a href="/projects/michiharu_baba"> <img src="https://mg-img.s3.amazonaws.com/projects/michiharu_baba/f513d228-a477-49e2-ac5c-e655ddce83bb" alt="写真家・馬場道浩作品集"> </a></div>
		  <div class="project-box">
			<p class="project-title"> <a href="/projects/michiharu_baba"> 写真家・馬場道浩　独立25周年、自身初の作品集制作プロジェクト </a> </p>
			<ul class="project-tip cf">
			  <li class="project-tip-category" style="width:120px;"> <a href="categories/Photography">写真</a> </li>
			  <li class="project-tip-presenter"> MICHI… </li>
			</ul>
			<div class="project-summary ">
			  <div class="progress">
				<div class="gauge" style="width:77%;"> </div>
			  </div>
			  <ul class="project-status">
				<li class="project-status-fund">
				  <p class="project-status-label">集まった金額</p>
				  <p class="project-status-var"> <span>2,337,507</span>円</p>
				</li>
				<li class="project-status-per">
				  <p class="project-status-label">達成率</p>
				  <p class="project-status-var"><span>77</span>%</p>
				</li>
				<!-- あと〜日 or 終了後 -->
				<li class="project-status-last">
				  <p class="project-status-label">あと</p>
				  <p class="project-status-var"><span>41</span>日</p>
				</li>
			  </ul>
			</div>
		  </div>
		</li>
	  </ul>
	</div>
        <!--/.draftingRight  --> 