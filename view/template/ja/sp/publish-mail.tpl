{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />

</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			{include file="{$lang}/publish-title.tpl"}
			<ul class="tab clearfix mb15">
				<li><a href="{$pagelink_publish}?p_no={$project_info.no}">支援者の一覧</a></li>
				<li><a href="{$pagelink_publish_update}?p_no={$project_info.no}">アップデート</a></li>
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">メール</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
      <div id="fragment-3" class="clearfix">
        <h3>メール</h3>
        <table class="myMailBox">
          <tbody>
            <tr>
              <th scope="col" class="mAdd">送信元</th>
              <th scope="col" class="subject">件名</th>
              <th scope="col" class="create_time">送信日時</th>
              <th scope="col" class="reply">返信</th>
            </tr>
						{foreach name=outer item=con from=$mail_list}
            <tr {if $con.from_user_id!=$project_info.user_no&&$con.status=="0"}style="font-weight:bold;"{/if}>
							{if $con.from_user_id==$project_info.user_no}
								<td class="mAdd">自分<br/>＜{$con.email}＞</td>
							{else}
								<td class="mAdd">{$con.member_name}<br/>＜{$con.email}＞</td>
							{/if}
							<td class="subject">{$con.message_title}</td>
              <td class="create_time">{$con.create_date|date_format:"%Y年%m月%d日%H:%M:%S"}</td>
							{if $con.from_user_id!=$project_info.user_no}
								<td class="replybtn"><a href="{$pagelink_publish_add}?p_no={$project_info.no}&m_id={$con.from_user_id}&ms_id={$con.no}">返信</a></td>
							{else}
								<td class="replybtn">&nbsp;</td>
							{/if}
            </tr>
						{/foreach}
          </tbody>
        </table>
      </div>
      <!--/#fragment-3  -->
      <p class="mypageBack"><a href="{$pagelink_mypage}">&lt;&nbsp;マイページへ戻る</a></p>
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
