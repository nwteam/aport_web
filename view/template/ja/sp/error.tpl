{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/start.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.C_ASSETS}js/lib/colorbox/colorbox.css" />
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/form.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
	{include file="{$lang}/sf-header.tpl"}

<div class="wrapper">
<div class="page-ttl-wrapper">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">A-PORT</h2>
	</div>
</div>

<div class="body">
	<div class="container container-single">
		{if $msgFlg==1}
		<!-- Facebook新規登録時 アカウント登録済みのエラー -->
			<p class="errorTitle">登録済みのアカウントです</p>

			<p class="toTop" style="text-align:center;"><a href="{$pagelink_login}">ログインページへ</a></p>

		{elseif $msgFlg==2}
		<!-- Facebook新規登録時 メールアドレス登録済みのエラー -->
			<p class="errorTitle">既にメールアドレスが登録されていますので、新規登録はできません</p>

			<p class="toTop" style="text-align:center;"><a href="{$pagelink_login}">ログインページへ</a></p>

		{elseif $msgFlg==3}
		<!-- Facebook新規登録時年齢制限エラー -->
			<p class="errorTitle">18歳未満の方のアクセスは固くお断りします</p>

		{elseif $msgFlg==99}
		<!-- エラー -->
			<p class="errorTitle">エラーです</p>

		{else}
		<!-- 該当データなしの場合のエラー -->

			<p class="errorTitle">該当するデータが見つかりません</p>

			<p class="toTop" style="text-align:center;"><a href="{$pagelink_index}">トップページへ</a></p>
		{/if}
	</div>


</div>
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/jquery.rollover.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/colorbox/jquery.colorbox-min.js"></script>
{literal}
<script>
jQuery(document).ready(function($) {
  $('ol.pc-blc a img').rollover();
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $(".popup").colorbox({
    iframe:true,
    width:"740px",
    height:"490px"
  });
});
</script>
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
