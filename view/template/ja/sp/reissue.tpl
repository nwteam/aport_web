{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/signin.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<div id="wrapper">
	<div id="signIN">
		<div class="userEntry">
			<h2><strong>パスワード再発行</strong></h2>
			{if $finish_flg}
			<p class="notice">ご入力のアドレスにメールを送信いたしました。</p>
			{else}
			<p class="notice">ご登録いただいているメールアドレスを入力の上、<br>送信ボタンを押してください。</p>
			<form method="post">
				<table>
					<tr>
						<th>メールアドレス<span>*</span></th>
						<td>
						<input type="text" name="email" id="mail" value="{$input_data.email}"/>
						{if $err_msg.email}
							<p class="error" style="color:red;">※{$err_msg.email}</p>
						{/if}
						</td>
					</tr>
				</table>
				<input class="btnSubmit" type="submit" value="送信" id="sendForm" />
			</form>
			{/if}
		</div>
	</div>
  <!--/#contents-->
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
