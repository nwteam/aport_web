<div id="myPageSide">
	<dl>
	<dt>サポーター</dt>
		<dd><a href="{$pagelink_mypage_contribution}">応援したプロジェクト</a></dd>
		<dd><a href="{$pagelink_mypage_myprojects}">投稿したプロジェクト</a></dd>
		<dd><a href="{$pagelink_mypage_contribution_comment}">応援コメント</a></dd>
		<dd><a href="{$pagelink_mypage_mailbox}">受信メール</a></dd>
		<dd><a href="{$pagelink_mypage_sendmailbox}">送信済みメール</a></dd>
	</dl>
	<dl>
		<dt>アカウント情報</dt>
		<dd><a href="{$pagelink_mypage_profile_edit}">プロフィール編集</a></dd>
		<dd><a href="{$pagelink_mypage_mail_edit}">メールアドレス変更</a></dd>
		<dd><a href="{$pagelink_mypage_pwd_edit}">パスワード変更</a></dd>
		<!--<dd><a href="{$pagelink_mypage_mail_subscriptions}">メール通知設定</a></dd>-->
		<dd><a href="{$pagelink_mypage_address_edit}">配送先情報</a></dd>
	</dl>
</div>