{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/start.css">
<link rel="stylesheet" type="text/css" href="{$smarty.const.C_ASSETS}js/lib/colorbox/colorbox.css" />
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper">

{include file="{$lang}/sf-header.tpl"}


<div class="main-wrapper list-main-wrapper">
	<div class="main-inner">
		<div class="container-wide">
			<div class="s60" id="ship1"><img src="{$smarty.const.C_ASSETS}images/pc/ship_s.gif"></div>
			<div class="s60" id="ship2"><img src="{$smarty.const.C_ASSETS}images/pc/ship_M.gif"></div>
			<div class="s60" id="ship3"><img src="{$smarty.const.C_ASSETS}images/pc/ship_L.gif"></div>
			<div class="container-inner-set">
				<p><img class="pc" src="{$smarty.const.C_ASSETS}images/pc/start_lead.gif" alt="プロジェクトをはじめましょう。"></p>
				<p><a href="{$pagelink_reg_project}" target="_self"><img class="btn-start-entry" src="{$smarty.const.C_ASSETS}images/common/start_btn_enrty.png" alt="プロジェクトの申し込みをする"></a></p>
			</div>
		</div>
	</div>
</div>
<!-- スマートフォン表示専用 -->
<div class="page-ttl-wrapper list-main-wrapper-sp">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">プロジェクトをはじめましょう</h2>
		<p class="sub-text">インターネットを通じてプロジェクトに共感した支援者から必要な資金を集めることが出来ます。まずは、気軽に相談してみてください。</p>
		<p class="btn"><a href="{$pagelink_reg_project}" target="_self"><img src="{$smarty.const.C_ASSETS}images/common/start_btn_enrty.png" alt="プロジェクトの申し込みをする"></a></p>
    </div>
</div>
<!-- /スマートフォン表示専用 -->
  <div class="body">
    <div class="intro">
      <h3>A-portなら、プロジェクトを成功に導く強みがあります。</h3>
      <ul>
        <li><img class="circle pc-blc" src="{$smarty.const.C_ASSETS}images/start/circle01.gif" alt="発信力 が違います"><img class="circle sp-blc" src="{$smarty.const.C_ASSETS}images/start/circle01_sp.gif" alt="発信力 が違います"><span><img class="rope" src="{$smarty.const.C_ASSETS}images/start/rope.png" alt="">朝日新聞社が運営するサイトだから、あなたのプロジェクトを、いくつかの関連媒体を通じて広く発信していきます。さらに他媒体の掲載も目指し、プレスリリースを発信します。</span></li>
        <li><img class="circle pc-blc" src="{$smarty.const.C_ASSETS}images/start/circle02.gif" alt="リーチ力 が違います"><img class="circle sp-blc" src="{$smarty.const.C_ASSETS}images/start/circle02_sp.gif" alt="リーチ力 が違います"><span><img class="rope" src="{$smarty.const.C_ASSETS}images/start/rope.png" alt="">700万部の新聞読者のみなさまをはじめ、 幅広い世代にアプローチしていきます。</span></li>
        <li><img class="circle pc-blc" src="{$smarty.const.C_ASSETS}images/start/circle03.gif" alt="編集力 が違います"><img class="circle sp-blc" src="{$smarty.const.C_ASSETS}images/start/circle03_sp.gif" alt="編集力 が違います"><span>読者の心をぐっとつかむ見出しの立て方や、内容が明確に伝わるサマリー（前文）など、みなさまのプロジェクトの魅力を引き出すお手伝いをいたします。</span></li>
      </ul>
    </div>
    <!--/intro-->

    <div class="acwrap01">
      <h3>どんなアイデアがプロジェクトになるの？</h3>
      <p class="lead">あなただったらどんなプロジェクトができるか、想像を膨らませてみましょう。</p>
      <dl class="acMenu">
        <dt>続きを見る</dt>
        <dd>
          <h3>どんなアイデアもプロジェクトに！</h3>
          <p>アイデアを知った人が、「一緒にその夢をかなえたい！」「達成したら自分も利用したい！」と思えるようなものなら、<br>
            何でもプロジェクトになり得ます。ジャンルや規模（集めたい金額の多寡）も問いません。<br>
            これまで資金面が不安で実施できなかったアイデアや、一人では実行する勇気が出なかったアイデアはありませんか？</p>
          <p class="btn"><a href="{$pagelink_reg_project}" target="_self"><img src="{$smarty.const.C_ASSETS}images/start/btn_apply.gif" alt="プロジェクトの申し込みをする"></a><span>※プロジェクトのご相談も受け付けています。</span></p>
          <div class="top-how"><img src="{$smarty.const.C_ASSETS}images/start/howto_top.gif" alt=""></div>
          <div class="howto">
            <h3>たとえば、このような活用方法で！</h3>
            <ul>
              <li><img src="{$smarty.const.C_ASSETS}images/start/start-exp-img01.jpg" alt="image"><span class="txt18">アート、スポーツで！</span> <span>個展、本の出版、映画製作、コンサート、CD 制作、オリジナルプロダクツ制作など創作活動費を集めることが出来ます。また、スポーツ団体・選手の活動に必要な費用、施設の修復やイベント開催のための資金調達にもご利用いただけます。活動自体のPR や、ファンの獲得にもつなげることができるかもしれません。</span></li>
              <li><img src="{$smarty.const.C_ASSETS}images/start/start-exp-img02.jpg" alt="image"><span class="txt18">地元の伝統工芸、<br>
                技術を全国に発信！</span><span>日本全国各地の素晴らしい伝統工芸、地場産業や技術、アイデアを生かしたプロジェクトも大歓迎です。新商品開発や、伝統を後世に残していくための企画などを実施することで、資金調達だけでなく、多くの人に知っていただくきっかけにもなります。企業や団体での活用も！</span></li>
              <li><img src="{$smarty.const.C_ASSETS}images/start/start-exp-img03.jpg" alt="image"><span class="txt18">生活者との<br>
                新しい接点として</span><span>新商品の開発費の調達はもちろん、先行予約販売としてもご利用いただけます。その際、支援者との双方向のコミュニケーションをとることができます。マーケティングや、CSR 活動の場としてもご活用いただけます。</span></li>
              <li><img src="{$smarty.const.C_ASSETS}images/start/start-exp-img04.jpg" alt="image"><span class="txt18">地域の<br>
                魅力発信の場に</span><span>「街づくり」「観光活性」「環境保全」「文化伝承」など地域活性化につながる活動にもお使いいただけます。地域の課題に改めてフォーカスするきっかけにもなるかもしれません。</span></li>
            </ul>
          </div>
          <!--/howto-->
        </dd>
      </dl>
    </div>
    <div class="step-wrap">
      <h3><img src="{$smarty.const.C_ASSETS}images/start/step_tit.png" alt="初めてでも安心、5つのステップ"></h3>
      <div class="step-inner">
        <!--PC用表示-->
        <ol class="pc-blc">
          <li><img class="step01" src="{$smarty.const.C_ASSETS}images/start/step01.gif" alt="(1)プロジェクトを申し込む
          「プロジェクトを申し込む」ボタンを 押すと出てくる申請フォームに、 プロジェクトの内容をご記入下さい。"></li>
          <li><img class="step02" src="{$smarty.const.C_ASSETS}images/start/step02.gif" alt="(2)アイディアを練る
          審査通過後、プロジェクトページの 作成に入ります。 スタッフも相談に応じますので、 ブラッシュアップしていきましょう。">
          <!--point1-->
           <a class="popup" href="{$smarty.const.C_ASSETS}popup/step2-1.html"><img class="point01" src="{$smarty.const.C_ASSETS}images/start/point01.png" alt="「欲しい！」「支援したい！」と 思わせる魅力的なリターンを 設定しよう"></a>
          <!--point2-->
         <a class="popup" href="{$smarty.const.C_ASSETS}popup/step2-2.html"><img class="point02" src="{$smarty.const.C_ASSETS}images/start/point02.png" alt="文章、写真、動画で あなたの想いを伝えよう"></a>
          <!--point3-->
         <a class="popup" href="{$smarty.const.C_ASSETS}popup/step2-3.html"><img class="point03" src="{$smarty.const.C_ASSETS}images/start/point03.png" alt="目標金額は慎重に 設定しよう"></a></li>
          <li><img class="step03" src="{$smarty.const.C_ASSETS}images/start/step03.gif" alt="(3)資金調達スタート＆PR
          資金調達スタートです。 目標金額達成に向けて、 プロジェクトを積極的に発信し、 多くの人を巻き込みましょう。">
          <!--point4-->
          <a class="popup" href="{$smarty.const.C_ASSETS}popup/step3-1.html"><img class="point04" src="{$smarty.const.C_ASSETS}images/start/point04.png" alt="プロジェクトを公開したら、SNSをフル活用してPRしよう"></a>
          <!--point5-->
          <a class="popup" href="{$smarty.const.C_ASSETS}popup/step3-3.html"><img class="point05" src="{$smarty.const.C_ASSETS}images/start/point05.png" alt="活動報告は、A-portでも定期的に発信しよう"></a>
          <!--point6-->
           <a class="popup" href="{$smarty.const.C_ASSETS}popup/step3-2.html"><img class="point06" src="{$smarty.const.C_ASSETS}images/start/point06.png" alt="身近な人を 巻き込むことから はじめよう"></a>
          </li>
          <li><img class="step04" src="{$smarty.const.C_ASSETS}images/start/step04.gif" alt="(4)募集終了
          プロジェクトが成立したら、 まずは支援者にお礼メッセージを 送りましょう。 そして、実行に移していきましょう。"></li>
          <li><img class="step05" src="{$smarty.const.C_ASSETS}images/start/step05.gif" alt="(5)実行＆リターン送付
          プロジェクト実行の経過を随時、 支援者に報告しましょう。 約束のリターンの送付が終わったら、 プロジェクト終了です。"></li>
        </ol>
        <!--PCここまで-->
        <!--SP用表示-->
        <div class="sp-blc">
          <p><img class="step01" src="{$smarty.const.C_ASSETS}images/start/step01_sp.gif" alt="(1)プロジェクトを申し込む
          「プロジェクトを申し込む」ボタンを 押すと出てくる申請フォームに、 プロジェクトの内容をご記入下さい。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg01.png" alt=""></p>
          <p><img class="step02" src="{$smarty.const.C_ASSETS}images/start/step02_sp.gif" alt="(2)アイディアを練る
          審査通過後、プロジェクトページの 作成に入ります。 スタッフも相談に応じますので、 ブラッシュアップしていきましょう。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg02.png" alt=""></p>
          <dl class="acMenu">
            <dt> <img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">「欲しい！」「支援したい！」と思わせる<br>
              魅力的なリターンを設定しよう</dt>
            <dd>プロジェクトに共感してもらうことも大切ですが、「これが欲しい」と思っていただける<span class="ylw">魅力的なリターンの設定</span>も必要不可欠です。リターンは、「モノ」以外にも、関係者限定イベントの参加や、支援者の名前を公式サイトなどに掲載するといった無形のモノもあります。<span class="ylw">特別な魅力があるものほど支援者の心をつかむ</span>ことができます。金額に応じた複数種類の魅力的なリターンをつくってみましょう。<br />
				事務局も随時相談に乗ります。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg03.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">文章、写真、動画で
              あなたの想いを伝えよう</dt>
            <dd>クラウドファンディングは、<span class="ylw">いかに多くの方々の共感を得られることが出来るか</span>が、プロジェクトの成功を左右します。あなたの想い、プロジェクトの概要や背景について、正確にわかりやすく文章で伝えましょう。そして、さらに「素敵」と思っていただけるよう、<span class="ylw">写真、動画を丁寧に織り込んでいくことも大切です。</span><br />
				見出しや文章校正などは事務局でもサポートいたします。写真・動画撮影でお困りの場合は、専門業者などご紹介できますのでご相談ください。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg04.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">目標金額は慎重に設定しよう</dt>
            <dd>プロジェクトを実行するために必要な経費を丁寧に計算してみましょう。<br />
				見積もりをとることはもちろんのこと、リターンの発送費、場合によっては対応する人の「人件費」が必要になります。<br />
				<span class="ylw">支援者の理解が得られる目標金額</span>を設定してください。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg05.png" alt=""></p>
          <p><img class="step03" src="{$smarty.const.C_ASSETS}images/start/step03_sp.gif" alt="(3)資金調達スタート＆PR
          資金調達スタートです。 目標金額達成に向けて、 プロジェクトを積極的に発信し、 多くの人を巻き込みましょう。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg06.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">プロジェクトを公開したら、
              SNSをフル活用してPRしよう</dt>
            <dd>SNSを上手に活用し、<span class="ylw">積極的にあなたのプロジェクトをPRしましょう。</span>あなたの熱意、魅力的なリターン、プロジェクトの進捗状況などを定期的に発信することを心がけましょう。投稿へのコメントや支援者へのお礼も忘れずに・・・。<br />
				さらに、<span class="ylw">仲間と一緒にイベントなどを開催して盛り上げていくことも有効な手段</span>です。私たちも、あなたのプロジェクトを広めるために最大限支援させて頂きます。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg07.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">身近な人を巻き込むことから
              はじめよう</dt>
            <dd>まずは、あなたのプロジェクトの仲間に<span class="ylw">身近な人・・家族、友人、知人を巻き込むことが大切です。</span>家族や友人などの共感が得られないプロジェクトが、まったくの他人から賛同を得られることは難しいかもしれません。<br />
				プロジェクトへの想いを身近な人に地道に伝え、<span class="ylw">共感の輪をどんどん広げていく</span>ことを心がけましょう。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg08.png" alt=""></p>
          <dl class="acMenu">
            <dt><img class="point-sp" src="{$smarty.const.C_ASSETS}images/start/point.png" alt="point">活動報告は、A-portでも
              定期的に発信しよう</dt>
            <dd>CFサイトでの<span class="ylw">活動報告も定期的に実施して、支援者とのつながりも大事にしましょう。</span>支援者は、あなたの報告を受けて、さらにあなたのプロジェクトを発信し、プロジェクト終了後も、あなたのサポーターであり続けてくれるかもしれません。</dd>
          </dl>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg09.png" alt=""></p>
          <p><img class="step04" src="{$smarty.const.C_ASSETS}images/start/step04_sp.gif" alt="(4)募集終了
          プロジェクトが成立したら、 まずは支援者にお礼メッセージを 送りましょう。 そして、実行に移していきましょう。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg10.png" alt=""></p>
          <p><img class="step05" src="{$smarty.const.C_ASSETS}images/start/step05_sp.gif" alt="(5)実行＆リターン送付
          プロジェクト実行の経過を随時、 支援者に報告しましょう。 約束のリターンの送付が終わったら、 プロジェクト終了です。"></p>
          <p class="bg-img"><img src="{$smarty.const.C_ASSETS}images/start/ship_bg11.png" alt=""></p>
        </div>
        <!--SPここまで-->
      </div>
      <!--/step-inner-->
      <div class="mind">
        <h3>起案者の心構え</h3>
        <ul>
          <li>「欲しい！」「支援したい！」と思わせる魅力的なリターンを設定しよう</li>
          <li>文章、写真、動画であなたの想いを伝えよう</li>
          <li>目標金額は慎重に設定しよう</li>
          <li>プロジェクトを公開したら、SNSをフル活用しよう</li>
          <li>身近な人を巻き込むことからはじめよう</li>
          <li>活動報告は、A-portでも定期的に発信しよう</li>
        </ul>
        <p>※プロジェクトの成功率を高める秘訣についての資料は、実際にプロジェクトを立ち上げることが決まった方々に別途、差し上げております。</p>
      </div>
      <!--/mind-->
    </div>
    <!--/step-wrap-->

    <p class="btn pc-blc"><a href="{$pagelink_reg_project}" target="_self"><img src="{$smarty.const.C_ASSETS}images/start/btn_apply.gif" alt="プロジェクトの申し込みをする"></a></p>
    <p class="btn sp-blc"><a href="{$pagelink_reg_project}" target="_self"><img src="{$smarty.const.C_ASSETS}images/start/btn_apply_sp.gif" alt="プロジェクトの申し込みをする"></a></p>

    <div class="choice" id="project_type">
      <h3>選べる２つの資金調達法</h3>
      <p>実行者（起案者）は、２つの資金調達方法から選択することができます。</p>
      <div class="tbl-wrap">

        <div class="typebox">
          <p><img class="icon-a" src="{$smarty.const.C_ASSETS}images/start/a.png" alt="A"></p>
          <table>
           <tr>
             <th><h4>達成時実行型（All or Nothing型）</h4></th>
           </tr>
           <tr>
             <td class="funding-type-txt">
               <ul>
                 <li><span>■</span>All or Nothing方式です。目標金額を達成した場合のみ、<br>
                   調達した資金を手にすることができます。</li>
                 <li><span>■</span>目標金額に達成しなかった場合、プロジェクトは不成立となり資金調達はキャンセルされ、特典の実行もされません。</li>
               </ul>
             </td>
           </tr>
           <tr>
             <td><span class="yel">ローンチ資金を集めるのに最適</span></td>
           </tr>
         </table>
       </div>

        <div class="typebox">
         <p><img class="icon-b" src="{$smarty.const.C_ASSETS}images/start/b.png" alt="B"></p>
         <table class="last">
           <tr>
             <th><h4>実行確約型</h4></th>
           </tr>
             <tr>
              <td class="funding-type-txt">
                <ul>
                  <li><span>■</span>目標金額に達成しない場合でも、調達した資金を手にすることが出来ます。</li>
                  <li><span>■</span>実行者は掲載時にプロジェクトの実施を確約する必要があります。</li>
                </ul>
              </td>
            </tr>
            <tr>
              <td><span class="yel">実行が決まっているプロジェクトが対象</span></td>
            </tr>
          </table>
        </div>

        <img class="select-pc pc-blc" src="{$smarty.const.C_ASSETS}images/start/select.png" alt="選べる">
        <img class="select-sp sp-blc" src="{$smarty.const.C_ASSETS}images/start/select_sp.png" alt="選べる">

      </div>
      <!--/tbl_wrap-->
    </div>
    <!--/choice-->

    <div class="faq">
      <h3>ご不明な点がある方は、こちらのよくあるご質問をご覧ください。</h3>
      <table>
        <tr>
          <td><dl class="acMenu">
              <dt><span class="640r1">プロジェクトを企画するにはどうしたら始められますか？</span></dt>
              <dd>「プロジェクトをはじめる」ページから”プロジェクトの申し込みをする”ボタンを押して、お申込みシートに必要事項をご記入いただき、お申し込みください。プロジェクトの申請を頂いてから、７営業日を目安にお返事させて頂きます。</dd>
            </dl></td>
          <td><dl class="acMenu">
              <dt><span>達成時実行型（All or Nothing型）とはなんですか？</span></dt>
              <dd>「All or Nothing方式」です。目標金額を達成した場合のみ、調達した資金を手にすることができます。ローンチ資金を集めるのに適しています。</dd>
            </dl></td>
        </tr>
        <tr>
          <td><dl class="acMenu">
              <dt>実行確約型とはなんですか？</dt>
              <dd>目標金額に達成しない場合でも、調達した資金を手にすることが出来ます。ただし、実行者（起案者）は掲載時にプロジェクトの実施を確約する必要があります。既に実行が決まっていて、スケールアップ等のために必要な資金を募りたいプロジェクトや、過去に実施実績があるプロジェクトなどに適した方式です。</dd>
            </dl></td>
          <td><dl class="acMenu">
              <dt>審査にはどのくらい時間がかかりますか？</dt>
              <dd>プロジェクトの申請を頂いてから、７営業日を目安にお返事させて頂きます。</dd>
            </dl></td>
        </tr>
        <tr>
          <td><dl class="acMenu">
              <dt>リターンとはなんでしょうか？</dt>
              <dd>プロジェクトを支援し、資金提供を誓約してくれたサポーター（支援者）に対してのお返しです。リターンは資金額によって、複数種類用意されています。</dd>
            </dl></td>
          <td><dl class="acMenu">
              <dt>まだ未成年なのですが、支援や企画は可能ですか？</dt>
              <dd>未成年者が支援する場合、本サービスの利用について、親権者から事前の同意を得て下さい。企画する場合、親権者が責任をもって監督することに同意する必要があり、別途書面の提出をしていただきます。</dd>
            </dl></td>
        </tr>
        <tr>
          <td><dl class="acMenu">
              <dt>法人でのプロジェクト企画も可能ですか？</dt>
              <dd>もちろん可能です。</dd>
            </dl></td>
          <td><dl class="acMenu">
              <dt>A-portの手数料はいくらですか？</dt>
              <dd>手数料は、クレジットカード決済手数料を含めて20％（消費税別）を頂いております。ただし、実行確約型で目標金額に達しなかった場合、手数料は25％（消費税別）となります。</dd>
            </dl></td>
        </tr>
        <tr>
          <td><dl class="acMenu">
              <dt><span>どのカテゴリーにも当てはまらないようなプロジェクトは、採用されないのでしょうか？</span></dt>
              <dd>現在のカテゴリーに当てはまらなくても、掲載の検討は可能です。まずはご相談ください。</dd>
            </dl></td>
          <td><dl class="acMenu">
              <dt>プロジェクトが不成立になった場合のプロジェクト支援金やリターンはどのようになりますか？</dt>
              <dd>・実行確約型は、実行確約型ですので、決済が行われリターンが提供されます。<br />・達成時実行型（All or Nothing型）は、決済が行われず、リターンも提供されません。</dd>
            </dl></td>
        </tr>
        <tr>
          <td><dl class="acMenu">
              <dt>起案したプロジェクトを削除する事は出来ますか？</dt>
              <dd>実行者が自らの意思でプロジェクトを削除することは原則できません。</dd>
            </dl></td>
          <td><dl class="acMenu">
              <dt>調達した資金は課税対象となりますか？</dt>
              <dd>課税されることがありますので、課税の有無や課税額などについては、自ら専門家にお尋ねいただくようお願いいたします。</dd>
            </dl></td>
        </tr>
      </table>
      <p><a href="{$pagelink_faq}"><span>よくある質問をもっと見る</span></a></p>
    </div>
    <!--/faq-->

    <div class="bottom">
      <p class="pc-blc"><a href="{$pagelink_reg_project}" target="_self"><img src="{$smarty.const.C_ASSETS}images/start/btn_apply2.gif" alt="プロジェクトの申し込みをする"></a></p>
      <p class="sp-blc"><a href="{$pagelink_reg_project}" target="_self"><img src="{$smarty.const.C_ASSETS}images/start/btn_apply_sp.gif" alt="プロジェクトの申し込みをする"></a></p>
    </div>

  </div><!--body-->
</div>

{include file="{$lang}/sf-footer.tpl"}
</div>
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/accordion.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/jquery.rollover.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/colorbox/jquery.colorbox-min.js"></script>
{literal}
<script>
jQuery(document).ready(function($) {
  $('ol.pc-blc a img').rollover();

});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $(".popup").colorbox({
    iframe:true,
    width:"740px",
    height:"490px"
  });
});
</script>
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>