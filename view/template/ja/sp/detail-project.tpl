{include file="{$lang}/sf-head.tpl"}

<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/blog.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
<meta property="og:type" content="website">
<meta property="og:site_name" content="A-port｜朝日新聞社のクラウドファンディングサイト">
<meta property="og:url" content="{$smarty.const.ROOT_URL}/projects/{$item.hope_url}">
<meta property="og:title" content="{$item.public_title}| 【A-port】">
<meta property="og:description" content="{$item.public_title}">
<meta property="og:image" content="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}">
<script>
{literal}
$(function() {
    $(".modalHead > .closeBtn").click(function() {
        $(".modalSection").addClass("off")
    }),
    $(".append_modal").click(function() {
        $(".modalSection").removeClass("off"),
        console.log("test"),
        console.log("test")
    }),
    $(".modalCover").click(function() {
        $(".modalSection").addClass("off")
    }),
    $(".disabled").click(function() {
        return ! 1
    })
});
{/literal}
</script>
</head>
<body>
<div id="wrapper" class="clearfix">
{include file="{$lang}/sf-header.tpl"}

<div class="page-ttl-wrapper" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">{$item.public_title}</h2>
		<ul class="page-ttl-info cf">
			<li class="page-ttl-info-presenter"><a href="#">{$actress.public_name}</a></li>
			<li class="page-ttl-info-category"><a href="#">{$item.category_name}</a></li>
		</ul>
	</div>
</div>
{if $item.movie_type=="1"}
<script>
{literal}
		// 2. This code loads the IFrame Player API code asynchronously.
		var tag = document.createElement('script');
		tag.src = "//www.youtube.com/player_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		// 3. This function creates an <iframe> (and YouTube player)
		//    after the API code downloads.
		var player;
		function onYouTubeIframeAPIReady() {
				player = new YT.Player('moviePlayer', {
{/literal}
						videoId: '{$youtube_id}',
{literal}
						events: {
								'onReady': onPlayerReady
						}
				});
		}
		// 4. The API will call this function when the video player is ready.
		function onPlayerReady(event) {
				$('.project-cover').click(function(){
						$('.project-cover').hide();
						event.target.playVideo();
				});
		}
{/literal}
</script>
{/if}
{if $item.movie_type=="2"}
<script>
{literal}
	$(function() {
			var player = $('iframe');
			var playerOrigin = '*';
			var status = $('.status');

			// Listen for messages from the player
			if (window.addEventListener) {
					window.addEventListener('message', onMessageReceived, false);
			}
			else {
					window.attachEvent('onmessage', onMessageReceived, false);
			}

			// Handle messages received from the player
			function onMessageReceived(event) {
					// Handle messages from the vimeo player only
					if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
							return false;
					}
					if (playerOrigin === '*') {
							playerOrigin = event.origin;
					}
					var data = JSON.parse(event.data);
					
					switch (data.event) {
							case 'ready':
									onReady();
									break;
							case 'playProgress':
									onPlayProgress(data.data);
									break;
							case 'pause':
									onPause();
									break;
							case 'finish':
									onFinish();
									break;
					}
			}
			// Call the API when a button is pressed
			$('.project-cover').on('click', function() {
					$('.project-cover').hide();
					//post($(this).text().toLowerCase());
					post('play');
			});
			// Helper function for sending a message to the player
			function post(action, value) {
					var data = {
						method: action
					};
					if (value) {
							data.value = value;
					}
					var message = JSON.stringify(data);
					player[0].contentWindow.postMessage(data, playerOrigin);
			}
			function onReady() {
					status.text('ready');
					post('addEventListener', 'pause');
					post('addEventListener', 'finish');
					post('addEventListener', 'playProgress');
			}
			function onPause() {
					status.text('paused');
			}
			function onFinish() {
					status.text('finished');
			}
			function onPlayProgress(data) {
					status.text(data.seconds + 's played');
			}
	});
{/literal}
</script>
{/if}
<div class="body">
	<div class="container container-offset cf">
		<div class="col-main">
			<div class="col-inner">
				<div class="local-nav-wrapper">
					<ul class="local-nav">
						<li class="local-nav-home count-off active">
							<a href="{$pagelink_detail_project}{$item.hope_url}">
								<p>ホーム</p>
							</a>
						</li>
						<li class="local-nav-update count-on">
							<a href="{$pagelink_detail_update}{$item.hope_url}/1">
								<p>アップデート</p>
								<p class="count"><span>{$total_count_update}</span></p>
							</a>
						</li>
						<li class="local-nav-collector count-on">
							<a href="{$pagelink_detail_supporter}{$item.hope_url}/1">
								<p>支援者</p>
								<p class="count"><span>{$total_count}</span></p>
							</a>
						</li>
					</ul>
				</div>
				<div class="project-thum">
					
					<div class="project-cover">
						{if $item.movie_type!="0"}
						<p class="btn-play">再生する</p>
						{/if}
						<img class="cover-image" src="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}" alt="プロジェクトイメージ">
					</div>
					{if $item.movie_type=="1"}
					<div class="movie-player">
						<div id="moviePlayer"></div>
					</div>
					{/if}
					{if $item.movie_type=="2"}
					<div class="movie-player">
						<iframe src="//player.vimeo.com/video/{$vimemo_id}?api=1"  width="585" height="418" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
					</div>
					{/if}
					<div class="project-cover-base">
						<img class="cover-image" src="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}" alt="プロジェクトイメージ">
					</div>
				</div>

				<div class="sns-btn-set">
					<ul class="clearfix">
						<li class="fblike"><div class="fb-like" expr:data-href="data:post.url" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
						<li class="tweet">
							<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
							{literal}
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
							{/literal}
            </li>
						<li class="hatena"><a href="http://b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="simple" title="はてなブックマークに追加"><img src="https://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="はてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="https://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async></script></li>
						<li class="append_modal blog"><a>ブログに埋め込む</a></li>
					</ul>
				</div>

				<div class="project-description">
					<h2 class="project-description-ttl">このプロジェクトについて</h2>
					<div class="project-description-inner jsc-project-description">
						{$item.project_text|nl2br nofilter}
					</div>
				</div>
				<div class="project-detail">
					<div class="btn-project-detail-open jsc-btn-project-detail-open">
						<a href="javascript:void(0);">続きを見る</a>
					</div>
					<div class="btn-project-detail-close jsc-btn-project-detail-close jsc-dn">
						<a href="javascript:void(0);">閉じる</a>
					</div>
				</div>
					{if $item.in_invest_flg}
						{if $current_user.user_no}
							{if $current_user.user_no!=$item.user_no}
								<div class="project-invest-box">
									<div class="btn-project-invest">
										<a href="{$pagelink_collection_ticket}?no={$item.no}">プロジェクトを応援する</a>
									</div>
								</div>
							{/if}
						{else}
								<div class="project-invest-box">
									<div class="btn-project-invest">
										<a href="{$pagelink_login}">応援をするにはログインが必要です</a>
									</div>
								</div>
						{/if}
					{else}
								<div class="project-invest-box">
									<div class="btn-project-invest">
										<a href="#">応援期間終了</a>
									</div>
								</div>
					{/if}
					<div class="project-social-bottom-wrapper cf">
						<ul class="project-social-bottom cf">
							<li class="fb"><a href="http://www.facebook.com/share.php?u={$smarty.const.ROOT_URL}/projects/{$item.hope_url}" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">facebookでシェア</a></li>
							<li class="tw"><a href="http://twitter.com/share?url={$smarty.const.ROOT_URL}/projects/{$item.hope_url}&text=～{$item.public_title}～ / 【A-port】」" target="_blank">Twitterでシェア</a></li>
							<li class="append_modal blog"><a>ブログに埋め込む</a></li>
						</ul>
					</div>
			</div>
		</div><!--/col-main-->
		<div class="modalSection off">
			<!-- <div class="modalSection off"> で非表示
			-->
			<div class="modalCover"></div>
			<div class="modalArea">
				<div class="modalPosition">
					<div class="modalBox">
						<div class="modalHead">
							<h4 class="modalTitle">プロジェクトをブログやサイトで紹介する</h4>
							<span class="closeBtn">×</span>
						</div>
						<div class="modalInner">
							<div class="pjtItemCode">
								<p>テキストエリアのコードをあなたのブログやサイトのHTMLに張り付けると、右にあるようなプロジェクトウィジェットが表示されます。</p>
								<textarea readonly="readonly">&lt;iframe src='{$smarty.const.ROOT_URL}projects/{$item.hope_url}/widget' frameborder="0" scrolling="no" width="313" height="403"&gt;&lt;/iframe&gt;</textarea>
							</div>
							<div class="pjtItemPreview">
								<iframe src="{$smarty.const.ROOT_URL}projects/{$item.hope_url}/widget" frameborder="0" scrolling="no" width="313" height="401"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{include file="{$lang}/detail-sub.tpl"}

		<ol class="breadcrumb cf">
			<li><a href="#">サイトトップ</a></li>
			<li><a href="#">{$item.category_name}</a></li>
			<li><a href="#">{$item.public_title}</a></li>
		</ol>
	</div><!--/container-->
</div><!--/body-->
<!-- スマートフォン表示専用 -->
<div class="project-social-sp-wrapper">
	<h3 class="project-social-sp-ttl">このプロジェクトをシェアする</h3>
	<ul class="project-social-sp cf">
		<li class="fb"><a href="http://www.facebook.com/share.php?u={$smarty.const.ROOT_URL}/projects/{$item.hope_url}" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">facebookでシェア</a></li>
		<li class="tw"><a href="http://twitter.com/share?url={$smarty.const.ROOT_URL}/projects/{$item.hope_url}&text=～{$item.public_title}～ / 【A-port】」" target="_blank">Twitterでシェア</a></li>
		<li class="line"><a href="http://line.me/R/msg/text/?～{$item.public_title}～ / 【A-port】{$smarty.const.ROOT_URL}/projects/{$item.hope_url}">LINEで送る</a></li>
	</ul>
</div>
<!-- スマートフォン表示専用 -->
{include file="{$lang}/sf-footer.tpl"}
</div>
<!--/#wrapper-->
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}

</body>
</html>
