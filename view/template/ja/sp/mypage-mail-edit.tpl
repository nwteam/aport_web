{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<div id="myMailEdit">
	<h2>メールアドレスの変更</h2>
	<form accept-charset="UTF-8" action="" method="post">
	{if $err_msg.top}
		<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
	{/if}
	{if $err_msg.new_email}
		<p class="error" style="color:red;">※新しいメールアドレス：{$err_msg.new_email}</p>
	{/if}
	{if $err_msg.password}
		<p class="error" style="color:red;">※パスワード：{$err_msg.password}</p>
	{/if}
	{if $finish_msg}
		<p class="update">{$finish_msg}</p>
	{/if}
	<dl>
	  <dt>現在のメールアドレス</dt>
	  <dd>{$input_data.email}</dd>
	  <input type="hidden" name="org_email" id="org_email" value="{$input_data.org_email}" />
	  <dt>新しいメールアドレス</dt>
	  <dd>
	    <input autocomplete="off" id="new_email" name="new_email" type="email" value="{$input_data.new_email}" />
	  </dd>
	  <dt>パスワード</dt>
	  <dd>
	    <input id="password" name="password" type="password" />
	  </dd>
	</dl>
	<p class="actions">
	  <input data-disable-with="変更中…" name="mail_update" type="submit" value="変更する" />
	</p>
	</form>
	</div>
	<!-- #myMailEdit --> 
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
