{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
	<ul class="project-list-index">
	{foreach from=$project_data_list item="item" name="loop"}
		<li>
		<div class="project-img"><a href="{$pagelink_drafting1}?p_no={$item.no}" ><img src="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}" alt="{$item.public_title}" width=260 height=186></a></div>
		<div class="project-box">
			<p class="project-title"><a href="{$pagelink_drafting1}?p_no={$item.no}" >{$item.public_title}</a></p>
			<ul class="project-tip cf">
				<li class="project-tip-category" style="width:120px;"><a href="{$pagelink_list_project}?c_no={$item.category_no}">{$item.category_name}</a></li>
				<li class="project-tip-presenter">{$item.member_name}</li>
			</ul>
			{if $item.status=='1'&&$item.percent<100&&$item.diff_in_days!='-'}
				<div class="project-summary">
					<div class="progress">
						<div class="gauge" style="width:100%;">
							<div class="gauge2" {if $item.percent<100}style="width:{$item.percent}%;"{else}style="width:100%;"{/if}></div>
						</div>
					</div>
					<ul class="project-status">
						<li class="project-status-fund">
							<p class="project-status-label" style="width:130px;">集まった金額</p>
							<p class="project-status-var"><span>{$item.now_summary|number_format}</span>円</p>
						</li>
						<li class="project-status-per">
							<p class="project-status-label">達成率</p>
							<p class="project-status-var"><span>{$item.percent}</span>%</p>
						</li>
						<li class="project-status-last">
							<p class="project-status-label">あと</p>
							<p class="project-status-var">{$item.diff_in_days}日</p>
						</li>
					</ul>
				</div>
			{elseif $item.status=='3'||$item.percent>=100||$item.diff_in_days=='-'}
					<div class="project-summary funded">
						<div class="progress"> FUNDED! </div>
						<ul class="project-status">
							<li class="project-status-fund">
								<p class="project-status-label" style="width:120px;">集まった金額</p>
								<p class="project-status-var"><span>{$item.now_summary|number_format}</span>円</p>
							</li>
							<li class="project-status-per">
								<p class="project-status-label">達成率</p>
								<p class="project-status-var"><span>{$item.percent}</span>%</p>
							</li>
							<li class="project-status-last">
								<p class="project-status-label">終了</p>
								<p class="project-status-var">{$item.invest_limit|date_format:"%y年%m月%d日"}</p>
							</li>
						</ul>
				</div>
			{/if}
		</div>
		</li>
	{/foreach}
	</ul>
</body>
</html>
