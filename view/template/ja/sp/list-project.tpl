{include file="{$lang}/sf-head.tpl"}

<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/list.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->

</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div id="wrapper" class="clearfix">
<div class="main-wrapper list-main-wrapper">
	<div class="main-inner">
		<div class="container-wide">
			<img class="pc" src="{$smarty.const.C_ASSETS}images/pc/people.gif" alt="A-portでは、様々なプロジェクトが応援を求めています。">
		</div>
	</div>
</div>
<!-- スマートフォン表示専用 -->
<div class="page-ttl-wrapper list-main-wrapper-sp">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">プロジェクトを探す</h2>
		<p class="sub-text">A-portでは、さまざまなプロジェクトがあなたの支援を待っています。</p>
	</div>
</div>
<!-- /スマートフォン表示専用 -->
<div class="body">
	<div class="container">
		<div class="local-nav-sort-wrapper">
			<ul class="local-nav-sort cf">
				<li class="local-nav-sort-new"><a href="{$pagelink_list_project}?hid_page_list=1">新着順</a></li>
				<li class="local-nav-sort-day"><a href="{$pagelink_list_project}?hid_page_list=2">期限間近順</a></li>
				<li class="local-nav-sort-fund"><a href="{$pagelink_list_project}?hid_page_list=3">応援金額の多い順</a></li>
				<!-- <li class="local-nav-sort-progress"><a href="#">達成済</a></li> -->
			</ul>
		</div>
		<div class="local-nav-category-wrapper jsc-local-nav-category">
			<h3 class="local-nav-category-ttl jsc-local-nav-category-toggle"><a href="javascript:void(0);">カテゴリから探す</a></h3>
			<ul class="local-nav-category cf">
            	{foreach name=outer item=con from=$categorylist}
				<li><a href="{$pagelink_list_project}?c_no={$con.no}">{$con.category_name}</a></li>
                {/foreach}
			</ul>
		</div>
		<div class="project-list-ttl-wrapper cf">
			<h3 class="project-list-ttl">プロジェクト一覧</h3>
			<p class="project-list-number">プロジェクト件数：<span>{$total_count}</span>件</p>
		</div>
		<div class="project-list-wrapper">
			<ul class="project-list">

		{foreach from=$result_list item="item" name="loop"}
				<li>
					<div class="project-img"><a href="{$pagelink_detail_project}{$item.hope_url}"><img src="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}" alt="{$item.public_title}" width=260 height=186></a></div>
					<div class="project-box">
						<p class="project-title"><a href="{$pagelink_detail_project}{$item.hope_url}">{$item.public_title}</a></p>
						<ul class="project-tip cf">
							<li class="project-tip-category" style="width:120px;"><a href="{$pagelink_list_project}?c_no={$item.category_no}">{$item.category_name}</a></li>
							<li class="project-tip-presenter">{$item.member_nickname}</li>
						</ul>
						{if $item.status=='1'&&$item.percent<100&&$item.diff_in_days!='-'}
						<div class="project-summary">
							<div class="progress">
								<div class="gauge" style="width:100%;">
									<div class="gauge2" {if $item.percent<100}style="width:{$item.percent}%;"{else}style="width:100%;"{/if}>
									</div>
								</div>
							</div>
							<ul class="project-status">
								<li class="project-status-fund">
									<p class="project-status-label" style="width:130px;">集まった金額</p>
									<p class="project-status-var"><span>{$item.now_summary|number_format}</span>円</p>
								</li>
								<li class="project-status-per">
									<p class="project-status-label">達成率</p>
									<p class="project-status-var"><span>{$item.percent}</span>%</p>
								</li>
								<li class="project-status-last">
									<p class="project-status-label">あと</p>
									<p class="project-status-var">{$item.diff_in_days}日</p>
								</li>
							</ul>
						</div>
						{elseif $item.status=='3'||$item.percent>=100||$item.diff_in_days=='-'}
								<div class="project-summary funded">
									<div class="progress"> FUNDED! </div>
									<ul class="project-status">
										<li class="project-status-fund">
											<p class="project-status-label" style="width:130px;">集まった金額</p>
											<p class="project-status-var"><span>{$item.now_summary|number_format}</span>円</p>
										</li>
										<li class="project-status-per">
											<p class="project-status-label">達成率</p>
											<p class="project-status-var"><span>{$item.percent}</span>%</p>
										</li>
										<li class="project-status-last">
												<p class="project-status-label">終了</p>
												<p class="project-status-var">{$item.invest_limit|date_format:"%y年%m月%d日"}</p>
										</li>
									</ul>
							</div>
						{/if}
					</div>
				</li>
                {/foreach}

			</ul>
		</div><!--/project-list-wrapper-->
	</div><!--/container-->
</div>
<div class="pagination-wrapper">
	<ul class="pagination roboto-b cf">
		{my_page countpage=$total_page nowpage=$nowpage m_page_list=$m_page_list}
	</ul>
</div>
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
