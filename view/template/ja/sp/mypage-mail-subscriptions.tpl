{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<div id="myPwEdit">
	<h2 class="hdLv02">メール通知設定</h2>
	<form accept-charset="UTF-8" action="" method="post">
	{if $finish_msg}
		<p class="update">{$finish_msg}</p>
	{/if}
	  <dl class="formGroup">
	    <dt>ニュースレター購読</dt>
	    <dd>
	      <div class="flagTable">
		<div class="flagTableGroup">
		  <div class="flagControl">
		    <label>
		      <input id="mail_flg" name="mail_flg" type="radio" value="1" {if $input_data.mail_flg=='1'}checked="checked"{else}{/if}/>
		      購読する</label>
		    <label>
		      <input id="mail_flg" name="mail_flg" type="radio" value="0" {if $input_data.mail_flg=='0'}checked="checked"{else}{/if}/>
		      購読しない（解除後に2,3回届く可能性があります）</label>
		  </div>
		</div>
	      </div>
	    </dd>
	    <dt>活動報告通知</dt>
	    <dd>
	      <div class="flagTable">
		<div class="flagTableGroup">
		  <div class="flagControl">
		    <label>
		      <input id="activity_push_flg" name="activity_push_flg" type="radio" value="1" {if $input_data.activity_push_flg=='1'}checked="checked"{else}{/if}/>
		      受け取る</label>
		    <label>
		      <input id="activity_push_flg" name="activity_push_flg" type="radio" value="0" {if $input_data.activity_push_flg=='0'}checked="checked"{else}{/if}/>
		      受け取らない</label>
		  </div>
		</div>
	      </div>
	    </dd>
	  </dl>
	  <p class="actions">
	    <input name="mail_flg_update" type="submit" value="設定する" />
	  </p>
	</form>
	</div>
	<!-- #myPwEdit --> 
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
