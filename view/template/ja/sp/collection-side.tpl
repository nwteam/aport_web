<div class="side">
	<div id="projectSideSummary" class="pjtItem pcCon">
		<div class="pjtImage"> <img alt="{$project_info.title}" src="{$smarty.const.C_IMG_PROJECT}{$project_info.cover_img}" /> </div>
		<div class="pjtTitle">{$project_info.title}</div>
		<ul class="pjtTip">
		  <li class="presenter">{$drafter_info.public_name}</li>
		</ul>
	</div>
	{if $project_info.project_type=="1"}
	<div id="pledge_howto" class="howtoBox sideSection">
		<dl>
		  <dt>プロジェクトが成立した場合</dt>
		  <dd>あなたが支援しているプロジェクトが目標金額に到達した場合にのみ、あなたのカードへの課金が行われます。目標金額に到達しなかった場合は、お使いのカードに課金されることはありません。
			</dd>
		  <dt>プロジェクトが不成立になった場合</dt>
		  <dd>目標金額に到達しなかった場合は、支援者が誓約した資金提供の約束が取り消されると同様に、すべての誓約リターンも同時に取り消されます。
			</dd>
		  <dt>成立したプロジェクトへの誓約をキャンセル出来ますか？</dt>
		  <dd>応援後７日以内のキャンセルを受け付けております。但し、上記の条件に合致していたとしても、以下の場合はキャンセルができませんのでご注意ください。</br>
					・すでにプロジェクトが成立している場合</br>
					・プロジェクトの募集期間が残り８日未満であった場合</br>
					・キャンセル申請時にプロジェクトが成立していた場合
			</dd>
		</dl>
	</div>
	{else}
	<div id="pledge_howto" class="howtoBox sideSection">
		<dl>
		  <dt>プロジェクトの成立/不成立に関して</dt>
		  <dd>本プロジェクトは、実施を前提として応援を募る実行確約型である為、目標金額に到達するかを問わずファンディングは実行され、あなたのカードへの課金／銀行振り込みへの受け付けが行われます。
		    実行者は集めた分のプロジェクト資金を獲得することができると共に、サポーターはリターンの誓約内容を受け取る事が出来ます。</dd>
		  <dt>成立したプロジェクトへの誓約をキャンセル出来ますか？</dt>
		  <dd>実行確約型のプロジェクトへ応援頂いた場合のキャンセルはできませんのでご了承ください。
		    クラウドファンディングの性質上、サポーターへのリターンは受注生産品となりますので、サポーターが行った支払い手続につき決済が完了した時点以降のキャンセル、リターンの返品または返金はできません。</dd>
		</dl>
	</div>
	{/if}
	<!-- #howtoBox --> 
</div>
<!--.side--> 