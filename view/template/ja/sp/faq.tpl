{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/faq.css">
</head>
<body>
<div class="wrapper">
{include file="{$lang}/sf-header.tpl"}
  <div class="page-ttl-wrapper" style="padding-top:0px;">
    <div class="container page-ttl-inner cf">
      <h2 class="page-ttl">よくあるご質問</h2>
    </div>
  </div>
  <div class="body" id="faq">
    <div class="container container-offset cf">
      <div class="col-main">
        <div class="col-inner">
          <div id="navi">
            <h3 class="roboto">Q&amp;A LIST</h3>
            <ul>
              <li><a href="#construction"><span>クラウドファンディングの仕組みについてのご質問</span></a></li>
              <li><a href="#collector"><span>支援（支援者）に関するご質問</span></a></li>
              <li><a href="#presenter"><span>起案（起案者）に関するご質問</span></a></li>
              <li><a href="#other"><span>その他のご質問</span></a></li>
            </ul>
          </div>
          <div id="construction" class="ac-wrap">
            <h4 class="ttl-blue">クラウドファンディングの仕組みについてのご質問</h4>
            <dl class="acMenu list-line">
              <dt>Q1.&nbsp;支援者、起案者とはどのような意味でしょうか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A1.</th>
                    <td>支援者はプロジェクトを支援する人、起案者はプロジェクトを起案する人のことを意味しています。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q2.&nbsp;達成時実行型（All or Nothing型）とはなんですか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A2.</th>
                    <td>「All or Nothing型」、「達成後支援型」と呼ばれる方式で、目標金額を達成した場合のみ、調達した資金を手にすることができます。ローンチ資金を集めるのに適しています。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q3.&nbsp;実行確約型とはなんですか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A3.</th>
                    <td>「実行確約型」と呼ばれる方式で、目標金額に達成しない場合でも、調達した資金を手にすることが出来ます。ただし、起案者（起案者）は掲載時にプロジェクトの実施を確約する必要があります。<br>
                      既に実行が決まっていて、スケールアップ等のために必要な資金を募りたいプロジェクトや、過去に実施実績があるプロジェクトなどに適した方式です。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q4.&nbsp;リターンとはなんですか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A4.</th>
                    <td>プロジェクトを支援し、資金提供を誓約した支援者（支援者）に対してのお返しです。リターンは資金額によって、複数種類用意されています。</td>
                  </tr>
                </table>
              </dd>
            </dl>
          </div>
          <!--/ac-wrap-->

          <div id="collector" class="ac-wrap">
            <h4 class="ttl-blue">支援（支援者）に関するご質問</h4>
            <dl class="acMenu list-line" >
              <dt>Q1.&nbsp;支援はどうやったらできますか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A1.</th>
                    <td><ul class="indent">
                        <li>&#9312;サイトよりログイン、あるいはA-portの会員になっていない方は「新規登録」を行い、応援（支援）したいプロジェクトを選択してください。</li>
                        <li>&#9313;「プロジェクトを応援する」ボタンをクリックするか、ご希望のリターンをご選択ください。</li>
                        <li>&#9314;次ページで、応援（支援）する金額、リターンの内容を確認いただき、決済を行ってください。</li>
                      </ul></td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q2.&nbsp;対応しているクレジットカード会社を教えてください。</dt>
              <dd>
                <table>
                  <tr>
                    <th>A2.</th>
                    <td>VISAとMaster Cardです。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q3.&nbsp;クレジットカード決済以外で支援する方法はありますか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A3.</th>
                    <td>実行確約型（実行確約型）のプロジェクトに関しては、銀行振り込みも可能ですが、達成時実行型（All or Nothing型）（達成後支援型）に関しては、クレジットカードのみとなります。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q4.&nbsp;海外に居住していても、支援することができますか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A4.</th>
                    <td>海外居住者の利用に関しては、各プロジェクトによって提供できるチケットが限られる場合があるため、起案者（起案者）に確認してください。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q5.&nbsp;支援したプロジェクトをキャンセルできますか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A5.</th>
                    <td>達成時実行型（All or Nothing型）のプロジェクトに限り支援後７日以内のキャンセルを受け付けております。但し、上記の条件に合致していたとしても、以下の場合はキャンセルができませんのでご注意ください。
                      <ul class="indent">
                        <li>&#9312;プロジェクトの募集期間が残り８日未満であった場合</li>
                        <li>&#9313;支援者（支援者）募集期間中に目標金額に達したプロジェクトで、達成前に応援頂いた場合</li>
                      </ul>
                      なお、実行確約型のプロジェクトへ応援頂いた場合のキャンセルはできませんのでご了承ください。 </td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q6.&nbsp;支援したプロジェクトが成立しなかった場合、プロジェクト支援金やリターンはどのようになりますか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A6.</th>
                    <td><ul class="indent">
                        <li>&#9312;実行確約型は、実行確約型ですので、決済が行われリターンが提供されます。</li>
                        <li>&#9313;達成時実行型（All or Nothing型）は、プロジェクトが達成されなかった場合、決済が履行されず、リターンも提供されません。</li>
                      </ul></td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q7.&nbsp;支援したプロジェクトのリターンは、いつ受け取ることができるのでしょうか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A7.</th>
                    <td>プロジェクトによってリターンを受け取ることができる時期は異なります。プロジェクトの詳細に記入してありますのでご確認ください。また、必要に応じて、起案者（起案者）にメッセージを送って確認してください。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q8.&nbsp;領収証が欲しいのですが？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A8.</th>
                    <td>起案者が発行することになっております。起案者に直接、ご依頼ください。</td>
                  </tr>
                </table>
              </dd>
            </dl>
          </div>
          <!--/ac-wrap-->

          <div id="presenter" class="ac-wrap">
            <h4 class="ttl-blue">起案（起案者）に関するご質問</h4>
            <dl class="acMenu list-line" >
              <dt>Q1.&nbsp;プロジェクトの起案はどうやったらできますか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A1.</th>
                    <td><a href="{$pagelink_start}">「プロジェクトをはじめる」</a>ページから“プロジェクトの申し込みをする”ボタンを押して、お申込みシートに必要事項をご記入いただき、お申し込みください。プロジェクトの申請を頂いてから、7営業日を目安にお返事させて頂きます。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q2.&nbsp;実行したいアイデアがありますが、支援者（支援者）が集まるかどうか自信が持てません。</dt>
              <dd>
                <table>
                  <tr>
                    <th>A2.</th>
                    <td><a href="{$pagelink_start}">「プロジェクトをはじめる」</a>ページの“プロジェクトの申し込みをする”ボタンを押して、“お申込みシート”に相談内容をご記入ください。事務局スタッフが相談に応じ、アドバイスをします。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q3.&nbsp;プロジェクトの文章を編集・校正してくれますか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A3.</th>
                    <td>ご提出いただいた原稿を編集・校正はさせて頂きます。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q4.&nbsp;法人や団体単位でのプロジェクト起案も可能ですか？<br></dt>
              <dd>
                <table>
                  <tr>
                    <th>A4.</th>
                    <td>可能です。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <span class="nomal">※起案者用のQAは、<a href="{$pagelink_start}">「プロジェクトをはじめる」</a>ページにもございます。</span>
          </div>
          <!--/ac-wrap-->

          <div id="other" class="ac-wrap">
            <h4 class="ttl-blue">その他のご質問</h4>
            <dl class="acMenu list-line" >
              <dt>Q1.&nbsp;未成年なのですが、支援や起案は可能ですか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A1.</th>
                    <td>未成年者が支援する場合、本サービスの利用について、親権者から事前の同意を得て下さい。起案する場合、親権者が責任をもって監督することに同意する必要があり、別途書面の提出をしていただきます。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q2.&nbsp; A-portは、朝日新聞社とどのような関係があるのでしょうか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A2.</th>
                    <td>A-portは朝日新聞社が運営するクラウドファンディングサイトです。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q3.&nbsp;MotionGallery社との関係を教えてください。</dt>
              <dd>
                <table>
                  <tr>
                    <th>A3.</th>
                    <td>A-portは株式会社MotionGalleryに運営の一部を委託しています。また、決済にかかわる業務についても同社のシステムを利用しております。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q4.&nbsp;電話での対応は可能ですか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A4.</th>
                    <td>プロジェクトの起案・提案などのご相談は、<a href="{$pagelink_start}">「プロジェクトをはじめる」</a>ページの“プロジェクトの申し込みをする”ボタンを押して、お申込みフォームからご連絡頂くようお願いいたします。<br>
                      支援に関するお問い合わせについては<a href="{$pagelink_inquiry}">問い合わせフォーム</a>、もしくは電話（A-portサポート事務局&nbsp;&#8481;<span class="tel-link">03-6869-9001</span>）でお受けしております。※電話はプロジェクト起案者の方の対応はしておりませんのでご了承ください。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q5.&nbsp;A-portに掲載されれば、朝日新聞や関連する媒体で取り上げてもらえるのでしょうか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A5.</th>
                    <td>お約束はできませんが、朝日新聞社関連の媒体でご紹介できるよう最大限努力させて頂きます。その他、他媒体への掲載も目指して、プロジェクトを起案者と共に盛り上げていきます。</td>
                  </tr>
                </table>
              </dd>
            </dl>
            <dl class="acMenu list-line" >
              <dt>Q6.&nbsp;退会するにはどうしたらいいでしょうか？</dt>
              <dd>
                <table>
                  <tr>
                    <th>A6.</th>
                    <td><a href="{$pagelink_inquiry}">問い合わせフォーム</a>より、退会希望されることをお伝えください。</td>
                  </tr>
                </table>
              </dd>
            </dl>
          </div>
          <!--/ac-wrap-->

        </div>
        <!--/col-inner-->
      </div>
      <!--/col-main-->
			{include file="{$lang}/other-sub.tpl"}
      <ol class="breadcrumb cf">
        <li><a href="/">サイトトップ</a></li>
        <li>よくあるご質問</li>
      </ol>
    </div>
    <!--/container-->
  </div>
  <!--/body-->

{include file="{$lang}/sf-footer.tpl"}
</div>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/lib/accordion.js"></script>
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script src="{$smarty.const.C_ASSETS}js/faq.js"></script>
</body>
</html>
