{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<div id="myPwEdit">
	<h2>パスワードの変更</h2>
	<form accept-charset="UTF-8" action="" method="post">
	{if $err_msg.top}
		<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
	{/if}
	{if $err_msg.new_password}
		<p class="error" style="color:red;">※新しいパスワード：{$err_msg.new_password}</p>
	{/if}
	{if $finish_msg}
		<p class="update">{$finish_msg}</p>
	{/if}
	  <dl>
	    <p  style="color:red;">
	      SNSアカウントでユーザ登録されています。<br/>
				ユーザ情報変更の前にログインパスワードを設定してください。
	    </p>
	    <dt>新しいパスワードを入力</dt>
	    <dd>
	      <input autocomplete="off" name="new_password" id="new_password" type="password" />
	      <p class="note">※半角英数字8～12文字</p>
	    </dd>
	    <dt>新しいパスワードを再入力</dt>
	    <dd>
	      <input name="password2" id="password2" type="password" />
	      <p class="note">※半角英数字8～12文字</p>
	    </dd>
	  </dl>
	  <div class="actions">
	    <input name="new_pwd" type="submit" value="設定する" />
	  </div>
	</form>
	</div>
	<!-- #myPwEdit --> 
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
