{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<h2>投稿したプロジェクト</h2>
		<dt style="	margin: 25px 0 10px 0; padding: 8px 20px;font-size: 15px;background: #317AB7;color: #fff;font-weight: bold;">入稿待ち</dt>
		<dd style="padding-bottom: 8px;">
			<ul class="project-list-index">
			{foreach from=$project_data_list item="item" name="loop"}
				<li>
				<div class="project-img"><a href="{$pagelink_drafting1}?p_no={$item.no}" ><img src="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}" alt="{$item.public_title}" width=260 height=186></a></div>
				<div class="project-box">
					<p class="project-title"><a href="{$pagelink_drafting1}?p_no={$item.no}" >{$item.public_title}</a></p>
					<ul class="project-tip cf">
						<li class="project-tip-category" style="width:120px;"><a href="{$pagelink_list_project}?c_no={$item.category_no}">{$item.category_name}</a></li>
						<li class="project-tip-presenter">{$item.member_nickname}</li>
					</ul>
					<div class="project-summary">
						<div class="progress">
							<div class="gauge" style="width:100%;">
								<div class="gauge2" {if $item.percent<100}style="width:{$item.percent}%;"{else}style="width:100%;"{/if}>
								</div>
							</div>
						</div>
						<ul class="project-status">
							<li class="project-status-fund">
								<p class="project-status-label" style="width:130px;">集まった金額</p>
								<p class="project-status-var"><span>{$item.now_summary|number_format}</span>円</p>
							</li>
							<li class="project-status-per">
								<p class="project-status-label">達成率</p>
								<p class="project-status-var"><span>{$item.percent}</span>%</p>
							</li>
							<li class="project-status-last">
								<p class="project-status-label">あと</p>
								<p class="project-status-var">{$item.diff_in_days}日</p>
							</li>
						</ul>
					</div>
				</div>
				</li>
			{/foreach}
			</ul>
		</dd>
		<dt id="wait_for_check" style="	margin: 25px 0 10px 0; padding: 8px 20px;font-size: 15px;background: #317AB7;color: #fff;font-weight: bold;">審査中</dt>
		<dd style="padding-bottom: 8px;">
			<ul class="project-list-index">
			{foreach from=$project_data_list_Checking item="item_Checking" name="loop"}
				<li>
				<div class="project-img"><a href="#" ><img src="{$smarty.const.C_IMG_PROJECT}{$item_Checking.cover_img}" alt="{$item_Checking.public_title}" width=260 height=186></a></div>
				<div class="project-box">
					<p class="project-title"><a href="#" >{$item_Checking.public_title}</a></p>
					<ul class="project-tip cf">
						<li class="project-tip-category" style="width:120px;"><a href="#">{$item_Checking.category_name}</a></li>
						<li class="project-tip-presenter">{$item_Checking.member_nickname}</li>
					</ul>
					<div class="project-summary">
						<div class="progress">
							<div class="gauge" style="width:100%;">
								<div class="gauge2" {if $item_Checking.percent<100}style="width:{$item_Checking.percent}%;"{else}style="width:100%;"{/if}>
								</div>
							</div>
						</div>
						<ul class="project-status">
							<li class="project-status-fund">
								<p class="project-status-label" style="width:130px;">集まった金額</p>
								<p class="project-status-var"><span>{$item_Checking.now_summary|number_format}</span>円</p>
							</li>
							<li class="project-status-per">
								<p class="project-status-label">達成率</p>
								<p class="project-status-var"><span>{$item_Checking.percent}</span>%</p>
							</li>
							<li class="project-status-last">
								<p class="project-status-label">あと</p>
								<p class="project-status-var">{$item_Checking.diff_in_days}日</p>
							</li>
						</ul>
					</div>
				</div>
				</li>
			{/foreach}
			</ul>
		</dd>
		<dt style="	margin: 25px 0 10px 0; padding: 8px 20px;font-size: 15px;background: #317AB7;color: #fff;font-weight: bold;">修正待ち</dt>
		<dd style="padding-bottom: 8px;">
			<ul class="project-list-index">
			{foreach from=$project_data_list_Change item="item_Change" name="loop"}
				<li>
				<div class="project-img"><a href="{$pagelink_drafting1}?p_no={$item_Change.no}" ><img src="{$smarty.const.C_IMG_PROJECT}{$item_Change.cover_img}" alt="{$item_Change.public_title}" width=260 height=186></a></div>
				<div class="project-box">
					<p class="project-title"><a href="{$pagelink_drafting1}?p_no={$item_Change.no}" >{$item_Change.public_title}</a></p>
					<ul class="project-tip cf">
						<li class="project-tip-category" style="width:120px;"><a href="{$pagelink_list_project}?c_no={$item_Change.category_no}">{$item_Change.category_name}</a></li>
						<li class="project-tip-presenter">{$item_Change.member_nickname}</li>
					</ul>
					<div class="project-summary">
						<div class="progress">
							<div class="gauge" style="width:100%;">
								<div class="gauge2" {if $item_Change.percent<100}style="width:{$item_Change.percent}%;"{else}style="width:100%;"{/if}>
								</div>
							</div>
						</div>
						<ul class="project-status">
							<li class="project-status-fund">
								<p class="project-status-label" style="width:130px;">集まった金額</p>
								<p class="project-status-var"><span>{$item_Change.now_summary|number_format}</span>円</p>
							</li>
							<li class="project-status-per">
								<p class="project-status-label">達成率</p>
								<p class="project-status-var"><span>{$item_Change.percent}</span>%</p>
							</li>
							<li class="project-status-last">
								<p class="project-status-label">あと</p>
								<p class="project-status-var">{$item_Change.diff_in_days}日</p>
							</li>
						</ul>
					</div>
				</div>
				</li>
			{/foreach}
			</ul>
		</dd>
		<dt style="	margin: 25px 0 10px 0; padding: 8px 20px;font-size: 15px;background: #317AB7;color: #fff;font-weight: bold;">審査完了</dt>
		<dd style="padding-bottom: 8px;">
			<ul class="project-list-index">
			{foreach from=$project_data_list_Checked item="item_Checked" name="loop"}
				<li>
				<div class="project-img"><a href="{$pagelink_publish}?p_no={$item_Checked.no}"  target="_blank" ><img src="{$smarty.const.C_IMG_PROJECT}{$item_Checked.cover_img}" alt="{$item_Checked.public_title}" width=260 height=186></a></div>
				<div class="project-box">
					<p class="project-title"><a href="{$pagelink_publish}?p_no={$item_Checked.no}" target="_blank" >{$item_Checked.public_title}</a></p>
					<ul class="project-tip cf">
						<li class="project-tip-category" style="width:120px;"><a href="{$pagelink_list_project}?c_no={$item_Checked.category_no}">{$item_Checked.category_name}</a></li>
						<li class="project-tip-presenter">{$item_Checked.member_nickname}</li>
					</ul>
					{if $item_Checked.status=='1'&&$item_Checked.percent<100&&$item_Checked.diff_in_days!='-'}
						<div class="project-summary">
							<div class="progress">
								<div class="gauge" style="width:100%;">
									<div class="gauge2" {if $item_Checked.percent<100}style="width:{$item_Checked.percent}%;"{else}style="width:100%;"{/if}></div>
								</div>
							</div>
							<ul class="project-status">
								<li class="project-status-fund">
									<p class="project-status-label" style="width:130px;">集まった金額</p>
									<p class="project-status-var"><span>{$item_Checked.now_summary|number_format}</span>円</p>
								</li>
								<li class="project-status-per">
									<p class="project-status-label">達成率</p>
									<p class="project-status-var"><span>{$item_Checked.percent}</span>%</p>
								</li>
								<li class="project-status-last">
									<p class="project-status-label">あと</p>
									<p class="project-status-var">{$item_Checked.diff_in_days}日</p>
								</li>
							</ul>
						</div>
					{elseif $item_Checked.status=='3'||$item_Checked.percent>=100||$item_Checked.diff_in_days=='-'}
							<div class="project-summary funded">
								<div class="progress"> FUNDED! </div>
								<ul class="project-status">
									<li class="project-status-fund">
										<p class="project-status-label" style="width:120px;">集まった金額</p>
										<p class="project-status-var"><span>{$item_Checked.now_summary|number_format}</span>円</p>
									</li>
									<li class="project-status-per">
										<p class="project-status-label">達成率</p>
										<p class="project-status-var"><span>{$item_Checked.percent}</span>%</p>
									</li>
									<li class="project-status-last">
										<p class="project-status-label">終了</p>
										<p class="project-status-var">{$item_Checked.invest_limit|date_format:"%y年%m月%d日"}</p>
									</li>
								</ul>
						</div>
					{/if}
				</div>
				</li>
			{/foreach}
			</ul>
		</dd>
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
