{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
<style type="text/css">
{literal}
.hide{display:none;}.invisible{visibility:hidden;}.overflow{overflow:hidden;}
.clear{display:block;clear:both;height:0;overflow:hidden;}

/* formbox */
#drafting .succeed{background:url(assets/images/check_ok.gif) no-repeat -105px 0;}
#drafting .focus{color:#999;line-height:22px;*line-height:20px;}
#drafting .null,#drafting .error{color:red;line-height:22px;*line-height:20px;}
{/literal}
</style>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul class="tab clearfix mb15">
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting1.php?p_no={$input_data.no}">基本情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">リターン内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting3.php?p_no={$input_data.no}">プロジェクト内容</a></li>
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">SNSとPR</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting5.php?p_no={$input_data.no}" >起案者情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting6.php?p_no={$input_data.no}">プレビュー</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
			<!--LV5 fragment-1 Start -->
			<div id="fragment-4" class="clearfix">
				<div class="draftingLeft">
					{if $err_msg.top}
							<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
					{/if}
					<h3><span>1</span>Facebook</h3>
					<p>プロジェクトに関係するFacebookページ、またはプロジェクト実行者のアカウントを記入してください。</p>
					<p>https://www.facebook.com/<input id="facebook_page" name="facebook_page" size="45" type="text" value="{$input_data.facebook_page}"/></p>
					<label id="facebook_page_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="facebook_page_error"></label>
					<h3><span>2</span>Twitter</h3>
					<p>Twitterのアカウントを設定してください。</p>
					<p>@<input id="twitter_account" name="twitter_account" size="35" type="text" value="{$input_data.twitter_account}"/></p>
					<label id="twitter_account_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="twitter_account_error"></label>
					<h3><span>3</span>ブログやホームページ</h3>
					<p>ブログやその他のホームページを持っている方は記入してください。</p>
					<ul class="yourUrl">
						<li>URL1 :<input id="blog_url_1" name="blog_url_1" size="65" type="text" value="{$input_data.blog_url_1}"/></li>
						<label id="blog_url_1_succeed" class="blank"></label>
						<span class="clear"></span>
						<label id="blog_url_1_error"></label>
						{if $err_msg.blog_url_1}
								<p class="errorTitle" style="color:red;">※{$err_msg.blog_url_1}</p>
						{/if}
						<li>URL2 :<input id="blog_url_2" name="blog_url_2" size="65" type="text" value="{$input_data.blog_url_2}"/></li>
						<label id="blog_url_2_succeed" class="blank"></label>
						<span class="clear"></span>
						<label id="blog_url_2_error"></label>
						{if $err_msg.blog_url_2}
								<p class="errorTitle" style="color:red;">※{$err_msg.blog_url_2}</p>
						{/if}
						<li>URL3 :<input id="blog_url_3" name="blog_url_3" size="65" type="text" value="{$input_data.blog_url_3}"/></li>
						<label id="blog_url_3_succeed" class="blank"></label>
						<span class="clear"></span>
						<label id="blog_url_3_error"></label>
						{if $err_msg.blog_url_3}
								<p class="errorTitle" style="color:red;">※{$err_msg.blog_url_3}</p>
						{/if}
					</ul>
					<ul class="formStep">
						<li class="formPrev"><a href="{$smarty.const.ROOT_URL}ja/drafting3.php?p_no={$input_data.no}">戻る</a></li>
						<li class="formNext"><input id="next_f4" value="次へ" type="submit" name="d4" style="float:right;"/>
						<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-4  --> 
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}

<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.form.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
