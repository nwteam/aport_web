<div id="myProfile">
{if $input_data.profile_img==""}
	<p id="myThumb"><img src="{$smarty.const.C_ASSETS}images/noimage.png" alt="{$input_data.member_name}" width=200 height=200/></p>
{else}
	<p id="myThumb"><img src="{$smarty.const.C_IMG_ACTRESS}{$input_data.profile_img}" alt="{$input_data.member_name}" width=200 height=200/></p>
{/if}
	<div id="myDescription">
		<h2>{$input_data.member_name}</h2>
		<ul>
			<li id="myPlace">{$array_area[$input_data.add_1]}</li>
			<li id="myUrl"><a href="{$input_data.hp_url}" target="_blank">{$input_data.hp_url}</a></li>
		</ul>
		<p>{$input_data.profile}</p>
	</div>
	<!-- #myDescription --> 
</div>