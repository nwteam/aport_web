{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<!--LV1 wrapper Start -->
<div class="wrapper">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="upDateEdit" class="clearfix">
			<!--LV4 tab Start -->
			{include file="{$lang}/publish-title.tpl"}
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
      <div id="updateArea">
				<h3>メール明細</h3>
        <dl class="clearfix">
          <dt>送信元</dt>
          <dd>
            <p>{$input_data.msg_id}</p>
          </dd>
          <dt>件名</dt>
          <dd>
            <p>{$input_data.msg_id}</p>
          </dd>
          <dt>本文</dt>
          <dd class="htmlEditor">
						<p>{$input_data.msg_id}</p>
        </dl>
        <div class="actions">
          <input name="sbm_back" type="submit" value="戻る" />
          <input name="sbm_update" type="submit" value="返信" />
        </div>
        <!--/#fragment-1  -->
			</form>
			</div>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
