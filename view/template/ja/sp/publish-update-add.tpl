{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
<!-- HTML エディタ -->
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}js/redactor/redactor.css"/>

<script src="{$smarty.const.C_ASSETS}js/redactor/redactor.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/langs/ja.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/video.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/table.js"></script>
<script type="text/javascript">
{literal}
$(document).ready(function () {
	$('#update_contents').redactor({
		//buttons:['html','formatting','bold','italic','deleted','unorderedlist','orderedlist','outdent','indent','image','video','file','table','link','alignment','horizontalrule'],
		buttonSource: true,
		convertVideoLinks:true,
		imageUpload:'upload.php',
		plugins: ['table', 'video'],
		lang:'ja',
		formatting: ['h2', 'p', 'blockquote']
		});
});
{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<!--LV1 wrapper Start -->
<div class="wrapper">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="upDateEdit" class="clearfix">
			<!--LV4 tab Start -->
			{include file="{$lang}/publish-title.tpl"}
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="u_id" value="{$input_data.u_id}">
			{if $err_msg.top}
					<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
			{/if}
      <div id="updateArea">
				{if $input_data.u_id==""}
					<h3>アップデートを作成する</h3>
				{else}
					<h3>アップデート編集</h3>
				{/if}
        <dl class="clearfix">
          <dt>タイトル（必須）</dt>
          <dd>
            <input id="update_title" name="update_title" type="text" value="{$input_data.update_title}"/>
						{if $err_msg.update_title}
							<span class="red" style="color:red;">※{$err_msg.update_title}</span><br />
						{/if}
          </dd>
          <dt>公開範囲</dt>
          <dd>
            <select name="update_type">
              <option value="0" {if $input_data.update_type=="0"}selected{/if}>全体に公開</option>
              <option value="1" {if $input_data.update_type=="1"}selected{/if}>支援者限定</option>
            </select>
          </dd>
          <dt>本文</dt>
          <dd class="htmlEditor">
            <textarea cols="80" id="update_contents" name="update_contents" rows="10">{$input_data.update_contents}</textarea>
						{if $err_msg.update_contents}
							<span class="red" style="color:red;">※{$err_msg.update_contents}</span><br />
						{/if}
        </dl>
        <div class="actions">
				{if $input_data.u_id==""}
          <input name="sbm_back" type="submit" value="戻る" />
          <input name="sbm_update" type="submit" value="投稿する" />
				{else}
          <input name="sbm_back" type="submit" value="戻る" />
          <input name="sbm_update" type="submit" value="保存する" />
				{/if}
        </div>
        <!--/#fragment-1  -->
			</form>
			</div>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
