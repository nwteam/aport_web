{include file="{$lang}/sf-head.tpl"}

<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
<div id="wrapper" class="clearfix">
{include file="{$lang}/sf-header.tpl"}
<div class="page-ttl-wrapper" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">{$item.public_title}</h2>
		<ul class="page-ttl-info cf">
			<li class="page-ttl-info-presenter"><a href="#">{$actress.public_name}</a></li>
			<li class="page-ttl-info-category"><a href="#">{$item.category_name}</a></li>
			<li class="page-ttl-info-location"><a href="#">{$array_area[$actress.area]}</a></li>
		</ul>
	</div>
</div>
<div class="body">
	<div class="container container-offset cf">
		<div class="col-main">
			<div class="col-inner">
				<div class="local-nav-wrapper">
					<ul class="local-nav">
						<li class="local-nav-home count-off">
							<a href="{$pagelink_detail_project}{$item.hope_url}">
								<p>ホーム</p>
							</a>
						</li>
						<li class="local-nav-update count-on active">
							<a href="{$pagelink_detail_update}{$item.hope_url}/1">
								<p>アップデート</p>
								<p class="count"><span>{$total_count_update}</span></p>
							</a>
						</li>
						<li class="local-nav-collector count-on">
							<a href="{$pagelink_detail_supporter}{$item.hope_url}/1">
								<p>支援者</p>
								<p class="count"><span>{$total_count}</span></p>
							</a>
						</li>
					</ul>
				</div>
				<div class="projectUpdateList">
					{assign var=i value=$total_count_update}
					{foreach name=outer item=con from=$update_list}
					<!--update normal-->
					<div class="updateBox">
						<h3 class="title">{$con.update_title}</h3>
						<ul class="updataMeta">
							<li class="vol">Vol.{$i}</li>
							<li class="date">{$con.create_date|date_format:"%Y年%m月%d日%H:%M"}</li>
						</ul>
						{if $con.update_type=="0"||$member_info.user_no==$con.user_no||$member_info.user_no==$item.user_no}
							<div class="updatebody">
										<p>{$con.update_contents|nl2br nofilter}</p>
										<div class="readMore">
											<a href="/updates/detail/{$item.hope_url}/{$con.update_id}">続きを読む</a>
										</div>
							</div>
						{else}
							<div class="updatebody">
									<div class="limitedBoxCover">
										<p class="LtdTtl">支援者限定アップデート</p>
										<p>プロジェクトを支援していただいた支援者限定の記事です。</p>
										<p class="BtnLock"><a href="{$pagelink_login}">ログイン</a></p>
									</div>
							</div>
						{/if}
					</div>
					<!--/update normal-->
					{$i=$i-1}
					{/foreach}
					<div class="pagination-wrapper  pagination-sp-hide">
							<ul class="pagination roboto-b cf">
								{update_page countpage=$total_page nowpage=$nowpage hope_url=$item.hope_url}
							</ul>
					</div>
				</div>
			</div>
		</div>
		{include file="{$lang}/detail-sub.tpl"}
		<ol class="breadcrumb cf">
			<li><a href="#">サイトトップ</a></li>
			<li><a href="#">{$item.category_name}</a></li>
			<li><a href="#">{$item.public_title}</a></li>
		</ol>
	</div><!--/container-->
</div><!--/body-->
<!-- スマートフォン表示専用 -->
<div class="project-social-sp-wrapper">
	<h3 class="project-social-sp-ttl">このプロジェクトをシェアする</h3>
	<ul class="project-social-sp cf">
		<li class="fb"><a href="http://www.facebook.com/share.php?u=https://a-port.asahi.com/projects/xxx/" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">facebookでシェア</a></li>
		<li class="tw"><a href="http://twitter.com/share?url=https://a-port.asahi.com/projects/xxx/&text=～MGから自動的に出力されるコラムタイトル入る～ / 朝日新聞社が運営するクラウドファンディングサイト「A-port（エーポート）」" target="_blank">Twitterでシェア</a></li>
		<li class="line"><a href="http://line.me/R/msg/text/?～MGから自動的に出力されるコラムタイトル入る～ / 朝日新聞社が運営するクラウドファンディングサイト「A-port（エーポート）」https://a-port.asahi.com/projects/xxx/">LINEで送る</a></li>
	</ul>
</div>
<!-- スマートフォン表示専用 -->
{include file="{$lang}/sf-footer.tpl"}
</div>
<!--/#wrapper-->
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}

</body>
</html>
