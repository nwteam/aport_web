{include file="{$lang}/sf-head.tpl"}
<link href="{$smarty.const.C_ASSETS}css/common.css" media="screen" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/column.css" media="screen" rel="stylesheet" />
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}lib/html5shiv-printshiv-776c428db58d65d46e807ac934e8a1a3.js"></script>
<script src="{$smarty.const.C_ASSETS}lib/respond-448013aff8d268bb6b82475310302815.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper">
{include file="{$lang}/sf-header.tpl"}
    <div class="column-main-wrapper">
        <h2 class="column-main-ttl"><img src="{$smarty.const.C_ASSETS}images/column/ttl-column-main.png" alt="A-port OFFICIAL COLUMN"></h2>
    </div>
    <div class="body">
        <div class="container container-column cf">
            <div class="col-main">
                <div class="col-inner">
                    <div class="column-detail">
		    {foreach name=outer item=con from=$result_list}
                        <div class="column-status cf">
                            <p class="column-category"> <span class="{$con.category_class}">{$con.category_name}</span> </p>
                        </div>
                        <p class="column-date roboto-b">{$con.insert_time}</p>
                        <h3 class="column-title">{$con.title}</h3>
			{if $con.cover_img!=''}
				<div class="column-thum"><img src="{$con.cover_img}" alt="{$con.title}"></div>
			{/if}
                        <div class="column-description">
                            {$con.contents|nl2br nofilter}
                        </div>
                        <div class="column-detail-bottom cf">
                            <ul class="column-detail-meta">
                                <li>
                                    <dl class="column-detail-meta-list">
                                        <dt>CATEGORY:</dt>
					{foreach item=con from=$categorylist}
						<dd> <a href="{$con.cat_no}">{$con.cat_name}</a> </dd>
					{/foreach}
                                    </dl>
                                </li>
                                <li>
                                    <dl class="column-detail-meta-list">
                                        <dt>TAGS:</dt>
                                        {foreach item=con from=$taglist}
						<dd><a href="column.php?tag={$con.tag_id}">{$con.tag_name}</a></dd>
                                        {/foreach}
                                    </dl>
                                </li>
                            </ul>
                        </div>
		    {/foreach}
                    </div>
                </div>
            </div>

            {include file="{$lang}/column-side.tpl"}
        </div>
    </div>

    <div class="pagination-wrapper  pagination-sp-hide">
        <ul class="pagination roboto-b cf">
            <!--li class="prev"><a href="/column/150610">PREV</a></li-->
            <li class="page active column"><a href="column.php">コラムトップ</a></li>
            <!--li class="next"><a href="/column/150618">NEXT</a></li-->
        </ul>
    </div>
    
</div>
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
