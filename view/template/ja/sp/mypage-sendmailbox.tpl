{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<div id="myMailBox">
	<h2>メール</h2>
	<table>
	  <tbody>
	    <tr>
	      <th scope="col" class="mAdd">送信先</th>
	      <th scope="col" class="subject">件名</th>
        <th scope="col" class="create_time">送信日時</th>
	    </tr>
			{foreach name=outer item=con from=$mail_list}
			<tr>
				<td>{$con.member_name}<br/>＜{$con.email}＞</td>
				<td>{$con.message_title}</td>
				<td>{$con.create_date|date_format:"%Y年%m月%d日%H:%M:%S"}</td>
			</tr>
			{/foreach}
	  </tbody>
	</table>
	</div>
	<!-- #myMailEdit --> 
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
