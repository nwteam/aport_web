{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/form.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<div class="wrapper">
<div class="page-ttl-wrapper" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">プロジェクトの申し込みをする</h2>
	</div>
</div>
<div class="body">
	<div class="container container-single">
		<div class="project-new-step step2">step2</div>
		<div class="col-inner">
			<h3 class="h3"><span>Step.2</span>確認</h3>
			<form method="post" action="{$pagelink_reg_project_confirm}">
				<input type="hidden" name="name" value="{$input_data.name}">
				<input type="hidden" name="email" value="{$input_data.email}">
				<input type="hidden" name="fb_url" value="{$input_data.fb_url}">
				<input type="hidden" name="project_name" value="{$input_data.project_name}">
				<input type="hidden" name="category_no" value="{$input_data.category_no}">
				<input type="hidden" name="project_text" value="{$input_data.project_text}">
				<input type="hidden" name="wish_price" value="{$input_data.wish_price}">
				<input type="hidden" name="comment_text" value="{$input_data.comment_text}">
				<div class="input-form-wrapper">
					<ul class="input-form confirm">
						<li>
							<dl class="input-form-inner input-applicant cf">
								<dt>お名前</dt>
								<dd>{$input_data.name}</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-email cf">
								<dt>メールアドレス</dt>
								<dd>{$input_data.email}</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-facebook cf">
								<dt>Facebookアカウント</dt>
								<dd>{$input_data.fb_url}</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-name cf">
								<dt>プロジェクト名</dt>
								<dd>{$input_data.project_name}</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-category-id cf">
								<dt>カテゴリ名</dt>
								<dd>{$array_category[$input_data.category_no]}</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-description cf">
								<dt>プロジェクト概要</dt>
								<dd> {$input_data.project_text}</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-goal-amount cf">
								<dt>想定目標金額</dt>
								<dd>{$input_data.wish_price|number_format}円</dd>
							</dl>
						</li>
						<li>
							<dl class="input-form-inner input-ticket-description cf">
								<dt>想定リターン</dt>
								<dd> {$input_data.comment_text}</dd>
							</dl>
						</li>
					</ul>
					<p class="mt30 tac">内容をご確認の上、「確定する」ボタンを押してください。</p>
					<div class="form-submit-wraper">
						<div class="form-submit-btn">
							<input type="submit" name="commit" value="確定する">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
