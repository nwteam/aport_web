{include file="{$lang}/sf-head.tpl"}

<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/guide.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}js/common.js">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->

</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div class="wrapper">
<!-- スマートフォン表示専用 -->
<div class="page-ttl-wrapper list-main-wrapper-sp" style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">クラウドファンディングとは</h2>
		<p class="sub-text">誰でも、いますぐ、ネットでワンコインから参加できる支援の輪。<br>ここでは、クラウドファンディングについてその歴史や市場の動向も含めてご紹介します。</p>
	</div>
</div>
<!-- /スマートフォン表示専用 -->
<div class="body">
	<div class="cf-container">

		<div id="navi-panel">
			<p class="index">INDEX</p>
			<ul>
				<li class="size-s"><a href="#area01">「クラウドファンディング」言葉の由来</a></li>
				<li class="size-m"><a href="#area04">拡大するクラウドファンディング市場</a></li>
				<li class="size-s"><a href="#area02">こんなに古くからある!資金集めの考え方</a></li>
				<li class="size-m"><a href="#area05">急成長する「購入型」クラウドファンディング、５つの理由</a></li>
				<li class="size-s"><a href="#area03">クラウドファンディング、４つのタイプ</a></li>
				<li class="size-m"><a href="#area06">クラウドファンディングのリスク</a></li>
			</ul>
		</div>

		<div id="area01" class="clrboth">
			<h3>「クラウドファンディング」言葉の由来</h3>
			<p class="cf-img"><img src="{$smarty.const.C_ASSETS}images/guide/reason_cf.png" alt="Crowd Funding"></p>
			<p class="txt-set1">クラウドファンディングとは、「こんなモノやサービスを作りたい」「世の中の問題を、こんなふうに解決したい」といったアイデアやプロジェクトを持つ起案者（実行者）が、専用のインターネットサイトを通じて、世の中に呼びかけ共感した人から広く資金を集める方法です。</p>

			<div class="apch-box">
				<img class="apch-left" src="{$smarty.const.C_ASSETS}images/guide/approach_01.png" alt="一般的な資金調達">
				<img class="apch-right" src="{$smarty.const.C_ASSETS}images/guide/approach_02.png" alt="クラウドファンディング">
			</div>

		</div>
		<!-- ▲　area01 -->


		<div id="area02" class="clrboth">
			<h3>こんなに古くからある!資金集めの考え方</h3>
				<div class="img"><img src="{$smarty.const.C_ASSETS}images/guide/temple.jpg" alt="寺院の修繕"></div>
				<div class="tx">
					<p>インターネット決済が浸透し、世界で数多くのクラウドファンディングサイトが生まれています。日本では、2011年の東日本大震災をきっかけに、被災地復興事業の資金調達という形で一気に広まりました。<br>
					実は、クラウドファンディングの考え方や仕組みは、日本でも昔から存在していました。例えば寺などの改修工事。鎌倉幕府ができる直前の1180年、焼き討ちで焼失した東大寺と大仏の修復・再建のため、再興事業を指揮した重源は、全国各地を回り信者や有志から少額の寄付を集め、修復・再建を実現しました。この頃から、寺院や仏像などの新造・修復・再建のため庶民から広く寄付を求める「勧進」が広がりました。修繕が終わると、寄付者の名前が寺に記されることもありました。</p>
					<p class="bydb">2002年4月8日付朝日新聞大阪本社朝刊「奈良の大仏　ののちゃんの自由研究」などによる</p>
				</div>
		</div>
		<!-- ▲　area02 -->


		<div id="area03" class="clrboth">
			<h3>クラウドファンディング、4つのタイプ</h3>
			<p class="mgn40">クラウドファンディングは、資金や支援者へのリターン（特典）のあり方によって４つのタイプに分類されます。</p>
				<ul>
					<li><img src="{$smarty.const.C_ASSETS}images/guide/type_donate.png" alt="寄付型"></li>
					<li><img src="{$smarty.const.C_ASSETS}images/guide/type_invest.png" alt="投資型"></li>
					<li><img src="{$smarty.const.C_ASSETS}images/guide/type_loan.png" alt="融資型"></li>
					<li><img src="{$smarty.const.C_ASSETS}images/guide/type_purchase.png" alt="購入型"></li>
				</ul>

				<p class="bold">朝日新聞のクラウドファンディング「A‐port」は、<span class="col-blue">「購入型」のクラウドファンディングサイト</span>の一つです。</p>
				<p class="point">クラウドファンディングのアイデアは多岐にわたり、幅広い分野で活用されています。新しいテクノロジーを使った商品開発、映画製作や本の出版、ゲームやアプリの開発、アーティストのＣＤ制作やコンサート運営、伝統工芸品の制作、建築物の改修、懐かしのモノをよみがえらせる計画、先進医療の研究、企業の運転資金調達、新興国の貧困層への自立支援など。<br />
                A-portではプロジェクトの起案者を「実行者」、資金提供してプロジェクトを支援する人を「サポーター」と呼んでいます。資金援助することで、サポーターがもらえる特典を「リターン」と呼んでいます。<br>
                A-portでは目標金額を達成した場合のみ調達した資金を受け取る「達成時実行型（All or Nothing型）」と、目標金額に達成しない場合でも調達した資金を受け取ることができる「実行確約型」を用意しており、目的に応じて使い分けることができます。実行確約型の実行者は、掲載時にプロジェクトの実施を確約する必要があります。
				</p>
		</div>
		<!-- ▲　area03 -->


		<div id="area04" class="clrboth">
			<h3>拡大するクラウドファンディング市場</h3>
				<div class="tx">
					<p>業界専門の市場調査をする米マスソリューション社による2013年版のクラウドファンディング・インダストリー・リポートによると、世界の2012年のクラウドファンディング市場の規模は27億ドル（約2700億円）で前年比81％増と急伸し、うまく資金調達できたプロジェクトが100万件を超えたとされています。今後さらに成長傾向にあり、2013年には、51億ドル（約5100億円）規模と予測しています。</p>
					<h4 class="mgn30">クラウドファンディングで世界最高額 約1300万ドル</h4>
					<p>2014年8月、米クラウドファンディングサイト「Kickstarter（キックスターター）」で世界最高額の約1300万ドルを調達したのは、ブレンダーやスピーカー、電子機器が充電できるUSBポートなどを備えた多機能クーラーボックス「クーレスト・クーラー」。2012年5月に当時の最高額（約1030万ドル）を調達したスマートウォッチ「ぺブル」の記録を塗り替えました。（１ドル＝100円として計算）</p>
				</div>
				<img class="img" src="{$smarty.const.C_ASSETS}images/guide/illust-chart01.png" alt="クラウドファンディング・インダストリー・リポート">

					<h4 class="clrboth">今後さらなる盛り上がりが期待されるクラウドファンディング</h4>
					<p class="more-left">Kickstarterは2009年4月にアメリカの民間企業が立ち上げた、購入型サイトの草分け的存在で、現在世界最大のクラウドファンディングサイトです。サイトによると、これまでに7万件のプロジェクトに対し、700万人から計10億ドルの資金提供があったとされています。<br />
					日本でもサイト開設が相次ぎ、普及が見込まれています。2011年4月には「READYFOR？（レディーフォー）」が開設、同6月には「CAMPFIRE（キャンプファイアー）」、同7月には「MotionGallery（モーションギャラリー）」がオープンし、日本の市場を牽引してきました。また2013年8月、ＩＴ企業のサイバーエージェントも子会社を設立、「Makuake（マクアケ）」というサイトで参入し、市場の拡大が見込まれます。2014年2月の東京都知事選では、大手サイト「ShootingStar（シューティングスター）」で約740万円の資金を集めた候補者が出て話題になりました。<br />
					そのほか、扱うプロジェクトのジャンルを絞ったものなど、特色あるサイトが続々と登場しています。「寄付型」として日本最大規模を誇る「JapanGiving（ジャパンギビング）」や、映画などのコンテンツ・アート系のプロジェクトが充実している「MotionGallery（モーションギャラリー）」、ものづくりにかかわるプロジェクトに特化した「zenmono（ゼンモノ）」、地域や地元を応援するプロジェクトを扱う「FAAVO（ファーボ）」などがあります。</p>
					<p class="more-right">
					さらに、「kibidango（きびだんご）」、「WESYM（ウィシム）」、「GREENFUNDING（グリーンファンディング）」、「COUNTDOWN（カウントダウン）」など、続々とサイトが立ち上がり、今後のさらなる盛り上がりが期待されます。（2015年１月現在）<br />
					上記の多くは購入型のクラウドファンディングとなります。購入型は、「All or Nothing型」と「実行確約型」に分類されます。「All or Nothing型」は、目標金額を達成した場合のみ、資金を手にすることができます。目標金額に達成しなかった場合はプロジェクト不成立となります。資金調達はキャンセルされ、リターンの実行もされません。<br />
					「実行確約型」は、目標金額に達成しない場合でも、調達した資金を手にすることができるクラウドファンディングです。掲載時にプロジェクトの実施を確約している実行者が利用することができます。朝日新聞の「A‐port」は、「All or Nothing型」と「実行確約型」の２つを採用しています。</p>

		</div>
		<!-- ▲　area04 -->


		<div id="area05" class="clrboth">
			<h3>急成長する「購入型」クラウドファンディング、5つの理由</h3>
				<div class="tx">
					<h4>世界・国内ともに急成長／国や自治体も注目する購入型</h4>
					<p class="txt-set2">クラウドファンディング・インダストリー・リポートによると、クラウドファンディングの４タイプの中でも、「購入型」のプラットフォーム数は最も多く、世界的に中小企業が資金調達に採用しているため急成長を遂げているとされています。<br />
					日本では、購入型と寄付型は原則的に金融商品取引法の規制対象ではないため参入障壁が低いことや、個人や団体・企業が手軽に起案しやすいという特徴から、サイトの数・調達金額ともに近年急伸しています。<br />
					「2014年版中小企業白書」（中小企業庁）では、クラウドファンディングにより「個人が企業に対して直接出資することが可能になり、様々な理由で金融機関等からの資金調達が困難であった企業の資金調達の可能性が拡大」するとされています。<br />
					特に購入型への期待は大きく、大阪府などの地方自治体も地元の中小企業に対して、クラウドファンディングを用いた資金調達支援に乗り出しています。<br />
					なぜ購入型が注目されるのでしょうか。購入型の特徴をまとめました。</p>
				</div>

				<div id="construction" class="ac-wrap clboth">
				<dl class="acMenu">
						<dt><img class="icon-01" src="{$smarty.const.C_ASSETS}images/guide/icon_01.png">誰もがプレーヤーになれる</dt>
						<dd><p class="subttl">個人・団体・企業の大小を問わず誰でも起案者</p>
							<p>クラウドファンディングサイト運営者の審査を通れば、個人・団体・企業の大小を問わず誰でも起案者としてプロジェクトを立ち上げることができます。また支援者も、プロの投資家のようなプロセスを経ることなく、共感したプロジェクトにインターネットを通じて誰でも簡単に資金を投じることができます。</p>
						</dd>
					</dl>
					<dl class="acMenu">
						<dt><img class="icon-02" src="{$smarty.const.C_ASSETS}images/guide/icon_02.png">双方向のコミュニケーションで、芽生える仲間意識</dt>
						<dd><p class="subttl">思いを語ることで、「人対人」のつながりが生まれる</p>
							<p>起案者が顔や名前を出して思いを語ることで、「人対人」のつながりが生まれやすくなります。支援者は応援メッセージを送ったり、質問があればコメント欄などで尋ねたりでき、起案者と直接コミュニケーションを取ることができます。コミュニケーションツールとして、SNS（ソーシャルネットワーキングサービス）をうまく活用してプロジェクトを「拡散」させれば、より多くの支援者を獲得できる可能性もあります。</p></dd>
					</dl>
					<dl class="acMenu">
						<dt><img class="icon-03" src="{$smarty.const.C_ASSETS}images/guide/icon_03.png">ついのめり込んでしまうゲーム性</dt>
						<dd><p class="subttl">支援者をわくわくさせることができるかが腕の見せどころ</p>
							<p>クラウドファンディングには様々なゲーム性が組み込まれています。支援者としては、自分が応援しているプロジェクトの資金獲得状況がアップデートされるため、期間内に目標額を達成できるかどうかをハラハラしながら見守ったり、時には、いてもたってもいられずＳＮＳなどで知り合いに支援を呼びかけてみたり。<br>起案者は、活動報告やストーリーをリアルタイムに報告することで、いかに支援者をわくわくさせることができるかが腕の見せどころです。</p></dd>
					</dl>
					<dl class="acMenu">
						<dt><img class="icon-04" src="{$smarty.const.C_ASSETS}images/guide/icon_04.png">魅力的なリターン（特典）</dt>
						<dd><p class="subttl">特別な魅力があるものほど支援者の心をつかむ</p>
							<p>「購入型」のリターン内容は、起案者が決められるため、支援者への魅力的なリターンを用意することは、プロジェクト成功の一つの鍵と言えるかもしれません。「完成した商品を先行して購入できる」「映画にあなたの名前が刻まれる」「あなただけの演奏会」「映画に出演できる」など多岐に及び、ほかにはない、特別な魅力があるものほど支援者の心をつかむことができます。</p></dd>
					</dl>
					<dl class="acMenu" >
						<dt><img class="icon-05" src="{$smarty.const.C_ASSETS}images/guide/icon_05.png">ファン獲得・PR・マーケティング、思わぬ効果も</dt>
						<dd><p class="subttl">プロジェクト自体がニュースやＳＮＳなどで話題にのぼることも</p>
							<p>必要な資金を調達するという目的以外にも、様々な効果が期待できます。支援者は、夢やアイデア、プロジェクトを応援するファンとして、起案者の心強い味方になってくれることでしょう。また、「こんなに面白いアイデアが登場」「あの企業がクラウドファンディングで資金調達呼びかけ」「目標の何倍もの資金を調達」など、プロジェクト自体がニュースやＳＮＳなどで話題にのぼることも少なくありません。思わぬＰＲ効果を生む可能性もあります。また、商品開発などのプロジェクトの場合、どういった層から何人の支援者が集まるかによって、開発前のマーケティング調査に役立つこともあります。</p></dd>
					</dl>
				</div>
				<!--/ac-wrap-->


		</div>
		<!-- ▲　area05-->


		<div id="area06" class="clrboth">
			<h3>クラウドファンディングのリスク</h3>
			<div class="more-left">
				<h4>支援者にとってのリスク</h4>
				<p>世界の例では、目標額に達しても、資金面や技術面において起案者の当初の見通しが甘かったなどの理由でプロジェクトが完遂されず、支援者にリターンが用意できなかったケースがあります。その場合は、見通しを誤り、約束した商品やサービスを作ることができなかった起案者に責任が生じます。
クラウドファンディングは新しい資金調達の概念のため、こういったリスクから支援者を守る制度はまだ整備されていません。リスクがあることを認識し、支援する前に、メールやコメントなどで質問するなどして、疑問や懸念点を払拭しておく必要があるといえます。
</p>
			</div>
			<div class="more-right">
				<h4>起案者にとってのリスク</h4>
				<p>起案者は支援金額が目標に達したら、責任を持ってプロジェクトを遂行しなければなりません。資金を集めても万が一プロジェクトが頓挫すれば、自身が責任を負うことになります。目標金額の設定や、プロジェクトの計画、戦略、リソースに不備がないかどうか、各サイトに起案する前に十分企画を練る必要があります。また、商品のイメージが違う、不良品だったという場合も起案者に責任が生じます。
また、スケジュールが遅れたり、リターンが間に合わなかったりする場合も考えられます。日頃支援者の方々に対して十分なコミュニケーションを取り、こまめに経過報告をすることで、信頼関係を維持することも大切です。
</p>
			</div>

		</div>
		<!-- ▲　area06 -->
	</div>
	<!-- ▲　cf-container -->
	<p class="bydb">参考文献：山本 純子著「入門クラウドファンディング スタートアップ、新規プロジェクト実現のための資金調達法」（日本実業出版社）</p>

	</div>
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/lib/jquery-1.10.2.min.js"></script>
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script src="{$smarty.const.C_ASSETS}js/guide.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/lib/accordion.js"></script>
{literal}
<script type="text/javascript">
$(function(){
	$('a[href^=#]').click(function(){
		var speed = 300;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$("html, body").animate({scrollTop:position}, speed, "swing");
		return false;
	});
});
</script>


<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
