{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/signin.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
<script type='text/javascript'>
{literal}
function kiyaku_check(location_str){
	if(document.entryFrm.rulesParticipant.checked){ // 規約にチェックされているかチェック
		location.href=location_str;
	}else{
		alert("利用規約を確認してください");
	}
}
{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div id="wrapper">
  <div id="signIN">
   <div class="userEntry">
    <h2><img src="{$smarty.const.C_ASSETS}images/logo_aport_title.png" alt="A-port"><strong>会員登録</strong></h2>

	{if $err_msg.top}
		<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
	{/if}
	<form id="entryFrm" name="entryFrm" action="" method="post">
		<dl>
		<dt>氏名 (必須)</dt>
		<dd>
		  <input name="name_1" id="name_1" value="{$input_data.name_1}" maxlength="100" size="10" placeholder="姓"/>
			{if $err_msg.name_1}
				<p class="error" style="color:red;">※{$err_msg.name_1}</p>
			{/if}
		  <input name="name_2" id="name_2" value="{$input_data.name_2}" maxlength="100" size="10" placeholder="名"/>
			{if $err_msg.name_2}
				<p class="error" style="color:red;">※{$err_msg.name_2}</p>
			{/if}
		</dd>
		<dt>ニックネーム (任意)</dt>
		<dd>
		  <input id="nickname" name="nickname" type="text" value="{$input_data.nickname}"/>
			{if $err_msg.nickname}
				<p class="error" style="color:red;">※{$err_msg.nickname}</p>
			{/if}
		  <p class="note">※全角可20文字以内</p>
		  <p class="mT10">
		  <input name="ml_flg" type="hidden" value="0" />
		  <input id="ml_flg" name="ml_flg" type="checkbox" value="1" />
		  表示名に使う<span class="note">（あなたの名前がニックネームで表示されます）</span>
		  </p>
	    </dd>
		<dt>メールアドレス (必須)</dt>
		<dd>
		  <input maxlength="100" size="40" name="email" id="email" value="{$input_data.email}"/>
			{if $err_msg.email}
				<p class="error" style="color:red;">※{$err_msg.email}</p>
			{/if}
		</dd>
		<dt>メールアドレスの確認 (必須)</dt>
		<dd>
		  <input maxlength="100" size="40" name="email2" id="email2" value="{$input_data.email2}"/> 
			{if $err_msg.email2}
				<p class="error" style="color:red;">※{$err_msg.email2}</p>
			{/if}
		  <p class="note">※確認のため、もう一度メールアドレを入力してください。</p>
		</dd>
		<dt>パスワード (必須)</dt>
		<dd>
		  <input type="password" maxlength="100" size="40" name="password" id="password" value="{$input_data.password}"/>
			{if $err_msg.password}
				<p class="error" style="color:red;">※{$err_msg.password}</p>
			{/if}
		  <p class="note">（半角英数字8～12文字）</p>
		</dd>
		<dt>パスワードの確認 (必須)</dt>
		<dd>
		  <input type="password" maxlength="100" size="40" name="password2" id="password2" value="{$input_data.password2}"/>
			{if $err_msg.password2}
				<p class="error" style="color:red;">※{$err_msg.password2}</p>
			{/if}
		  <p class="note">※確認のため、もう一度パスワードを入力してください。</p>
		</dd>
		</dl>
		<p >
		<input type="checkbox" name="rulesParticipant" value="1" {if $input_data.rulesParticipant}checked="checked" {/if} />
		<a class="notice" href="{$pagelink_usage_project}"  target="_blank" >利用規約</a> に同意する
		</p>
		{if $err_msg.rulesParticipant}
			<span class="error" style="color:red;">※{$err_msg.rulesParticipant}</span>
		{/if}
		<p class="notice">入力内容をご確認のうえ、「送信」ボタンをクリックしてください。</p>
		<div>
		 <input type="submit" class="btnSubmit" value="送信" id="sendForm" name="sendForm" onClick="return confirm('入力した内容を送信します。よろしいですか？');" />
		</div>
	</form>
    </div>
  </div>
  <!-- #signIN --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
