{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
<style type="text/css">
{literal}
.hide{display:none;}.invisible{visibility:hidden;}.overflow{overflow:hidden;}
.clear{display:block;clear:both;height:0;overflow:hidden;}

/* formbox */
#drafting .succeed{background:url(assets/images/check_ok.gif) no-repeat -105px 0;}
#drafting .focus{color:#999;line-height:22px;*line-height:20px;}
#drafting .null,#drafting .error{color:red;line-height:22px;*line-height:20px;}
{/literal}
</style>

</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul class="tab clearfix mb15">
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting1.php?p_no={$input_data.no}">基本情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">リターン内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting3.php?p_no={$input_data.no}">プロジェクト内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting4.php?p_no={$input_data.no}" >SNSとPR</a></li>
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">起案者情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting6.php?p_no={$input_data.no}">プレビュー</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
			<div id="fragment-5" class="clearfix">
				<div class="draftingLeft">
					{if $err_msg.top}
							<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
					{/if}
					<h3><span>1</span>プロフィール画像</h3>
					<p>プロフィール画像（200×200px）を設定してください。</p>
					{if $smarty.session.TMP_FUP13!=""}
						<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP13}" width=200 height=200 />
					{elseif $input_data.profile_img!=""}
						<img src="{$smarty.const.C_IMG_ACTRESS}{$input_data.profile_img}" width=200 height=200 />
						<input type="hidden" name="new_profile_img" value="{$input_data.profile_img}" />
					{/if}
					<p><input type="file" name="up_file3" value="1" size="40" class="" aria-invalid="false" /></p>
					{if $err_msg.profile_img}
							<p class="errorTitle" style="color:red;">※{$err_msg.profile_img}</p>
					{/if}
					<p>ここにプロフィール画像が入ります</p>
					<h3><span>2</span>氏名・団体名</h3>
					<p>氏名の場合はフルネームで　団体の場合は正式名称をご記入ください。</p>
					<p><input id="full_name" name="full_name" size="45" type="text" value="{$input_data.full_name}"/></p>
					{if $err_msg.full_name}
							<p class="errorTitle" style="color:red;">※{$err_msg.full_name}</p>
					{/if}
					<label id="full_name_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="full_name_error"></label>
					<h3><span>3</span>自己紹介</h3>
					<p>自己（団体）紹介を入力してください。（300文字以内）</p>
					<p><textarea cols="50" id="introduce" maxlength="300" name="introduce" rows="10">{$input_data.introduce}</textarea><br></p>
					{if $err_msg.introduce}
							<p class="errorTitle" style="color:red;">※{$err_msg.introduce}</p>
					{/if}
					<label id="introduce_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="introduce_error"></label>
					<h3><span>4</span>金融機関情報</h3>
					<p>集まった資金をお引き渡しするための金融口座を入力してください。</p>
					<table>
						<tbody>
							<tr>
								<th scope="row">金融機関名</th>
								<td>
									<label><input type="radio" name="bank_name" value="みずほ銀行" {if $input_data.bank_name=='みずほ銀行'}checked="checked"{else}{/if}/>みずほ銀行</label>
									<label><input type="radio" name="bank_name" value="三菱東京UFJ銀行" {if $input_data.bank_name=='三菱東京UFJ銀行'}checked="checked"{else}{/if}/>三菱東京UFJ銀行</label>
									<label><input type="radio" name="bank_name" value="三井住友銀行" {if $input_data.bank_name=='三井住友銀行'}checked="checked"{else}{/if}/>三井住友銀行</label> 
									<label><input type="radio" name="bank_name" value="りそな銀行" {if $input_data.bank_name=='りそな銀行'}checked="checked"{else}{/if}/>りそな銀行</label> 
									<label><input type="radio" name="bank_name" value="ゆうちょ銀行" {if $input_data.bank_name=='ゆうちょ銀行'}checked="checked"{else}{/if}/>ゆうちょ銀行</label><br>
									<span class="cautionMini">※上記以外の場合は銀行名を正しくご記入ください。</span><br>
									<input id="bank_name_txt" name="bank_name_txt" size="45" type="text" {if $input_data.bank_name=='みずほ銀行'||$input_data.bank_name=='三菱東京UFJ銀行'||$input_data.bank_name=='三井住友銀行'||$input_data.bank_name=='りそな銀行'||$input_data.bank_name=='ゆうちょ銀行'} value="" {else} value="{$input_data.bank_name}" {/if}/>
									{if $err_msg.bank_name_txt}
											<p class="errorTitle" style="color:red;">※{$err_msg.bank_name_txt}</p>
									{/if}
									<label id="bank_name_txt_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="bank_name_txt_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">支店名</th>
								<td>
								<input id="branch_bank" name="branch_bank" size="45" type="text" value="{$input_data.branch_bank}" />
								{if $err_msg.branch_bank}
										<p class="errorTitle" style="color:red;">※{$err_msg.branch_bank}</p>
								{/if}
								<label id="branch_bank_succeed" class="blank"></label>
								<span class="clear"></span>
								<label id="branch_bank_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">支店コード</th>
								<td>
									<input id="branch_bank_code" name="branch_bank_code" maxlength="30" size="45" type="text" value="{$input_data.branch_bank_code}" /><br>
									<span class="cautionMini">※支店コードを半角数字で設定してください。 </span>
									{if $err_msg.branch_bank_code}
											<p class="errorTitle" style="color:red;">※{$err_msg.branch_bank_code}</p>
									{/if}
									<label id="branch_bank_code_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="branch_bank_code_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">口座種別</th>
								<td>
									<label><input type="radio" id="card_type" name="card_type" value="1" {if $input_data.card_type=='1'}checked="checked"{else}{/if}/>普通</label>
									<label><input type="radio" id="card_type" name="card_type" value="2" {if $input_data.card_type=='2'}checked="checked"{else}{/if}/>当座</label>
									<label><input type="radio" id="card_type" name="card_type" value="3" {if $input_data.card_type=='3'}checked="checked"{else}{/if}/>貯蓄</label>
									{if $err_msg.card_type}
											<p class="errorTitle" style="color:red;">※{$err_msg.card_type}</p>
									{/if}
								 </td>
							</tr>
							<tr>
								<th scope="row">口座番号</th>
								<td>
									<input id="card_number" name="card_number" maxlength="30" size="45" type="text" value="{$input_data.card_number}" /><br>
									<span class="cautionMini">※ゆうちょ銀行を登録される場合、振込専用番号をご入力ください。</span>
									{if $err_msg.card_number}
											<p class="errorTitle" style="color:red;">※{$err_msg.card_number}</p>
									{/if}
									<label id="card_number_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="card_number_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">口座名義人（カナ）</th>
								<td>
									<input id="card_owner" name="card_owner" size="45" type="text" value="{$input_data.card_owner}" /><br>
									<span class="cautionMini">※通帳に記載された名称を全角カタカナでご記入ください。<br>※法人格は名称で入力してください。</span>
									{if $err_msg.card_owner}
											<p class="errorTitle" style="color:red;">※{$err_msg.card_owner}</p>
									{/if}
									<label id="card_owner_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="card_owner_error"></label>
								</td>
							</tr>
						</tbody>
					</table>
					<ul class="formStep">
						<li class="formPrev"><a href="{$smarty.const.ROOT_URL}ja/drafting4.php?p_no={$input_data.no}">戻る</a></li>
						<li class="formNext"><input id="next_f5" value="次へ" type="submit" name="d5" style="float:right;"//>
						<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-5  -->
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}

<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.form.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
