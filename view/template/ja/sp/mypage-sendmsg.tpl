{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<div id="myMailBox">
		<h2>質問を送る</h2>
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
				{if $err_msg.top}
						<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
				{/if}
				<div class="messageForm">
					<dl>
					<dt>宛先</dt>
					<dd>
						<div class="sendUser set">
							<div class="userIcBr"><img src="{$smarty.const.C_IMG_ACTRESS}{$maker_info.profile_img}" class="userIcon"></div>
							<p class="userName">{$maker_info.full_name}</p>
							<p class="userDescription">実行者<span></span></p>
						</div>
					</dd>
					<dt>本文</dt>
					<dd>
						<textarea cols="80" id="message_body" name="message_body" rows="10"></textarea>
						<p class="note">※10000文字以内</p>
						{if $err_msg.message_body}
							<span class="red" style="color:red;">※{$err_msg.message_body}</span><br />
						{/if}
					</dd>
					</dl>
					<input id="to_user_id" name="to_user_id" type="hidden" value="{$input_data.to_user_id}">
					<div class="actions">
          <input name="sbm_back" type="submit" value="戻る" />
					<input data-disable-with="遷移中…" name="sbm_update" type="submit" value="確認する">
					</div>
				</div>
			</form>
	</div>
	<!-- #myMailEdit --> 
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
