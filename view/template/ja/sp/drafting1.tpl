{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />

<script src="{$smarty.const.C_ASSETS}js/jquery-ui.min.js"></script>
<script src="{$smarty.const.C_ASSETS}js/jquery-ui-timepicker-addon.js"></script>
<script src="{$smarty.const.C_ASSETS}js/jquery-ui-timepicker-ja.js"></script>
<script src="{$smarty.const.C_ASSETS}js/jquery-ui-sliderAccess.js"></script>
<link href="{$smarty.const.C_ASSETS}css/smoothness/jquery-ui.css" media="all" rel="stylesheet" />
<style type="text/css">
{literal}
.hide{display:none;}.invisible{visibility:hidden;}.overflow{overflow:hidden;}
.clear{display:block;clear:both;height:0;overflow:hidden;}

/* formbox */
#drafting .succeed{background:url(assets/images/check_ok.gif) no-repeat -105px 0;}
#drafting .focus{color:#999;line-height:22px;*line-height:20px;}
#drafting .null,#drafting .error{color:red;line-height:22px;*line-height:20px;}
{/literal}
</style>
<script type='text/javascript'>
{literal}
$(document).ready(function () {
	$("#project_record_date").datepicker({
		minDate: new Date().toLocaleString( ), 
		beforeShowDay: $.datepicker.noWeekends,
		dateFormat: "yy/mm/dd",
		timeFormat: 'hh:mm:ss'
	});
	$("#invest_limit").datepicker({
		minDate: new Date().toLocaleString( ), 
		beforeShowDay: $.datepicker.noWeekends,
		dateFormat: "yy/mm/dd"
	});
});
{/literal}
</script>
<script type="text/javascript">
{literal}
function showType1()
{
	document.getElementById("p_type1").style.display="inline";
	document.getElementById("p_type2").style.display="none";
}
function showType2()
{
	document.getElementById("p_type1").style.display="none";
	document.getElementById("p_type2").style.display="inline";
}
{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul class="tab clearfix mb15">
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">基本情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">リターン内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting3.php?p_no={$input_data.no}">プロジェクト内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting4.php?p_no={$input_data.no}" >SNSとPR</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting5.php?p_no={$input_data.no}" >起案者情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting6.php?p_no={$input_data.no}">プレビュー</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
			<!--LV5 fragment-1 Start -->
			<div id="fragment-1" class="clearfix">
				<!--LV6 draftingLeft Start -->
				<div class="draftingLeft">
					{if $err_msg.top}
							<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
					{/if}
					<!--LV7 プロジェクトタイプ -->
					<h3><span>1</span>プロジェクトタイプ（資金調達法）　プロジェクトタイプを選択してください。</h3>
					<ul>
						<li>
							<label>
								<input type="radio" name="project_type" id="project_type" onclick="showType1();" value="1" {if $input_data.project_type=='1'}checked="checked"{else}{/if}/>
								達成時実行型（All or Nothing）　&nbsp;募集期間80日以内
							</label>
						</li>
						<li>
							<label>
								<input type="radio" name="project_type" id="project_type"  onclick="showType2();" value="2"  {if $input_data.project_type=='2'}checked="checked"{else}{/if}/>
								実行確約型　　　　　　　　　　　募集期間120日以内
							</label>
						</li>
					</ul>
					<label id="project_type_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="project_type_error"></label>
					<p><a href="{$pagelink_start}#project_type" target="_blank">※ 詳細はこちら</a></p>
					<!--LV7 プロジェクトタイトル -->
					<h3><span>2</span>プロジェクトタイトル　プロジェクトのタイトルを記入してください（３０文字～４０文字）</h3>
					<p>
						<input id="public_title" name="public_title" size="45" type="text" value="{$input_data.public_title}"/>
					</p>
					<label id="public_title_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="public_title_error"></label>
					<p>人の関心を引き、一目でどんなプロジェクトかが、わかるタイトルをつけましょう。</p>
					<!--LV7 サムネイル画像 -->
					<h3><span>3</span>キービジュアル（メイン画像）　プロジェクトのキービジュアルを選択してください。</h3>
					{if $smarty.session.TMP_FUP11!=""}
						<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP11}" width=608 height=435 />
					{elseif $input_data.cover_img!=""}
						<img src="{$smarty.const.C_IMG_PROJECT}{$input_data.cover_img}" width=608 height=435 />
						<input type="hidden" name="cover_img" value="{$input_data.cover_img}" />
					{/if}
					<p><input type="file" name="up_file1"  size="40"aria-invalid="false"/></p>
					<p>写真のクオリティによって、支援金額に影響があるため、サムネイル画像にはこだわりの一枚をアップロードしてください。</p>
					<p>・画像サイズ 608 × 435px</p>
					<p>・2MB以下で16:9の高品質な画像ファイル（JPEG、PNG形式）</p>
					<p>・キャッチコピーなどを入れて、よりわかりやすくしましょう</p>
					{if $err_msg.up_file1}
						<span class="red" style="color:red;">※{$err_msg.up_file1}</span><br />
					{/if}
					<!--LV7 目標金額 -->
					<h3><span>4</span>目標金額　　目標金額を半角数字で設定してください。</h3>
					<p><input id="wish_price" name="wish_price" size="45" type="text" value="{$input_data.wish_price}"/>円（税込）</p>
					{if $err_msg.wish_price}
						<span class="red" style="color:red;">※{$err_msg.wish_price}</span><br />
					{/if}
					<label id="wish_price_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="wish_price_error"></label>
					<!--LV7 動画 -->
					<h3><span>5</span>メイン動画　　キービジュアルに埋め込まれる動画です</h3>
					<p>動画がある場合は、以下の動画サービスから選択し、動画のURLを記入してください。<br>
						<span class="cautionMini"> ※URLには&amp;を入れないでください。動画が再生されなくなります。</span></p>
					<label><input type="radio" name="movie_type" value="1" {if $input_data.movie_type=='1'}checked="checked"{else}{/if} />YouTube</label>
					<label><input type="radio" name="movie_type" value="2"  {if $input_data.movie_type=='2'}checked="checked"{else}{/if} />Vimemo</label>
					<label><input type="radio" name="movie_type" value="0"  {if $input_data.movie_type=='0'}checked="checked"{else}{/if} />なし</label>
					<p><input id="movie_url" name="movie_url" size="45" type="text" value="{$input_data.movie_url}"/></p>
					{if $err_msg.movie_url}
						<span class="red" style="color:red;">※{$err_msg.movie_url}</span><br />
					{/if}
					<label id="movie_url_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="movie_url_error"></label>
					<p>・高品質（720p以上推奨）</p>
					<p>・動画の長さが1分30秒〜3分</p>
					<!--LV7 カテゴリー -->
					<h3><span>6</span>カテゴリー</h3>
					<p>カテゴリーを１つ選択してください。</p>
					<p><select name="category_no">{html_options options=$array_category selected=$input_data.category_no }</select></p>
					<!--LV7 期間設定 -->
					<h3><span>7</span>掲載期間設定</h3>
					<p id="p_type1" {if $input_data.project_type=='1'}style="display:inline;"{else}style="display:none;"{/if}>達成時実行型（All or Nothing）を選択しています。掲載期間は最大80日です。
					<p id="p_type2" {if $input_data.project_type=='2'}style="display:inline;"{else}style="display:none;"{/if}>実行確約型を選択しています。掲載期間は最大120日です。
					<p>
						<span class="cautionMini">
						＊プロジェクト終了日は、平日を指定してください。<br>
						＊プロジェクト開始日は、審査を申請頂く日から 7日後 以降に設定してください<br>
						＊設定した期間を、掲載後に変更する事はできません。短縮のみ認められます。<br>
						＊特典にイベント招待券などが含まれる場合、掲載終了日とプロジェクト実施日の間に余裕を持たせましょう。
						特典実施までのスケジュールがきつくなってしまうおそれがあります。<br>
						＊他プラットフォームとのクラウドファンディングの併用はお断りしております
						</span>
					</p>
					<p><input id="project_record_date" name="project_record_date" size="25" type="text" value="{$input_data.project_record_date|date_format:"%Y/%m/%d"}"/>〜<input id="invest_limit" name="invest_limit" size="25" type="text" value="{$input_data.invest_limit|date_format:"%Y/%m/%d"}"/></p>
					{if $err_msg.project_record_date}
							<p class="errorTitle" style="color:red;">※{$err_msg.project_record_date}</p>
					{/if}
					<!--LV7 お礼メッセージと画像 -->
					<h3><span>8</span>お礼メッセージと画像　　支援者が申し込みを確定した際に表示されます。</h3>
					<p>
						175×175pxのJPEG,PNG,GIF画像ファイル<br>
						<span class="cautionMini"> ※画像ファイルの横サイズが異なる場合、歪んで表示されてしまうのでご注意ください<br>
						※写真は実行者自身の顔写真だと、共感されやすく、シェアされやすい傾向があります
						</span>
					</p>
					<p>
						{if $smarty.session.TMP_FUP12!=""}
							<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP12}" width=175 height=175 />
						{elseif $input_data.thanks_image!=""}
							<img src="{$smarty.const.C_IMG_PROJECT}{$input_data.thanks_image}" width=175 height=175 />
							<input type="hidden" name="thanks_image" value="{$input_data.thanks_image}" />
						{/if}
						<br/><input type="file" name="up_file2"  size="40"aria-invalid="false"/>
					</p>
					{if $err_msg.up_file2}
						<span class="red" style="color:red;">※{$err_msg.up_file2}</span><br />
					{/if}
					<p><textarea cols="50" id="thanks_msg" maxlength="300" name="thanks_msg" rows="10" >{$input_data.thanks_msg}</textarea></p>
					<label id="thanks_msg_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="thanks_msg_error"></label>
					<h3><span>9</span>希望URL</h3>
					<p>URLに使用するＩＤです(https://a-port.asahi.com/projects/xxxx のxxxxがに当ります)。</p>
					<p>半角英数字、_(アンダースコア)、-(ハイフン)のみで、6文字以上20文字以内でご記入ください。</p>
          <p>{$smarty.const.ROOT_URL}projects/ <input id="hope_url" name="hope_url" size="45" type="text"  value="{$input_data.hope_url}"/></p>
					{if $err_msg.hope_url}
						<span class="red" style="color:red;">※{$err_msg.hope_url}</span><br />
					{/if}
					<!--LV7 次へ -->
					<!--<div id="ui-tab2"><ul class="formStep"><li class="formNext"><a href="#fragment-2">次へ</a></li></ul></div>-->
					<ul class="formStep">
					<li class="formNext"><input id="next_f1" value="次へ" type="submit" name="d1" style="float:right;"/>
					<div class="clearfix"></div>
					<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>
					</li>
					
					</ul>
					
				</div>
				<!--/.LV6 draftingLeft End -->
				<!--LV6 draftingRight Start -->

				<!--/.LV6 draftingRight End -->
			</div>
			<!--/LV5 fragment-1 End -->
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}

<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.form.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
