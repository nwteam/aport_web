{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/signin.css">

<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<div id="wrapper">
  <div id="signIN">
    <h2><strong>登録が完了しました</strong></h2>
    <h4>ログインしてお楽しみください！</h4>
    <p id="membershipMessage"><a href="{$pagelink_login}"><input class="btnLogin" name="sbm_login" id="btnLogin" type="button" value="ログインする" /></a></p>
  </div>
  <!--/#contents--> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
