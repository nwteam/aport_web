{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />

<link href="{$smarty.const.C_ASSETS}css/pages/ui.tabs.css" media="all" rel="stylesheet" />
<!-- HTML エディタ -->
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/nicEdit.js"></script>
<!-- <link rel="stylesheet" href="{$smarty.const.C_ASSETS}js/redactor/redactor.css"/>
<script src="{$smarty.const.C_ASSETS}js/redactor/redactor.js"></script>-->
<script type="text/javascript">
{literal}
//	bkLib.onDomLoaded(function() {
//		new nicEditor({maxHeight : 600}).panelInstance('project_text');
	//$('#project_text').redactor({imageUpload:'/modules/upload.php'});
//});
//$(function(){
//	CKEDITOR.replace( '#project_text' );
//});
{/literal}
</script>
<script src="{$smarty.const.C_ASSETS}js/jquery-ui.min.js"></script>
<script src="{$smarty.const.C_ASSETS}js/jquery-ui-timepicker-addon.js"></script>
<script src="{$smarty.const.C_ASSETS}js/jquery-ui-timepicker-ja.js"></script>
<script src="{$smarty.const.C_ASSETS}js/jquery-ui-sliderAccess.js"></script>
<link href="{$smarty.const.C_ASSETS}css/smoothness/jquery-ui.css" media="all" rel="stylesheet" />
<style type="text/css">
{literal}
.hide{display:none;}.invisible{visibility:hidden;}.overflow{overflow:hidden;}
.clear{display:block;clear:both;height:0;overflow:hidden;}

/* formbox */
#drafting .succeed{background:url(assets/images/check_ok.gif) no-repeat -105px 0;}
#drafting .focus{color:#999;line-height:22px;*line-height:20px;}
#drafting .null,#drafting .error{color:red;line-height:22px;*line-height:20px;}
{/literal}
</style>
<script src="{$smarty.const.C_ASSETS}js/ui.core.js"></script>
<script src="{$smarty.const.C_ASSETS}js/ui.tabs.min.js"></script>

<script type='text/javascript'>
{literal}
$(function(){
	$("#project_record_date").datepicker({
		minDate: new Date().toLocaleString( ), 
		beforeShowDay: $.datepicker.noWeekends,
		dateFormat: "yy/mm/dd",
		timeFormat: 'hh:mm:ss'
	});
	$("#invest_limit").datepicker({
		minDate: new Date().toLocaleString( ), 
		beforeShowDay: $.datepicker.noWeekends,
		dateFormat: "yy/mm/dd"
	});
});
$(document).ready(function () {
	$('#ui-tab > ul').tabs({ fx: { opacity: 'toggle', duration: 'normal'  } });
{/literal}
	{if ($preview_flg=="1")}
{literal}
		$('#ui-tab > ul').tabs('enable', 5);
		$('#ui-tab > ul').tabs('select', 5);
{/literal}
	{else}
{literal}
		$('#ui-tab > ul').tabs('disable', 5); 
{/literal}
	{/if}
{if $err_msg.profile_img}
{literal}
		$('#ui-tab > ul').tabs('select', 4);
{/literal}
{/if}
{literal}
	// Disable プレビュータブ
	$(".tab_link").click(function(){
		$('#ui-tab > ul').tabs('disable', 5); 
		return false;
	});

	// fragment-1 次へ
	$("#next_f1").click(function(){
		$('#ui-tab > ul').tabs('select', 1); 
		return false;
	});
	// fragment-2 戻る
	$("#prv_f2").click(function(){
		$('#ui-tab > ul').tabs('select', 0);
		return false;
	});
	// fragment-2 次へ
	$("#next_f2").click(function(){
		$('#ui-tab > ul').tabs('select', 2);
		return false;
	});
	// fragment-3 戻る
	$("#prv_f3").click(function(){
		$('#ui-tab > ul').tabs('select', 1);
		return false;
	});
	// fragment-3 次へ
	$("#next_f3").click(function(){
		$('#ui-tab > ul').tabs('select', 3);
		return false;
	});
	// fragment-4 戻る
	$("#prv_f4").click(function(){
		$('#ui-tab > ul').tabs('select', 2);
		return false;
	});
	// fragment-4 次へ
	$("#next_f4").click(function(){
		$('#ui-tab > ul').tabs('select', 4);
		return false;
	});
	// fragment-5 戻る
	$("#prv_f5").click(function(){
		$('#ui-tab > ul').tabs('select', 3);
		return false;
	});
	// fragment-5 次へ
	$("#next_f5").click(function(){
		//$('#ui-tab > ul').tabs('enable', 5);
		//$('#ui-tab > ul').tabs('select', 5);
		document.frm_tab.submit();
		//return false;
	});
	// fragment-6 戻る
	$("#prv_f6").click(function(){
		$('#ui-tab > ul').tabs('select', 4);
		$('#ui-tab > ul').tabs('disable', 5); 
		return false;
	});
	// fragment-6 変更を提出
	$("#next_f6").click(function(){
		document.frm_tab.submit();
	});

});
{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul id="tab">
				<li id="tab01"><a class="tab_link" href="#fragment-1">基本情報</a></li>
				<li id="tab02"><a class="tab_link" href="#fragment-2">リターン内容</a></li>
				<li id="tab03"><a class="tab_link" href="#fragment-3">プロジェクト内容</a></li>
				<li id="tab04"><a class="tab_link" href="#fragment-4">SNSとPR</a></li>
				<li id="tab05"><a class="tab_link" href="#fragment-5">実行者情報</a></li>
				<li id="tab06"><a class="tab_link" href="#fragment-6">プレビュー</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
			<!--LV5 fragment-1 Start -->
			<div id="fragment-1" class="clearfix">
				<!--LV6 draftingLeft Start -->
				<div class="draftingLeft">
					{if $err_msg.top}
							<p class="errorTitle" style="color:red;">※{$err_msg.top}</p>
					{/if}
					<!--LV7 プロジェクトタイプ -->
					<h3><span>1</span>プロジェクトタイプ</h3>
					<p>プロジェクトタイプを選択してください。</p>
					<ul>
						<li>
							<label>
								<input type="radio" name="project_type" id="project_type" value="1" {if $input_data.project_type=='1'}checked="checked"{else}{/if}/>
								達成時実行型（All or Nothing型）
							</label>
						</li>
						<li>
							<label>
								<input type="radio" name="project_type" id="project_type" value="2"  {if $input_data.project_type=='2'}checked="checked"{else}{/if}/>
								実行確約型
							</label>
						</li>
					</ul>
					<label id="project_type_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="project_type_error"></label>
					<p><a href="{$pagelink_start}#project_type" target="_blank">※ 詳細はこちら</a></p>
					<!--LV7 プロジェクトタイトル -->
					<h3><span>2</span>プロジェクトタイトル</h3>
					<p>プロジェクトのタイトルを記入してください。（30文字〜40文字）</p>
					<p>
						<input id="public_title" name="public_title" size="45" type="text" value="{$input_data.public_title}"/>
					</p>
					<label id="public_title_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="public_title_error"></label>
					<p>・サービスや商品の名前が含まれている</p>
					<p>・興味を惹くようなキーワード</p>
					<p>例:みんなで作ろう！世界があっと驚く画期的なアクションゲーム</p>
					<!--LV7 サムネイル画像 -->
					<h3><span>3</span>サムネイル画像</h3>
					<p>プロジェクトのサムネイル画像を選択してください。</p>
					{if $result_messages.cover_img}
						<span class="red">{$result_messages.cover_img}</span><br />
					{/if}
					{if $smarty.session.TMP_FUP11!=""}
						<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP11}" width=587 height=419 />
					{elseif $input_data.cover_img!=""}
						<img src="{$smarty.const.C_IMG_PROJECT}{$input_data.cover_img}" width=587 height=419 />
						<input type="hidden" name="cover_img" value="{$input_data.cover_img}" />
					{/if}
					<p><input type="file" name="up_file1"  size="40"aria-invalid="false"/></p>
					<p>写真のクオリティによって、支援金額に影響があるため、サムネイル画像にはこだわりの一枚をアップロードしてください。</p>
					<p>・画像サイズ 608 × 435px</p>
					<p>・高品質 JPEG80以上（Photoshop出力時）で16:9の画像</p>
					<p>・タイトルやキーワード等、文字を含んだメッセージ</p>
					<!--LV7 目標金額 -->
					<h3><span>4</span>目標金額</h3>
					<p>目標金額を半角数字で設定してください。 <br>
						<span class="cautionMini">※目標金額がクラウドファンディングのプロジェクトとしてふさわしくない場合、審査を落とすケースがあります。ご了承ください。</span>
					</p>
					<p><input id="wish_price" name="wish_price" size="45" type="text" value="{$input_data.wish_price}"/>円（税込）</p>
					<label id="wish_price_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="wish_price_error"></label>
					<!--LV7 動画 -->
					<h3><span>5</span>動画</h3>
					<p>動画がある場合は、以下の動画サービスから選択し、動画のURLを記入してください。<br>
						<span class="cautionMini"> ※URLには&amp;を入れないでください。動画が再生されなくなります。</span></p>
					<label><input type="radio" name="movie_type" value="1" {if $input_data.movie_type=='1'}checked="checked"{else}{/if} />YouTube</label>
					<label><input type="radio" name="movie_type" value="2"  {if $input_data.movie_type=='2'}checked="checked"{else}{/if} />Vimemo</label>
					<label><input type="radio" name="movie_type" value="0"  {if $input_data.movie_type=='0'}checked="checked"{else}{/if} />なし</label>
					<p><input id="movie_url" name="movie_url" size="45" type="text" value="{$input_data.movie_url}"/></p>
					<label id="movie_url_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="movie_url_error"></label>
					<p>・高品質（720p以上推奨）</p>
					<p>・動画の長さが1分30秒〜3分</p>
					<p>・実行者本人の出演</p>
					<p>・オススメしたいリターンの説明</p>
					<!--LV7 カテゴリー -->
					<h3><span>6</span>カテゴリー</h3>
					<p>カテゴリーを１つ選択してください。</p>
					<p><select name="category_no">{html_options options=$array_category selected=$input_data.category_no }</select></p>
					<!--LV7 期間設定 -->
					<h3><span>7</span>期間設定</h3>
					<p>プロジェクトの掲載期間を設定してください（最大120日間）
					<p>
						<span class="cautionMini">
						＊プロジェクト終了日は、平日を指定してください。<br>
						＊プロジェクト開始日は、審査を申請頂く日から 7日後 以降に設定してください<br>
						＊設定した期間を、掲載後に変更する事はできません。短縮のみ認められます。<br>
						＊特典にイベント招待券などが含まれる場合、掲載終了日とプロジェクト実施日の間に余裕を持たせましょう。
						&nbsp;特典実施までのスケジュールがきつくなってしまうおそれがあります。<br>
						＊他プラットフォームとのクラウドファンディングの併用はお断りしております
						</span>
					</p>
					<p><input id="project_record_date" name="project_record_date" size="25" type="text" value="{$input_data.project_record_date|date_format:"%Y/%m/%d"}"/>〜<input id="invest_limit" name="invest_limit" size="25" type="text" value="{$input_data.invest_limit|date_format:"%Y/%m/%d"}"/></p>
					{if $err_msg.project_record_date}
							<p class="errorTitle" style="color:red;">※{$err_msg.project_record_date}</p>
					{/if}
					<!--LV7 お礼メッセージと画像 -->
					<h3><span>8</span>お礼メッセージと画像</h3>
					<p>
						175×175pxで10MB以下のJPEG,PNG,GIF画像ファイル<br>
						<span class="cautionMini"> ※画像ファイルの際、横サイズが異なる場合、歪んで表示されてしまうのでご注意ください<br>
						※写真は実行者自身の顔写真だと、共感されやすく、シェアされやすい傾向があります
						</span>
					</p>
					<p>
						{if $result_messages.thanks_image}
							<span class="red">{$result_messages.thanks_image}</span><br />
						{/if}
						{if $smarty.session.TMP_FUP12!=""}
							<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP12}" width=175 height=175 />
						{elseif $input_data.thanks_image!=""}
							<img src="{$smarty.const.C_IMG_PROJECT}{$input_data.thanks_image}" width=175 height=175 />
							<input type="hidden" name="thanks_image" value="{$input_data.thanks_image}" />
						{/if}
						<br/><input type="file" name="up_file2"  size="40"aria-invalid="false"/>
					</p>
					<p>ここにお礼の画像が入ります</p>
					<p>支援したサポーターに対するお礼のメッセージを記入してください。</p>
					<p><textarea cols="50" id="thanks_msg" maxlength="300" name="thanks_msg" rows="10" >{$input_data.thanks_msg}</textarea></p>
					<label id="thanks_msg_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="thanks_msg_error"></label>
					<p>お礼のメッセージと画像は、サポーターが支援した際に表示されるThankyouページで使用します。</p>
					<p>あなたのプロジェクトに支援したサポーターに対して、感謝の気持ちを伝えましょう。</p>
					<!--LV7 次へ -->
					<!--<div id="ui-tab2"><ul class="formStep"><li class="formNext"><a href="#fragment-2">次へ</a></li></ul></div>-->
					<ul class="formStep">
					<li class="formNext">
					<input id="next_f1" value="次へ" type="button" style="float:right;"/>
					<!--<div class="clearfix"></div>
					<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>-->
					</li>
					
					</ul>
					
				</div>
				<!--/.LV6 draftingLeft End -->
				<!--LV6 draftingRight Start -->
				{include file="{$lang}/drafting-side.tpl"}
				<!--/.LV6 draftingRight End -->
			</div>
			<!--/LV5 fragment-1 End -->
			<!--LV5 fragment-2 Start -->
			<div id="fragment-2">
				<div class="draftingLeft">
					{for $i=1;$i<6;$i++}
						{if $i<6&&$i!=1}
							<p id="add0{$i}" class="addReturn" >＋ Returnを追加</p>
						{/if}
						{if $i>1}
							{if $input_data.return_min.$i!=""}
								<div id="return0{$i}">
							{else}
								<div id="return0{$i}" style="display:none;">
							{/if}
						{/if}
						<h3><span>{$i}</span>{$i}つめのリターン</h3>
						<table>
							<tbody>
								<tr>
									<th scope="row">リターン金額（半角数字）</th>
									<td>
									<input id="return_min_{$i}" name="return_min[{$i}]" size="35" type="text" value="{$input_data.return_min.$i}"/>円（税込）
									<label id="return_min_{$i}_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="return_min_{$i}_error"></label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターンタイトル（40文字以内）</th>
									<td>
									<input id="return_title_{$i}" name="return_title[{$i}]" size="35" type="text"  value="{$input_data.return_title.$i}"/>
									<label id="return_title_{$i}_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="return_title_{$i}_error"></label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターン内容</th>
									<td>
									<textarea cols="50" id="return_text_{$i}" maxlength="300" name="return_text[{$i}]" rows="10">{$input_data.return_text.$i}</textarea>
									<label id="return_text_{$i}_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="return_text_{$i}_error"></label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターンの郵送<span class="cautionMini">「必要」を選択すると支援者の住所情報を取得します</span></th>
									<td>
										<label><input type="radio" name="return_post_flg[{$i}]" value="1" {if $input_data.return_post_flg.$i=='1'}checked="checked"{else}{/if}/>必要</label>
										<label><input type="radio" name="return_post_flg[{$i}]" value="0" {if $input_data.return_post_flg.$i=='0'}checked="checked"{else}{/if}/>不必要</label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターンのお届け予定</th>
									<td>
										<select name="return_year[{$i}]">{html_options options=$array_post_year selected=$input_data.return_year.$i }</select>年
										<select name="return_month[{$i}]">{html_options options=$array_post_month selected=$input_data.return_month.$i }</select>月
									</td>
								</tr>
								<tr>
									<th scope="row">リターンの最大数（半角数字）</th>
									<td>
										<input id="max_count_{$i}" name="max_count[{$i}]" size="35" type="text" value="{$input_data.max_count.$i}"/>個</br>
										<span class="cautionMini">※限度数を上限なしに設定する場合は、「9999」と入力してください</span>
										<label id="max_count_{$i}_succeed" class="blank"></label>
										<span class="clear"></span>
										<label id="max_count_{$i}_error"></label>
									</td>
								</tr>
								<tr>
									<th scope="row">リターン画像（280×165pxで3MB以下のJPEG,PNG,GIFファイル）</th>
									<td>
									{if $err_msg[return_img][$i]}
											<p class="errorTitle" style="color:red;">※{$err_msg[return_img][$i]}</p>
									{/if}
									{if $smarty.session.TMP_FUP.$i!=""}
										<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP.$i}" width=280 height=165 />
									{elseif $input_data.return_img.$i!=""}
										<img src="{$smarty.const.C_IMG_PROJECT}{$input_data.return_img.$i}" width=280 height=165 />
										<input type="hidden" name="new_return_img[{$i}]" value="{$input_data.return_img.$i}" />
									{/if}
									<p><input type="file" name="upload_file{$i}" size="40" aria-invalid="false" /></p>
									</td>
								</tr>
							</tbody>
						</table>
					{/for}
					</div>
					</div>
					</div>
					</div>
					<ul class="formStep">
						<li class="formPrev"><input id="prv_f2" value="戻る" type="button"/></li>
						<li class="formNext"><input id="next_f2" value="次へ" type="button" style="float:right;"/>
						<!--<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>-->
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-2  -->
			<div id="fragment-3" class="clearfix">
				<div class="draftingLeft" >
					<p>例１：あなたの夢が今、カタチになる。テキストはダミーです<br>
						例２：テキストはダミーですテキストはダミーです<br>
						例３：テキストはダミーですテキストはダミーですテキストはダミーです</p>
          <div class="htmlEditor">
            <textarea class="ckeditor" style="width: 820px; height:600px;" id="project_text" name="project_text" >{$input_data.project_text}</textarea>
            <script src="{$smarty.const.C_ASSETS}js/ckeditor/ckeditor.js"></script>
          </div>
					<ul class="formStep">
						<li class="formPrev"><input id="prv_f3" value="戻る" type="button"/></li>
						<li class="formNext"><input id="next_f3" value="次へ" type="button" style="float:right;"/>
						<!--<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>-->
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-3  -->
			<div id="fragment-4" class="clearfix">
				<div class="draftingLeft">
					<h3><span>1</span>Facebook</h3>
					<p>プロジェクトに関係するFacebookページ、またはプロジェクト実行者のアカウントを記入してください。</p>
					<p>https://www.facebook.com/<input id="facebook_page" name="facebook_page" size="45" type="text" value="{$input_data.facebook_page}"/></p>
					<label id="facebook_page_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="facebook_page_error"></label>
					<h3><span>2</span>Twitter</h3>
					<p>Twitterのアカウントを設定してください。</p>
					<p>@<input id="twitter_account" name="twitter_account" size="35" type="text" value="{$input_data.twitter_account}"/></p>
					<label id="twitter_account_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="twitter_account_error"></label>
					<h3><span>3</span>ブログやホームページ</h3>
					<p>ブログやその他のホームページを持っている方は記入してください。</p>
					<ul class="yourUrl">
						<li>URL1 :<input id="blog_url_1" name="blog_url_1" size="65" type="text" value="{$input_data.blog_url_1}"/></li>
						<label id="blog_url_1_succeed" class="blank"></label>
						<span class="clear"></span>
						<label id="blog_url_1_error"></label>
						<li>URL2 :<input id="blog_url_2" name="blog_url_2" size="65" type="text" value="{$input_data.blog_url_2}"/></li>
						<label id="blog_url_2_succeed" class="blank"></label>
						<span class="clear"></span>
						<label id="blog_url_2_error"></label>
						<li>URL3 :<input id="blog_url_3" name="blog_url_3" size="65" type="text" value="{$input_data.blog_url_3}"/></li>
						<label id="blog_url_3_succeed" class="blank"></label>
						<span class="clear"></span>
						<label id="blog_url_3_error"></label>
					</ul>
					<ul class="formStep">
						<li class="formPrev"><input id="prv_f4" value="戻る" type="button"/></li>
						<li class="formNext"><input id="next_f4" value="次へ" type="button" style="float:right;"/>
						<!--<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>-->
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-4  -->
			
			<div id="fragment-5" class="clearfix">
				<div class="draftingLeft">
					{if $err_msg.profile_img}
							<p class="errorTitle" style="color:red;">※{$err_msg.profile_img}</p>
					{/if}
					<h3><span>1</span>プロフィール画像</h3>
					<p>プロフィール画像（200×200px）を設定してください。</p>
					{if $smarty.session.TMP_FUP13!=""}
						<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP13}" width=200 height=200 />
					{elseif $input_data.profile_img!=""}
						<img src="{$smarty.const.C_IMG_ACTRESS}{$input_data.profile_img}" width=200 height=200 />
						<input type="hidden" name="new_profile_img" value="{$input_data.profile_img}" />
					{/if}
					<p><input type="file" name="up_file3" value="1" size="40" class="" aria-invalid="false" /></p>
					<p>ここにプロフィール画像が入ります</p>
					<h3><span>2</span>氏名</h3>
					<p>氏名を入力してください。（本名のフルネームでお願いします）</p>
					<p><input id="full_name" name="full_name" size="45" type="text" value="{$input_data.full_name}"/></p>
					<label id="full_name_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="full_name_error"></label>
					<h3><span>3</span>自己紹介</h3>
					<p>自己紹介を入力してください。（300文字以内）</p>
					<p><textarea cols="50" id="introduce" maxlength="300" name="introduce" rows="10">{$input_data.introduce}</textarea><br></p>
					<label id="introduce_succeed" class="blank"></label>
					<span class="clear"></span>
					<label id="introduce_error"></label>
					<h3><span>4</span>金融機関情報</h3>
					<p>集まった資金をお引き渡しするための金融口座を入力してください。</p>
					<table>
						<tbody>
							<tr>
								<th scope="row">金融機関名</th>
								<td>
									<label><input type="radio" name="bank_name" value="みずほ銀行" {if $input_data.bank_name=='みずほ銀行'}checked="checked"{else}{/if}/>みずほ銀行</label>
									<label><input type="radio" name="bank_name" value="三菱東京UFJ銀行" {if $input_data.bank_name=='三菱東京UFJ銀行'}checked="checked"{else}{/if}/>三菱東京UFJ銀行</label>
									<label><input type="radio" name="bank_name" value="三井住友銀行" {if $input_data.bank_name=='三井住友銀行'}checked="checked"{else}{/if}/>三井住友銀行</label> 
									<label><input type="radio" name="bank_name" value="りそな銀行" {if $input_data.bank_name=='りそな銀行'}checked="checked"{else}{/if}/>りそな銀行</label> 
									<label><input type="radio" name="bank_name" value="ゆうちょ銀行" {if $input_data.bank_name=='ゆうちょ銀行'}checked="checked"{else}{/if}/>ゆうちょ銀行</label><br>
									<span class="cautionMini">※上記以外の場合は銀行名を正しくご記入ください。</span><br>
									<input id="bank_name_txt" name="bank_name_txt" size="45" type="text" {if $input_data.bank_name=='みずほ銀行'||$input_data.bank_name=='三菱東京UFJ銀行'||$input_data.bank_name=='三井住友銀行'||$input_data.bank_name=='りそな銀行'||$input_data.bank_name=='ゆうちょ銀行'} value="" {else} value="{$input_data.bank_name}" {/if}/>
									<label id="bank_name_txt_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="bank_name_txt_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">支店名</th>
								<td>
								<input id="branch_bank" name="branch_bank" size="45" type="text" value="{$input_data.branch_bank}" />
								<label id="branch_bank_succeed" class="blank"></label>
								<span class="clear"></span>
								<label id="branch_bank_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">支店コード</th>
								<td>
									<input id="branch_bank_code" name="branch_bank_code" size="45" type="text" value="{$input_data.branch_bank_code}" /><br>
									<span class="cautionMini">※支店コードを半角数字で設定してください。 </span>
									<label id="branch_bank_code_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="branch_bank_code_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">口座種別</th>
								<td>
									<label><input type="radio" id="card_type" name="card_type" value="1" {if $input_data.card_type=='1'}checked="checked"{else}{/if}/>普通</label>
									<label><input type="radio" id="card_type" name="card_type" value="2" {if $input_data.card_type=='2'}checked="checked"{else}{/if}/>当座</label>
									<label><input type="radio" id="card_type" name="card_type" value="3" {if $input_data.card_type=='3'}checked="checked"{else}{/if}/>貯蓄</label>
								 </td>
							</tr>
							<tr>
								<th scope="row">口座番号</th>
								<td>
									<input id="card_number" name="card_number" size="45" type="text" value="{$input_data.card_number}" /><br>
									<span class="cautionMini">※ゆうちょ銀行を登録される場合、振込専用番号をご入力ください。</span>
									<label id="card_number_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="card_number_error"></label>
								</td>
							</tr>
							<tr>
								<th scope="row">口座名義人（カナ）</th>
								<td>
									<input id="card_owner" name="card_owner" size="45" type="text" value="{$input_data.card_owner}" /><br>
									<span class="cautionMini">※通帳に記載された名称を全角カタカナでご記入ください。<br>※法人格は名称で入力してください。<br>※口座名義人（カナ）を半角カナのみで設定してください。</span>
									<label id="card_owner_succeed" class="blank"></label>
									<span class="clear"></span>
									<label id="card_owner_error"></label>
								</td>
							</tr>
						</tbody>
					</table>
					<ul class="formStep">
						<li class="formPrev"><input id="prv_f5" value="戻る" type="button"/></li>
						<li class="formNext"><input id="next_f5" value="保存・プレビュー" type="submit" name="preview" style="float:right;"/>
						<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「保存・プレビュー」ボタンを押すことで、入力した内容は全て保存されます。</p>
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-5  -->
			<div id="fragment-6" class="clearfix">
				<div class="container container-offset cf border-10px">
				<div class="page-ttl-wrapper"  style="padding-top:0px;">
					<div class="container page-ttl-inner cf">
						<h2 class="page-ttl">{$input_data.public_title}</h2>
						<ul class="page-ttl-info cf" style="margin-right: 50px;">
							<li class="page-ttl-info-presenter"><a href="#">{$input_data.full_name}</a></li>
							<li class="page-ttl-info-category"><a href="#">{$array_category[$input_data.category_no]}</a></li>
						</ul>
					</div>
				</div>
					<div class="col-main">
						<div class="col-inner">
							<div class="local-nav-wrapper">
								<ul class="local-nav">
									<li class="local-nav-home count-off active">
										<a href="#">
											<p>ホーム</p>
										</a>
									</li>
									<li class="local-nav-update count-on">
										<a href="#">
											<p>アップデート</p>
											<p class="count"><span>0</span></p>
										</a>
									</li>
									<li class="local-nav-collector count-on">
										<a href="#">
											<p>サポーター</p>
											<p class="count"><span>0</span></p>
										</a>
									</li>
								</ul>
							</div>
							<div class="project-thum">
								<div class="project-cover">
									<p class="btn-play">再生する</p>
									{if $smarty.session.TMP_FUP11!=""}
										<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP11}" width=587 height=419 />
									{elseif $input_data.cover_img!=""}
										<img class="cover-image" src="{$smarty.const.C_IMG_PROJECT}{$input_data.cover_img}" alt="プロジェクトイメージ">
									{/if}
								</div>
								<div class="movie-player">
									<iframe id="moviePlayer" frameborder="0" allowfullscreen="1" title="YouTube video player" width="608" height="434" src="{$input_data.movie_url}"></iframe>
								</div>
								<div class="project-cover-base">
									{if $smarty.session.TMP_FUP11!=""}
										<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP11}" width=587 height=419 />
									{elseif $input_data.cover_img!=""}
										<img class="cover-image" src="{$smarty.const.C_IMG_PROJECT}{$input_data.cover_img}" alt="プロジェクトイメージ">
									{/if}
								</div>
							</div>
							<div class="sns-btn-set">
								<ul class="clearfix">
									<li class="fblike"><div class="fb-like" data-layout="button" data-action="like" data-show-faces="true" data-share="true"></div></li>
									<li class="tweet"><a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
									{literal}
									<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
									{/literal}
									</li>
									<li class="gplus"><div class="g-plusone" data-size="medium" data-annotation="none"></div>
									{literal}
									<script type="text/javascript">(function() {var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;po.src = 'https://apis.google.com/js/plusone.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);})();</script>
									{/literal}
									</li>
									<li class="hatena"><a href="http://b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="simple" title="はてなブックマークに追加"><img src="https://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="はてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="https://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async></script></li>
									<li class="blog"><a href="#">ブログに埋め込む</a></li>
								</ul>
							</div>
							<div class="project-description">
								<h2 class="project-description-ttl">このプロジェクトについて</h2>
								<div class="project-description-inner jsc-project-description">
									{$input_data.project_text|nl2br nofilter}
								</div>
							</div>
							<div class="project-detail">
								<div class="btn-project-detail-open jsc-btn-project-detail-open">
									<a href="javascript:void(0);">続きを見る</a>
								</div>
								<div class="btn-project-detail-close jsc-btn-project-detail-close jsc-dn">
									<a href="javascript:void(0);">閉じる</a>
								</div>
							</div>

							<div class="project-invest-box">
								<div class="btn-project-invest">
									<a href="#">プロジェクトを応援する</a>
								</div>
							</div>
							<div class="project-social-bottom-wrapper cf">
								<ul class="project-social-bottom cf">
									<li class="fb"><a href="http://www.facebook.com/share.php?u=https://a-port.asahi.com/projects/xxx/" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">facebookでシェア</a></li>
									<li class="tw"><a href="http://twitter.com/share?url=https://a-port.asahi.com/projects/xxx/&text=～MGから自動的に出力されるプロジェクトの説明テキスト入る～ / 朝日新聞社が運営するクラウドファンディングサイト「A-port（エーポート）」" target="_blank">Twitterでシェア</a></li>
									<li class="blog"><a href="#">ブログに埋め込む</a></li>
								</ul>
							</div>
						</div>
					</div><!--/col-main-->
					<div class="col-sub">
						<div class="col-inner">
							<ul class="sub-project-status">
								<li class="sub-project-status-collector">
									<p class="sub-project-status-label">サポーター</p>
									<p class="sub-project-status-var"><span>0</span>人</p>
								</li>
								<li class="sub-project-status-last">
									<p class="sub-project-status-label">残り期間</p>
									<p class="sub-project-status-var"><span>{$input_data.diff_in_days}</span>日</p>
								</li>
								<li class="sub-project-status-fund">
									<p class="sub-project-status-label">集まっている金額</p>
									<p class="sub-project-status-var"><span>0</span>円</p>
									<p class="sub-project-status-target">目標金額：<span>{$input_data.wish_price|number_format}</span>円</p>
								</li>
								<li class="sub-project-status-per">
									<p class="sub-project-status-label">達成率<span>0</span>%</p>
									<div class="progress">
										<div class="gauge" style="width:100%;">
											<div class="gauge2" style="width:0%;"></div>
										</div>
									</div>
								</li>
							</ul>
							{if $input_data.project_type=="1"}
								<p class="project-notes">このプロジェクトでは、<span class="">{$input_data.invest_limit|date_format:"%Y年%m月%d日"}23:59</span>までに目標に到達した場合のみ、ファンディングが実行されます。</p>
							{else}
								<p class="project-notes">このプロジェクトでは、目標到達に関わらず、<span class="">{$input_data.invest_limit|date_format:"%Y年%m月%d日"}23:59</span>までに集まった金額がファンディングされます。</p>
							{/if}
							<div class="btn-project-invest">
								<a href="#">プロジェクトを応援する</a>
							</div>
						</div>
						<div class="col-inner">
							<h4 class="sub-presenter-ttl">実行者</h4>
							<div class="sub-presenter-profile cf">
								{if $smarty.session.TMP_FUP13!=""}
									<div class="sub-presenter-thum"><img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP13}" alt="実行者イメージ"></div>
								{elseif $input_data.profile_img!=""}
									<div class="sub-presenter-thum"><img src="{$smarty.const.C_IMG_ACTRESS}{$input_data.profile_img}" alt="実行者イメージ"></div>
								{/if}
								<div class="sub-presenter-meta">
									<p class="sub-presenter-name">{$input_data.full_name}</p>
									<p class="sub-presenter-location">{$array_area[$current_user.area]}</p>
								</div>
							</div>
							<div class="sub-presenter-link">
								<a href="#">{$input_data.blog_url_1}</a>
							</div>
							<div class="sub-presenter-detail">
								{mb_substr($input_data.introduce,0,20,'utf-8')}
								<span class="jsc-sub-presenter-detail-dot">...</span>
								<span class="jsc-sub-presenter-detail-inner jsc-dn">
								{mb_substr($input_data.introduce,21,1000,'utf-8')}
								</span>
								<div class="sub-presenter-detail-more-open jsc-sub-presenter-detail-more-open"><a href="javascript:void(0);">続きを見る</a></div>
								<div class="sub-presenter-detail-more-close jsc-sub-presenter-detail-more-close jsc-dn"><a href="javascript:void(0);">閉じる</a></div>
							</div>
							<div class="btn-presenter-question">
								<a href="#">質問や意見をメールする</a>
							</div>
						</div>
						<div class="sub-ticketlist-wrapper">
							<ul class="sub-ticketlist">
							{for $k=1 to 6}
								{if $input_data['return_min'][$k]!=""}
								<li>
									{if $input_data['max_count'][$k]=="9999"||$input_data['max_count'][$k]==""}
									{else}
										<span class="limitedTicket">残り{$input_data['max_count'][$k]}枚</span>
									{/if}
									<p class="sub-ticketlist-fund jsc-sub-ticketlist-fund-toggle"><span>{$input_data['return_min'][$k]|number_format}</span>円</p>
									<div class="sub-ticketlist-inner jsc-sub-ticketlist-fund-inner">
										<p class="sub-ticketlist-return-ttl">リターン</p>
										{if $smarty.session.TMP_FUP.$k!=""}
											<p class="sub-ticketlist-img"><img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP.$k}" alt="" width=280 height=165></p>
										{elseif $input_data.return_img.$k!=""}
											<p class="sub-ticketlist-img"><img src="{$smarty.const.C_IMG_PROJECT}{$input_data.return_img.$k}" alt="" width=280 height=165></p>
										{/if}
										<ul class="sub-ticketlist-return">
											{$input_data['return_text'][$k]}
										</ul>
										<p class="sub-ticketlist-collector">
											<span class="sub-ticketlist-collector-label">サポーターの数</span>
											<span class="sub-ticketlist-collector-var">0</span>人
										</p>
										<p class="sub-ticketlist-delivery">
											お届け予定：<span class="sub-ticketlist-delivery-var">{$input_data['return_year'][$k]}年{$input_data['return_month'][$k]}月上旬</span>
										</p>
										<div class="project-invest-box">
											<div class="btn-project-invest">
												<a href="#">プロジェクトを応援する</a>
											</div>
										</div>
									</div>
								</li>
								{/if}
							{/for}
							</ul>
						</div>
					</div><!--/col-sub-->
					<ol class="breadcrumb cf">
						<li><a href="#">サイトトップ</a></li>
						<li><a href="#">{$array_category[$input_data.category_no]}</a></li>
						<li><a href="#">{$input_data.public_title}</a></li>
					</ol>
				</div><!--/container-->
				<ul class="formStep">
					<li class="formPrev"><input id="prv_f6" value="戻る"  type="button"/></li>
					<li class="formNext"><input id="next_f6" value="審査へ提出" type="submit" name="sbm_update"/></li>
					<!--<li class="formNext"><input id="next_f6" value="変更を提出" type="submit" name="sbm_update"/></li>-->
				</ul>
			</div>
			<!--/#fragment-6  --> 
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
<script type='text/javascript'>
{literal}
$(function() {
	$('#add02').click(function(){
	$(this).next('#return02').slideToggle();
	});
	$('#add03').click(function(){
	$(this).next('#return03').slideToggle();
	});
	$('#add04').click(function(){
	$(this).next('#return04').slideToggle();
	});
	$('#add05').click(function(){
	$(this).next('#return05').slideToggle();
	});
});
{/literal}
</script>
{include file="{$lang}/sf-footer.tpl"}

<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.form.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
