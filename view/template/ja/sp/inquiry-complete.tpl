{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/form.css">
</head>
<body>
<div id="wrapper" class="clearfix">
{include file="{$lang}/sf-header.tpl"}

<div id="wrapper" class="clearfix">
<div class="page-ttl-wrapper">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">お問い合わせ</h2>
    <p style="text-align: center;margin-top:20px;">送信が完了しました。<br>
この度はお問い合わせ頂きまして、誠にありがとうございました。</p>
    <p style="text-align: center;margin-top:20px;"><a href="{$pagelink_index}">トップページへ</a></p>
	</div>
</div>
  <!--/#contents-->
<!-- /main -->
</div>
<!--/#wrapper-->
</div>
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
