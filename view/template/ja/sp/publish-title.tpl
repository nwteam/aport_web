      <div id="myProject">
        <p id="myProjectThumb"><img src="{$smarty.const.C_IMG_PROJECT}{$project_info.cover_img}" alt="{$project_info.public_title}"/></p>
        <div id="projectDescription">
          <h2>{$project_info.public_title}</h2>
          <p>{$project_info.thanks_msg|nl2br nofilter}</p>
        </div>
        <!-- #projectDescription --> 
      </div>
      <!-- #myProject -->