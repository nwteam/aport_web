<!DOCTYPE html>
<html lang="ja" prefix="og: http://ogp.me/ns# fb: http://www.facebook.com/2008/fbml mixi: http://mixi-platform.com/ns#">
<head>
<meta charset="utf-8">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<![endif]-->
<title>{$str_site_title}</title>
<meta name="robots" content="noindex,nofollow">
<meta name="keywords" content="{$str_meta_keywords}" />
<meta name="description" content="{$str_meta_description}" />
<link rel="shortcut icon" href="{$smarty.const.C_ASSETS}images/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="{$smarty.const.C_ASSETS}images/icons/apple-touch-icon-152x152.png" />
<script type="text/JavaScript" src="{$smarty.const.C_ASSETS}js/jquery-1.8.3.min.js"></script>
<link href='{$smarty.const.C_ASSETS}css/fonts.css' rel='stylesheet' type='text/css'>
<meta content="authenticity_token" name="csrf-param" />
<meta content="suA+dmrbBfNrZHkXElOFBLjBop9FksifHyGJmji/gGA=" name="csrf-token" />