 {include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<script type='text/javascript'>
{literal}

{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<div id="wrapper" class="clearfix">
	<ul id="step">
		<li>1. 応援リターンの選択 </li>
		<li>2. 決済情報の入力 </li>
		<li class="active">3. 応援内容の確認 </li>
		<li>4. 完了<span>（応援コメントを寄せよう）</span> </li>
	</ul>
	<div id="projectStart">
		<div id="projectContents">
      <h2>応援内容の確認</h2>
			{if $err_msg1.top}
				<p class="errorTitle" style="color:red;">※{$err_msg1.top}</p>
			{/if}
			{if $err_msg1.charge_err_msg}
				<p class="errorTitle" style="color:red;">※{$err_msg1.charge_err_msg}</p>
			{/if}
			<form accept-charset="UTF-8" action="" method="post">
				{if $payment_method=="Credit"}
				<h3>決済情報をご確認ください。</h3>
					<dl class="confirmList">
						<dt>カード番号</dt>
						<dd>{$card_no}</dd>
					</dl>
					<dl class="confirmList">
						<dt>有効期限</dt>
						<dd>{$expdate_month}月/{$expdate_year}年</dd>
					</dl>
					<dl class="confirmList">
						<dt>カード名義</dt>
						<dd>{$hld_name}</dd>
					</dl>
					<dl class="confirmList">
						<dt>セキュリティコード</dt>
						<dd>{$security_code}</dd>
					</dl>
					<dl class="confirmList">
						<dt>支払い方法</dt>
						<dd><img src="{$smarty.const.C_ASSETS}images/logo_visa.png" alt="クレジットカード"/></dd>
					</dl>
				{/if}
				{if $payment_method=="cvs"}
					<h3>決済情報をご確認ください。</h3>
					<dl class="confirmList">
						<dt>コンビニ</dt>
						<dd>{$array_cvs[$convenience]}</dd>
					</dl>
					<dl class="confirmList">
						<dt>氏名</dt>
						<dd>{$ks_name_1}&nbsp;{$ks_name_2}</dd>
					</dl>
					<dl class="confirmList">
						<dt>氏名カナ</dt>
						<dd>{$ks_name_kana_1}&nbsp;{$ks_name_kana_2}</dd>
					</dl>
					<dl class="confirmList">
						<dt>電話番号</dt>
						<dd>{$ks_tel}</dd>
					</dl>
					<dl class="confirmList">
						<dt>支払い方法</dt>
						{if $convenience=="00002"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni01.png" alt="FamilyMart"/></dd>
						{/if}
						{if $convenience=="00004"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni02.png" alt="サークルK"/></dd>
						{/if}
						{if $convenience=="00001"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni03.png" alt="LAWSON"/></dd>
						{/if}
						{if $convenience=="00005"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni04.png" alt="MINI STOP"/></dd>
						{/if}
						{if $convenience=="00008"}
							<dd><img src="{$smarty.const.C_ASSETS}images/logo_conveni05.png" alt="Seicomart"/></dd>
						{/if}
					</dl>
				{/if}
				{if $payment_method=="payeasy"}
					<h3>決済情報をご確認ください。</h3>
					<dl class="confirmList">
						<dt>氏名</dt>
						<dd>{$ks_name_1}&nbsp;{$ks_name_2}</dd>
					</dl>
					<dl class="confirmList">
						<dt>氏名カナ</dt>
						<dd>{$ks_name_kana_1}&nbsp;{$ks_name_kana_2}</dd>
					</dl>
					<dl class="confirmList">
						<dt>電話番号</dt>
						<dd>{$ks_tel}</dd>
					</dl>
					<dl class="confirmList">
						<dt>支払い方法</dt>
						<dd><table>
						<tr>
						<th><img src="{$smarty.const.C_ASSETS}images/logo_payeasy.png" alt="ペイジー"/></th>
						<td>ゆうちょ銀行 / みずほ銀行 / 三井住友銀行 / 三菱東京UFJ銀行 / りそな銀行<br>
						<span>※その他の取り扱い銀行は<a href="http://www.econtext.jp/customer/pay_payeasy/list.php" target="_blank">こちら</a></span>
						</td>
						</tr></table></dd>
					</dl>
				{/if}
				<h3>応援額とリターン内容をご確認ください。</h3>
				<dl class="confirmList">
					<dt>プロジェクト名</dt>
					<dd>{$project_info.public_title}</dd>
				</dl>
				<dl class="confirmList">
					<dt>応援額</dt>
					<dd>{$total_amount|number_format}円（税込）</dd>
				</dl>
				<dl class="confirmList">
					<dt>リターン名</dt>
					<dd>{$selected_present_info.min|number_format}円 リターン</dd>
				</dl>
				<dl class="confirmList">
					<dt>特典</dt>
					<dd>{$selected_present_info.text}</dd>
				</dl>
				<h3>お届け先</h3>
				<h4>下記の住所が特典のお届け先として登録されています。</h4>
				{if $err_msg1.post_address}
					<p class="errorTitle" style="color:red;">※{$err_msg1.post_address}</p>
				{/if}
				<dl class="confirmList">
					<dt>氏名</dt>
					<dd>{$member_info.post_user_name}</dd>
					<input type="hidden" name="post_user_name" value="{$member_info.post_user_name}">
				</dl>
				<dl class="confirmList">
					<dt>郵便番号</dt>
					<dd>{$member_info.post_zip_code}</dd>
					<input type="hidden" name="post_zip_code" value="{$member_info.post_zip_code}">
				</dl>
				<dl class="confirmList">
					<dt>住所</dt>
					<dd>{$array_area[$member_info.post_add_1]}{$member_info.post_add_2}{$member_info.post_add_3}</dd>
					<input type="hidden" name="post_add_1" value="{$member_info.post_add_1}">
					<input type="hidden" name="post_add_2" value="{$member_info.post_add_2}">
					<input type="hidden" name="post_add_3" value="{$member_info.post_add_3}">
				</dl>
				<dl class="confirmList">
					<dt>電話番号</dt>
					<dd>{$member_info.post_tel}</dd>
					<input type="hidden" name="post_tel" value="{$member_info.post_tel}">
				</dl>
				<p id="paymentLink"><a href="{$pagelink_mypage_address_edit}?action=change&no={$project_info.no}&present_no={$selected_present_info.present_no}" target="_blank">お届け先を変更する</a></p>
				<p class="actions">
					<input class="return" name="back" type="submit" value="戻る" />
					<input class="btnLv01" name="sbm_send" type="submit" value="申し込みを確定する" />
				</p>
			</form>
		</div>
		<!-- #projectContnets -->
		{include file="{$lang}/collection-side.tpl"}
	</div>
	<!-- #projectStart --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
