{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />

<!-- HTML エディタ -->
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}js/redactor/redactor.css"/>

<script src="{$smarty.const.C_ASSETS}js/redactor/redactor.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/langs/ja.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/video.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/table.js"></script>
<script type="text/javascript">
{literal}
$(document).ready(function () {
	$('#project_text').redactor({
		//buttons:['html','formatting','bold','italic','deleted','unorderedlist','orderedlist','outdent','indent','image','video','file','table','link','alignment','horizontalrule'],
		buttonSource: true,
		convertVideoLinks:true,
		imageUpload:'upload.php',
		plugins: ['table', 'video'],
		lang:'ja',
		formatting: ['h2', 'p', 'blockquote']
		});
});
{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul class="tab clearfix mb15">
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">支援者の一覧</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">アップデート</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">メール</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting4.php?p_no={$input_data.no}" >インサイト</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
      <div id="fragment-1" class="clearfix">
        <h3>コレクターの一覧</h3>
        <p class="draftingUtilityBtn"><a href="#">一覧をCSVでダウンロード</a></p>
        <table class="draftingTable">
          <tbody>
            <tr>
              <th colspan="2" scope="col" class="collectorName">支援者名</th>
              <th scope="col" class="cheer">応援額</th>
              <th scope="col" class="return">リターン</th>
              <th scope="col" class="cheerDate">応援日</th>
              <th scope="col" class="collectorMemo">メモ（<a href="#">編集する</a>）</th>
              <th scope="col" class="collectorMail">メール</th>
            </tr>
            <tr>
              <td class="collectorThumb"><img src="{$smarty.const.C_ASSETS}images/projects/collector-thum-noimage.png" alt="支援者名"/></td>
              <td class="collectorName">支援者名</td>
              <td class="cheer">100,000円</td>
              <td class="return">100,000円リターン</td>
              <td class="cheerDate">2015/08/25</td>
              <td class="collectorMemo"><a href="#">メモメモメモメモメモメモメモ</a></td>
              <td class="collectorMail"><a href="#"><img src="{$smarty.const.C_ASSETS}images/icon_mail.png" width="20" height="14" alt="mail"/>(0)</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <!--/#fragment-1  -->
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
