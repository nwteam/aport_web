{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/option.css">
<link href="{$smarty.const.C_ASSETS}css/flexslider.css" rel="stylesheet" type="text/css" media="all">
<script src="{$smarty.const.C_ASSETS}js/jquery.flexslider-min.js"></script>
<script>
{literal}
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "fade",
    animationLoop: true,
    itemMargin: 0
  });
});
{/literal}
</script>
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper">

{include file="{$lang}/sf-header.tpl"}

<div id="mainSliderPc" class="flexslider carousel">
	<span class="wave"></span>
	<ul class="slides">
		<li><a href="{$pagelink_about}"><img src="{$smarty.const.C_ASSETS}images/mainvisual.jpg" alt="新しくなったA-port" /></a></li>
		<li><a href="{$pagelink_about}"><img src="{$smarty.const.C_ASSETS}images/mainvisual2.jpg" alt="A-portで、みんなの支援を集めよう！"/></a></li>
		<li><a href="{$pagelink_about}"><img src="{$smarty.const.C_ASSETS}images/mainvisual3.png" alt="リニューアルオープン"/></a></li>
	</ul>
	</div>
<div id="mainSliderSp" class="flexslider carousel">
	<span class="wave"></span>
	<ul class="slides">
		<li><a href="{$pagelink_about}"><img src="{$smarty.const.C_ASSETS}images/main01-sp-anm01.png" alt="新しくなったA-port" /></a></li>
		<li><a href="{$pagelink_about}"><img src="{$smarty.const.C_ASSETS}images/main01-sp-anm02.png" alt="A-portで、みんなの支援を集めよう！"/></a></li>
		<li><a href="{$pagelink_about}"><img src="{$smarty.const.C_ASSETS}images/main01-sp-anm03.png" alt="リニューアルオープン"/></a></li>
	</ul>
</div>
<!--
<div class="main-wrapper">
	<div class="main-inner">
		<div class="container-wide">
			<a href="{$pagelink_about}">
			<img class="pc jsc-gif-animate jsc-dn" src="{$smarty.const.C_ASSETS}images/pc/main_f12_02.gif" width="100%" alt="A-portについてもっと詳しく">
			<img class="sp jsc-gif-animate" src="{$smarty.const.C_ASSETS}images/sp/main01-sp-anm.gif" width="100%" alt="A-portについてもっと詳しく">
			</a>
		</div>
	</div>
</div>
-->
<div class="body">
	<div class="container-wide">
		<div class="project-list-index-wrapper">
			<ul class="project-list-index">
				{foreach name="loop" item="new_data" from=$new_data_list}
					<li {if $smarty.foreach.loop.iteration == 1} class="pickup1" {elseif $smarty.foreach.loop.iteration == 4}  class="pickup2" {/if} >
						{if $new_data.percent>=100}<div class="ribbon"></div>{/if}
						{if $new_data.view_flg==1}<div class="seal"></div>{/if}
						<div class="project-img">
							<a href="{$pagelink_detail_project}{$new_data.hope_url}">
							{if $smarty.foreach.loop.iteration == 1}
								<img src="{$smarty.const.C_IMG_PROJECT}{$new_data.cover_img}" alt="{$new_data.public_title} " width=580 height=419>
							{elseif $smarty.foreach.loop.iteration == 4}
								<img src="{$smarty.const.C_IMG_PROJECT}{$new_data.cover_img}" alt="{$new_data.public_title}" width=580 height=419>
							{else}
								<img src="{$smarty.const.C_IMG_PROJECT}{$new_data.cover_img}" alt="{$new_data.public_title}" width=260 height=186>
							{/if}
							</a>
						</div>
						<div class="project-box">
							<p class="project-title"><a href="{$pagelink_detail_project}{$new_data.hope_url}">{$new_data.public_title}</a></p>
							<ul class="project-tip cf">
								<li class="project-tip-category" style="width:120px;"><a href="{$pagelink_list_project}?c_no={$new_data.category_no}">{$new_data.category_name}</a></li>
								<li class="project-tip-presenter">{$new_data.member_nickname}</li>
							</ul>
							{if $new_data.status=='1'&&$new_data.percent<100&&$new_data.diff_in_days!='-'}
								<div class="project-summary">
									<div class="progress">
										<div class="gauge" style="width:100%;">
											<div class="gauge2" {if $new_data.percent<100}style="width:{$new_data.percent}%;"{else}style="width:100%;"{/if}></div>
										</div>
									</div>
									<ul class="project-status">
										<li class="project-status-fund">
											<p class="project-status-label" style="width:130px;">集まった金額</p>
											<p class="project-status-var"><span>{$new_data.now_summary|number_format}</span>円</p>
										</li>
										<li class="project-status-per">
											<p class="project-status-label">達成率</p>
											<p class="project-status-var"><span>{$new_data.percent}</span>%</p>
										</li>
										<li class="project-status-last">
											<p class="project-status-label">あと</p>
											<p class="project-status-var">{$new_data.diff_in_days}日</p>
										</li>
									</ul>
								</div>
							{elseif $new_data.status=='3'||$new_data.percent>=100||$new_data.diff_in_days=='-'}
									<div class="project-summary funded">
										<div class="progress"> FUNDED! </div>
										<ul class="project-status">
											<li class="project-status-fund">
												<p class="project-status-label" style="width:120px;">集まった金額</p>
												<p class="project-status-var"><span>{$new_data.now_summary|number_format}</span>円</p>
											</li>
											<li class="project-status-per">
												<p class="project-status-label">達成率</p>
												<p class="project-status-var"><span>{$new_data.percent}</span>%</p>
											</li>
											<li class="project-status-last">
												<p class="project-status-label">終了</p>
												<p class="project-status-var">{$new_data.invest_limit|date_format:"%y年%m月%d日"}</p>
											</li>
										</ul>
								</div>
							{/if}
						</div>
					</li>
				{/foreach}
			</ul>
			<div class="btn-project-more">
				<a href="{$pagelink_list_project}"><img src="{$smarty.const.C_ASSETS}images/common/btn-project-more.png" alt="プロジェクトをもっと見る"></a>
			</div>
		</div>
		<div class="aLittleAfter">
		 <h2>あとひと押しのプロジェクト</h2>
			<ul class="project-list-index">
				{foreach name="loop" item="comp_data" from=$comp_data_list}
				<li>
					<div class="project-img"> <a href="{$pagelink_detail_project}{$comp_data.hope_url}"> <img src="{$smarty.const.C_IMG_PROJECT}{$comp_data.cover_img}" alt="{$comp_data.public_title}" width=260 height=186> </a></div>
					<div class="project-box">
						<p class="project-title"> <a href="{$pagelink_detail_project}{$comp_data.hope_url}">{$comp_data.public_title}</a> </p>
					</div>
				</li>
				{/foreach}
			</ul>
		</div><!--/.aLittleAfter-->
	</div>
	<div class="project-nav-wrapper">
		<div class="container-wide">
			<div class="project-nav-inner">
				<ul class="project-nav cf">
					<li><a href="{$pagelink_list_project}?hid_page_list=1"><img src="{$smarty.const.C_ASSETS}images/common/project-nav-02.png" alt="新着プロジェクト"></a></li>
					<li><a href="{$pagelink_list_project}?hid_page_list=2"><img src="{$smarty.const.C_ASSETS}images/common/project-nav-03.png" alt="期限間近のプロジェクト"></a></li>
					<li><a href="{$pagelink_list_project}?hid_page_list=3"><img src="{$smarty.const.C_ASSETS}images/common/project-nav-01.png" alt="応援金額の多いプロジェクト"></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="news">
	 <div class="newsInner">
		 <h2>A-portからのお知らせ</h2>
		 
		 <dl>
			<dt>2015.07.20</dt>
			<dd class="newsTitle">お知らせタイトルお知らせタイトル</dd>
			<dd>お知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入ります</dd>
		 </dl>
		 
		 <dl>
			<dt>2015.07.17</dt>
			<dd class="newsTitle">お知らせタイトル</dd>
			<dd>お知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入ります</dd>
		 </dl>
		 
		 <dl>
			<dt>2015.07.15</dt>
			<dd class="newsTitle">お知らせタイトルお知らせタイトルお知らせタイトル</dd>
			<dd>お知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入りますお知らせの内容が入ります</dd>
		 </dl>
		 
		 </div>
	</div><!--/.news-->
	<!--
	<div class="recommend-category-wrapper">
		<div class="container-wide">

			<h3 class="recommend-category-ttl"><img src="{$smarty.const.C_ASSETS}images/common/ttl-recommend-category.png" alt="おすすめカテゴリー"></h3>
			<div class="recommend-category-inner">
				<ul class="recommend-category cf">
					<li><a href="/categories/Art/"><img src="{$smarty.const.C_ASSETS}images/common/recommend-category-01.png" alt="Art アート"></a></li>
					<li><a href="/categories/Sports/"><img src="{$smarty.const.C_ASSETS}images/common/recommend-category-02.png" alt="Sports スポーツ"></a></li>
					<li><a href="/categories/Product/"><img src="{$smarty.const.C_ASSETS}images/common/recommend-category-03.png" alt="Product プロダクト"></a></li>
					<li><a href="/categories/Film/"><img src="{$smarty.const.C_ASSETS}images/common/recommend-category-04.png" alt="Film 映画"></a></li>
				</ul>
			</div>
		</div>
	</div>
	-->
	<div class="partner-list-wrapper">
		<div class="container">
			<h3 class="partner-list-ttl"><img src="{$smarty.const.C_ASSETS}images/common/ttl-partner-list.png" alt="関連サイト"></h3>
			<ul class="partner-list">
				<li><a href="http://www.huffingtonpost.jp/" target="_blank"><img src="{$smarty.const.C_ASSETS}images/common/bnr-partner-01.png" alt="THE HUFFINGTON POST"></a></li>
			</ul>
		</div>
	</div>
</div>

{include file="{$lang}/sf-footer.tpl"}
</div>
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
<div class="fixedStartProject">
<a href="{$pagelink_start}"><img src="{$smarty.const.C_ASSETS}images/side_start.png" alt="プロジェクトをはじめる"/></a>
</div>
</body>
</html>