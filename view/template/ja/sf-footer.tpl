<div class="prj-sub-nav-wrapper">
	<div class="container">
		<h3 class="prj-sub-nav-wrapper-ttl"><img src="{$smarty.const.C_ASSETS}images/common/ttl-prj-sub-nav.png" alt="プロジェクトを探す"></h3>
		<div class="prj-sub-nav-inner">
			<ul class="prj-sub-nav cf">
				<li><a href="{$pagelink_list_project}?c_no=1">アート</a></li>
				<li><a href="{$pagelink_list_project}?c_no=2">本</a></li>
				<li><a href="{$pagelink_list_project}?c_no=3">映画</a></li>
				<li><a href="{$pagelink_list_project}?c_no=4">ゲーム</a></li>
				<li><a href="{$pagelink_list_project}?c_no=5">音楽</a></li>
				<li><a href="{$pagelink_list_project}?c_no=6">パフォーマンス</a></li>
				<li><a href="{$pagelink_list_project}?c_no=7">写真</a></li>
				<li><a href="{$pagelink_list_project}?c_no=8">テクノロジー</a></li>
				<li><a href="{$pagelink_list_project}?c_no=9">コミュニティ</a></li>
				<li class="no-border"><a href="{$pagelink_list_project}?c_no=10">ファッション</a></li>
				<li><a href="{$pagelink_list_project}?c_no=11">フード</a></li>
				<li><a href="{$pagelink_list_project}?c_no=12">ジャーナリズム</a></li>
				<li><a href="{$pagelink_list_project}?c_no=13">プロダクト</a></li>
				<li><a href="{$pagelink_list_project}?c_no=14">アニメ</a></li>
				<li><a href="{$pagelink_list_project}?c_no=15">演劇・ダンス</a></li>
				<li><a href="{$pagelink_list_project}?c_no=16">スポーツ</a></li>
				<li><a href="{$pagelink_list_project}?c_no=17">伝統工芸</a></li>
				<li class="no-border"><a href="{$pagelink_list_project}?c_no=18">旅</a></li>
			</ul>
		</div>
	</div>
</div>


<div class="social-bottom-wrapper">
	<div class="container social-bottom-inner cf">
		<h3 class="social-bottom-ttl"><img src="{$smarty.const.C_ASSETS}images/pc/ttl-social-bottom.png" alt="FacebookとTwitterで、最新情報やおすすめプロジェクトなどをお届け中！"></h3>
		<ul class="social-bottom cf">
			<li class="fb"><a href="https://www.facebook.com/a.port.cf?ref=hl" target="_blank"><img src="{$smarty.const.C_ASSETS}images/common/social-fb.png" alt="facebook"></a></li>
			<li class="tw"><a href="https://twitter.com/AsahiAport" target="_blank"><img src="{$smarty.const.C_ASSETS}images/common/social-tw.png" alt="twitter"></a></li>
		</ul>
	</div>
</div>
<div id="jsi-scroll-top-fade" class="scroll-top">
	<a href="javascript:void(0);" id="jsi-link-scroll-top" class="scroll-top-arrow">TOPへ戻る</a>
</div>
<footer class="footer">
	<div class="footer-g-nav-wrapper">
		<div class="container">
			<nav class="footer-g-nav">
				<ul class="footer-g-nav-list">
					<li><a href="{$pagelink_about}">A-portについて</a></li>
					<li><a href="{$pagelink_guide}">クラウドファンディングとは</a></li>
					<li><a href="{$pagelink_column}">コラム</a></li>
					{if $current_user.user_no}
						<li><a href="{$pagelink_mypage}">マイページ</a></li>
						<li><a href="{$pagelink_logout}">ログアウト</a></li>
					{else}
						<li><a href="{$pagelink_login}">ログイン</a></li>
						<li><a href="{$pagelink_register}">新規登録</a></li>
					{/if}
					<li><a href="{$pagelink_faq}">よくあるご質問</a></li>
					<li><a href="{$pagelink_usage_project}">利用規約</a></li>
					<li><a href="{$pagelink_privacypolicy}">プライバシーポリシー</a></li>
					<li><a href="{$pagelink_tokusyo}">特定商取引法の表記</a></li>
					<li><a href="http://www.asahi.com/shimbun/company/outline/" target="_blank">運営会社</a></li>
					<li><a href="{$pagelink_inquiry}">お問い合わせ</a></li>
				</ul>
			</nav>
		</div>
	</div>
	<div class="footer-inner cf">
		<div class="footer-social-wrapper cf">
			<ul class="footer-social cf">
				<li><a href="http://www.facebook.com/share.php?u=https://a-port.asahi.com/" class="fb" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;"><img src="{$smarty.const.C_ASSETS}images/common/btn-fb-share.png" alt="facebookでシェア"></a></li>
				<li><iframe id="twitter-widget-i1441234968457629956" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" src="https://platform.twitter.com/widgets/tweet_button.420281f7dd393a35b17552fb11b499a9.ja.html#_=1441234968466&amp;count=none&amp;dnt=false&amp;id=twitter-widget-i1441234968457629956&amp;lang=ja&amp;original_referer=https%3A%2F%2Fa-port.asahi.com%2F&amp;size=m&amp;text=%E6%9C%9D%E6%97%A5%E6%96%B0%E8%81%9E%E7%A4%BE%E3%81%AE%E3%82%AF%E3%83%A9%E3%82%A6%E3%83%89%E3%83%95%E3%82%A1%E3%83%B3%E3%83%87%E3%82%A3%E3%83%B3%E3%82%B0%E3%82%B5%E3%82%A4%E3%83%88%E3%80%8CA-port%EF%BC%88%E3%82%A8%E3%83%BC%E3%83%9D%E3%83%BC%E3%83%88%EF%BC%89%E3%80%8D%E3%80%82&amp;type=share&amp;url=https%3A%2F%2Fa-port.asahi.com%2F" style="position: static; visibility: visible; width: 72px; height: 20px;" data-url="https://a-port.asahi.com/"></iframe></li>
				<li>
					<span>
						<script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411"></script>
						<script type="text/javascript">
						{literal}
						new media_line_me.LineButton({"pc":false,"lang":"ja","type":"a","text":"朝日新聞社のクラウドファンディングサイト「A-port（エーポート）」","withUrl":true});
						{/literal}
						</script>
					</span>
				</li>
			</ul>
		</div>
		<div class="footer-logo-wrapper">
			<div class="footer-logo-inner cf">
				<div class="footer-logo footer-logo-asahi-shinbun">
					<a href="http://www.asahi.com/shimbun/" target="_blank"><img src="{$smarty.const.C_ASSETS}images/common/logo-asahi-shinbun.png" alt="朝日新聞"></a>
				</div>
				<p class="footer-logo-txt">A-portは朝日新聞社のクラウドファンディングサイトです。</p>
			</div>
			<div class="footer-logo-inner cf">
				<div class="footer-logo footer-logo-motion-gallery">
				</div>
			</div>
		</div>
		<div class="copyright">
			<p>Copyright &copy; The Asahi Shimbun Company.<br />No reproduction or republication without written permission.</p>
		</div>
	</div>
</footer>
