{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/blog.css">
<script src="//www.youtube.com/player_api"></script>
<meta property="og:type" content="website">
<meta property="og:site_name" content="A-port｜朝日新聞社のクラウドファンディングサイト">
<meta property="og:url" content="{$smarty.const.ROOT_URL}/projects/{$input_data.hope_url}">
<meta property="og:title" content="{$input_data.public_title}| 【A-port】">
<meta property="og:description" content="{$input_data.public_title}">
<meta property="og:image" content="{$smarty.const.C_IMG_PROJECT}{$input_data.cover_img}">
<script>
{literal}
$(function() {
    $(".modalHead > .closeBtn").click(function() {
        $(".modalSection").addClass("off")
    }),
    $(".append_modal").click(function() {
        $(".modalSection").removeClass("off"),
        console.log("test"),
        console.log("test")
    }),
    $(".modalCover").click(function() {
        $(".modalSection").addClass("off")
    }),
    $(".disabled").click(function() {
        return ! 1
    })
});
{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul class="tab clearfix mb15">
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting1.php?p_no={$input_data.no}">基本情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">リターン内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting3.php?p_no={$input_data.no}">プロジェクト内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting4.php?p_no={$input_data.no}" >SNSとPR</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting5.php?p_no={$input_data.no}" >起案者情報</a></li>
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">プレビュー</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
			<div id="fragment-6" class="clearfix">
				<div class="container container-offset cf border-10px">
				<p style="padding: 35px 25px 10px 25px;">下記プレビューで原稿内容、リターン内容（お届け日）、ファンディングタイプ、掲載期間など間違いないかご確認ください。<br>
						ご確認後、「審査へ提出」ボタンを押してください。</p>
				<div class="page-ttl-wrapper" style="padding-top:0px;">
					<div class="container page-ttl-inner cf">
						<h2 class="page-ttl">{$input_data.public_title}</h2>
						<ul class="page-ttl-info cf" style="margin-right: 50px;">
							<li class="page-ttl-info-presenter"><a href="#">{$input_data.full_name}</a></li>
							<li class="page-ttl-info-category"><a href="#">{$array_category[$input_data.category_no]}</a></li>
						</ul>
					</div>
				</div>
				{if $input_data.movie_type=="1"}
				<script>
				{literal}
						// 2. This code loads the IFrame Player API code asynchronously.
						var tag = document.createElement('script');
						tag.src = "//www.youtube.com/player_api";
						var firstScriptTag = document.getElementsByTagName('script')[0];
						firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
						// 3. This function creates an <iframe> (and YouTube player)
						//    after the API code downloads.
						var player;
						function onYouTubeIframeAPIReady() {
								player = new YT.Player('moviePlayer', {
				{/literal}
										videoId: '{$youtube_id}',
				{literal}
										events: {
												'onReady': onPlayerReady
										}
								});
						}
						// 4. The API will call this function when the video player is ready.
						function onPlayerReady(event) {
								$('.project-cover').click(function(){
										$('.project-cover').hide();
										event.target.playVideo();
								});
						}
				{/literal}
				</script>
				{/if}
				{if $input_data.movie_type=="2"}
				<script>
				{literal}
					$(function() {
							var player = $('iframe');
							var playerOrigin = '*';
							var status = $('.status');

							// Listen for messages from the player
							if (window.addEventListener) {
									window.addEventListener('message', onMessageReceived, false);
							}
							else {
									window.attachEvent('onmessage', onMessageReceived, false);
							}

							// Handle messages received from the player
							function onMessageReceived(event) {
									// Handle messages from the vimeo player only
									if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
											return false;
									}
									if (playerOrigin === '*') {
											playerOrigin = event.origin;
									}
									var data = JSON.parse(event.data);
									
									switch (data.event) {
											case 'ready':
													onReady();
													break;
											case 'playProgress':
													onPlayProgress(data.data);
													break;
											case 'pause':
													onPause();
													break;
											case 'finish':
													onFinish();
													break;
									}
							}
							// Call the API when a button is pressed
							$('.project-cover').on('click', function() {
									$('.project-cover').hide();
									//post($(this).text().toLowerCase());
									post('play');
							});
							// Helper function for sending a message to the player
							function post(action, value) {
									var data = {
										method: action
									};
									if (value) {
											data.value = value;
									}
									var message = JSON.stringify(data);
									player[0].contentWindow.postMessage(data, playerOrigin);
							}
							function onReady() {
									status.text('ready');
									post('addEventListener', 'pause');
									post('addEventListener', 'finish');
									post('addEventListener', 'playProgress');
							}
							function onPause() {
									status.text('paused');
							}
							function onFinish() {
									status.text('finished');
							}
							function onPlayProgress(data) {
									status.text(data.seconds + 's played');
							}
					});
				{/literal}
				</script>
				{/if}
					<div class="col-main">
						<div class="col-inner">
							<div class="local-nav-wrapper">
								<ul class="local-nav">
									<li class="local-nav-home count-off active">
										<a href="#">
											<p>ホーム</p>
										</a>
									</li>
									<li class="local-nav-update count-on">
										<a href="#">
											<p>アップデート</p>
											<p class="count"><span>0</span></p>
										</a>
									</li>
									<li class="local-nav-collector count-on">
										<a href="#">
											<p>支援者</p>
											<p class="count"><span>0</span></p>
										</a>
									</li>
								</ul>
							</div>
							<div class="project-thum">
								<div class="project-cover">
									{if $input_data.movie_type!="0"}
									<p class="btn-play">Play</p>
									{/if}
									{if $smarty.session.TMP_FUP11!=""}
										<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP11}" width="585" height="418" />
									{elseif $input_data.cover_img!=""}
										<img class="cover-image" src="{$smarty.const.C_IMG_PROJECT}{$input_data.cover_img}" alt="プロジェクトイメージ">
									{/if}
								</div>
								{if $input_data.movie_type=="1"}
								<div class="movie-player">
									<div id="moviePlayer"></div>
								</div>
								{/if}
								{if $input_data.movie_type=="2"}
								<div class="movie-player">
									<iframe src="//player.vimeo.com/video/{$vimemo_id}?api=1"  width="585" height="418" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
								</div>
								{/if}
								<div class="project-cover-base">
									{if $smarty.session.TMP_FUP11!=""}
										<img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP11}" width="608" height="435" />
									{elseif $input_data.cover_img!=""}
										<img class="cover-image" src="{$smarty.const.C_IMG_PROJECT}{$input_data.cover_img}" alt="プロジェクトイメージ">
									{/if}
								</div>
								
							</div>
							<div class="sns-btn-set">
								<ul class="clearfix">
									<li class="fblike"><div class="fb-like" expr:data-href="data:post.url" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
									<li class="tweet">
										<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
										{literal}
										<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
										{/literal}
									</li>
									<li class="hatena"><a href="http://b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="simple" title="はてなブックマークに追加"><img src="https://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="はてなブックマークに追加" width="20" height="20" style="border: none;" /></a><script type="text/javascript" src="https://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async></script></li>
									<li class="append_modal blog"><a>ブログに埋め込む</a></li>
								</ul>
							</div>
							<div class="project-description">
								<h2 class="project-description-ttl">このプロジェクトについて</h2>
								<div class="project-description-inner jsc-project-description">
									{$input_data.project_text|nl2br nofilter}
								</div>
							</div>
							<div class="project-detail">
								<div class="btn-project-detail-open jsc-btn-project-detail-open">
									<a href="javascript:void(0);">続きを見る</a>
								</div>
								<div class="btn-project-detail-close jsc-btn-project-detail-close jsc-dn">
									<a href="javascript:void(0);">閉じる</a>
								</div>
							</div>

							<div class="project-invest-box">
								<div class="btn-project-invest">
									<a href="#">プロジェクトを応援する</a>
								</div>
							</div>
							<div class="project-social-bottom-wrapper cf">
								<ul class="project-social-bottom cf">
									<li class="fb"><a href="http://www.facebook.com/share.php?u={$smarty.const.ROOT_URL}/projects/{$input_data.hope_url}" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">facebookでシェア</a></li>
									<li class="tw"><a href="http://twitter.com/share?url={$smarty.const.ROOT_URL}/projects/{$input_data.hope_url}&text=～{$input_data.public_title}～ / 【A-port】」" target="_blank">Twitterでシェア</a></li>
									<li class="append_modal blog"><a>ブログに埋め込む</a></li>
								</ul>
							</div>
						</div>
					</div><!--/col-main-->
					<div class="modalSection off">
						<!-- <div class="modalSection off"> で非表示
						-->
						<div class="modalCover"></div>
						<div class="modalArea">
							<div class="modalPosition">
								<div class="modalBox">
									<div class="modalHead">
										<h4 class="modalTitle">プロジェクトをブログやサイトで紹介する</h4>
										<span class="closeBtn">×</span>
									</div>
									<div class="modalInner">
										<div class="pjtItemCode">
											<p>テキストエリアのコードをあなたのブログやサイトのHTMLに張り付けると、右にあるようなプロジェクトウィジェットが表示されます。</p>
											<textarea readonly="readonly">&lt;iframe src='{$smarty.const.ROOT_URL}projects/{$input_data.hope_url}/widget' frameborder="0" scrolling="no" width="313" height="403"&gt;&lt;/iframe&gt;</textarea>
										</div>
										<div class="pjtItemPreview">
											<iframe src="{$smarty.const.ROOT_URL}projects/{$input_data.hope_url}/widget" frameborder="0" scrolling="no" width="313" height="401"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sub">
						<div class="col-inner">
							<ul class="sub-project-status">
								<li class="sub-project-status-collector">
									<p class="sub-project-status-label">支援者</p>
									<p class="sub-project-status-var"><span>0</span>人</p>
								</li>
								<li class="sub-project-status-last">
									<p class="sub-project-status-label">残り期間</p>
									<p class="sub-project-status-var"><span>{$input_data.diff_in_days}</span>日</p>
								</li>
								<li class="sub-project-status-fund">
									<p class="sub-project-status-label">集まっている金額</p>
									<p class="sub-project-status-var"><span>0</span>円</p>
									<p class="sub-project-status-target">目標金額：<span>{$input_data.wish_price|number_format}</span>円</p>
								</li>
								<li class="sub-project-status-per">
									<p class="sub-project-status-label">達成率<span>0</span>%</p>
									<div class="progress">
										<div class="gauge" style="width:100%;">
											<div class="gauge2" style="width:0%;"></div>
										</div>
									</div>
								</li>
							</ul>
							{if $input_data.project_type=="1"}
								<p class="project-notes">このプロジェクトでは、<span class="">{$input_data.invest_limit|date_format:"%Y年%m月%d日"}23:59</span>までに目標に到達した場合のみ、ファンディングが実行されます。</p>
							{else}
								<p class="project-notes">このプロジェクトでは、目標到達に関わらず、<span class="">{$input_data.invest_limit|date_format:"%Y年%m月%d日"}23:59</span>までに集まった金額がファンディングされます。</p>
							{/if}
							<div class="btn-project-invest">
								<a href="#">プロジェクトを応援する</a>
							</div>
						</div>
						<div class="col-inner">
							<h4 class="sub-presenter-ttl">実行者</h4>
							<div class="sub-presenter-profile cf">
								{if $smarty.session.TMP_FUP13!=""}
									<div class="sub-presenter-thum"><img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP13}" alt="実行者イメージ"></div>
								{elseif $input_data.profile_img!=""}
									<div class="sub-presenter-thum"><img src="{$smarty.const.C_IMG_ACTRESS}{$input_data.profile_img}" alt="実行者イメージ"></div>
								{/if}
								<div class="sub-presenter-meta">
									<p class="sub-presenter-name">{$input_data.full_name}</p>
									<p class="sub-presenter-location">{$array_area[$current_user.area]}</p>
								</div>
							</div>
							<div class="sub-presenter-link">
								<a href="#">{$input_data.blog_url_1}</a>
							</div>
							<div class="sub-presenter-detail">
								{mb_substr($input_data.introduce,0,20,'utf-8')}
								<span class="jsc-sub-presenter-detail-dot">...</span>
								<span class="jsc-sub-presenter-detail-inner jsc-dn">
								{mb_substr($input_data.introduce,21,1000,'utf-8')}
								</span>
								<div class="sub-presenter-detail-more-open jsc-sub-presenter-detail-more-open"><a href="javascript:void(0);">続きを見る</a></div>
								<div class="sub-presenter-detail-more-close jsc-sub-presenter-detail-more-close jsc-dn"><a href="javascript:void(0);">閉じる</a></div>
							</div>
							<!--
							<div class="btn-presenter-question">
								<a href="#">質問や意見をメールする</a>
							</div>
							-->
						</div>
						<div class="sub-ticketlist-wrapper">
							<ul class="sub-ticketlist">
							{for $k=1 to 30}
								{if $input_data['return_min'][$k]!=""}
								<li>
									{if $input_data['max_count'][$k]=="9999"||$input_data['max_count'][$k]==""}
									{else}
										<span class="limitedTicket">残り{$input_data['max_count'][$k]}枚</span>
									{/if}
									<p class="sub-ticketlist-fund jsc-sub-ticketlist-fund-toggle"><span>{$input_data['return_min'][$k]|number_format}</span>円</p>
									<div class="sub-ticketlist-inner jsc-sub-ticketlist-fund-inner">
										<p class="returnTitle">{$input_data.return_title.$k}</p>
										<p class="sub-ticketlist-return-ttl">リターン</p>
										{if $smarty.session.TMP_FUP.$k!=""}
											<p class="sub-ticketlist-img"><img src="{$smarty.const.C_IMG_TMP}{$smarty.session.TMP_FUP.$k}" alt="" width=280 height=165></p>
										{elseif $input_data.return_img.$k!=""}
											<p class="sub-ticketlist-img"><img src="{$smarty.const.C_IMG_PROJECT}{$input_data.return_img.$k}" alt="" width=280 height=165></p>
										{/if}
										<ul class="sub-ticketlist-return">
											{$input_data['return_text'][$k]|nl2br nofilter}
										</ul>
										<p class="sub-ticketlist-collector">
											<span class="sub-ticketlist-collector-label">支援者の数</span>
											<span class="sub-ticketlist-collector-var">0</span>人
										</p>
										<p class="sub-ticketlist-delivery">
											お届け予定：<span class="sub-ticketlist-delivery-var">{$input_data['return_year'][$k]}年{$input_data['return_month'][$k]}月上旬</span>
										</p>
										<div class="project-invest-box">
											<div class="btn-project-invest">
												<a href="#">プロジェクトを応援する</a>
											</div>
										</div>
									</div>
								</li>
								{/if}
							{/for}
							</ul>
						</div>
					</div><!--/col-sub-->
					<ol class="breadcrumb cf">
						<li><a href="#">サイトトップ</a></li>
						<li><a href="#">{$array_category[$input_data.category_no]}</a></li>
						<li><a href="#">{$input_data.public_title}</a></li>
					</ol>
				</div><!--/container-->
				<ul class="formStep">
					<li class="formPrev"><a href="{$smarty.const.ROOT_URL}ja/drafting5.php?p_no={$input_data.no}">戻る</a></li>
					<li class="formNext"><input id="next_f6" value="審査へ提出" type="submit" name="sbm_update"/></li>
					<!--<li class="formNext"><input id="next_f6" value="変更を提出" type="submit" name="sbm_update"/></li>-->
				</ul>
			</div>
			<!--/#fragment-6  --> 
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
<!-- スマートフォン表示専用 -->
<div class="project-social-sp-wrapper">
	<h3 class="project-social-sp-ttl">このプロジェクトをシェアする</h3>
	<ul class="project-social-sp cf">
		<li class="fb"><a href="http://www.facebook.com/share.php?u={$smarty.const.ROOT_URL}/projects/{$input_data.hope_url}" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">facebookでシェア</a></li>
		<li class="tw"><a href="http://twitter.com/share?url={$smarty.const.ROOT_URL}/projects/{$input_data.hope_url}&text=～{$input_data.public_title}～ / 【A-port】」" target="_blank">Twitterでシェア</a></li>
		<li class="line"><a href="http://line.me/R/msg/text/?～{$input_data.public_title}～ / 【A-port】{$smarty.const.ROOT_URL}/projects/{$input_data.hope_url}">LINEで送る</a></li>
	</ul>
</div>
<!-- スマートフォン表示専用 -->
{include file="{$lang}/sf-footer.tpl"}

<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.js"></script>
<script type="text/javascript" src="{$smarty.const.C_ASSETS}js/Validate.form.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
