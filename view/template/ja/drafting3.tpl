{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/detail.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<link href="{$smarty.const.C_ASSETS}css/pages/drafting.css" media="all" rel="stylesheet" />

<!-- HTML エディタ -->
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}js/redactor/redactor.css"/>

<script src="{$smarty.const.C_ASSETS}js/redactor/redactor.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/langs/ja.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/video.js"></script>
<script src="{$smarty.const.C_ASSETS}js/redactor/table.js"></script>
<script type="text/javascript">
{literal}
$(document).ready(function () {
	$('#project_text').redactor({
		//buttons:['html','formatting','bold','italic','deleted','unorderedlist','orderedlist','outdent','indent','image','video','file','table','link','alignment','horizontalrule'],
		buttonSource: true,
		convertVideoLinks:true,
		imageUpload:'upload.php',
		plugins: ['table', 'video'],
		lang:'ja',
		formatting: ['h2', 'p', 'blockquote']
		});
});
{/literal}
</script>
</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<!--LV1 wrapper Start -->
<div id="wrapper" class="clearfix">
	<!--LV2 drafting Start -->
	<div id="drafting">
		<!--LV3 ui-tab Start -->
		<div id="ui-tab">
			<!--LV4 tab Start -->
			<ul class="tab clearfix mb15">
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting1.php?p_no={$input_data.no}">基本情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">リターン内容</a></li>
				<li><a href="javascript:void(0);" style="background-color:#1A6CA3;color:#FFF;">プロジェクト内容</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting4.php?p_no={$input_data.no}" >SNSとPR</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting5.php?p_no={$input_data.no}" >起案者情報</a></li>
				<li><a href="{$smarty.const.ROOT_URL}ja/drafting6.php?p_no={$input_data.no}">プレビュー</a></li>
			</ul>
			<!--LV4 tab End -->
			<!--LV4 form Start -->
			<form id="frm_tab" name="frm_tab" method="post" enctype="multipart/form-data" action="">
			<input type="hidden" name="no" value="{$input_data.no}">
			<input type="hidden" name="add_1" value="{$input_data.add_1}">
			<div id="fragment-3" class="clearfix">
				<div class="draftingLeft" >
					<p>このプレゼン文書の目的は、A-portを見る人の関心を引き、プロジェクトについて理解してもらい、支援したいと思ってもらうことです。思いを長々とつづっても、読み手は途中で読むのをやめてしまいかねません。まずタイトルでプロジェクトへの関心を持ってもらい、最初の要旨や概要の部分で大まかな内容を理解してもらうことが、最も重要です。奇をてらう表現は必要ありません。内容が整理されていて、簡潔で誰にもわかりやすいことが第一です。全体量は長くても原稿用紙４枚強（約１８００字）までが目安とお考えください。
					（以下のグレーの文字で書かれた内容が、おすすめの構成です。ぜひご参照ください）</p>
          <div class="htmlEditor">
            <textarea style="width: 780px; height:600px;" id="project_text" name="project_text" >
						{if $input_data.project_text==""}
■タイトル


■このプロジェクトについて
プロジェクトの要旨を簡潔に伝える部分です。１２０字以内で記入してください。「いつ」「どこで」「だれが」「何を」するために資金を集めるのか、それによって実現したいことは何か、具体的にわかりやすく書いてください。






■見出し①
プロジェクトの概要を書いてください。自己紹介を交えつつ、「いつ」「どこで」「誰が」「何を」「どのように」するプロジェクトなのか、全体像を具体的に記入してください。支援したいと思ってもらえるように、アピールするポイントも入れてください。この概要だけで全体が理解できるようにするのが重要で、これより後ろは読まれないかもしれないという前提でお書きください。４００字以内が目安です。









■見出し②
プロジェクトに関わりのある今までの活動、作品などを交え、今回、なぜこのプロジェクトを立ち上げるのか、背景、理由を書いてください。これまでどんなことをしてきたのか、どうしてプロジェクトを思い立ったかなど、わかりやすく説明してください。これまでの活動や作品の画像やＵＲＬを入れると読みやすくなります。４００字以内が目安です。







■見出し③
募集するお金の使い道や、それによって実現したい事、それが賛同者や社会にとってどんな価値があるのか、どういう効果を期待できるのかを書いてください。共感を得るのに重要なポイントです。どんな人に向けて訴えるのか、誰が賛同してくれるか想像しながら書きましょう。「資金が集まったらこんな事が始まるんだ！」と期待してもらえる内容が理想的です。４００字以内が目安です。






■見出し④
主なリターン（特典）についての説明を書いてください。それぞれのサンプル画像を掲載すると効果的です。別の欄に詳細が載るので、すべて書く必要はなく、魅力的な部分に絞って書いてください。２００字以内が目安です。






■想定されるリスク
プロジェクトの実施スケジュールが遅れる可能性、現時点の誓約から変更がある可能性など、プロジェクトが持つリスク、障害と、その対策ついて書いてください。２００字以内が目安です。






■見出し⑤
最後にプロジェクトにかけるあなたの想いや、ページを見てくれた人へのお礼の気持ちを書きましょう。１２０字以内が目安です。
{else}
						{$input_data.project_text}
{/if}
						</textarea>
          </div>
					<ul class="formStep">
						<li class="formPrev"><a href="{$smarty.const.ROOT_URL}ja/drafting2.php?p_no={$input_data.no}">戻る</a></li>
						<li class="formNext"><input id="next_f3" value="次へ" type="submit" name="d3" style="float:right;"/>
						<div class="clearfix"></div>
						<p style="float:right;margin-top:15px;">「次へ」ボタンを押すことで、上記内容は全て保存されます。</p>
						</li>
					</ul>
				</div>
			</div>
			<!--/#fragment-3  -->
			</form>
		</div>
		<!-- #ui-tab --> 
	</div>
	<!-- #drafting --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
