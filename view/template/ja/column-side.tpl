<div class="col-sub">
    <div class="col-inner">
        <div class="column-sub">
            <div class="sub-column-box">
                <h3 class="ttl-column-sub ttl-sub-category">CATEGORY</h3>
                <ul class="sub-column-category">
                	{foreach item=con from=$categorylist}
                    <li><a href="column.php?c_no={$con.cat_no}">{$con.cat_name}</a></li>
                    {/foreach}
                </ul>
            </div>
            <div class="sub-column-box">
                <h3 class="ttl-column-sub ttl-sub-recent-entries">RECENT ENTRIES</h3>
                <ul class="sub-column-list">
                    {foreach item=con from=$column_recent_list}
                    <li><a href="column-detail.php?id={$con.id}">{$con.title}</a></li>
                    {/foreach}
                </ul>
            </div>
            <div class="sub-column-box">
                <h3 class="ttl-column-sub ttl-sub-tags">TAGS</h3>
                <ul class="sub-column-tags">
                	{foreach item=con from=$taglist}
                    <li><a href="column.php?tag={$con.tag_id}">{$con.tag_name}</a></li>
                    {/foreach}
                </ul>
            </div>
            
            <div class="sub-column-box">
                <h3 class="ttl-column-sub ttl-sub-share">SHARE</h3>
                <ul class="sub-column-social cf">
                    <li class="fb"><a href="javascript:void(0);"><img src="{$smarty.const.C_ASSETS}images/column/social-fb.png"></a></li>
                    <li class="tw"><a href="javascript:void(0);"><img src="{$smarty.const.C_ASSETS}images/column/social-tw.png"></a></li>
                    <li class="line"><a href="javascript:void(0);"><img src="{$smarty.const.C_ASSETS}images/column/social-line.png"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>