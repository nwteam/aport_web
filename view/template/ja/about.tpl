{include file="{$lang}/sf-head.tpl"}

<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/about.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->

</head>
<body>
{include file="{$lang}/sf-header.tpl"}

<div class="wrapper">
<div class="page-ttl-wrapper"  style="padding-top:0px;">
	<div class="container page-ttl-inner cf">
		<h2 class="page-ttl">A-portについて</h2>
		<p class="sub-text">A-portは、朝日新聞社が運営するクラウドファンディングサイトです。<br />誰もが等しく挑戦でき、「クラウドファンディングで支援する」というお金の使い方が日本に定着することを目指しています。</p>
	</div>
</div>
<div class="body">
	<div class="cnt-inner">
		<div class="illust">
			<p><img src="{$smarty.const.C_ASSETS}images/about/about_ship_1.gif"></p>
			<p><img src="{$smarty.const.C_ASSETS}images/about/about_ship_wave2.gif"></p>
			<p><img src="{$smarty.const.C_ASSETS}images/about/about_ship_wave3.gif"></p>
		</div>
		<div class="cnt-txt">

			<h3><img src="{$smarty.const.C_ASSETS}images/about/about_lead_text.png" alt=""></h3>
			<h4>A-portの成り立ち</h4>
			<p>朝日新聞社は、言論・報道機関として、世の中のさまざまな出来事を読者のみなさまにお知らせすることを使命としています。素晴らしい技術や独創的なアイデア、高い志を持つ方々が社会の課題解決に向けて取り組んでいる地道な活動にも光を当ててきました。</p>
			<p>そして今、情報の発信から一歩進め、情報に共感するみなさまが自ら直接支援の輪に加わる「場」を創出したいと考えました。</p>
			<p>いくら夢や情熱があっても、個人がゼロから新しい商品やサービスを生み出す資金を得るのは容易ではありません。でも、インターネットで少額からの資金提供を呼びかけられる「クラウドファンディング」は、その可能性を大きく広げるしかけです。</p>
			<p>一人ひとりのアクションはたとえ小さくても、こうしたチャレンジの成功を後押しし、夢の実現を積み重ねていくことが、世の中を明るくし、社会をよりよく、生き生きとしたものにすると信じ、私たちは新たなクラウドファンディングサイト「A-port」を立ち上げました。</p>
			<p>新聞社として培ってきた情報発信力などの強みを活かしつつ、この仕組みが幅広い世代に支持され、日本に定着していくことを目指します。</p>

			<h4>「A-port」ネーミングの由来</h4>
			<p>A-port は、夢を持った起案者（実行者）が帆を立てて出航への準備をする港（port）のような存在です。</p>
			<p>冒頭の「A」には、ここに集った起案者（実行者）と支援者（サポーター）が一緒になって冒険（Adventure）する、行動（Action）する、という意味が込められています。</p>

			<h4>ミッション・ビジョンについて</h4>
			<p><span class="txt-ttlsub2">私たちのミッション</span><br />
			誰もが等しく挑戦でき、誰もが等しく支援できる社会をつくる。<br />
			そして、世の中の課題を解決し、新しいビジネス、文化を創出していく。<br /></p>
			<p class="mt20 mb30"><span class="txt-ttlsub2">私たちのビジョン</span><br />
			私たちは、日本に「クラウドファンディングで支援する」という新しいお金の使い方が定着することを目指していきます。<br /></p>

		</div>
	</div>
		<ol class="breadcrumb cf">
			<li><a href="/">サイトトップ</a></li>
			<li>A-portについて</li>
		</ol>
	</div><!--/container-->
</div>
<!--/#wrapper-->

{include file="{$lang}/sf-footer.tpl"}

<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
<script src="{$smarty.const.C_ASSETS}js/changePosition.js"></script>
{literal}
<!--twitter-->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
{/literal}
</body>
</html>
