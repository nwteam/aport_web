{$user_data.name}様

A-port運営事務局です。登録手続きありがとうございます。

仮登録が完了しました。

以下のURLをクリックして本登録を完了させてください。

{$regist_url}

‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
朝日新聞クラウドファンディング A-port
WEB : https://a-port.asahi.com/
MAIL: info@a-port-mail.com
‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
