本メールはシステムの自動応答です。御用の際は「info@a-port-mail.com」迄ご連絡下さい。)
{$m_data.name_1} {$m_data.name_2} 様

A-portをご利用いただきありがとうございます。
ご支援をお申し込みいただいておりました下記の件につきまして、ご入金を確認いたしました。
応援内容を反映させていただきましたので、是非サイトでご確認ください。
お忙しい所、ご対応頂き誠にありがとうございました。

https://a-port.asahi.com/projects/{$p_data.hope_url}

■支援内容
・プロジェクト名：{$p_data.public_title}
・入金金額：{$total_amount|number_format}円
・リターン
{$pre_data.text|nl2br nofilter}

-----------------------------------------
朝日新聞 クラウドファンディング A-port
 WEB : https://a-port.asahi.com/
 MAIL: info@a-port-mail.com
 -----------------------------------------