{$db_data.name_1} {$db_data.name_2}様
A-port運営事務局です。登録手続きありがとうございます。

本登録が完了しました。

{if $db_data.member_name!=""}
ログイン後、マイページのアカウント情報にて
・ニックネームの変更
・プロフィール画像の登録
・パスワードの変更
を行ってください。

{/if}

‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
朝日新聞社クラウドファンディング A-port
WEB : https://a-port.asahi.com/
MAIL: info@a-port-mail.com
‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥



