{$m_data.member_name}様

「{$p_data.project_name}」へ支援登録をしていただきましたが、
クレジットカード認証でエラーが発生いたしましたので、ご連絡差し上げます。

■支援No：{$i_data.no}
■プロジェクト名：{$p_data.project_name}
■支援コース：{$pre_data.title}
■コース内容：
{$pre_data.text}

■支援金額：{$i_data.invest_amount|number_format}円


大変申し訳ございませんが、上記の支援登録を完了することができませんでした。
クレジットカード情報をご確認の上、再度支援登録をしていただくようお願いいたします。



‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
朝日新聞クラウドファンディング A-port
WEB : https://a-port.asahi.com/
MAIL: info@a-port-mail.com
‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
