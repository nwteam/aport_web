
 ‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
 A-port 
 支援受け付け完了 
 ‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥ 
 
 
 {$m_data.name_1} {$m_data.name_2} 様
 
 この度は、A-portのプロジェクト 『{$p_data.public_title}』を支援頂き、誠にありがとうございます。 
 支援の申請を受け付け、A-portのプロジェクト 『{$p_data.public_title}』の支援者になりました事をここにお知らせ致します。
 
 A-portのプロジェクト 『{$p_data.public_title}』が、目標金額{$p_data.wish_price|number_format}円に到達出来る様、ご協力お願い致します！
 
 ■支援内容
 
 プロジェクト名 A-portのプロジェクト 『{$p_data.public_title}』
 応援した金額 {$total_amount|number_format}円
 リターン
 『{$pre_data.text|nl2br nofilter}』をお送りします。
 お届け先 〒{$m_data.post_zip_code} {$a_area[$m_data.add_1]}{$m_data.post_add_2}{$m_data.post_add_3}
 ※ お届け先の変更を希望される場合は{$d_data.email}より直接起案者にご連絡下さい。
 
 
 --------------------------
 朝日新聞 クラウドファンディング A-port
 WEB : https://a-port.asahi.com/
 MAIL: {$C_INQUIRY_EMAIL}
 --------------------------


