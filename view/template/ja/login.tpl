{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/signin.css">
<!--[if lt IE 9]>
<script src="{$smarty.const.C_ASSETS}js/lib/html5shiv-printshiv.js"></script>
<script src="{$smarty.const.C_ASSETS}js/lib/respond.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper">

{include file="{$lang}/sf-header.tpl"}
 <!--#contents-->
  <div id="signIN">
      <h2><img src="{$smarty.const.C_ASSETS}images/logo_aport_title.png" alt="A-port">のプロジェクトを応援するには、ログインが必要です。</h2>
      <div class="contentBlockLeft"> 
          <h3>メールアドレスでログインする</h3>
	{if $err_msg.top}
		<p class="errorTitle" style="color:red;">{$err_msg.top|nl2br nofilter}</p>
	{/if}
          <form method="post">
            <dl>
              <dt>メールアドレス</dt>
              <dd>
                <input type="text" id="email" maxlength="100" size="45" name="email" value="{$input_data.email}" style="width:100%"/>
                <p class="note">※半角英数字</p>
              </dd>
              <dt>パスワード</dt>
              <dd>
                <input type="password" name="password" size="30" id="pw" value="{$input_data.password}" style="width:100%"/>
                <p class="note">※半角英数字</p>
              </dd>
            </dl>
            <div>
              <input class="btnSubmit" name="sbm_login" id="btnLogin" type="submit" value="ログインする" />
            </div>
          </form>
          <ul class="troubleLinkList">
            <li><a href="{$pagelink_register}">新規登録はこちら</a></li>
            <li><a href="{$pagelink_reissue}">パスワードをお忘れの場合はこちら</a></li>
          </ul>
      </div>
      
      
      <div class="contentBlockRight">
          <h3>Facebookを利用してログインする</h3>
          <p class="facebookLogin"> <a class="btnFacebookL" href="{$pagelink_login}?type=f">Facebookでログインする</a> </p>
          <p id="fbNote" class="note">※ログイン後、自動的にサイトに戻ります。</p>
<!--
					<h3>Twitterを利用してログインする</h3>
          <p class="twitterLogin"> <a class="btnTwitterL" href="#">Twitterでログインする</a> </p>
          <p id="twNote" class="note">※ログイン後、自動的にサイトに戻ります。</p>
-->
      </div>
      <!-- .contentBlockRight --> 
  </div>
  <!-- #signIN --> 
 <!--/#contents-->
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
