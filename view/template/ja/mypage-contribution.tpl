{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<h2>応援したプロジェクト</h2>
	<ul class="project-list-index">
	{foreach from=$project_data_list item="item" name="loop"}
		<li>
		<div class="project-img"><a href="{$pagelink_detail_project}{$item.hope_url}"><img src="{$smarty.const.C_IMG_PROJECT}{$item.cover_img}" alt="{$item.public_title}"  width=260 height=186></a></div>
		<div class="project-box">
			<p class="project-title"><a href="{$pagelink_detail_project}{$item.hope_url}">{$item.public_title}</a></p>
			<ul class="project-tip cf">
				<li class="project-tip-category" style="width:120px;"><a href="{$pagelink_list_project}?c_no={$item.category_no}">{$item.category_name}</a></li>
				<li class="project-tip-presenter">{$item.member_nickname}</li>
			</ul>
			<div class="project-summary">
				<div class="progress">
					<div class="gauge" style="width:100%;">
						<div class="gauge2" style="width:100%;">
							<div class="gauge3" style="width:50%;">
							</div>
						</div>
					</div>
				</div>
				<ul class="project-status">
					<li class="project-status-fund">
						<p class="project-status-label" style="width:130px;">集まった金額</p>
						<p class="project-status-var"><span>{$item.now_summary|number_format}</span>円</p>
					</li>
					<li class="project-status-per">
						<p class="project-status-label">達成率</p>
						<p class="project-status-var"><span>{$item.percent}</span>%</p>
					</li>
					<li class="project-status-last">
						<p class="project-status-label">あと</p>
						<p class="project-status-var">{$item.diff_in_days}日</p>
					</li>
				</ul>
			</div>
		</div>
		</li>
	{foreachelse}
	<p class="noProject">支援しているプロジェクトはありません</p>
	{/foreach}
	</ul>
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
