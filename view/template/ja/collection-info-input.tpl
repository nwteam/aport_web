{include file="{$lang}/sf-head.tpl"}
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link href="{$smarty.const.C_ASSETS}css/project.css" media="all" rel="stylesheet" />
<script type='text/javascript'>
{literal}

{/literal}
</script>

<title>{$str_site_title}</title>

</head>
<body>
{include file="{$lang}/sf-header.tpl"}
<div id="wrapper" class="clearfix">
	<ul id="step">
		<li>1. 応援リターンの選択 </li>
		<li class="active">2. 決済情報の入力 </li>
		<li>3. 応援内容の確認 </li>
		<li>4. 完了<span>（応援コメントを寄せよう）</span> </li>
	</ul>
	<div id="projectStart">
		<div id="projectContents">
			<h2>決済情報の入力</h2>
			{if $err_msg1.top}
				<p class="errorTitle" style="color:red;">※{$err_msg1.top}</p>
			{/if}
			<form id="paymentForm" accept-charset="UTF-8" action="" method="post">
			<input type="hidden" id="pm" name="pm" value="{$payment_method}" />
				{if $payment_method=="Credit"}
				<h3>カード情報</h3>
				<p id="available">ご利用できるカード：<img alt="「VISA」「MasterCard」" src="{$smarty.const.C_ASSETS}images/logo_visa.png" /></p>
					<dl>
						<dt>カード番号</dt>
						<dd>
							<input class="half" id="card_no" placeholder="1111 2222 3333 4444" size="36" name="card_no" type="text" value="{$input_data.card_no}" />
							{if $err_msg1.card_no}
								<p class="error" style="color:red;">※{$err_msg1.card_no}</p>
							{/if}
						</dd>
					</dl>
					<dl>
						<dt>有効期限</dt>
						<dd>
							<select name="expdate_month">
								{html_options  options=$array_expdate_m selected=$input_data.expdate_month }
							</select>
							/
							<select name="expdate_year">
								{html_options  options=$array_expdate_y selected=$input_data.expdate_year }
							</select>
							{if $err_msg1.expdate_month}
								<p class="error" style="color:red;">※{$err_msg1.expdate_month}</p>
							{else if $err_msg1.expdate_year}
								<p class="error" style="color:red;">※{$err_msg1.expdate_year}</p>
							{/if}
						</dd>
					</dl>
					<dl>
						<dt> カード名義</dt>
						<dd>
						<input id="hld_name" placeholder="TARO YAMADA" name="hld_name" size="36" type="text" value="{$input_data.hld_name}" />
						{if $err_msg1.hld_name}
							<p class="error" style="color:red;">※{$err_msg1.hld_name}</p>
						{/if}
						</dd>
					</dl>
					<dl>
						<dt> セキュリティコード </dt>
						<dd>
						<input id="security_code" placeholder="123" name="security_code" size="15" type="text" value="{$input_data.security_code}" />
						{if $err_msg1.security_code}
							<p class="error" style="color:red;">※{$err_msg1.security_code}</p>
						{/if}
							<span id="securityCode">セキュリティコードは、クレジットカードの裏面または表面に記載された3桁もしくは4桁の番号です。</span>
						</dd>
					</dl>
				{/if}
				{if $payment_method=="cvs"}
					 <ul id="paymentSelect">
							<li class="paySelect">
								<dl style="border-bottom:0px;">
									<dt>
										<span class="iconConvenience">コンビニ</span> 
									</dt>
									<dd>
											{if $err_msg1.convenience}
												<p class="error" style="color:red;">※{$err_msg1.convenience}</p>
											{/if}
										<ul>
											<label>
											<li>
												<input class="rd conveni_select" id="collection_conveni_status_" name="convenience" type="radio" value="00002" />
												<span><img src="{$smarty.const.C_ASSETS}images/logo_conveni01.png" alt="FamilyMart"/></span> </li>
											</label>
											<label>
											<li>
												<input class="rd conveni_select" id="collection_conveni_status_" name="convenience" type="radio" value="00004" />
												<span><img src="{$smarty.const.C_ASSETS}images/logo_conveni02.png" alt="サークルK"/></span> </li>
											</label>
											<label>
											<li>
												<input class="rd conveni_select" id="collection_conveni_status_" name="convenience" type="radio" value="00001" />
												<span><img src="{$smarty.const.C_ASSETS}images/logo_conveni03.png" alt="LAWSON"/></span> </li>
											</label>
											<label>
											<li>
												<input class="rd conveni_select" id="collection_conveni_status_" name="convenience" type="radio" value="00005" />
												<span><img src="{$smarty.const.C_ASSETS}images/logo_conveni04.png" alt="MINI STOP"/></span> </li>
											</label>
											<label>
											<li>
												<input class="rd conveni_select" id="collection_conveni_status_" name="convenience" type="radio" value="00008" />
												<span><img src="{$smarty.const.C_ASSETS}images/logo_conveni05.png" alt="Seicomart"/></span> </li>
											</label>
										</ul>
									</dd>
								</dl>
							</li>
							<li class="paySelect">
								<dl style="border-bottom:0px;">
								<dt>氏名(姓)</dt>
								<dd style="background:#F5F2F0;">
									<input class="half" id="ks_name_1" size="18" name="ks_name_1" type="text" value="{$input_data.ks_name_1}" />
									{if $err_msg1.ks_name_1}
										<p class="error" style="color:red;">※{$err_msg1.ks_name_1}</p>
									{/if}
								</dd>
								</dl>
							</li>
							<li class="paySelect">
								<dl style="border-bottom:0px;">
									<dt>氏名(名)</dt>
									<dd style="background:#F5F2F0;">
										<input class="half" id="ks_name_2" size="18" name="ks_name_2" type="text" value="{$input_data.ks_name_2}" />
										{if $err_msg1.ks_name_2}
											<p class="error" style="color:red;">※{$err_msg1.ks_name_2}</p>
										{/if}
									</dd>
								</dl>
							</li>
							<li class="paySelect">
							<dl style="border-bottom:0px;">
								<dt>氏名カナ(姓)</dt>
								<dd style="background: #F5F2F0;">
									<input class="half" id="ks_name_kana_1" size="18" name="ks_name_kana_1" type="text" value="{$input_data.ks_name_kana_1}" />
									{if $err_msg1.ks_name_kana_1}
										<p class="error" style="color:red;">※{$err_msg1.ks_name_kana_1}</p>
									{/if}
								</dd>
							</dl>
							</li>
							<li class="paySelect">
								<dl style="border-bottom:0px;">
									<dt>氏名カナ(名)</dt>
									<dd style="background: #F5F2F0;">
										<input class="half" id="ks_name_kana_2" size="18" name="ks_name_kana_2" type="text" value="{$input_data.ks_name_kana_2}" />
										{if $err_msg1.ks_name_kana_2}
											<p class="error" style="color:red;">※{$err_msg1.ks_name_kana_2}</p>
										{/if}
									</dd>
								</dl>
							</li>
							<li class="paySelect">
								<dl style="border-bottom:0px;">
									<dt>電話番号（半角数字）</dt>
									<dd style="background:#F5F2F0;">
										<input id="ks_tel" name="ks_tel" size="18" type="text" value="{$input_data.ks_tel}" />
										{if $err_msg1.ks_tel}
											<p class="error" style="color:red;">※{$err_msg1.ks_tel}</p>
										{/if}
									</dd>
								</dl>
							</li>
						</ul>
				{/if}
				{if $payment_method=="payeasy"}
						<dl>
						<dt>氏名(姓)</dt>
						<dd>
							<input class="half" id="ks_name_1" size="18" name="ks_name_1" type="text" value="{$input_data.ks_name_1}" />
							{if $err_msg1.ks_name_1}
								<p class="error" style="color:red;">※{$err_msg1.ks_name_1}</p>
							{/if}
						</dd>
						</dl>
						<dl>
							<dt>氏名(名)</dt>
							<dd>
								<input class="half" id="ks_name_2" size="18" name="ks_name_2" type="text" value="{$input_data.ks_name_2}" />
								{if $err_msg1.ks_name_2}
									<p class="error" style="color:red;">※{$err_msg1.ks_name_2}</p>
								{/if}
							</dd>
						</dl>
						<dl>
							<dt>氏名カナ(姓)</dt>
							<dd>
								<input class="half" id="ks_name_kana_1" size="18" name="ks_name_kana_1" type="text" value="{$input_data.ks_name_kana_1}" />
								{if $err_msg1.ks_name_kana_1}
									<p class="error" style="color:red;">※{$err_msg1.ks_name_kana_1}</p>
								{/if}
							</dd>
						</dl>
						<dl>
							<dt>氏名カナ(名)</dt>
							<dd>
								<input class="half" id="ks_name_kana_2" size="18" name="ks_name_kana_2" type="text" value="{$input_data.ks_name_kana_2}" />
								{if $err_msg1.ks_name_kana_2}
									<p class="error" style="color:red;">※{$err_msg1.ks_name_kana_2}</p>
								{/if}
							</dd>
						</dl>
						<dl>
						<dt>電話番号（半角数字）</dt>
						<dd>
							<input id="ks_tel" name="ks_tel" size="18" type="text" value="{$input_data.ks_tel}" />
							{if $err_msg1.ks_tel}
								<p class="error" style="color:red;">※{$err_msg1.ks_tel}</p>
							{/if}
						</dd>
					</dl>
				{/if}
				<ul class="actions">
					<li><input class="return" name="back" type="submit" value="戻る" /></li>
					<li><input class="confirmation" name="sbm_send" type="submit" value="確認画面へ" /></li>
				</ul>
			</form>
		</div>
		{include file="{$lang}/collection-side.tpl"}
	</div>
	<!-- #projectStart --> 
</div>
<!--/#wrapper-->
<script type='text/javascript'>
{literal}
function goback() {
		window.history.back(-1);
}
{/literal}
</script>
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
