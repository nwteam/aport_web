{include file="{$lang}/sf-head.tpl"}
<title>{$str_site_title}</title>
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/common.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/pages/index.css">
<link rel="stylesheet" href="{$smarty.const.C_ASSETS}css/mypage.css">
</head>
<body>
{include file="{$lang}/sf-header.tpl"}</a>
<div id="wrapper" class="clearfix">
<div id="myPage">
	{include file="{$lang}/mypage-profile.tpl"}</a>
	<!-- #myProfile -->
	{include file="{$lang}/mypage-side.tpl"}</a>
	<!-- #myPageSide -->
	<div id="myPageContents">
	<div id="myAddress">
	<h2>配送先情報</h2>
	{if $finish_msg}
		<p class="update">{$finish_msg}</p><br/>
	{/if}
	<p>配送が必要な応援リターンを選択した場合、ここで登録した住所に送られます。 </p>
	<form accept-charset="UTF-8" action="" method="post">
	<dl>
	  <dt>宛名</dt>
	  <dd>
	    <input id="post_user_name" name="post_user_name" placeholder="宛名を入力してください" size="45" type="text" value="{$input_data.post_user_name}"/>
	  </dd>
	  <dt>郵便番号</dt>
	  <dd> 〒
	    <input id="post_zip_code" maxlength="15" name="post_zip_code" placeholder="123-4567" size="15" type="text"  value="{$input_data.post_zip_code}"/>
	  </dd>
	  <dt>都道府県</dt>
	  <dd>
	    <select id="post_add_1" name="post_add_1">
				{html_options  options=$array_area selected=$input_data.post_add_1 }
	    </select>
	  </dd>
	  <dt>市町村/番地</dt>
	  <dd>
	    <input id="post_add_2" maxlength="100" name="post_add_2" placeholder="例：新宿区新宿1-2-3" size="45" type="text"  value="{$input_data.post_add_2}"/>
	  </dd>
	  <dt>建物名・部屋番号</dt>
	  <dd>
	    <input id="post_add_3" maxlength="100" name="post_add_3" placeholder="例：新宿ビル 102" size="45" type="text"  value="{$input_data.post_add_3}"/>
	  </dd>
	  <dt>電話番号</dt>
	  <dd>
	    <input id="post_tel" maxlength="15" name="post_tel" placeholder="例：01-1234-5678" size="45" type="text"  value="{$input_data.post_tel}"/>
	  </dd>
	</dl>
	<p class="actions">
	  <input type="submit" value="設定する" name="post_info_update">
	</p>
	<p class="txt-caption">*郵送先情報変更以前に応援頂いた特典の郵送先は、変更前の住所に郵送されます。郵送先変更をご希望の方は、実行者に直接お問い合わせ下さい。</p>
	</div>
	</form>
	</div>
	<!-- #myPwEdit --> 
	</div>
	<!-- #myPageContents --> 
</div>
<!-- #myPage --> 
</div>
<!--/#wrapper-->
{include file="{$lang}/sf-footer.tpl"}
<script src="{$smarty.const.C_ASSETS}js/common.js"></script>
</body>
</html>
