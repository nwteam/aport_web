<?php
/**
 * 暗号化文字列を返す
 */
function to_hash($str) {
	return hash_hmac("sha256", $str, false);
	//$hash = password_hash($str,PASSWORD_DEFAULT,array('cost' => 10));
	//return $hash;
}

  // ランキング月別表示
  function setYearMonth() {
    $arr_month = array();
    $i_cnt = 0;
    $yyyymm = date("Y-m", mktime(0,0,0,date("m"),date("d"),date("Y")));
    while ($C_START_MONTH != $yyyymm) {
      $arr_month[] = $yyyymm;
      $i_cnt++;
      $yyyymm = date("Y-m", mktime(0,0,0,date("m")-$i_cnt,date("d"),date("Y")));
    }
    return($arr_month);
  }

  // 入力確認
  function cleanup($copy) {
    $copy=trim($copy);
    $copy=htmlspecialchars($copy, ENT_QUOTES);
    $copy=eregi_replace("%","&#37;",$copy);
    $copy=eregi_replace("<","&lt;",$copy);
    $copy=eregi_replace(">","&gt;",$copy);
    $copy=eregi_replace("&amp;","&",$copy);
    $copy=StripSlashes($copy);
  //$copy=nl2br($copy);
    return($copy);
  }

  // ログイン確認
  function isLoginConfirm($db_host, $db_user, $db_pass, $db_name, $user_id) {
    if (isset($user_id) && $user_id != "0") {
      return true;
    } else {
      return false;
    }
  }

  // 会員登録確認
  function isMemberConfirm($db_host, $db_user, $db_pass, $db_name, $user_id) {
    $connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

    $query  = " select count(*) as cnt ";
    $query .= "   from sf_member ";
    $query .= "  where 1 = 1 ";
    $query .= "    and user_no = '%s'";
    $query .= "    and status  = '0'";
    $query .= "    and del_flg = '0'";
		$query = sprintf(
			$query,
			mysql_real_escape_string($user_id)
		);
    $result = mysql_query($query, $connect);
    $data = mysql_fetch_array($result);
    $total = $data['cnt'];

    if ($total>0) {
      return true;
    } else {
      return false;
    }
  }

  // 免許書写真/保険証番号確認
  function isMemberLicense($db_host, $db_user, $db_pass, $db_name, $user_id) {
    $connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

    $query  = " select * ";
    $query .= "   from sf_member ";
    $query .= "  where 1 = 1 ";
    $query .= "    and user_no = '%s'";
    $query .= "    and status  = '0'";
    $query .= "    and del_flg = '0'";
		$query = sprintf(
			$query,
			mysql_real_escape_string($user_id)
		);
    $result = mysql_query($query, $connect);
    $data = mysql_fetch_array($result);
    $m_img1 = $data['license_image1'];
    $m_img2 = $data['license_image2'];
    $m_no = $data['license_no'];

    //if (!isset($m_img1) || $m_img1 == "" || !isset($m_img2) || $m_img2 == "" || !isset($m_no) || $m_no == "") {
    if (!isset($m_no) || $m_no == "") {
      return false;
    } else {
      return true;
    }
  }

  function getDiffTime($start_date) {
    $l_start_attr = explode(" ", $start_date);
    $l_start_attr_date = explode("-", $l_start_attr[0]);
    $l_start_attr_time = explode(":", $l_start_attr[1]);

    $date1 = mktime($l_start_attr_time[0],$l_start_attr_time[1],$l_start_attr_time[2],
                    $l_start_attr_date[1],$l_start_attr_date[2],$l_start_attr_date[0]);

    $date2 = mktime();

    $now_date = date("Y-m-d h:m:s");

    $total_secs = ($date1 - $date2);
    $diff_in_days = floor($total_secs / 86400);
    $rest_hours = $total_secs % 86400;

    $diff_in_hours = floor($rest_hours / 3600);
    $rest_mins = $rest_hours % 3600;

    $diff_in_mins = floor($rest_mins / 60);
    $diff_in_secs = floor($rest_mins % 60);
    $time_diff = $diff_in_days."日 ".$diff_in_hours."時間 ".$diff_in_mins."分 ".$diff_in_secs."秒";

    if ($total_secs <= 0) {
      $time_diff = "終了";
    }

    return $time_diff;
  }

  function convDate($m_date) {
    $m_date = str_replace("-",".",$m_date);
    return $m_date;
  }

  function convDateExp($m_date) {
    $m_date = str_replace("-","/",$m_date);
    return $m_date;
  }

  function convDateSlash($m_date) {
    $m_date = str_replace("/",".",$m_date);
    return $m_date;
  }

  function getParamdata($url,$param_name){
    if($url){
    	$p_url=parse_url($url);
    	parse_str($p_url[query], $params);
		$param_data=$params[$param_name];
    }
  	return $param_data;
  }

	/**
	 * ログインセッションからMemberを取得する
	 */
	function getMemberSession() {
		if (isset($_SESSION[MEMBER_SESSION_NAME])) {
			return $_SESSION[MEMBER_SESSION_NAME];
		}
		return false;
	}

	/**
	 * ログインセッションにMemberを保存する
	 * @param Member $member
	 */
	function setMemberSession($member) {

		$_SESSION[MEMBER_SESSION_NAME] = $member;
	}

	/**
	 * ログインセッションを破棄する
	 */
	function deleteMemberSession() {
		unset($_SESSION[MEMBER_SESSION_NAME]);
	}


	/**
	 * メッセージセッションからMemberを取得する
	 */
	function getMsgSession() {
		if (isset($_SESSION['message_session'])) {
			return $_SESSION['message_session'];
		}
		return false;
	}
	/**
	 * ログインセッションにMsgを保存する
	 * @param Msg $member
	 */
	function setMsgSession($msg) {

		$_SESSION['message_session'] = $msg;
	}

	/**
	 * ログインセッションを破棄する
	 */
	function deleteMsgSession() {
		unset($_SESSION['message_session']);
	}

	/*
     *
     * チェック用関数
     * ※フォームでの入力チェックで使用するが、パラでも使用可能
     *
     * $s:チェックする文字列
     * $chk:チェック番号
     *
     *
     * $inputmaxLen:MAX文字数
     * $inputminLen:MIN文字数
     */

	function check($data,$checkdata,$err_msg) {

		//必須チェック
		$errResult=chkNull($data,$checkdata,$err_msg);

		foreach($checkdata["string"] as $key => $val){
		//入力データを取得
		$input = $data[$key];
		//チェック用データの取得
		$inputmaxLen = $checkdata["max"][$key];
		$inputminLen = $checkdata["min"][$key];
		$inputChk = $checkdata["chk"][$key];
		$inputType = $checkdata["type"][$key];
		$string = $checkdata["string"][$key];

		//入力されているものに関してチェックを行う
		if($input != ""){
			if($inputType == "text" || $inputType == "textarea"){
				$ret=0;
				$ret=chkString($input, $inputChk,$inputmaxLen,$inputminLen);

				if($ret != 0){
					if($ret == 1){
						$errResult[$key]=$err_msg[12];
					}elseif($ret == 5){
						$errResult[$key]=$err_msg[13];
					}elseif($ret == 3){
						$errResult[$key]=$err_msg[14];
						//	}elseif($ret == 4){
					//	$this->addMessage($key,$string."にタブが入っています。");
					}else{
						if($inputChk == 0){
							$errResult[$key]=$err_msg[10];
						}elseif($inputChk == 1){
							$errResult[$key]=$err_msg[1];
						}elseif($inputChk == 3){
							$errResult[$key]=$err_msg[3];
						}elseif($inputChk == 6){
							$errResult[$key]=$err_msg[5];
						}elseif($inputChk == 7){
							$errResult[$key]=$err_msg[6];
						}elseif($inputChk == 8){
							$errResult[$key]=$err_msg[10];
						}elseif($inputChk == 11){
							$errResult[$key]=$err_msg[8];
						}elseif($inputChk == 12){
							$errResult[$key]=$err_msg[9];

						}else{
							$errResult[$key]=$err_msg[2];
						}
					}
				}
			}
		}
	}

		//print_r_with_pre($errResult);

		return $errResult;

	}

	function chkNull($data,$checkdata,$err_msg){
		$ncnt=0;
		$NerrArr=array();
		foreach($checkdata["string"] as $key => $val){

			$y = $checkdata["nchk"][$key];
			$string = $checkdata["string"][$key];
			if($y == "1"){
				$input = trim($data[$key]);
				if($input == ""){
					/*
					if($checkdata["type"][$key]=="radio" || $checkdata["type"][$key]=="check"){
						$this->addMessage($key, $string."にチェックしてください。");
					}
					else if($checkdata["type"][$key]=="pull"){
						$this->addMessage($key, $string."を選択してください。");
					}
					else{
						$this->addMessage($key, $string."を入力してください。");
					}
					*/
					$NerrArr[$key]=$err_msg[0];

				}
			}

		}
		return $NerrArr;
	}


 	function chkString($s, $chk,$inputmaxLen,$inputminLen){

		if($inputmaxLen>0 && mb_strlen($s,"UTF-8") > $inputmaxLen){
			return 1;
		}

		if (mb_strlen($s,"UTF-8") < $inputminLen){
			return 5;
		}

		switch($chk) {
			case 1 :  // 半角英数チェック
				$s=str_replace(" ","",$s);
	        	if (!mb_ereg("^[0-9a-zA-Z\-\_\.:/~#=&\?-]*$",$s)){
					return 2;
				}
				return 0;
				break;
			case 2 :  // 数値チェック
				if (!mb_ereg("^[[:digit:]]+$", $s)){
					return 2;
				}
				return 0;
				break;

			case 3 :  //E-mailチェック
	       		if(!mb_ereg("^[a-zA-Z0-9!#$%&*+/=?^_{|}~.-]+\@[a-zA-Z0-9-]+\.+[a-zA-Z0-9.-]+$", $s)) {
	            	return 2;
	        	}
	        	return 0;
	        	break;
			case 5 : //郵便番号/電話番号等 数字と"-"チェック
				if (!mb_ereg("^[0-9-]*$",$s)){
            		return 2;
        		}

				return 0;
	        	break;

			case 6: //全角カナチェック

				$s=str_replace(array(" ","　"),"",$s);
				$zenkanaK = "　アイエウオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヲンァィゥェォヴガギグゲゴザジズゼゾダヂヅデドバビブベボパピプペポャュョッー（）・";
				for($i=0;$i<strlen($s);$i=$i+2){
					$p = strpos($zenkanaK, substr($s, $i ,2));
					if($p == FALSE){
						return 2;
					}
				}
				return 0;
				break;
			case 7: //全角かなチェック

				$s=str_replace(array(" ","　"),"",$s);
				$zenkanaH = " あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほ".
							"まみむめもやゆよらりるれろわをんぁぃぅぇぉがぎぐげござじずぜぞ".
							"だぢづでどばびぶべぼぱぴぷぺぽゃゅょっー ";
				for($i=0;$i<mb_strlen($s,"UTF-8");$i++){
					$p = mb_strpos($zenkanaH, mb_substr($s, $i ,1,"UTF-8"));
					if($p == FALSE){
						return 2;
					}
				}
				return 0;
				break;
			case 8: //半角カナが含まれていたらout

				if (mb_ereg('[ｱ-ﾝ]',$s)){
	            	return 2;
	        	}
				return 0;
				break;
			case 11: //数値と/
				if (!mb_ereg("^[0-9./･]*$",$s)){
	            	return 2;
	        	}
				return 0;
				break;
			case 12: //数値と.
				if (!mb_ereg("^[0-9.]*$",$s)){
					return 2;
	        	}
				return 0;
				break;

			default:
				return 0;
	        	break;

	     }

		 return 0;
	}

/**
 * mb_send_mailを使ってメールを送信する
 * @param String $to 宛先メールアドレス
 * @param String_type $fromAddress 差出人メールアドレス
 * @param String_type $fromName 差出人名
 * @param String_type $subject 件名
 * @param String_type $mailBody 本文
 */
/*
function send_mail($to, $fromAddress, $fromName, $subject, $mailBody,$ccAddress="",$bccAddress="") {
	try {
		mb_language("japanese");
		mb_internal_encoding("utf-8");
		$from = mb_encode_mimeheader(mb_convert_encoding($fromName, "JIS","utf-8"))."<" . $fromAddress . ">";
		if($ccAddress!=""){
			$cc = "\nCc:".$ccAddress;
		}

		if($bccAddress!=""){
			$bcc = "\nBcc:".$bccAddress;
		}

		mb_send_mail($to, $subject, $mailBody, "From:" . $from.$cc.$bcc);

	}catch (Exception $e) {
		print($e->getMessage());
	}

}
*/

	function send_mail($to, $fromAddress, $fromName, $subject, $mailBody,$ccAddress="",$bccAddress="") {
	try {

//	    $smtp_param = array(
//	        'host'=>'smtp.myav.xyz',
//	        'port'=> 587 ,
//	        'from'=>$C_SEND_EMAIL,
//	        'protocol'=>'SMTP_AUTH',
//	        'user'=>'test@myav.xyz',
//	        'pass' => 'FrkUXHJr',
//	    );
//	    $smtp_param = array(
//	        'host'=>'email-smtp.us-east-1.amazonaws.com',
//	        'port'=> 587 ,
//	        'smtp_crypto'=> 'tls' ,
//	        'from'=>$C_SEND_EMAIL,
//	        'from'=>'info@nestwinds.com',
//	        'protocol'=>'SMTP_AUTH',
//	        'user'=>'AKIAIX6W4YL3QPMUKWTQ',
//	        'pass' => 'AtxYKclQD1P4aDPlDvaK1wHfFrkcAkBaRRkIHmSJqL7b',
//	    );
	    $smtp_param = array(
	        'host'=>'smtp.mailgun.org',
	        'port'=> 587 ,
	        'from'=>$C_SEND_EMAIL,
//	        'from'=>'info@nestwinds.com',
	        'protocol'=>'SMTP_AUTH',
	        'user'=>'postmaster@aport-asahi.com',
	        'pass' => '14adfbc33c9715507b03835d9de42b48',
	    );
//$ccAddress='info@nestwinds.com';
//$bccAddress='info@nestwinds.com';
//$fromAddress='info@nestwinds.com';
	   //SMTP送信
	    $mail = new Qdmail();
	    $mail -> smtp(true);
	    $mail ->smtpServer($smtp_param);
	    $mail ->to($to);
		if($ccAddress!=""){
			$mail ->cc($ccAddress);
		}
		if($bccAddress!=""){
			$mail ->bcc($bccAddress);
		}
		$mail ->subject($subject);
	    $mail ->from($fromAddress,$fromName);
	    $mail ->text($mailBody);
	    $return_flag = $mail ->send();
//			echo $return_flag;die;
	    return $return_flag;

	}catch (Exception $e) {
		print($e->getMessage());
	}

}

 /**
 * メールタイトルを取得
 */
function getMailSubject($no) {
	$mail_title_arr=CommonMessageArray::$mail_subject;

	return $mail_title_arr[$no];

}
	/**年のリストを作成する
	 *
	 * $start:リスト開始年　nullであれば1900年～
	 * $end:現在の年+何年を終了年にするか　現在の年が2013 $end=2 とした場合、2015年までがリストに表示される
	 * $nullstr:リストボックスの一番上にnull選択を入れる場合は１を指定
	 * return 年リストの配列
	 */
	function makeYearList($start=1900,$end,$nullstr=0) {
		$ret=array();
		if($nullstr==1) $ret[""]="--";

		//endは現在の年＋$end
		$end=date("Y",time())+$end;

		for($c=$start;$c<=$end;$c++){
			$ret[$c]=$c;
		}
		return $ret;
	}

	/**月のリストを作成する
	 *
	 * $nullstr:リストボックスの一番上にnull選択を入れる場合は１を指定
	 * return 月リストの配列
	 */
	function makeMonthList($nullstr=0) {
		$ret=array();
		if($nullstr==1) $ret[""]="--";
		for($c=1;$c<=12;$c++){
			$ret[$c]=$c;
		}
		return $ret;
	}

	/**日のリストを作成する
	 *
	 * $nullstr:リストボックスの一番上にnull選択を入れる場合は１を指定
	 * return日リストの配列
	 */
	function makeDayList($nullstr=0) {
		$ret=array();
		if($nullstr==1) $ret[""]="--";
		for($c=1;$c<=31;$c++){
			$ret[$c]=$c;
		}
		return $ret;
	}

	//受け取ったパラメータをエンティティ
	function makeGetRequest($param) {
		if(is_array($param)){

			foreach($param as $key=>$val){
				if(is_array($val)){
					foreach($val as $k2=>$v2){
						$v_input_data[$key][$k2]=htmlspecialchars($v2,ENT_QUOTES);
					}
				}
				else{
					$v_input_data[$key]=htmlspecialchars($val,ENT_QUOTES);
				}
			}
			return $v_input_data;
		}
		else{
			return htmlspecialchars($param,ENT_QUOTES);
		}
	}

	function makeGetRequest_decode($param) {
		if(is_array($param)){
			foreach($param as $key=>$val){
				if(is_array($val)){
					foreach($val as $k2=>$v2){

						if(is_array($v2)){
							foreach($v2 as $k3=>$v3){
								$v_input_data[$key][$k2][$k3]=htmlspecialchars_decode($v3,ENT_QUOTES);
							}
						}
						else{
							$v_input_data[$key][$k2]=htmlspecialchars_decode($v2,ENT_QUOTES);
						}
					}
				}
				else{
					$v_input_data[$key]=htmlspecialchars_decode($val,ENT_QUOTES);
				}
			}
			return $v_input_data;
		}
		else{
			return htmlspecialchars_decode($param,ENT_QUOTES);
		}
	}

/**
 * エラーログ出力処理
 */
function getAgentType() {
	$ua=$_SERVER['HTTP_USER_AGENT'];
	if((strpos($ua,'iPhone')!==false)||(strpos($ua,'iPod')!==false)||(strpos($ua,Android)!==false)) {
		return "/sp";
	}else{
		return ;
	}
}
/**
 * エラーログ出力処理
 */
function write_log($str) {

            $note = "\n".$str."\n";

			$fp = fopen(LOG_F, "a+");

			flock($fp,2);
			fwrite($fp,$note);
			flock($fp,3);
			fclose($fp);
}
  /**
 * print_rに<pre>タグをつけて実行した結果を出力する
 */
function print_r_with_pre($obj) {
	print "<pre>";
	print print_r($obj);
	print "</pre>";
}
/**
 * /投稿したプロジェクト一覧情報の取得(入稿中)
 */
function getMyProjectListInfo($connect,$user_no){

	$query  = " select distinct a.* , ";
	$query .= "        b.category_name as category_name , ";
	$query .= "        c.* , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  left join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "  inner join sf_category b ";
	$query .= "     on a.category_no = b.no ";
	$query .= "    and b.del_flg = '0' ";
	$query .= "  inner join sf_member c ";
	$query .= "     on a.user_no = c.user_no ";
	$query .= "    and c.del_flg = '0' ";
	$query .= "  where 1 ";
	$query .= "    and a.chk_status = '0'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.user_no = '%s'";
	$query .= "  order by a.create_date desc";
	$query = sprintf(
		$query,
		mysql_real_escape_string($user_no)
	);

	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	//$arr_data   = mysql_fetch_array($result);

	return $result;
}
/**
 * /投稿したプロジェクト一覧情報の取得(審査待)
 */
function getMyCheckingProjectListInfo($connect,$user_no){

	$query  = " select distinct a.* , ";
	$query .= "        b.category_name as category_name , ";
	$query .= "        c.* , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  left join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "  inner join sf_category b ";
	$query .= "     on a.category_no = b.no ";
	$query .= "    and b.del_flg = '0' ";
	$query .= "  inner join sf_member c ";
	$query .= "     on a.user_no = c.user_no ";
	$query .= "    and c.del_flg = '0' ";
	$query .= "  where 1 ";
	$query .= "    and a.chk_status = '1'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.user_no = '%s'";
	$query .= "  order by a.create_date desc";
	$query = sprintf(
		$query,
		mysql_real_escape_string($user_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	//$arr_data   = mysql_fetch_array($result);

	return $result;
}
/**
 * /投稿したプロジェクト一覧情報の取得(修正依頼)
 */
function getMyChangeProjectListInfo($connect,$user_no){

	$query  = " select distinct a.* , ";
	$query .= "        b.category_name as category_name , ";
	$query .= "        c.* , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  left join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "  inner join sf_category b ";
	$query .= "     on a.category_no = b.no ";
	$query .= "    and b.del_flg = '0' ";
	$query .= "  inner join sf_member c ";
	$query .= "     on a.user_no = c.user_no ";
	$query .= "    and c.del_flg = '0' ";
	$query .= "  where 1 ";
	$query .= "    and a.chk_status = '2'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.user_no = '%s'";
	$query .= "  order by a.create_date desc";
	$query = sprintf(
		$query,
		mysql_real_escape_string($user_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	//$arr_data   = mysql_fetch_array($result);

	return $result;
}
/**
 * /投稿したプロジェクト一覧情報の取得(審査完了)
 */
function getMyCheckedProjectListInfo($connect,$user_no){

	$query  = " select distinct a.* , ";
	$query .= "        b.category_name as category_name , ";
	$query .= "        c.* , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  left join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "  inner join sf_category b ";
	$query .= "     on a.category_no = b.no ";
	$query .= "    and b.del_flg = '0' ";
	$query .= "  inner join sf_member c ";
	$query .= "     on a.user_no = c.user_no ";
	$query .= "    and c.del_flg = '0' ";
	$query .= "  where 1 ";
	$query .= "    and a.chk_status = '3'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.user_no = '%s'";
	$query .= "  order by a.create_date desc";
	$query = sprintf(
		$query,
		mysql_real_escape_string($user_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	//$arr_data   = mysql_fetch_array($result);

	return $result;
}
/**
 * /投稿したプロジェクト情報の取得
 */
function getMyProjectInfo($connect,$project_no,$user_no){

	$query  = " select a.* , ";
	$query  .= "  a.status as project_status , ";
	$query  .= "  b.category_name as category_name , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  inner join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "    and d.lang = 'ja'  ";
	$query .= "  inner join sf_category b ";
	$query .= "     on a.category_no = b.no ";
	$query .= "    and b.del_flg = '0' ";
	$query .= "  where 1 ";
	$query .= "    and a.no = '%s'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.user_no = '%s'";

	$query = sprintf(
		$query,
		mysql_real_escape_string($project_no),
		mysql_real_escape_string($user_no)
	);

	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /オーダー情報の取得
 */
function getOrderInfo($connect,$order_id){

	$query  = " select * ";
	$query .= "   from sf_invest ";
	$query .= "  where 1 ";
	$query .= "    and order_id = '%s'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($order_id)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /プロジェクト情報の取得
 */
function getProjectInfo($connect,$project_no){

	$query  = " select a.* , ";
	$query  .= "  a.status as project_status , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  inner join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "    and d.lang = 'ja'  ";
	$query .= "  where 1 ";
	$query .= "    and a.no = '%s'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.status > '0'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($project_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /プロジェクト情報の取得
 */
function getProjectInfoWithOutFetch($connect,$project_no){

	$query  = " select distinct a.* , ";
	$query .= "        b.category_name as category_name , ";
	$query .= "        c.* , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  left join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "  inner join sf_category b ";
	$query .= "     on a.category_no = b.no ";
	$query .= "    and b.del_flg = '0' ";
	$query .= "  inner join sf_member c ";
	$query .= "     on a.user_no = c.user_no ";
	$query .= "    and c.del_flg = '0' ";
	$query .= "  where 1 ";
	$query .= "    and a.no = '%s'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.status > '0'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($project_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	//$arr_data   = mysql_fetch_array($result);

	return $result;
}
/**
 * /プロジェクト情報の取得 from 希望URL
 */
function getProjectFromHopeUrlInfo($connect,$hope_url){

	$query  = " select a.* , ";
	$query  .= "  a.status as project_status , ";
	$query .= "        d.* ";
	$query .= "   from sf_project a ";
	$query .= "  inner join sf_project_detail d ";
	$query .= "     on a.no = d.project_no ";
	$query .= "    and d.lang = 'ja'  ";
	$query .= "  where 1 ";
	$query .= "    and a.hope_url = '%s'";
	$query .= "    and a.del_flg = '0'";
	$query .= "    and a.status > '0'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($hope_url)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /希望URL重複チェック
 */
function getHopeUrlCnt($connect,$hope_url,$project_no){

	$query  = " select count(*) cnt ";
	$query .= "   from sf_project ";
	$query .= "  where 1 ";
	$query .= "    and no <> '%s'";
	$query .= "    and hope_url = '%s'";
	$query .= "    and del_flg = '0'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($project_no),
		mysql_real_escape_string($hope_url)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
	//return 0;
}
/**
 * /該当プロジェクトの支援コース情報の取得
 */
function getProjectPresentInfo($connect,$project_no){

	$query  = " select * from sf_prj_present where project_no = '%s' order by min";
	$query = sprintf(
		$query,
		mysql_real_escape_string($project_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	//$arr_data   = mysql_fetch_array($result);

	return $result;
}
/**
 * /支援数の取得
 */
function getPresentCntInfo($connect,$project_no,$present_no){

	$query  = " select count(no) as cnt from sf_invest where present_no = '%s' and project_no = '%s' and status>0 and status<=91 ";
	$query = sprintf(
		$query,
		mysql_real_escape_string($present_no),
		mysql_real_escape_string($project_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}

/**
 * /選択した支援コース情報の取得
 */
function getPresentInfo($connect,$present_no){

	$query  = " select * from sf_prj_present where present_no = '%s'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($present_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}

/**
 * /hope_urlよりProject_noの取得
 */
function getProjectNoByHopeURL($connect,$hope_url){

	$query  = " select no as project_no from sf_project where hope_url = '%s'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($hope_url)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /Project_noよりhope_urlの取得
 */
function getHopeURLByProjectNo($connect,$project_no){

	$query  = " select hope_url from sf_project where no = '%s'";
	$query = sprintf(
		$query,
		mysql_real_escape_string($project_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}

/**
 * /起案者情報取得
 */
function getDrafterInfo($connect,$project_owner){

	$query  = " select a.* , d.* from sf_actress as a
			inner join sf_actress_detail as d on a.actress_no=d.actress_no and d.lang='ja'
			where a.actress_no = '%s' ";
	$query = sprintf(
		$query,
		mysql_real_escape_string($project_owner)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /会員情報取得
 */
function getMemberInfo($connect,$user_no){

	$query  = " select * from sf_member where user_no = '%s' and status='1' and del_flg='0' ";
	$query = sprintf(
		$query,
		mysql_real_escape_string($user_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /アップデータ情報取得
 */
function getUpdateInfo($connect,$update_id){

	$query  = " select * from sf_update_msg where update_id = '%s' and del_flg='0' ";
	$query = sprintf(
		$query,
		mysql_real_escape_string($update_id)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}
/**
 * /画像取得
 */
function getImage($connect,$image_type,$parent_no){

	$query  = " select * from general_images where image_type = '%s' and parent_no = '%s' ";
	$query = sprintf(
		$query,
		mysql_real_escape_string($image_type),
		mysql_real_escape_string($parent_no)
	);
	$result = mysql_query("set names utf8");
	$result = mysql_query($query, $connect);
	$arr_data   = mysql_fetch_array($result);

	return $arr_data;
}

function exePayCredit($itemCd,$amount,$orderId,$memberId,$cardNo,$expDate,$secCd,$payMethod,$paytimes){
	$ret=0;
	require_once( 'com/gmo_pg/client/input/EntryTranInput.php');
	require_once( 'com/gmo_pg/client/input/ExecTranInput.php');
	require_once( 'com/gmo_pg/client/input/EntryExecTranInput.php');
	require_once( 'com/gmo_pg/client/tran/EntryExecTran.php');

	//入力パラメータクラスをインスタンス化します
	//取引登録時に必要なパラメータ
	$entryInput = new EntryTranInput();
	$entryInput->setShopId( PGCARD_SHOP_ID );//ショップ ID
	$entryInput->setShopPass( PGCARD_SHOP_PASS );//ショップパスワード
	$entryInput->setJobCd( "CAPTURE");//処理区分 CHECK：有効性チェック CAPTURE：即時売上 AUTH：仮売上 SAUTH：簡易オーソリ
	$entryInput->setOrderId( $orderId );//オーダーID ショップ発行の取引識別ID
	//$entryInput->setItemCode( $itemCd );//商品コード 省略時はシステム固定値("0000990")を適用。7 桁未満のコードを入力する場合は、右詰め・前ゼロを埋めて7 桁にしてください。
	$entryInput->setAmount( $amount);//利用金額 『有効性チェック』を除く処理区分は必須
	$entryInput->setTax(0);//税送料
	$entryInput->setTdFlag( "");//3D セキュア使用フラグ 0：行なわない(デフォルト) 1：行なう
	$entryInput->setTdTenantName( "");//3D セキュア表示店舗名

	//決済実行のパラメータ
	$execInput = new ExecTranInput();

	//カード番号入力型・会員ID決済型に共通する値です。
	$execInput->setOrderId($orderId);

	//支払方法に応じて、支払回数のセット要否が異なります。
	//1：一括 2：分割 3：ボーナス一括 4：ボーナス分割 5：リボ
	$execInput->setMethod($payMethod);
	if( $payMethod == '2' || $payMethod == '4'){//支払方法が、分割またはボーナス分割の場合、支払回数を設定します。
		$execInput->setPayTimes($paytimes);
	}
	//このサンプルでは、加盟店自由項目１～３を全て利用していますが、これらの項目は任意項目です。
	//利用しない場合、設定する必要はありません。
	//また、加盟店自由項目に２バイトコードを設定する場合、SJISに変換して設定してください。
// 	$execInput->setClientField1( mb_convert_encoding( $_POST['ClientField1'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
// 	$execInput->setClientField2( mb_convert_encoding( $_POST['ClientField2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
// 	$execInput->setClientField3( mb_convert_encoding( $_POST['ClientField3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );

	//HTTP_ACCEPT,HTTP_USER_AGENTは、3Dセキュアサービスをご利用の場合のみ必要な項目です。
	//Entryで3D利用フラグをオンに設定した場合のみ、設定してください。
	//設定する場合、カード所有者のブラウザから送信されたリクエストヘッダの値を、無加工で
	//設定してください。
	//$execInput->setHttpUserAgent( $_SERVER['HTTP_USER_AGENT']);
	//$execInput->setHttpAccept( $_SERVER['HTTP_ACCEPT' ]);

	//ここから、カード番号入力型決済と会員ID型決済それぞれの場合で
	//異なるパラメータを設定します。

	//ここでは、「画面で会員IDが入力されたか」を判断基準にして、
	//決済のタイプを判別しています。
 	//$memberId = $_POST['MemberID'];

	if( 0 < strlen( $memberId )  ){//会員ID決済
		//サンプルでは、サイトID・サイトパスワードはコンスタント定義しています。
		$execInput->setSiteId( PGCARD_SITE_ID );
		$execInput->setSitePass( PGCARD_SITE_PASS );

		//会員IDは必須です。
		$execInput->setMemberId( $memberId );

		//登録カード連番は任意です。
		$cardSeq = "0";
		if( 0< strlen( $cardSeq ) ){
			$execInput->setCardSeq( $cardSeq );
		}

	}else{//カード番号決済
		//カード番号・有効期限は必須です。
		$execInput->setCardNo( $cardNo);
		$execInput->setExpire( $expDate);

		//セキュリティコードは任意です。
		$execInput->setSecurityCode( $secCd );

	}

	//取引登録＋決済実行の入力パラメータクラスをインスタンス化します
	$input = new EntryExecTranInput();/* @var $input EntryExecTranInput */
	$input->setEntryTranInput( $entryInput );
	$input->setExecTranInput( $execInput );

	//API通信クラスをインスタンス化します
	$exe = new EntryExecTran();/* @var $exec EntryExecTran */

	//パラメータオブジェクトを引数に、実行メソッドを呼びます。
	//正常に終了した場合、結果オブジェクトが返るはずです。
	$output = $exe->exec( $input );/* @var $output EntryExecTranOutput */
	//print_r($output);
	//実行後、その結果を確認します。
	if( $exe->isExceptionOccured() ){//取引の処理そのものがうまくいかない（通信エラー等）場合、例外が発生します。
		$ret=1;
		return $ret;
	}else{
		//例外が発生していない場合、出力パラメータオブジェクトが戻ります。
		if( $output->isErrorOccurred() ){//出力パラメータにエラーコードが含まれていないか、チェックしています。
			//print_r_with_pre($output);die;
			$ret=2;
			return $ret;
		}else if( $output->isTdSecure() ){//決済実行の場合、3Dセキュアフラグをチェックします。
			//3Dセキュアフラグがオンである場合、リダイレクトページを表示する必要があります。
			//サンプルでは、モジュールタイプに標準添付されるリダイレクトユーティリティを利用しています。

			//リダイレクト用パラメータをインスタンス化して、パラメータを設定します
			//require_once( 'com/gmo_pg/client/input/AcsParam.php');
			//require_once( 'com/gmo_pg/client/common/RedirectUtil.php');
			//$redirectInput = new AcsParam();
			//$redirectInput->setAcsUrl( $output->getAcsUrl() );
			//$redirectInput->setMd( $output->getAccessId() );
			//$redirectInput->setPaReq( $output->getPaReq() );
			//$redirectInput->setTermUrl( PGCARD_SAMPLE_URL . '/SecureTran.php');
			$ret=3;
			return $ret;
			//リダイレクトページ表示クラスをインスタンス化して実行します。
			//$redirectShow = new RedirectUtil();
			//print ($redirectShow->createRedirectPage( PGCARD_SECURE_RIDIRECT_HTML , $redirectInput ) );
			//exit();

		}
		//例外発生せず、エラーの戻りもなく、3Dセキュアフラグもオフであるので、実行結果を表示します。
	}
	return $output;
}
function exePaycvs($convenience,$amount,$orderId,$customername,$customerkana,$telno,$mailaddress){
	$ret=0;
	require_once( 'com/gmo_pg/client/input/EntryTranCvsInput.php');
	require_once( 'com/gmo_pg/client/input/ExecTranCvsInput.php');
	require_once( 'com/gmo_pg/client/input/EntryExecTranCvsInput.php');
	require_once( 'com/gmo_pg/client/tran/EntryExecTranCvs.php');

	//入力パラメータクラスをインスタンス化します
	//取引登録時に必要なパラメータ
	$entryInput = new EntryTranCvsInput();
	$entryInput->setShopId(PGCARD_SHOP_ID);//ショップ ID
	$entryInput->setShopPass(PGCARD_SHOP_PASS);//ショップパスワード
	$entryInput->setOrderId($orderId);//オーダーID ショップ発行の取引識別ID
	$entryInput->setAmount($amount);//利用金額 セブンイレブンの場合利用金額+税送料で200 円以上である必要があります。
	$entryInput->setTax(0);//税送料

	//決済実行のパラメータ
	$execInput = new ExecTranCvsInput();
	$execInput->setOrderId($orderId);

	//メールアドレス
		//支払先コンビニコード
		//00001 ローソン 00002 ファミリーマート00003 サンクス 
		//00004 サークルＫ 00005 ミニストップ 00006 デイリーヤマザキ
		//00007 セブンイレブン 00008 セイコーマート 00009 スリーエフ
	$execInput->setConvenience($convenience);//支払先コンビニコード 5桁 
	$execInput->setCustomerName( mb_convert_encoding($customername, 'SJIS' , PGCARD_SAMPLE_ENCODING ) );//氏名 セブンイレブンを指定した場合は、半角記号は使用できません。
	$execInput->setCustomerKana( mb_convert_encoding($customerkana, 'SJIS' , PGCARD_SAMPLE_ENCODING ) );//フリガナ
//	$execInput->setCustomerName($customername);//氏名 セブンイレブンを指定した場合は、半角記号は使用できません。
//	$execInput->setCustomerKana($customerkana);//フリガナ
	$execInput->setTelNo($telno);//電話番号
	//$execInput->setMailAddress($mailaddress);//結果通知先メールアドレス お客様へ結果通知先メールアドレス

	//実際には、以降のパラメータをお客様が直接入力することは無く、
	//購買内容を元に加盟店様システムで生成した値が設定されるものと思います。
	$shopmailadress="info@a-port-mail.com";
	$execInput->setShopMailAdress($shopmailadress);
	//$execInput->setReserveNo( $_POST['ReserveNo'] );
	//$execInput->setMemberNo( $_POST['MemberNo'] );
	
	//$execInput->setPaymentTermDay( $_POST['PaymentTermDay'] );
	
	//$execInput->setRegisterDisp1( mb_convert_encoding( $_POST['RegisterDisp1'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp2( mb_convert_encoding( $_POST['RegisterDisp2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp3( mb_convert_encoding( $_POST['RegisterDisp3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp4( mb_convert_encoding( $_POST['RegisterDisp4'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp5( mb_convert_encoding( $_POST['RegisterDisp5'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp6( mb_convert_encoding( $_POST['RegisterDisp6'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp7( mb_convert_encoding( $_POST['RegisterDisp7'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp8( mb_convert_encoding( $_POST['RegisterDisp8'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	
	//$execInput->setReceiptsDisp1( mb_convert_encoding( $_POST['ReceiptsDisp1'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp2( mb_convert_encoding( $_POST['ReceiptsDisp2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp3( mb_convert_encoding( $_POST['ReceiptsDisp3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp4( mb_convert_encoding( $_POST['ReceiptsDisp4'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp5( mb_convert_encoding( $_POST['ReceiptsDisp5'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp6( mb_convert_encoding( $_POST['ReceiptsDisp6'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp7( mb_convert_encoding( $_POST['ReceiptsDisp7'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp8( mb_convert_encoding( $_POST['ReceiptsDisp8'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp9( mb_convert_encoding( $_POST['ReceiptsDisp9'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp10( mb_convert_encoding( $_POST['ReceiptsDisp10'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	$receiptsdisp11="A-Port WEB 東京本社";
	$receiptsdisp12="03-3545-0131";
	$receiptsdisp13="09:00-18:00";
	$execInput->setReceiptsDisp11( mb_convert_encoding($receiptsdisp11, 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	$execInput->setReceiptsDisp12( $receiptsdisp12 );
	$execInput->setReceiptsDisp13( $receiptsdisp13 );
	
	//このサンプルでは、加盟店自由項目１～３を全て利用していますが、これらの項目は任意項目です。
	//利用しない場合、設定する必要はありません。
	//また、加盟店自由項目に２バイトコードを設定する場合、SJISに変換して設定してください。
	//$execInput->setClientField1( mb_convert_encoding( $_POST['ClientField1'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setClientField2( mb_convert_encoding( $_POST['ClientField2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setClientField3( mb_convert_encoding( $_POST['ClientField3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
//print_r_with_pre($execInput);
	//取引登録＋決済実行の入力パラメータクラスをインスタンス化します
	$input = new EntryExecTranCvsInput();/* @var $input EntryExecTranCvsInput */
	$input->setEntryTranCvsInput( $entryInput );
	$input->setExecTranCvsInput( $execInput );
	
	//API通信クラスをインスタンス化します
	$exe = new EntryExecTranCvs();/* @var $exec EntryExecTranCvs */
	
	//パラメータオブジェクトを引数に、実行メソッドを呼びます。
	//正常に終了した場合、結果オブジェクトが返るはずです。
	$output = $exe->exec( $input );/* @var $output EntryExecTranCvsOutput */
//print_r_with_pre($output);
	//実行後、その結果を確認します。
	
	if( $exe->isExceptionOccured() ){//取引の処理そのものがうまくいかない（通信エラー等）場合、例外が発生します。

		//サンプルでは、例外メッセージを表示して終了します。
		//require_once( PGCARD_SAMPLE_BASE . '/display/Exception.php');
		//exit();
		$ret=1;
		return $ret;
		
	}else{
		
		//例外が発生していない場合、出力パラメータオブジェクトが戻ります。
		
		if( $output->isErrorOccurred() ){//出力パラメータにエラーコードが含まれていないか、チェックしています。
			
			//サンプルでは、エラーが発生していた場合、エラー画面を表示して終了します。
			//require_once( PGCARD_SAMPLE_BASE . '/display/EntryExecError.php');
			//exit();
			$ret=2;
			return $ret;
		}

		//例外発生せず、エラーの戻りもなく、3Dセキュアフラグもオフであるので、実行結果を表示します。
	}

	//EntryExecTranCvs入力・結果画面
	//require_once( PGCARD_SAMPLE_BASE . '/display/EntryExecTranCvs.php' );
	return $output;
}

function exePayPayeasy($amount,$orderId,$customername,$customerkana,$telno,$mailaddress){
	$ret=0;
	require_once( 'com/gmo_pg/client/input/EntryTranPayEasyInput.php');
	require_once( 'com/gmo_pg/client/input/ExecTranPayEasyInput.php');
	require_once( 'com/gmo_pg/client/input/EntryExecTranPayEasyInput.php');
	require_once( 'com/gmo_pg/client/tran/EntryExecTranPayEasy.php');

	//入力パラメータクラスをインスタンス化します
	//取引登録時に必要なパラメータ
	$entryInput = new EntryTranPayEasyInput();
	$entryInput->setShopId(PGCARD_SHOP_ID);//ショップ ID
	$entryInput->setShopPass(PGCARD_SHOP_PASS);//ショップパスワード
	$entryInput->setOrderId($orderId);//オーダーID ショップ発行の取引識別ID
	$entryInput->setAmount($amount);//利用金額 セブンイレブンの場合利用金額+税送料で200 円以上である必要があります。
	$entryInput->setTax(0);//税送料

	//決済実行のパラメータ
	$execInput = new ExecTranPayEasyInput();
	$execInput->setOrderId($orderId);

	//
	$execInput->setCustomerName( mb_convert_encoding($customername, 'SJIS' , PGCARD_SAMPLE_ENCODING ) );//氏名 セブンイレブンを指定した場合は、半角記号は使用できません。
	$execInput->setCustomerKana( mb_convert_encoding($customerkana, 'SJIS' , PGCARD_SAMPLE_ENCODING ) );//フリガナ
	$execInput->setTelNo($telno);//電話番号
	//$execInput->setMailAddress($mailaddress);//結果通知先メールアドレス お客様へ結果通知先メールアドレス

	//実際には、以降のパラメータをお客様が直接入力することは無く、
	//購買内容を元に加盟店様システムで生成した値が設定されるものと思います。
	$shopmailadress="info@a-port-mail.com";
	$execInput->setShopMailAdress($shopmailadress);
	
	//$execInput->setPaymentTermDay( $_POST['PaymentTermDay'] );
	
	//$execInput->setRegisterDisp1( mb_convert_encoding( $_POST['RegisterDisp1'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp2( mb_convert_encoding( $_POST['RegisterDisp2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp3( mb_convert_encoding( $_POST['RegisterDisp3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp4( mb_convert_encoding( $_POST['RegisterDisp4'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp5( mb_convert_encoding( $_POST['RegisterDisp5'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp6( mb_convert_encoding( $_POST['RegisterDisp6'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp7( mb_convert_encoding( $_POST['RegisterDisp7'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setRegisterDisp8( mb_convert_encoding( $_POST['RegisterDisp8'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	
	//$execInput->setReceiptsDisp1( mb_convert_encoding( $_POST['ReceiptsDisp1'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp2( mb_convert_encoding( $_POST['ReceiptsDisp2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp3( mb_convert_encoding( $_POST['ReceiptsDisp3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp4( mb_convert_encoding( $_POST['ReceiptsDisp4'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp5( mb_convert_encoding( $_POST['ReceiptsDisp5'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp6( mb_convert_encoding( $_POST['ReceiptsDisp6'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp7( mb_convert_encoding( $_POST['ReceiptsDisp7'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp8( mb_convert_encoding( $_POST['ReceiptsDisp8'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp9( mb_convert_encoding( $_POST['ReceiptsDisp9'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setReceiptsDisp10( mb_convert_encoding( $_POST['ReceiptsDisp10'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	$receiptsdisp11="A-Port WEB 東京本社";
	$receiptsdisp12="03-3545-0131";
	$receiptsdisp13="09:00-18:00";
	$execInput->setReceiptsDisp11( mb_convert_encoding($receiptsdisp11, 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	$execInput->setReceiptsDisp12( $receiptsdisp12 );
	$execInput->setReceiptsDisp13( $receiptsdisp13 );
	
	//このサンプルでは、加盟店自由項目１～３を全て利用していますが、これらの項目は任意項目です。
	//利用しない場合、設定する必要はありません。
	//また、加盟店自由項目に２バイトコードを設定する場合、SJISに変換して設定してください。
	//$execInput->setClientField1( mb_convert_encoding( $_POST['ClientField1'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setClientField2( mb_convert_encoding( $_POST['ClientField2'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
	//$execInput->setClientField3( mb_convert_encoding( $_POST['ClientField3'] , 'SJIS' , PGCARD_SAMPLE_ENCODING ) );
//print_r_with_pre($execInput);
	//取引登録＋決済実行の入力パラメータクラスをインスタンス化します
	$input = new EntryExecTranPayEasyInput();/* @var $input EntryExecTranPayEasyInput */
	$input->setEntryTranPayEasyInput( $entryInput );
	$input->setExecTranPayEasyInput( $execInput );
	
	//API通信クラスをインスタンス化します
	$exe = new EntryExecTranPayEasy();/* @var $exec EntryExecTranPayEasy */
	
	//パラメータオブジェクトを引数に、実行メソッドを呼びます。
	//正常に終了した場合、結果オブジェクトが返るはずです。
	$output = $exe->exec( $input );/* @var $output EntryExecTranPayEasyOutput */
//print_r_with_pre($output);
	//実行後、その結果を確認します。
	
	if( $exe->isExceptionOccured() ){//取引の処理そのものがうまくいかない（通信エラー等）場合、例外が発生します。

		//サンプルでは、例外メッセージを表示して終了します。
		//require_once( PGCARD_SAMPLE_BASE . '/display/Exception.php');
		//exit();
		$ret=1;
		return $ret;
		
	}else{
		
		//例外が発生していない場合、出力パラメータオブジェクトが戻ります。
		
		if( $output->isErrorOccurred() ){//出力パラメータにエラーコードが含まれていないか、チェックしています。
			
			//サンプルでは、エラーが発生していた場合、エラー画面を表示して終了します。
			//require_once( PGCARD_SAMPLE_BASE . '/display/EntryExecError.php');
			//exit();
			$ret=2;
			return $ret;
		}

		//例外発生せず、エラーの戻りもなく、3Dセキュアフラグもオフであるので、実行結果を表示します。
	}

	//EntryExecTranPayEasy入力・結果画面
	//require_once( PGCARD_SAMPLE_BASE . '/display/EntryExecTranPayEasy.php' );
	return $output;
}

/**
 * 画像リサイズ
 * @param String $source 元ファイル名
 * @param Stirng $target リサイズファイル名
 * @param Int $width リサイズ幅
 * @param Int $height リアサイズ高
 * @return Boolean 処理結果
 */
function resize_image($source, $target, $width, $height="") {
	if (!$target) return false;

	// 画像のサイズとタイプを取得
	list($s_width,$s_height, $type) = getimagesize($source);

	// 画像を読み込む
	if ($type == 1) {
		$s_img = imagecreatefromgif($source);
	} else if ($type == 2) {
		$s_img = imagecreatefromjpeg($source);
	} else if ($type == 3) {
		$s_img = imagecreatefrompng($source);
	} else if ($type == 15) {
		$s_img = imagecreatefromwbmp($source);
	} else {
		return false;
	}

	// リサイズ後のサイズを計算
	// 縦長の場合
	if ($s_height > $s_width) {
		$new_height =  $height;
		$new_width = floor($s_width * ($new_height / $s_height));
	} else {
		// 横長
		$new_width = $width;
		$new_height = floor($s_height * ($new_width / $s_width));
	}

	// 空の画像を作成する
	$d_img = imageCreateTrueColor($new_width, $new_height);

	// 画像のリサイズ
	ImageCopyResampled($d_img,$s_img,0,0,0,0,$new_width,$new_height,$s_width,$s_height);

	// ファイルへ保存
	if ($type == 1) {
		imagegif($d_img, $target, 100);
	} else if ($type == 2) {
		imagejpeg($d_img, $target, 100);
	} else if ($type == 3) {
		imagepng($d_img, $target, 1);
	} else {
		imagebmp($d_img, $target, 100);
	}

	// メモリを解放する
	imagedestroy ($s_img);
	imagedestroy ($d_img);
	return true;
}

/**
 * 画像リサイズ
 *
 * 横幅は固定　基準の画像の縦横比率にてリサイズ
 *
 *
 * @param String $source 元ファイル名
 * @param Stirng $target リサイズファイル名
 * @param Int $width リサイズ幅
 * @param Int $height リアサイズ高
 * @return Boolean 処理結果
 */
function resize_image2($source, $target, $width, $height) {
	if (!$target) return false;

	// 画像のサイズとタイプを取得
	list($s_width,$s_height, $type) = getimagesize($source);

	// 画像を読み込む
	if ($type == 1) {
		$s_img = imagecreatefromgif($source);
	} else if ($type == 2) {
		$s_img = imagecreatefromjpeg($source);
	} else if ($type == 3) {
		$s_img = imagecreatefrompng($source);
	} else if ($type == 15) {
		$s_img = imagecreatefromwbmp($source);
	} else {
		return false;
	}

	// リサイズ後のサイズを計算

		$new_width = $width;
		$new_height = floor($s_height * ($new_width / $s_width));

/*
	// 縦長の場合
	if ($s_height > $s_width) {
		$new_width = $width;

		//比率を計算して縦の


		$new_height =  $height;
		$new_width = floor($s_width * ($new_height / $s_height));
	} else {
		// 横長
		$new_width = $width;
		$new_height = floor($s_height * ($new_width / $s_width));
	}
*/
	// 空の画像を作成する
	$d_img = imageCreateTrueColor($new_width, $new_height);

	// 画像のリサイズ
	ImageCopyResampled($d_img,$s_img,0,0,0,0,$new_width,$new_height,$s_width,$s_height);

	// ファイルへ保存
	if ($type == 1) {
		imagegif($d_img, $target, 100);
	} else if ($type == 2) {
		imagejpeg($d_img, $target, 100);
	} else if ($type == 3) {
		imagepng($d_img, $target, 1);
	} else {
		imagebmp($d_img, $target, 100);
	}

	// メモリを解放する
	imagedestroy ($s_img);
	imagedestroy ($d_img);
	return true;
}
function GetAlabNum($fnum){ 
	$nums = array("０","１","２","３","４","５","６","７","８","９");
	$fnums = "0123456789"; 
	for($i=0;$i<=9;$i++) $fnum = str_replace($nums[$i],$fnums[$i],$fnum); 
	$fnum = ereg_replace("[^0-9.]|^0{1,}","",$fnum); 
	if($fnum=="") $fnum=0; 
	return $fnum; 
}
/**
* Author : smallchicken
* Time   : 2009年6月8日16:46:05
* mode 1 : 强制裁剪，生成图片严格按照需要，不足放大，超过裁剪，图片始终铺满
* mode 2 : 和1类似，但不足的时候 不放大 会产生补白，可以用png消除。
* mode 3 : 只缩放，不裁剪，保留全部图片信息，会产生补白，
* mode 4 : 只缩放，不裁剪，保留全部图片信息，生成图片大小为最终缩放后的图片有效信息的实际大小，不产生补白
* 默认补白为白色，如果要使补白成透明像素，请使用SaveAlpha()方法代替SaveImage()方法
*
* 调用方法：
*
* $ic=new ImageCrop('old.jpg','afterCrop.jpg');
* $ic->Crop(120,80,2);
* $ic->SaveImage();
*        //$ic->SaveAlpha();将补白变成透明像素保存
* $ic->destory();
*
*
*/

function make_crop($src,$dst,$width,$height,$mode)
{
        $ic=new ImageCrop($src, $dst);
        $ic->Crop($width , $height , $mode);
        $ic->SaveImage();
        $ic->destory();
}

class ImageCrop{

	var $sImage;
	var $dImage;
	var $src_file;
	var $dst_file;
	var $src_width;
	var $src_height;
	var $src_ext;
	var $src_type;

	function ImageCrop($src_file,$dst_file=''){
		$this->src_file=$src_file;
		$this->dst_file=$dst_file;
		$this->LoadImage();
	}

	function SetSrcFile($src_file){
		$this->src_file=$src_file;
	}

	function SetDstFile($dst_file){
		$this->dst_file=$dst_file;
	}

	function LoadImage(){
		list($this->src_width, $this->src_height, $this->src_type) = getimagesize($this->src_file);
		switch($this->src_type) {
		case IMAGETYPE_JPEG :
		$this->sImage=imagecreatefromjpeg($this->src_file);
		$this->ext='jpg';
		break;
		case IMAGETYPE_PNG :
		$this->sImage=imagecreatefrompng($this->src_file);
		$this->ext='png';
		break;
		case IMAGETYPE_GIF :
		$this->sImage=imagecreatefromgif($this->src_file);
		$this->ext='gif';
		break;
		default:
		exit();
		}
	}

	function SaveImage($fileName=''){
		$this->dst_file=$fileName ? $fileName : $this->dst_file;
		switch($this->src_type) {
		case IMAGETYPE_JPEG :
		imagejpeg($this->dImage,$this->dst_file,100);
		break;
		case IMAGETYPE_PNG :
		imagepng($this->dImage,$this->dst_file);
		break;
		case IMAGETYPE_GIF :
		imagegif($this->dImage,$this->dst_file);
		break;
		default:
		break;
		}
	}

	function OutImage(){
		switch($this->src_type) {
		case IMAGETYPE_JPEG :
		header('Content-type: image/jpeg');
		imagejpeg($this->dImage);
		break;
		case IMAGETYPE_PNG :
		header('Content-type: image/png');
		imagepng($this->dImage);
		break;
		case IMAGETYPE_GIF :
		header('Content-type: image/gif');
		imagegif($this->dImage);
		break;
		default:
		break;
		}
	}

	function SaveAlpha($fileName=''){
		$this->dst_file=$fileName ? $fileName . '.png' : $this->dst_file .'.png';
		imagesavealpha($this->dImage, true);
		imagepng($this->dImage,$this->dst_file);
	}

	function OutAlpha(){
		imagesavealpha($this->dImage, true);
		header('Content-type: image/png');
		imagepng($this->dImage);
	}   

	function destory(){
		imagedestroy($this->sImage);
		imagedestroy($this->dImage);
	}
	
	function Crop($dst_width,$dst_height,$mode=1,$dst_file=''){
		if($dst_file) $this->dst_file=$dst_file;
		$this->dImage = imagecreatetruecolor($dst_width,$dst_height);

		$bg = imagecolorallocatealpha($this->dImage,255,255,255,127);
		imagefill($this->dImage, 0, 0, $bg);
		imagecolortransparent($this->dImage,$bg);

		$ratio_w=1.0 * $dst_width / $this->src_width;
		$ratio_h=1.0 * $dst_height / $this->src_height;
		$ratio=1.0;
		switch($mode){
		case 1:        // always crop
		if( ($ratio_w < 1 && $ratio_h < 1) || ($ratio_w > 1 && $ratio_h > 1)){
		$ratio = $ratio_w < $ratio_h ? $ratio_h : $ratio_w;
		$tmp_w = (int)($dst_width / $ratio);
		$tmp_h = (int)($dst_height / $ratio);
		$tmp_img=imagecreatetruecolor($tmp_w , $tmp_h);
		$src_x = (int) (($this->src_width-$tmp_w)/2) ;
		$src_y = (int) (($this->src_height-$tmp_h)/2) ;   
		imagecopy($tmp_img, $this->sImage, 0,0,$src_x,$src_y,$tmp_w,$tmp_h);   
		imagecopyresampled($this->dImage,$tmp_img,0,0,0,0,$dst_width,$dst_height,$tmp_w,$tmp_h);
		imagedestroy($tmp_img);
		}else{
		$ratio = $ratio_w < $ratio_h ? $ratio_h : $ratio_w;
		$tmp_w = (int)($this->src_width * $ratio);
		$tmp_h = (int)($this->src_height * $ratio);
		$tmp_img=imagecreatetruecolor($tmp_w ,$tmp_h);
		imagecopyresampled($tmp_img,$this->sImage,0,0,0,0,$tmp_w,$tmp_h,$this->src_width,$this->src_height);
		$src_x = (int)($tmp_w - $dst_width) / 2 ;
		$src_y = (int)($tmp_h - $dst_height) / 2 ;   
		imagecopy($this->dImage, $tmp_img, 0,0,$src_x,$src_y,$dst_width,$dst_height);
		imagedestroy($tmp_img);
		}
		break;
		case 2:        // only small
		if($ratio_w < 1 && $ratio_h < 1){
		$ratio = $ratio_w < $ratio_h ? $ratio_h : $ratio_w;
		$tmp_w = (int)($dst_width / $ratio);
		$tmp_h = (int)($dst_height / $ratio);
		$tmp_img=imagecreatetruecolor($tmp_w , $tmp_h);
		$src_x = (int) ($this->src_width-$tmp_w)/2 ;
		$src_y = (int) ($this->src_height-$tmp_h)/2 ;   
		imagecopy($tmp_img, $this->sImage, 0,0,$src_x,$src_y,$tmp_w,$tmp_h);   
		imagecopyresampled($this->dImage,$tmp_img,0,0,0,0,$dst_width,$dst_height,$tmp_w,$tmp_h);
		imagedestroy($tmp_img);
		}elseif($ratio_w > 1 && $ratio_h > 1){
		$dst_x = (int) abs($dst_width - $this->src_width) / 2 ;
		$dst_y = (int) abs($dst_height -$this->src_height) / 2;   
		imagecopy($this->dImage, $this->sImage,$dst_x,$dst_y,0,0,$this->src_width,$this->src_height);
		}else{
		$src_x=0;$dst_x=0;$src_y=0;$dst_y=0;
		if(($dst_width - $this->src_width) < 0){
		$src_x = (int) ($this->src_width - $dst_width)/2;
		$dst_x =0;
		}else{
		$src_x =0;
		$dst_x = (int) ($dst_width - $this->src_width)/2;
		}

		if( ($dst_height -$this->src_height) < 0){
		$src_y = (int) ($this->src_height - $dst_height)/2;
		$dst_y = 0;
		}else{
		$src_y = 0;
		$dst_y = (int) ($dst_height - $this->src_height)/2;
		}
		imagecopy($this->dImage, $this->sImage,$dst_x,$dst_y,$src_x,$src_y,$this->src_width,$this->src_height);
		}
		break;
		case 3:        // keep all image size and create need size
		if($ratio_w > 1 && $ratio_h > 1){
		$dst_x = (int)(abs($dst_width - $this->src_width )/2) ;
		$dst_y = (int)(abs($dst_height- $this->src_height)/2) ;
		imagecopy($this->dImage, $this->sImage, $dst_x,$dst_y,0,0,$this->src_width,$this->src_height);
		}else{
		$ratio = $ratio_w > $ratio_h ? $ratio_h : $ratio_w;
		$tmp_w = (int)($this->src_width * $ratio);
		$tmp_h = (int)($this->src_height * $ratio);
		$tmp_img=imagecreatetruecolor($tmp_w ,$tmp_h);
		imagecopyresampled($tmp_img,$this->sImage,0,0,0,0,$tmp_w,$tmp_h,$this->src_width,$this->src_height);
		$dst_x = (int)(abs($tmp_w -$dst_width )/2) ;
		$dst_y = (int)(abs($tmp_h -$dst_height)/2) ;
		imagecopy($this->dImage, $tmp_img, $dst_x,$dst_y,0,0,$tmp_w,$tmp_h);
		imagedestroy($tmp_img);
		}
		break;
		case 4:        // keep all image but create actually size
		if($ratio_w > 1 && $ratio_h > 1){
		$this->dImage = imagecreatetruecolor($this->src_width,$this->src_height);
		imagecopy($this->dImage, $this->sImage,0,0,0,0,$this->src_width,$this->src_height);
		}else{
		$ratio = $ratio_w > $ratio_h ? $ratio_h : $ratio_w;
		$tmp_w = (int)($this->src_width * $ratio);
		$tmp_h = (int)($this->src_height * $ratio);
		$this->dImage = imagecreatetruecolor($tmp_w ,$tmp_h);
		imagecopyresampled($this->dImage,$this->sImage,0,0,0,0,$tmp_w,$tmp_h,$this->src_width,$this->src_height);
		}
		break;
		}
	}// end Crop
}

function post_check($post){
	$arr_post=array();
	foreach ($post as $key=> $val){ 
		$val=SBC_DBC($val,0);
		$val = html_tag_chg($val);
		if (!get_magic_quotes_gpc()) 
		{
			$arr_post[$key] = addslashes($val); 
		}
		//$arr_post[$key] = str_replace("_", "\_", $val); 
		//$arr_post[$key] = str_replace("%", "\%", $val); 
		//$arr_post[$key] = nl2br($val); 
		$arr_post[$key] = str_replace("\0", "", $val);
		$arr_post[$key] = str_replace("\t", "    ", $val); //「\t」をスペース×4に変換
		$arr_post[$key] = mysql_real_escape_string($val);
		$arr_post[$key]= htmlspecialchars($val); 
	} 

	return $arr_post;
}
function get_check($get){
	$get = SBC_DBC($get,0);
	$get = html_tag_chg($get);
	if (!get_magic_quotes_gpc())
	{
		$get = addslashes($get); 
	}
	//$get = str_replace("_", "\_", $get); 
	//$get = str_replace("%", "\%", $get); 
	//$get = nl2br($get); 
	$get = str_replace("\0", "", $get);
	$get = str_replace("\t", "    ", $get); //「\t」をスペース×4に変換
	$get = mysql_real_escape_string($get);
	$get= htmlspecialchars($get); 
	

	return $get;
}
function arr_preg_change($post){
	$preg = "/<script[\s\S]*?<\/script>/i";
	$arr_post=array();
	foreach ($post as $key=> $val){ 
		$arr_post[$key] = preg_replace($preg,"",$val,3);
	} 

	return $arr_post;
}
function preg_change($get){
	$preg = "/<script[\s\S]*?<\/script>/i";
	$get = preg_replace($preg,"",$get,3);

	return $get;
}
function html_tag_chg($val){
	$val = str_replace("onchange","ｏｎｃｈａｎｇｅ", $val);
	$val = str_replace("onmouseover","ｏｎｍｏｕｓｅｏｖｅｒ", $val);
	$val = str_replace("onload","ｏｎｌｏａｄ", $val);
	$val = str_replace("onerror","ｏｎｅｒｒｏｒ", $val);
	$val = str_replace("javascript","ｊａｖａｓｃｒｉｐｔ", $val);
	$val = str_replace("document","ｄｏｃｕｍｅｎｔ", $val);
	$val = str_replace("onclick","ｏｎｃｌｉｃｋ", $val);
	$val = str_replace("alert","ａｌｅｒｔ", $val);
	$val = str_replace("script","ｓｃｒｉｐｔ", $val);

	return $val;
}
function date_format_php($string, $format="%b %e, %Y", $default_date=null)
{
    if (substr(php_OS,0,3) == 'WIN') {
           $_win_from = array ('%e',  '%T',       '%D');
           $_win_to   = array ('%#d', '%H:%M:%S', '%m/%d/%y');
           $format = str_replace($_win_from, $_win_to, $format);
    }
    if($string != '') {
        return strftime($format, smarty_make_timestamp($string));
    } elseif (isset($default_date) && $default_date != '') {
        return strftime($format, smarty_make_timestamp($default_date));
    } else {
        return;
    }
}
function isUrl($s)  
{  
return preg_match('/^http[s]?:\/\/'.  
    '(([0-9]{1,3}\.){3}[0-9]{1,3}'. 
    '|'. 
    '([0-9a-z_!~*\'()-]+\.)*'. 
    '([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.'. 
    '[a-z]{2,6})'.  // first level domain- .com or .museum  
    '(:[0-9]{1,4})?'.  
    '((\/\?)|'.  // a slash isn't required if there is no file name  
    '(\/[0-9a-zA-Z_!~\'
\.;\?:@&=\+\$,%#-\/^\*\|]*)?)$/',  
    $s) == 1;  
}  

function SBC_DBC($str,$args2) { //0:半角->全角；1，全角->半角
    $DBC = Array( '＆','＜' , '＞' , '＂' , '＇','｜');
  $SBC = Array('&','<', '>', '"', '\'','|');
 if($args2==0)
  return str_replace($SBC,$DBC,$str);  //半角->全角
 if($args2==1)
  return str_replace($DBC,$SBC,$str);  //全角->半角
 else
  return false;
}
?>