<?php
	$smarty = new MySmarty();

  //----------------------------------------
  // PAGE TITLE
  //----------------------------------------
	$smarty->assign("str_site_title", $str_site_title);	//タイトル


  //----------------------------------------
  // Meta Data
  //----------------------------------------
	$smarty->assign("str_meta_keywords",$str_meta_keywords);
  	$smarty->assign("str_meta_description",$str_meta_description);
	$smarty->assign("str_meta_og_type",$str_meta_og_type);
  	$smarty->assign("str_meta_og_description",$str_meta_og_description);

//----------------------------------------
  // IMAGE PATH
  //----------------------------------------
	$smarty->assign("C_IMG_URL",$C_IMG_URL);
	$smarty->assign("C_IMG_UPLOAD",$C_IMG_UPLOAD);

  //----------------------------------------
  // E-MAIL SETUP
  //----------------------------------------

  //----------------------------------------
  // PAGE SETUP
  //----------------------------------------
	$smarty->assign("url_home",$url_home);
	$smarty->assign("pagelink_index",$pagelink_index);

	$smarty->assign("pagelink_entry_facebook",$pagelink_entry_facebook);
	$smarty->assign("pagelink_login_facebook",$pagelink_login_facebook);

	$smarty->assign("pagelink_entry_twitter",$pagelink_entry_twitter);
	$smarty->assign("pagelink_login_twitter",$pagelink_login_twitter);

	$smarty->assign("pagelink_head",$pagelink_head);
	$smarty->assign("pagelink_header",$pagelink_header);
	$smarty->assign("pagelink_footer",$pagelink_footer);
	$smarty->assign("pagelink_ranking",$pagelink_ranking);
	$smarty->assign("pagelink_left",$pagelink_left);
	$smarty->assign("pagelink_top",$pagelink_top);
	$smarty->assign("pagelink_login",$pagelink_login);
	$smarty->assign("pagelink_logout",$pagelink_logout);
	$smarty->assign("pagelink_reissue",$pagelink_reissue);

	$smarty->assign("pagelink_mypage",$pagelink_mypage);
	$smarty->assign("pagelink_mypage_profile_edit",$pagelink_mypage_profile_edit);
	$smarty->assign("pagelink_mypage_mail_edit",$pagelink_mypage_mail_edit);
	$smarty->assign("pagelink_mypage_pwd_edit",$pagelink_mypage_pwd_edit);
	$smarty->assign("pagelink_mypage_set_new_pwd",$pagelink_mypage_set_new_pwd);
	$smarty->assign("pagelink_mypage_address_edit",$pagelink_mypage_address_edit);
	$smarty->assign("pagelink_mypage_mail_subscriptions",$pagelink_mypage_mail_subscriptions);

	$smarty->assign("pagelink_mypage_contribution",$pagelink_mypage_contribution);
	$smarty->assign("pagelink_mypage_myprojects",$pagelink_mypage_myprojects);
	$smarty->assign("pagelink_mypage_contribution_comment",$pagelink_mypage_contribution_comment);
	$smarty->assign("pagelink_mypage_mailbox",$pagelink_mypage_mailbox);
	$smarty->assign("pagelink_mypage_sendmailbox",$pagelink_mypage_sendmailbox);
	$smarty->assign("pagelink_mypage_mail_detail",$pagelink_mypage_mail_detail);
	$smarty->assign("pagelink_mypage_sendmsg",$pagelink_mypage_sendmsg);

	$smarty->assign("pagelink_project_widget",$pagelink_project_widget);

	$smarty->assign("pagelink_error",$pagelink_error);

	$smarty->assign("pagelink_register",$pagelink_register);
	$smarty->assign("pagelink_login_member",$pagelink_login_member);
	$smarty->assign("pagelink_login_password",$pagelink_login_password);

	$smarty->assign("pagelink_book_support",$pagelink_book_support);

	$smarty->assign("pagelink_collection_ticket",$pagelink_collection_ticket);//応援リターンの選択
	$smarty->assign("pagelink_collection_info_input",$pagelink_collection_info_input);//決済情報の入力
	$smarty->assign("pagelink_collection_info_confirm",$pagelink_collection_info_confirm);//応援内容の確認
	$smarty->assign("pagelink_collection_info_complete",$pagelink_collection_info_complete);//完了（応援コメントを寄せよう）

	$smarty->assign("pagelink_drafting",$pagelink_drafting);
	$smarty->assign("pagelink_drafting1",$pagelink_drafting1);
	$smarty->assign("pagelink_drafting2",$pagelink_drafting2);
	$smarty->assign("pagelink_drafting3",$pagelink_drafting3);
	$smarty->assign("pagelink_drafting4",$pagelink_drafting4);
	$smarty->assign("pagelink_drafting5",$pagelink_drafting5);
	$smarty->assign("pagelink_drafting6",$pagelink_drafting6);

	$smarty->assign("pagelink_publish",$pagelink_publish);
	$smarty->assign("pagelink_publish_add",$pagelink_publish_add);
	$smarty->assign("pagelink_publish_update",$pagelink_publish_update);
	$smarty->assign("pagelink_publish_update_add",$pagelink_publish_update_add);
	$smarty->assign("pagelink_publish_mail",$pagelink_publish_mail);
	$smarty->assign("pagelink_publish_mail_detail",$pagelink_publish_mail_detail);

	$smarty->assign("pagelink_reg_project",$pagelink_reg_project);
	$smarty->assign("pagelink_reg_project_confirm",$pagelink_reg_project_confirm);
	$smarty->assign("pagelink_reg_project_complete",$pagelink_reg_project_complete);

	$smarty->assign("pagelink_upt_project",$pagelink_upt_project);
	$smarty->assign("pagelink_upt_project_complete",$pagelink_upt_project_complete);

	$smarty->assign("pagelink_register",$pagelink_register);
	$smarty->assign("pagelink_register_complete",$pagelink_register_complete);
	$smarty->assign("pagelink_reg_member_complete",$pagelink_reg_member_complete);
	$smarty->assign("pagelink_reg_member_error",$pagelink_reg_member_error);

	$smarty->assign("pagelink_upt_member",$pagelink_upt_member);
	$smarty->assign("pagelink_upt_member_complete",$pagelink_upt_member_complete);

	$smarty->assign("pagelink_upt_member_project",$pagelink_upt_member_project);

	$smarty->assign("pagelink_book_invest",$pagelink_book_invest);
	$smarty->assign("pagelink_book_invest_complete",$pagelink_book_invest_complete);

	$smarty->assign("pagelink_upt_invest",$pagelink_upt_invest);
	$smarty->assign("pagelink_upt_invest_complete",$pagelink_upt_invest_complete);

	$smarty->assign("pagelink_inquiry",$pagelink_inquiry);//問い合わせ
	$smarty->assign("pagelink_inquiry_confirm",$pagelink_inquiry_confirm);
	$smarty->assign("pagelink_inquiry_complete",$pagelink_inquiry_complete);//問い合わせ完了

	$smarty->assign("pagelink_list_project",$pagelink_list_project);//プロジェクト一覧
	$smarty->assign("pagelink_detail_project",$pagelink_detail_project);//プロジェクト詳細
	$smarty->assign("pagelink_detail_update",$pagelink_detail_update);//プロジェクト詳細
	$smarty->assign("pagelink_detail_supporter",$pagelink_detail_supporter);//プロジェクト詳細
	$smarty->assign("pagelink_detail_project_sns",$pagelink_detail_project_sns);//プロジェクト詳細(いいね・ツイート用URL)

	$smarty->assign("pagelink_column",$pagelink_column);//コラム一覧
	$smarty->assign("pagelink_column_detail",$pagelink_column_detail);//コラム詳細

	$smarty->assign("pagelink_list_ranking",$pagelink_list_ranking);	//ランキング
	$smarty->assign("pagelink_list_actress",$pagelink_list_actress);	//起案者一覧

	$smarty->assign("pagelink_socialfund",$pagelink_socialfund);

	$smarty->assign("pagelink_about",$pagelink_about);
	$smarty->assign("pagelink_guide",$pagelink_guide);

	$smarty->assign("pagelink_privacypolicy",$pagelink_privacypolicy);
	$smarty->assign("pagelink_corporation",$pagelink_corporation);
	$smarty->assign("pagelink_usage",$pagelink_usage);
	$smarty->assign("pagelink_usage_support",$pagelink_usage_support);
	$smarty->assign("pagelink_usage_project",$pagelink_usage_project);
	$smarty->assign("pagelink_tokusyo",$pagelink_tokusyo);

	$smarty->assign("pagelink_faq",$pagelink_faq);

	$smarty->assign("pagelink_adsales",$pagelink_adsales);

	$smarty->assign("pagelink_news",$pagelink_news);
	$smarty->assign("pagelink_news_detail",$pagelink_news_detail);

	$smarty->assign("pagelink_list_contact",$pagelink_list_contact);
	$smarty->assign("pagelink_detail_contact",$pagelink_detail_contact);
	$smarty->assign("pagelink_send_contact",$pagelink_send_contact);
	$smarty->assign("pagelink_del_contact",$pagelink_del_contact);

	$smarty->assign("pagelink_new_project",$pagelink_new_project);
	$smarty->assign("pagelink_report_project",$pagelink_report_project);
	$smarty->assign("pagelink_support_project",$pagelink_support_project);
	$smarty->assign("pagelink_list_supporter",$pagelink_list_supporter);
	$smarty->assign("pagelink_list_member",$pagelink_list_member);

	$smarty->assign("pagelink_start",$pagelink_start);


  //----------------------------------------
  // MESSAGE SETUP
  //----------------------------------------

	$smarty->assign("str_err_login",$str_err_login);
	$smarty->assign("str_err_project",$str_err_project);
	$smarty->assign("str_entire_complete",$str_entire_complete);

	$smarty->assign("str_msg_project_insert_success",$str_msg_project_insert_success);
	$smarty->assign("str_msg_project_insert_fail",$str_msg_project_insert_fail);
	$smarty->assign("str_msg_project_update_success",$str_msg_project_update_success);
	$smarty->assign("str_msg_project_update_fail",$str_msg_project_update_fail);
	$smarty->assign("str_msg_project_delete_success",$str_msg_project_delete_success);
	$smarty->assign("str_msg_project_delete_fail",$str_msg_project_delete_fail);
	$smarty->assign("str_msg_project_not_exist",$str_msg_project_not_exist);

	$smarty->assign("str_msg_image_size_fail",$str_msg_image_size_fail);
	$smarty->assign("str_msg_image_upload_fail",$str_msg_image_upload_fail);
	$smarty->assign("str_msg_image_not_exist",$str_msg_image_not_exist);

	$smarty->assign("str_msg_member_not_exist",$str_msg_member_not_exist);
	$smarty->assign("str_msg_member_email_fail",$str_msg_member_email_fail);
	$smarty->assign("str_msg_member_insert_success",$str_msg_member_insert_success);
	$smarty->assign("str_msg_member_insert_fail",$str_msg_member_insert_fail);
	$smarty->assign("str_msg_member_update_success",$str_msg_member_update_success);
	$smarty->assign("str_msg_member_update_fail",$str_msg_member_update_fail);
	$smarty->assign("str_msg_member_password_success",$str_msg_member_password_success);
	$smarty->assign("str_msg_member_password_fail",$str_msg_member_password_fail);
	$smarty->assign("str_msg_member_insert_exist",$str_msg_member_insert_exist);

	$smarty->assign("str_msg_member_project_fail",$str_msg_member_project_fail);

	$smarty->assign("str_msg_invest_not_period",$str_msg_invest_not_period);

	$smarty->assign("str_msg_invest_insert_success",$str_msg_invest_insert_success);
	$smarty->assign("str_msg_invest_insert_fail",$str_msg_invest_insert_fail);
	$smarty->assign("str_msg_invest_insert_cancel",$str_msg_invest_insert_cancel);

	$smarty->assign("str_msg_invest_delete_success",$str_msg_invest_delete_success);
	$smarty->assign("str_msg_invest_delete_fail",$str_msg_invest_delete_fail);

	$smarty->assign("str_msg_invest_admin_success",$str_msg_invest_admin_success);
	$smarty->assign("str_msg_invest_admin_fail",$str_msg_invest_admin_fail);

	$smarty->assign("str_msg_inquiry_success",$str_msg_inquiry_success);
	$smarty->assign("str_msg_inquiry_fail",$str_msg_inquiry_fail);

	$smarty->assign("str_msg_list_project_no_exist",$str_msg_list_project_no_exist);

	$smarty->assign("str_msg_message_no_exist",$str_msg_message_no_exist);
	$smarty->assign("str_msg_message_delete_success",$str_msg_message_delete_success);
	$smarty->assign("str_msg_message_delete_fail",$str_msg_message_delete_fail);
	$smarty->assign("str_msg_message_send_success",$str_msg_message_send_success);
	$smarty->assign("str_msg_message_send_fail",$str_msg_message_send_fail);

	$smarty->assign("str_msg_comment_insert_success",$str_msg_comment_insert_success);
	$smarty->assign("str_msg_comment_insert_fail",$str_msg_comment_insert_fail);
	$smarty->assign("str_msg_comment_delete_success",$str_msg_comment_delete_success);
	$smarty->assign("str_msg_comment_delete_fail",$str_msg_comment_delete_fail);

  //----------------------------------------
  // array
  //----------------------------------------
	$smarty->assign("array_lang",$array_lang);

?>
