<?php
/**
 * common.config.php
 * @version 0.1
 * @author takeshima
 * @since 2012/05/14
 */

// 共通チェック配列クラス
class CommonChkArray {


	/*
	 *
	 * チェック配列の数字意味
	   ========================================================================================
	   string  --　項目名
	　 chk     --  チェックするタイプ (0:なし 1:半角英数 2:数値のみ 3:e-mail 5:郵便番号/電話番号等(数字と"-")
	　　　　　　　 6:全角カナ //7:全角かな  8:半角カナ 11:数値と/ 12:数値と小数点)
		       14:数値と-と:
	   nchk    --  必須項目フラグ(1:必須　0:任意）　　
	   ========================================================================================
	*/
//入力チェック用配列(登録時に使用するので、テーブルに入れたい項目を、dbstringのみフォームのinput全項目セットしておく。別テーブルに入れる場合は、ここには入れずにソース内で調整）
//string は エラー時に表示される文字列
//、



//
// BOKUTSUKU
// added by M.Fujisawa 20140918
//

//********* 会員登録 **********************************
	// ユーザー新規登録
	public  static $memberRegistCheckData = Array(
	"dbstring" =>  array(
			"nickname" => "ニックネーム",
			"email" => "メールアドレス",
			"member_name" => "氏名",
			"name_1" => "姓",
			"name_2" => "名",
			"ml_flg" => "表示フラグ",
/*
			"line_id" => "ラインID",
			"member_name" => "氏名",
			"zipcode" => "郵便番号",
			"add_1" => "都道府県",
			"address" => "住所",
			"tel" => "電話番号",
			"birth_year" => "生年月日Y",
			"birth_month" => "生年月日M",
			"birth_day" => "生年月日D",
			"twitter_id" => "twitter_id",
*/
			),
	"string" =>  array(
			"nickname" => "ニックネーム",
			"email" => "メールアドレス",
			"email2" => "メールアドレス",
			"password" => "パスワード",
			"password2" => "パスワード",
			"member_name" => "氏名",
			"name_1" => "姓",
			"name_2" => "名",
			"ml_flg" => "表示フラグ",
/*			"line_id" => "ラインID",
			"member_name" => "氏名",
			"zipcode" => "郵便番号",
			"add_1" => "都道府県",
			"address" => "住所",
			"tel" => "電話番号",
			"birth_year" => "生年月日Y",
			"birth_month" => "生年月日M",
			"birth_day" => "生年月日D",
*/
		),
     "type" => array(
			"nickname" => "text",
			"email" => "text",
			"password" => "text",
			"name_1" => "text",
			"name_2" => "text",
		/*
			"line_id" => "text",
			"member_name" => "text",
			"zipcode" => "text",
			"add_1" => "pull",
			"address" => "text",
			"tel" => "text",
			"birth_year" => "pull",
			"birth_month" => "pull",
			"birth_day" => "pull",
		*/
			),
	"nchk" => array(
			"name_1" => "1",
			"name_2" => "1",
			"email" => "1",
			"password" => "1",
		),
		"max" => array(
			"password" => "12",
			"nickname" => "20",
			),
	"min" => array(
			"password" => "8",
//			"nickname" => "4",
			),

	"chk" => array(
//			"zipcode" 	=> "5",
			"email" 	=> "3",
//			"tel" => "2",
			"password" => "1",
			)
		);

	// 支援時会員情報
	public  static $memberInvestCheckData = Array(
	"dbstring" =>  array(
		"member_name" => "お名前",
		"zipcode" => "郵便番号",
		"add_1" => "都道府県",
		"address" => "住所",
		"tel" => "電話番号",

		),
	"string" =>  array(
		"member_name" => "お名前",
		"zipcode" => "郵便番号",
		"add_1" => "都道府県",
		"address" => "住所",
		"tel" => "電話番号",
		),
     "type" => array(
			"member_name" => "text",
			"zipcode" => "text",
			"add_1" => "text",
			"address" => "text",
			"tel" => "text",
		),
	"nchk" => array(
			"member_name" => "1",
			"zipcode" => "1",
			"add_1" => "1",
			"address" => "1",
			"tel" => "1",
		),
		"max" => array(
//			"zip1" 			=> "3",
//			"zip2" 			=> "4",
			),
	"min" => array(
//			"zip1" 			=> "3",
//			"zip2" 			=> "4",
			),

	"chk" => array(
			"zipcode" => "5",
			"tel" => "2",
			)
		);

	// マイページ　会員情報更新
	public  static $memberEditCheckData = Array(
	"dbstring" =>  array(
			"name_1" => "姓",
			"name_2" => "名",
			"nickname" => "ニックネーム",
//			"email" => "メールアドレス",
			"member_name" => "氏名",
			"add_1" => "都道府県",
			"hp_url" => "WEBサイトURL",
			"profile" => "自己紹介",
			"profile_img" => "プロフィール画像",
			),
	"string" =>  array(
			"name_1" => "姓",
			"name_2" => "名",
			"nickname" => "ニックネーム",
//			"email" => "メールアドレス",
			"member_name" => "氏名",
			"add_1" => "都道府県",
			"hp_url" => "WEBサイトURL",
			"profile" => "自己紹介",
			"profile_img" => "プロフィール画像",
		),
     "type" => array(
			"name_1" => "text",
			"name_2" => "text",
			"nickname" => "text",
//			"email" => "text",
//			"new_password" => "text",
			"member_name" => "text",
			"add_1" => "pull",
			"hp_url" => "text",
			"profile" => "text",
			"profile_img" => "text",
			),
	"nchk" => array(
			"nickname" => "1",
		//	"email" => "1",
		//	"password" => "1",
		),
		"max" => array(
//			"new_password" => "12",
			"nickname" => "20",
			"profile" => "300",
			),
	"min" => array(
//			"new_password" => "8",
//			"nickname" => "4",
			),

	"chk" => array(
//			"email" 	=> "3",
//			"new_password" => "1",
			)
		);

	// マイページ　パスワード更新
	public  static $pwdEditCheckData = Array(
	"dbstring" =>  array(
			"password" => "パスワード",
			),
	"string" =>  array(
			"password" => "現在のパスワード",
			"new_password" => "新しいパスワード",
		),
	"type" => array(
			"password" => "text",
			"new_password" => "text",
			),
	"nchk" => array(
			"password" => "1",
			"new_password" => "1",
		),
	"max" => array(
			"new_password" => "12",
			),
	"min" => array(
			"new_password" => "8",
			),

	"chk" => array(
			"new_password" => "1",
			)
		);
	// マイページ　パスワード更新
	public  static $newPwdEditCheckData = Array(
	"dbstring" =>  array(
			"password" => "パスワード",
			),
	"string" =>  array(
			"new_password" => "新しいパスワード",
		),
	"type" => array(
			"new_password" => "text",
			),
	"nchk" => array(
			"new_password" => "1",
		),
	"max" => array(
			"new_password" => "12",
			),
	"min" => array(
			"new_password" => "8",
			),

	"chk" => array(
			"new_password" => "1",
			)
		);
	// マイページ　メールアドレス更新
	public  static $mailEditCheckData = Array(
	"dbstring" =>  array(
			"email" => "メールアドレス",
			),
	"string" =>  array(
			"password" => "パスワード",
			"new_email" => "新しいメールアドレス",
		),
	"type" => array(
			"password" => "text",
			"new_email" => "text",
			),
	"nchk" => array(
			"new_email" => "1",
			"password" => "1",
		),
	"max" => array(
			),
	"min" => array(
			),
	"chk" => array(
			"new_email" => "3",
			)
		);
//********* 起案者希望登録 **********************************
	//起案者登録
	public  static $actressRegistCheckData = Array(
	   "dbstring" =>  array(
			"name" => "名前",
			"fb_url" => "Facebookアカウント",
			"project_name" => "プロジェクト名",
			"category_no" => "カテゴリ名",
			"project_text" => "プロジェクト概要",
			"wish_price" => "想定目標金額",
			"comment_text" => "想定リターン",

			
	),
	"string" =>  array(
			"name" => "名前",
			"email" => "メールアドレス",
			"fb_url" => "Facebookアカウント",
			"project_name" => "プロジェクト名",
			"category_no" => "カテゴリ名",
			"project_text" => "プロジェクト概要",
			"wish_price" => "想定目標金額",
			"comment_text" => "想定リターン",
		),
     "type" => array(
			"name" => "text",
			"email" => "text",
			"fb_url" => "text",
			"project_name" => "text",
			"category_no" => "pull",
			"project_text" => "text",
			"wish_price" => "text",
			"comment_text" => "text",
			 			),
	"chk" => array(
			"email" => "3",
			"wish_price" => "2",
		),
	"max" => array(
			"name" => "70",
			"project_name" => "60",
			"project_text" => "1000",
			"comment_text" => "1000",
			),
	"min" => array(
			),
	"nchk" => array(
			"name" => "1",
			"email" => "1",
			"project_name" => "1",
			"category_no" => "1",
			"project_text" => "1",
			"wish_price" => "1",
			"comment_text" => "1",
			)
		);
	//起案者詳細登録
	public  static $actressRegistDetailCheckData = Array(
	   "dbstring" =>  array(
			"lang" => "言語コード",
			"name" => "名前",
			"area" => "居住地域",
			"age" => "年齢",
			"height" => "身長",
			"bust" => "B（バスト）",
			"bust_cup" => "カップ",
			"waist" => "W（ウエスト）",
			"hip" => "H（ヒップ）",
			"job" => "職業",
			"blood_type" => "血液型",
			"hobby" => "趣味",
			"favorite_food" => "好きな食べ物",
			"favorite_sports" => "好きなスポーツ",
			"dream" => "将来の夢",
			"play" => "やってみたいプレイ",
			"taste" => "S　or M",
			"favorite_man" => "好きな男性のタイプ",
			"want_to_place" => "行ってみたいところ",
				),
	"string" =>  array(
			"name" => "名前",
			"area" => "居住地域",
			"age" => "年齢",
			"height" => "身長",
			"bust" => "バスト",
			"bust_cup" => "カップ",
			"waist" => "ウエスト",
			"hip" => "ヒップ",
			"job" => "職業",
			"blood_type" => "血液型",
			"hobby" => "趣味",
			"favorite_food" => "好きな食べ物",
			"favorite_sports" => "好きなスポーツ",
			"dream" => "将来の夢",
			"play" => "やってみたいプレイ",
			"taste" => "S or M",
			"favorite_man" => "好きな男性のタイプ",
			"want_to_place" => "行ってみたいところ",
			),
     "type" => array(
			"name" => "text",
			"area" => "pull",
			"age" => "text",
			"height" => "text",
			"bust" => "text",
			"bust_cup" => "pull",
			"waist" => "text",
			"hip" => "text",
			"job" => "text",
			"blood_type" => "text",
			"hobby" => "text",
			"favorite_food" => "text",
			"favorite_sports" => "text",
			"dream" => "text",
			"play" => "text",
			"taste" => "pull",
			"favorite_man" => "text",
			"want_to_place" => "text",
			"memo" => "text",
			 			),
	"chk" => array(
			"age" => "2",
			"height" => "12",
			"bust" => "12",
			"waist" => "12",
			"hip" => "12",
			 						),
	"max" => array(
			),
	"min" => array(
			),
	"nchk" => array(
			"name" => "1",
			)
		);


//********* プロジェクト登録 **********************************
	//プロジェクト基本情報登録
	public  static $projectRegistBasicCheckData = Array(
	   "dbstring" =>  array(
			"no" => "プロジェクトNo",
			"public_title" => "共通タイトル",
//			"category_no" => "カテゴリーNO",
//			"type" => "タイプ",
//			"area" => "プロジェクト名",
			"project_owner" => "起案者No",
			"wish_price" => "達成条件　支援額",
//			"wish_supporter" => "達成条件　支援件数",
			"wish_share" => "達成条件　facebook、Twitter合算",
			"invest_limit" => "支援募集期限 ",
//			"project_start_date" => "プロジェクト開始日",
//			"project_record_date" => "プロジェクト登録日",
			"project_success_date" => "プロジェクト成立日",
//			"now_summary" => "支援金額 ",
//			"now_supporter" => "支援者数",
//			"now_like" => "いいね数",
//			"now_twitter" => "ツイート数 ",
			"movie_type" => "動画タイプ",
			"status" => "ステータス",
//			"recommend_flg" => "お勧めフラグ",
//			"recommend_text" => "お勧めテキスト",
		),
	"string" =>  array(
			"no" => "プロジェクトNo",
			"public_title" => "共通タイトル",
//			"category_no" => "カテゴリーNO",
//			"type" => "タイプ",
//			"area" => "プロジェクト名",
			"project_owner" => "起案者",
			"wish_price" => "達成条件　支援額",
//			"wish_supporter" => "達成条件　支援件数",
			"wish_share" => "達成条件　facebook、Twitter合算",
			"invest_limit" => "支援募集期限 ",
//			"project_start_date" => "プロジェクト開始日",
//			"project_record_date" => "プロジェクト登録日",
			"project_success_date" => "プロジェクト成立日",
//			"now_summary" => "支援金額 ",
//			"now_supporter" => "支援者数",
//			"now_like" => "いいね数",
//			"now_twitter" => "ツイート数 ",
			"movie_type" => "動画タイプ",
			"status" => "ステータス",
//			"recommend_flg" => "お勧めフラグ",
//			"recommend_text" => "お勧めテキスト",
					),
     "type" => array(
			"public_title" => "text",
			"project_owner" => "pull",
			"wish_price" => "text",
			"wish_share" => "text",
			"invest_limit" => "text",
			"movie_type" => "option",

					),
	"chk" => array(
			"wish_price" => "2",
			"wish_share" => "2",
			),
	"max" => array(
			),
	"min" => array(
			),
	"nchk" => array(
			"public_title" => "1",
			"project_owner" => "1",
			"wish_price" => "1",
			"wish_share" => "1",
			"invest_limit" => "1",
			"movie_type" => "1",
			)
		);
//********* 起案 **********************************
//起案基本情報登録
public  static $drafting1CheckData = Array(
	"dbstring" =>  array(
		"project_type" => "プロジェクトタイプ",
		"public_title" => "プロジェクトタイトル",
		"cover_img" => "サムネイル画像",
		"wish_price" => "目標金額",
		"movie_type" => "動画",
		"movie_url" => "動画URL ",
		"category_no" => "カテゴリー",
		"project_record_date" => "開始日",
		"invest_limit" => "終了日",
		"thanks_image" => "お礼画像",
		"thanks_msg" => "お礼メッセージ",
		"hope_url" => "希望URL",
	),
	"string" =>  array(
		"project_type" => "プロジェクトタイプ",
		"public_title" => "プロジェクトタイトル",
		"up_file1" => "サムネイル画像",
		"wish_price" => "目標金額",
		"movie_type" => "動画",
		"movie_url" => "動画URL ",
		"category_no" => "カテゴリー",
		"project_record_date" => "開始日",
		"invest_limit" => "終了日",
		"up_file2" => "お礼画像",
		"thanks_msg" => "お礼メッセージ",
		"hope_url" => "希望URL",
	),
	"type" => array(
		"project_type" => "option",
		"public_title" => "text",
		"wish_price" => "text",
		"movie_type" => "text",
		"movie_url" => "text ",
		"category_no" => "pull",
		"project_record_date" => "text",
		"invest_limit" => "text",
		"thanks_msg" => "text",
		"hope_url" => "text",
	),
	"chk" => array(
		"wish_price" => "2",
	),
	"max" => array(
		"public_title" => "80",
		"hope_url" => "255",
	),
	"min" => array(
	),
	"nchk" => array(
		"project_type" => "1",
		"public_title" => "1",
		"wish_price" => "1",
		"category_no" => "1",
		"project_record_date" => "1",
		"invest_limit" => "1",
		"hope_url" => "1",
	)
);

public static $drafting2CheckData = Array(
	"dbstring" =>  array(
	),
	"string" =>  array(
		"return_min[1]" => "リターン金額1",
		"return_min[2]" => "リターン金額2",
		"return_min[3]" => "リターン金額3",
		"return_min[4]" => "リターン金額4",
		"return_min[5]" => "リターン金額5",
		"return_min[6]" => "リターン金額6",
		"return_min[7]" => "リターン金額7",
		"return_min[8]" => "リターン金額8",
		"return_min[9]" => "リターン金額9",
		"return_min[10]" => "リターン金額10",
		"return_min[11]" => "リターン金額11",
		"return_min[12]" => "リターン金額12",
		"return_min[13]" => "リターン金額13",
		"return_min[14]" => "リターン金額14",
		"return_min[15]" => "リターン金額15",
		"return_min[16]" => "リターン金額16",
		"return_min[17]" => "リターン金額17",
		"return_min[18]" => "リターン金額18",
		"return_min[19]" => "リターン金額19",
		"return_min[20]" => "リターン金額20",
		"return_min[21]" => "リターン金額21",
		"return_min[22]" => "リターン金額22",
		"return_min[23]" => "リターン金額23",
		"return_min[24]" => "リターン金額24",
		"return_min[25]" => "リターン金額25",
		"return_min[26]" => "リターン金額26",
		"return_min[27]" => "リターン金額27",
		"return_min[28]" => "リターン金額28",
		"return_min[29]" => "リターン金額29",
		"return_min[30]" => "リターン金額30",
		"return_title[1]" => "リターンタイトル1",
		"return_title[2]" => "リターンタイトル2",
		"return_title[3]" => "リターンタイトル3",
		"return_title[4]" => "リターンタイトル4",
		"return_title[5]" => "リターンタイトル5",
		"return_title[6]" => "リターンタイトル6",
		"return_title[7]" => "リターンタイトル7",
		"return_title[8]" => "リターンタイトル8",
		"return_title[9]" => "リターンタイトル9",
		"return_title[10]" => "リターンタイトル10",
		"return_title[11]" => "リターンタイトル11",
		"return_title[12]" => "リターンタイトル12",
		"return_title[13]" => "リターンタイトル13",
		"return_title[14]" => "リターンタイトル14",
		"return_title[15]" => "リターンタイトル15",
		"return_title[16]" => "リターンタイトル16",
		"return_title[17]" => "リターンタイトル17",
		"return_title[18]" => "リターンタイトル18",
		"return_title[19]" => "リターンタイトル19",
		"return_title[20]" => "リターンタイトル20",
		"return_title[21]" => "リターンタイトル21",
		"return_title[22]" => "リターンタイトル22",
		"return_title[23]" => "リターンタイトル23",
		"return_title[24]" => "リターンタイトル24",
		"return_title[25]" => "リターンタイトル25",
		"return_title[26]" => "リターンタイトル26",
		"return_title[27]" => "リターンタイトル27",
		"return_title[28]" => "リターンタイトル28",
		"return_title[29]" => "リターンタイトル29",
		"return_title[30]" => "リターンタイトル30",
		"return_text[1]" => "リターン内容1",
		"return_text[2]" => "リターン内容2",
		"return_text[3]" => "リターン内容3",
		"return_text[4]" => "リターン内容4",
		"return_text[5]" => "リターン内容5",
		"return_text[6]" => "リターン内容6",
		"return_text[7]" => "リターン内容7",
		"return_text[8]" => "リターン内容8",
		"return_text[9]" => "リターン内容9",
		"return_text[10]" => "リターン内容10",
		"return_text[11]" => "リターン内容11",
		"return_text[12]" => "リターン内容12",
		"return_text[13]" => "リターン内容13",
		"return_text[14]" => "リターン内容14",
		"return_text[15]" => "リターン内容15",
		"return_text[16]" => "リターン内容16",
		"return_text[17]" => "リターン内容17",
		"return_text[18]" => "リターン内容18",
		"return_text[19]" => "リターン内容19",
		"return_text[20]" => "リターン内容20",
		"return_text[21]" => "リターン内容21",
		"return_text[22]" => "リターン内容22",
		"return_text[23]" => "リターン内容23",
		"return_text[24]" => "リターン内容24",
		"return_text[25]" => "リターン内容25",
		"return_text[26]" => "リターン内容26",
		"return_text[27]" => "リターン内容27",
		"return_text[28]" => "リターン内容28",
		"return_text[29]" => "リターン内容29",
		"return_text[30]" => "リターン内容30",
		"return_post_flg[1]" => "リターンの郵送1",
		"return_post_flg[2]" => "リターンの郵送2",
		"return_post_flg[3]" => "リターンの郵送3",
		"return_post_flg[4]" => "リターンの郵送4",
		"return_post_flg[5]" => "リターンの郵送5",
		"return_post_flg[6]" => "リターンの郵送6",
		"return_post_flg[7]" => "リターンの郵送7",
		"return_post_flg[8]" => "リターンの郵送8",
		"return_post_flg[9]" => "リターンの郵送9",
		"return_post_flg[10]" => "リターンの郵送10",
		"return_post_flg[11]" => "リターンの郵送11",
		"return_post_flg[12]" => "リターンの郵送12",
		"return_post_flg[13]" => "リターンの郵送13",
		"return_post_flg[14]" => "リターンの郵送14",
		"return_post_flg[15]" => "リターンの郵送15",
		"return_post_flg[16]" => "リターンの郵送16",
		"return_post_flg[17]" => "リターンの郵送17",
		"return_post_flg[18]" => "リターンの郵送18",
		"return_post_flg[19]" => "リターンの郵送19",
		"return_post_flg[20]" => "リターンの郵送20",
		"return_post_flg[21]" => "リターンの郵送21",
		"return_post_flg[22]" => "リターンの郵送22",
		"return_post_flg[23]" => "リターンの郵送23",
		"return_post_flg[24]" => "リターンの郵送24",
		"return_post_flg[25]" => "リターンの郵送25",
		"return_post_flg[26]" => "リターンの郵送26",
		"return_post_flg[27]" => "リターンの郵送27",
		"return_post_flg[28]" => "リターンの郵送28",
		"return_post_flg[29]" => "リターンの郵送29",
		"return_post_flg[30]" => "リターンの郵送30",
		"return_year[1]" => "リターンのお届け予定年1",
		"return_year[2]" => "リターンのお届け予定年2",
		"return_year[3]" => "リターンのお届け予定年3",
		"return_year[4]" => "リターンのお届け予定年4",
		"return_year[5]" => "リターンのお届け予定年5",
		"return_year[6]" => "リターンのお届け予定年6",
		"return_year[7]" => "リターンのお届け予定年7",
		"return_year[8]" => "リターンのお届け予定年8",
		"return_year[9]" => "リターンのお届け予定年9",
		"return_year[10]" => "リターンのお届け予定年10",
		"return_year[11]" => "リターンのお届け予定年11",
		"return_year[12]" => "リターンのお届け予定年12",
		"return_year[13]" => "リターンのお届け予定年13",
		"return_year[14]" => "リターンのお届け予定年14",
		"return_year[15]" => "リターンのお届け予定年15",
		"return_year[16]" => "リターンのお届け予定年16",
		"return_year[17]" => "リターンのお届け予定年17",
		"return_year[18]" => "リターンのお届け予定年18",
		"return_year[19]" => "リターンのお届け予定年19",
		"return_year[20]" => "リターンのお届け予定年20",
		"return_year[21]" => "リターンのお届け予定年21",
		"return_year[22]" => "リターンのお届け予定年22",
		"return_year[23]" => "リターンのお届け予定年23",
		"return_year[24]" => "リターンのお届け予定年24",
		"return_year[25]" => "リターンのお届け予定年25",
		"return_year[26]" => "リターンのお届け予定年26",
		"return_year[27]" => "リターンのお届け予定年27",
		"return_year[28]" => "リターンのお届け予定年28",
		"return_year[29]" => "リターンのお届け予定年29",
		"return_year[30]" => "リターンのお届け予定年30",
		"return_month[1]" => "リターンのお届け予定月1",
		"return_month[2]" => "リターンのお届け予定月2",
		"return_month[3]" => "リターンのお届け予定月3",
		"return_month[4]" => "リターンのお届け予定月4",
		"return_month[5]" => "リターンのお届け予定月5",
		"return_month[6]" => "リターンのお届け予定月6",
		"return_month[7]" => "リターンのお届け予定月7",
		"return_month[8]" => "リターンのお届け予定月8",
		"return_month[9]" => "リターンのお届け予定月9",
		"return_month[10]" => "リターンのお届け予定月10",
		"return_month[11]" => "リターンのお届け予定月11",
		"return_month[12]" => "リターンのお届け予定月12",
		"return_month[13]" => "リターンのお届け予定月13",
		"return_month[14]" => "リターンのお届け予定月14",
		"return_month[15]" => "リターンのお届け予定月15",
		"return_month[16]" => "リターンのお届け予定月16",
		"return_month[17]" => "リターンのお届け予定月17",
		"return_month[18]" => "リターンのお届け予定月18",
		"return_month[19]" => "リターンのお届け予定月19",
		"return_month[20]" => "リターンのお届け予定月20",
		"return_month[21]" => "リターンのお届け予定月21",
		"return_month[22]" => "リターンのお届け予定月22",
		"return_month[23]" => "リターンのお届け予定月23",
		"return_month[24]" => "リターンのお届け予定月24",
		"return_month[25]" => "リターンのお届け予定月25",
		"return_month[26]" => "リターンのお届け予定月26",
		"return_month[27]" => "リターンのお届け予定月27",
		"return_month[28]" => "リターンのお届け予定月28",
		"return_month[29]" => "リターンのお届け予定月29",
		"return_month[30]" => "リターンのお届け予定月30",
		"max_count[1]" => "リターンの最大数1",
		"max_count[2]" => "リターンの最大数2",
		"max_count[3]" => "リターンの最大数3",
		"max_count[4]" => "リターンの最大数4",
		"max_count[5]" => "リターンの最大数5",
		"max_count[6]" => "リターンの最大数6",
		"max_count[7]" => "リターンの最大数7",
		"max_count[8]" => "リターンの最大数8",
		"max_count[9]" => "リターンの最大数9",
		"max_count[10]" => "リターンの最大数10",
		"max_count[11]" => "リターンの最大数11",
		"max_count[12]" => "リターンの最大数12",
		"max_count[13]" => "リターンの最大数13",
		"max_count[14]" => "リターンの最大数14",
		"max_count[15]" => "リターンの最大数15",
		"max_count[16]" => "リターンの最大数16",
		"max_count[17]" => "リターンの最大数17",
		"max_count[18]" => "リターンの最大数18",
		"max_count[19]" => "リターンの最大数19",
		"max_count[20]" => "リターンの最大数20",
		"max_count[21]" => "リターンの最大数21",
		"max_count[22]" => "リターンの最大数22",
		"max_count[23]" => "リターンの最大数23",
		"max_count[24]" => "リターンの最大数24",
		"max_count[25]" => "リターンの最大数25",
		"max_count[26]" => "リターンの最大数26",
		"max_count[27]" => "リターンの最大数27",
		"max_count[28]" => "リターンの最大数28",
		"max_count[29]" => "リターンの最大数29",
		"max_count[30]" => "リターンの最大数30",
		"upload_file1" => "リターン画像1",
		"upload_file2" => "リターン画像2",
		"upload_file3" => "リターン画像3",
		"upload_file4" => "リターン画像4",
		"upload_file5" => "リターン画像5",
		"upload_file6" => "リターン画像6",
		"upload_file7" => "リターン画像7",
		"upload_file8" => "リターン画像8",
		"upload_file9" => "リターン画像9",
		"upload_file10" => "リターン画像10",
		"upload_file11" => "リターン画像11",
		"upload_file12" => "リターン画像12",
		"upload_file13" => "リターン画像13",
		"upload_file14" => "リターン画像14",
		"upload_file15" => "リターン画像15",
		"upload_file16" => "リターン画像16",
		"upload_file17" => "リターン画像17",
		"upload_file18" => "リターン画像18",
		"upload_file19" => "リターン画像19",
		"upload_file20" => "リターン画像20",
		"upload_file21" => "リターン画像21",
		"upload_file22" => "リターン画像22",
		"upload_file23" => "リターン画像23",
		"upload_file24" => "リターン画像24",
		"upload_file25" => "リターン画像25",
		"upload_file26" => "リターン画像26",
		"upload_file27" => "リターン画像27",
		"upload_file28" => "リターン画像28",
		"upload_file29" => "リターン画像29",
		"upload_file30" => "リターン画像30",
	),
	"type" => array(
		"return_min[1]" => "text",
		"return_min[2]" => "text",
		"return_min[3]" => "text",
		"return_min[4]" => "text",
		"return_min[5]" => "text",
		"return_min[6]" => "text",
		"return_min[7]" => "text",
		"return_min[8]" => "text",
		"return_min[9]" => "text",
		"return_min[10]" => "text",
		"return_min[11]" => "text",
		"return_min[12]" => "text",
		"return_min[13]" => "text",
		"return_min[14]" => "text",
		"return_min[15]" => "text",
		"return_min[16]" => "text",
		"return_min[17]" => "text",
		"return_min[18]" => "text",
		"return_min[19]" => "text",
		"return_min[20]" => "text",
		"return_min[21]" => "text",
		"return_min[22]" => "text",
		"return_min[23]" => "text",
		"return_min[24]" => "text",
		"return_min[25]" => "text",
		"return_min[26]" => "text",
		"return_min[27]" => "text",
		"return_min[28]" => "text",
		"return_min[29]" => "text",
		"return_min[30]" => "text",
		"return_title[1]" => "text",
		"return_title[2]" => "text",
		"return_title[3]" => "text",
		"return_title[4]" => "text",
		"return_title[5]" => "text",
		"return_text[1]" => "text",
		"return_text[2]" => "text",
		"return_text[3]" => "text",
		"return_text[4]" => "text",
		"return_text[5]" => "text",
		"return_post_flg[1]" => "text",
		"return_post_flg[2]" => "text",
		"return_post_flg[3]" => "text",
		"return_post_flg[4]" => "text",
		"return_post_flg[5]" => "text",
		"return_year[1]" => "pull",
		"return_year[2]" => "pull",
		"return_year[3]" => "pull",
		"return_year[4]" => "pull",
		"return_year[5]" => "pull",
		"return_month[1]" => "pull",
		"return_month[2]" => "pull",
		"return_month[3]" => "pull",
		"return_month[4]" => "pull",
		"return_month[5]" => "pull",
		"max_count[1]" => "text",
		"max_count[2]" => "text",
		"max_count[3]" => "text",
		"max_count[4]" => "text",
		"max_count[5]" => "text",
		"upload_file1" => "text",
		"upload_file2" => "text",
		"upload_file3" => "text",
		"upload_file4" => "text",
		"upload_file5" => "text",
		"upload_file6" => "text",
		"upload_file7" => "text",
		"upload_file8" => "text",
		"upload_file9" => "text",
		"upload_file10" => "text",
		"upload_file11" => "text",
		"upload_file12" => "text",
		"upload_file13" => "text",
		"upload_file14" => "text",
		"upload_file15" => "text",
		"upload_file16" => "text",
		"upload_file17" => "text",
		"upload_file18" => "text",
		"upload_file19" => "text",
		"upload_file20" => "text",
		"upload_file21" => "text",
		"upload_file22" => "text",
		"upload_file23" => "text",
		"upload_file24" => "text",
		"upload_file25" => "text",
		"upload_file26" => "text",
		"upload_file27" => "text",
		"upload_file28" => "text",
		"upload_file29" => "text",
		"upload_file30" => "text",
	),
	"chk" => array(
		"return_min[1]" => "2",
		"return_min[2]" => "2",
		"return_min[3]" => "2",
		"return_min[4]" => "2",
		"return_min[5]" => "2",
		"max_count[1]" => "2",
		"max_count[2]" => "2",
		"max_count[3]" => "2",
		"max_count[4]" => "2",
		"max_count[5]" => "2",
	),
	"max" => array(
		"return_title[1]" => "40",
		"return_title[2]" => "40",
		"return_title[3]" => "40",
		"return_title[4]" => "40",
		"return_title[5]" => "40",
		"max_count[1]" => "9999",
		"max_count[2]" => "9999",
		"max_count[3]" => "9999",
		"max_count[4]" => "9999",
		"max_count[5]" => "9999",
	),
	"min" => array(
		"max_count[1]" => "1",
		"max_count[2]" => "1",
		"max_count[3]" => "1",
		"max_count[4]" => "1",
		"max_count[5]" => "1",
		"return_min[1]" => "1000",
		"return_min[2]" => "1000",
		"return_min[3]" => "1000",
		"return_min[4]" => "1000",
		"return_min[5]" => "1000",
	),
	"nchk" => array(
	)
);
//起案基本情報登録
public  static $drafting3CheckData = Array(
	"dbstring" =>  array(
	),
	"string" =>  array(
		"project_text" => "プロジェクト内容",
	),
	"type" => array(
		"project_text" => "text",
	),
	"chk" => array(
	),
	"max" => array(
	),
	"min" => array(
	),
	"nchk" => array(
		"project_text" => "1",
	)
);
//起案基本情報登録
public  static $drafting4CheckData = Array(
	"dbstring" =>  array(
		"facebook_page" => "Facebook",
		"twitter_account" => "Twitter",
		"blog_url_1" => "ブログやホームページURL1",
		"blog_url_2" => "ブログやホームページURL2",
		"blog_url_3" => "ブログやホームページURL3",
	),
	"string" =>  array(
		"facebook_page" => "Facebookページ",
		"twitter_account" => "Twitterのアカウント",
		"blog_url" => "ブログやホームページ",
		"up_file13" => "プロフィール画像",
		"facebook_page" => "Facebook",
		"twitter_account" => "Twitter",
		"blog_url_1" => "ブログやホームページURL1",
		"blog_url_2" => "ブログやホームページURL2",
		"blog_url_3" => "ブログやホームページURL3",
	),
	"type" => array(
		"facebook_page" => "text",
		"twitter_account" => "text",
		"blog_url_1" => "text",
		"blog_url_2" => "text",
		"blog_url_3" => "text",
	),
	"chk" => array(
	),
	"max" => array(
	),
	"min" => array(
	),
	"nchk" => array(
	)
);
//起案基本情報登録
public  static $drafting5CheckData = Array(
	"dbstring" =>  array(
	),
	"string" =>  array(
		"up_file13" => "プロフィール画像",
		"full_name" => "氏名",
		"introduce" => "自己紹介",
		"bank_name" => "金融機関名",
		"branch_bank" => "支店名",
		"branch_bank_code" => "支店コード",
		"card_type" => "口座種別",
		"card_number" => "口座番号",
		"card_owner" => "口座名義人（カナ）",
	),
	"type" => array(
		"up_file13" => "text",
		"full_name" => "text",
		"introduce" => "text",
		"bank_name" => "text",
		"branch_bank" => "text",
		"branch_bank_code" => "text",
		"card_type" => "option",
		"card_number" => "text",
		"card_owner" => "text",
	),
	"chk" => array(
		"branch_bank_code" => "2",
		"card_number" => "2",
		"card_owner" => "6",
	),
	"max" => array(
		"branch_bank_code" => "30",
		"card_number" => "30",
	),
	"min" => array(
	),
	"nchk" => array(
	)
);
//起案基本情報登録
public  static $draftingCheckData = Array(
	"dbstring" =>  array(
		"project_type" => "プロジェクトタイプ",
		"public_title" => "プロジェクトタイトル",
		"cover_img" => "サムネイル画像",
		"wish_price" => "目標金額",
		"movie_type" => "動画",
		"movie_url" => "動画URL ",
		"category_no" => "カテゴリー",
		"project_record_date" => "開始日",
		"invest_limit" => "終了日",
		"thanks_image" => "お礼画像",
		"thanks_msg" => "お礼メッセージ",
		"facebook_page" => "Facebook",
		"twitter_account" => "Twitter",
		"blog_url_1" => "ブログやホームページURL1",
		"blog_url_2" => "ブログやホームページURL2",
		"blog_url_3" => "ブログやホームページURL3",
		//"project_text" => "プロジェクト内容",
	),
	"string" =>  array(
		"project_type" => "プロジェクトタイプ",
		"public_title" => "プロジェクトタイトル",
		"up_file11" => "サムネイル画像",
		"wish_price" => "目標金額",
		"movie_type" => "動画",
		"movie_url" => "動画URL ",
		"category_no" => "カテゴリー",
		"project_record_date" => "開始日",
		"invest_limit" => "終了日",
		"up_file12" => "お礼画像",
		"thanks_msg" => "お礼メッセージ",
		"return_min[1]" => "リターン金額1",
		"return_min[2]" => "リターン金額2",
		"return_min[3]" => "リターン金額3",
		"return_min[4]" => "リターン金額4",
		"return_min[5]" => "リターン金額5",
		"return_title[1]" => "リターンタイトル1",
		"return_title[2]" => "リターンタイトル2",
		"return_title[3]" => "リターンタイトル3",
		"return_title[4]" => "リターンタイトル4",
		"return_title[5]" => "リターンタイトル5",
		"return_text[1]" => "リターン内容1",
		"return_text[2]" => "リターン内容2",
		"return_text[3]" => "リターン内容3",
		"return_text[4]" => "リターン内容4",
		"return_text[5]" => "リターン内容5",
		"return_post_flg[1]" => "リターンの郵送1",
		"return_post_flg[2]" => "リターンの郵送2",
		"return_post_flg[3]" => "リターンの郵送3",
		"return_post_flg[4]" => "リターンの郵送4",
		"return_post_flg[5]" => "リターンの郵送5",
		"return_year[1]" => "リターンのお届け予定年1",
		"return_year[2]" => "リターンのお届け予定年2",
		"return_year[3]" => "リターンのお届け予定年3",
		"return_year[4]" => "リターンのお届け予定年4",
		"return_year[5]" => "リターンのお届け予定年5",
		"return_month[1]" => "リターンのお届け予定月1",
		"return_month[2]" => "リターンのお届け予定月2",
		"return_month[3]" => "リターンのお届け予定月3",
		"return_month[4]" => "リターンのお届け予定月4",
		"return_month[5]" => "リターンのお届け予定月5",
		"max_count[1]" => "リターンの最大数",
		"max_count[2]" => "リターンの最大数",
		"max_count[3]" => "リターンの最大数",
		"max_count[4]" => "リターンの最大数",
		"max_count[5]" => "リターンの最大数",
		"upload_file1" => "リターン画像1",
		"upload_file2" => "リターン画像2",
		"upload_file3" => "リターン画像3",
		"upload_file4" => "リターン画像4",
		"upload_file5" => "リターン画像5",
		"project_text" => "プロジェクト内容",
		"facebook_page" => "Facebookページ",
		"twitter_account" => "Twitterのアカウント",
		"blog_url" => "ブログやホームページ",
		"up_file13" => "プロフィール画像",
		"facebook_page" => "Facebook",
		"twitter_account" => "Twitter",
		"blog_url_1" => "ブログやホームページURL1",
		"blog_url_2" => "ブログやホームページURL2",
		"blog_url_3" => "ブログやホームページURL3",
		"full_name" => "氏名",
		"introduce" => "自己紹介",
		"bank_name" => "金融機関名",
//		"bank_code" => "銀行コード",
		"branch_bank" => "支店名",
		"branch_bank_code" => "支店コード",
		"card_type" => "口座種別",
		"card_number" => "口座番号",
		"card_owner" => "口座名義人（カナ）",
	),
	"type" => array(
		"project_type" => "option",
		"public_title" => "text",
		"up_file11" => "text",
		"wish_price" => "text",
		"movie_type" => "text",
		"movie_url" => "text ",
		"category_no" => "pull",
		"project_record_date" => "text",
		"invest_limit" => "text",
		"up_file12" => "text",
		"thanks_msg" => "text",
		"return_min[1]" => "text",
		"return_min[2]" => "text",
		"return_min[3]" => "text",
		"return_min[4]" => "text",
		"return_min[5]" => "text",
		"return_title[1]" => "text",
		"return_title[2]" => "text",
		"return_title[3]" => "text",
		"return_title[4]" => "text",
		"return_title[5]" => "text",
		"return_text[1]" => "text",
		"return_text[2]" => "text",
		"return_text[3]" => "text",
		"return_text[4]" => "text",
		"return_text[5]" => "text",
		"return_post_flg[1]" => "text",
		"return_post_flg[2]" => "text",
		"return_post_flg[3]" => "text",
		"return_post_flg[4]" => "text",
		"return_post_flg[5]" => "text",
		"return_year[1]" => "pull",
		"return_year[2]" => "pull",
		"return_year[3]" => "pull",
		"return_year[4]" => "pull",
		"return_year[5]" => "pull",
		"return_month[1]" => "pull",
		"return_month[2]" => "pull",
		"return_month[3]" => "pull",
		"return_month[4]" => "pull",
		"return_month[5]" => "pull",
		"max_count[1]" => "text",
		"max_count[2]" => "text",
		"max_count[3]" => "text",
		"max_count[4]" => "text",
		"max_count[5]" => "text",
		"upload_file1" => "text",
		"upload_file2" => "text",
		"upload_file3" => "text",
		"upload_file4" => "text",
		"upload_file5" => "text",
		"project_text" => "text",
		"up_file13" => "text",
		"facebook_page" => "text",
		"twitter_account" => "text",
		"blog_url_1" => "text",
		"blog_url_2" => "text",
		"blog_url_3" => "text",
		"full_name" => "text",
		"introduce" => "text",
		"bank_name" => "text",
//		"bank_code" => "text",
		"branch_bank" => "text",
		"branch_bank_code" => "text",
		"card_type" => "option",
		"card_number" => "text",
		"card_owner" => "text",
	),
	"chk" => array(
		"wish_price" => "2",
		"return_min[1]" => "2",
		"return_min[2]" => "2",
		"return_min[3]" => "2",
		"return_min[4]" => "2",
		"return_min[5]" => "2",
		"max_count[1]" => "2",
		"max_count[2]" => "2",
		"max_count[3]" => "2",
		"max_count[4]" => "2",
		"max_count[5]" => "2",
//		"bank_code" => "2",
		"branch_bank_code" => "2",
		"card_number" => "2",
		"card_owner" => "8",
	),
	"max" => array(
		"public_title" => "80",
		"introduce" => "600",
		"return_title[1]" => "40",
		"return_title[2]" => "40",
		"return_title[3]" => "40",
		"return_title[4]" => "40",
		"return_title[5]" => "40",
		"max_count[1]" => "9999",
		"max_count[2]" => "9999",
		"max_count[3]" => "9999",
		"max_count[4]" => "9999",
		"max_count[5]" => "9999",
	),
	"min" => array(
		"max_count[1]" => "1",
		"max_count[2]" => "1",
		"max_count[3]" => "1",
		"max_count[4]" => "1",
		"max_count[5]" => "1",
		"return_min[1]" => "1000",
		"return_min[2]" => "1000",
		"return_min[3]" => "1000",
		"return_min[4]" => "1000",
		"return_min[5]" => "1000",
	),
	"nchk" => array(
		"project_type" => "1",
		"public_title" => "1",
		//"up_file1" => "1",
		"wish_price" => "1",
		"category_no" => "1",
		"project_record_date" => "1",
		"invest_limit" => "1",
		"project_text" => "1",
	)
);

//アップデート情報登録
public  static $updateContentCheckData = Array(
	"dbstring" =>  array(
		"update_title" => "タイトル",
		"update_contents" => "アップデータ内容",
		"update_type" => "公開範囲",
	),
	"string" =>  array(
		"update_title" => "タイトル",
		"update_contents" => "アップデータ内容",
		"update_type" => "公開範囲",
	),
	"type" => array(
		"update_title" => "text",
		"update_contents" => "text",
		"update_type" => "pull",
	),
	"chk" => array(
	),
	"max" => array(
		"update_title" => "100",
		"update_contents" => "10000",
	),
	"min" => array(
	),
	"nchk" => array(
		"update_title" => "1",
	)
);
//アップデート情報登録
public  static $mailContentCheckData = Array(
	"dbstring" =>  array(
		"to_user_id" => "宛先",
		"message_title" => "件名",
		"message_body" => "本文",
	),
	"string" =>  array(
		"to_user_id" => "宛先",
		"message_title" => "件名",
		"message_body" => "本文",
	),
	"type" => array(
		"to_user_id" => "pull",
		"message_title" => "text",
		"message_body" => "text",
	),
	"chk" => array(
	),
	"max" => array(
		"message_title" => "100",
		"message_body" => "10000",
	),
	"min" => array(
	),
	"nchk" => array(
		"to_user_id" => "1",
		"message_title" => "1",
	)
);
//質問・意見メール
public  static $questionMailContentCheckData = Array(
	"dbstring" =>  array(
	),
	"string" =>  array(
		"message_body" => "本文",
	),
	"type" => array(
		"message_body" => "text",
	),
	"chk" => array(
	),
	"max" => array(
		"message_body" => "10000",
	),
	"min" => array(
	),
	"nchk" => array(
		"message_body" => "1",
	)
);
//********* 支援コース **********************************
	// 支援コース登録
	public  static $presetRegistCheckData = Array(
	"dbstring" =>  array(
			"project_no" => "プロジェクトNO",
			"lang" => "言語コード",
			"min" => "最低支援金額",
			"invest_limit" => "支援限度数",
			"sort" => "表示順",
			"title" => "タイトル",
			"text" => "支援コース内容",

			),
	"string" =>  array(
			"project_no" => "プロジェクトNO",
			"lang" => "言語コード",
			"min" => "最低支援金額",
			"invest_limit" => "支援限度数",
			"sort" => "表示順",
			"title" => "タイトル",
			"text" => "支援コース内容",
						),
     "type" => array(
			"min" => "text",
			"invest_limit" => "text",
			"sort" => "text",
			"title" => "text",
			"text" => "text",
									),
	"nchk" => array(
			"min" => "1",
			"invest_limit" => "1",
			"sort" => "1",
			"title" => "1",
			"text" => "1",
											),
		"max" => array(
//			"zip1" 			=> "3",
//			"zip2" 			=> "4",
			),
	"min" => array(
//			"zip1" 			=> "3",
//			"zip2" 			=> "4",
			),

	"chk" => array(
			"min" => "2",
			"limit" => "2",
			"sort" => "2",
			)
		);

//********* 支援 **********************************
	// 支援登録
	public  static $investCheckData = Array(
	"dbstring" =>  array(
			),
	"string" =>  array(
			"present_no" => "応援リターン",
			"amount_plus" => "追加金額",
			"payment_method" => "決済方法",
			),
	 "type" => array(
			"present_no" => "radio",
			"amount_plus" => "text",
			"payment_method" => "radio",
			),
	"nchk" => array(
			"present_no" => "1",
			"payment_method" => "1",
			),
	"max" => array(
			"amount_plus" => "9",
			),
	"min" => array(
			),
	"chk" => array(
			"amount_plus" => "2",
			)
		);

	// クレジットカード登録
	public  static $cardCheckData = Array(
	"dbstring" =>  array(
			),
	"string" =>  array(
			"card_no" => "カード番号",
			"expdate_month" => "有効期限",
			"expdate_year" => "有効期限",
			"hld_name" => "カード名義",
			"security_code" => "セキュリティコード",
									),
	"type" => array(
			"card_no" => "text",
			"expdate_month" => "pull",
			"expdate_year" => "pull",
			"hld_name" => "text",
			"security_code" => "text",
		),
	"nchk" => array(
			"card_no" => "1",
			"expdate_month" => "1",
			"expdate_year" => "1",
			"hld_name" => "1",
			"security_code" => "1",
			),
	"max" => array(
			"card_no" => "16",
			"security_code" => "4",
			),
	"min" => array(
			"card_no" => "16",
			"security_code" => "3",
			),

	"chk" => array(
			)
	);
	// CVS Info　チェック
	public  static $cvsInfoCheckData = Array(
	"dbstring" =>  array(
			),
	"string" =>  array(
			"convenience" => "コンビニ",
			"ks_name_1" => "氏名(姓)",
			"ks_name_2" => "氏名(名)",
			"ks_name_kana_1" => "氏名カナ(姓)",
			"ks_name_kana_2" => "氏名カナ(名)",
			"ks_tel" => "電話番号",
									),
	"type" => array(
			"convenience" => "radio",
			"ks_name_1" => "text",
			"ks_name_2" => "text",
			"ks_name_kana_1" => "text",
			"ks_name_kana_2" => "text",
			"ks_tel" => "text",
		),
	"nchk" => array(
			"convenience" => "1",
			"ks_name_1" => "1",
			"ks_name_2" => "1",
			"ks_name_kana_1" => "1",
			"ks_name_kana_2" => "1",
			"ks_tel" => "1",
			),
	"max" => array(
			),
	"min" => array(
			),

	"chk" => array(
			"ks_tel" => "5",
			)
	);
	// payeasy Info　チェック
	public  static $payeasyInfoCheckData = Array(
	"dbstring" =>  array(
			),
	"string" =>  array(
			"ks_name_1" => "氏名(姓)",
			"ks_name_2" => "氏名(名)",
			"ks_name_kana_1" => "氏名カナ(姓)",
			"ks_name_kana_2" => "氏名カナ(名)",
			"ks_tel" => "電話番号",
									),
	"type" => array(
			"ks_name_1" => "text",
			"ks_name_2" => "text",
			"ks_name_kana_1" => "text",
			"ks_name_kana_2" => "text",
			"ks_tel" => "text",
		),
	"nchk" => array(
			"ks_name_1" => "1",
			"ks_name_2" => "1",
			"ks_name_kana_1" => "1",
			"ks_name_kana_2" => "1",
			"ks_tel" => "1",
			),
	"max" => array(
			),
	"min" => array(
			),

	"chk" => array(
			"ks_tel" => "5",
			)
	);

	public  static $AdCheckData = Array(
	   "dbstring" =>  array(
				"name" => "広告名",
				"fee" 	=> "掲載料金",
				"link" => "リンク先",
				"view_flg" => "表示/非表示",
				"view_start" => "掲載期間開始",
				"view_end" => "掲載期間終了",

		),
		"string" =>  array(
				"name" => "広告名",
				"fee" 	=> "掲載料金",
				"link" => "リンク先",
				"view_start" => "掲載期間開始",
				"view_end" => "掲載期間終了",
		),
	     "type" => array(
				"name" => "text",
				"link" => "text",
				"fee" 	=> "text",
	 		 ),
	    "max" => array(
	 		 ),
	    "min" => array(
	 		 ),
	 	"chk" => array(
				"name" => "0",
				"fee" 	=> "2",
				),
		"nchk" => array(
				"name" => "1",
				"link" => "1",
				"fee" => "0",
				"view_start" => "1",
				"view_end" => "1",

			)
	);

	public  static $SlideCheckData = Array(
	   "dbstring" =>  array(
				"lang" => "言語",
				"link" => "リンク先",
				"view_flg" => "表示/非表示",

		),
		"string" =>  array(
				"link" => "リンク先",
		),
	     "type" => array(
				"link" => "text",
	 		 ),
	    "max" => array(
	 		 ),
	    "min" => array(
	 		 ),
	 	"chk" => array(
				),
		"nchk" => array(
				"link" => "1",

			)
	);
	//管理サイト　サイト管理者登録
	public  static $adminUpCheckData = Array(
	   "dbstring" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
		),
	"string" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
				),
     "type" => array(
			"user_id" => "text",
			"user_name" => "text",
			"email" 	=> "text",
		 ),
    "max" => array(
 		 ),
    "min" => array(
  		 ),
 	"chk" => array(
			"email" 	=> "3",

			),
	"nchk" => array(
			"user_id" => "1",
			"user_name" => "1",
			"email" 	=> "1",

			)
		);



	public  static $inquiryCheckData = Array(
	   "dbstring" =>  array(

		),
	"string" =>  array(
			"name" 	=> "氏名",
			"email" => "メールアドレス",
			"message" 	=> "内容",
		),
     "type" => array(
			"name" 	=> "text",
			"email" => "text",
			"message" 	=> "textarea",
		 ),
    "max" => array(
			"message" => "1000",
 		 ),
    "min" => array(
  		 ),
 	"chk" => array(
			"email" 	=> "3",

			),
	"nchk" => array(
			"name" 		=> "1",
			"email"	 	=> "1",
			"message" 		=> "1",

			)
		);

//********* 管理サイト　サイト管理者更新 **************************
	public  static $adminNewCheckData = Array(
	   "dbstring" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
		),
	"string" =>  array(
			"user_id" => "ユーザーID",
			"user_name" => "名前",
			"email" 	=> "メールアドレス",
				),
     "type" => array(
			"user_id" => "text",
			"user_name" => "text",
			"email" 	=> "text",
		 ),
    "max" => array(
 		 ),
    "min" => array(
  		 ),
 	"chk" => array(
			"email" 	=> "3",

			),
	"nchk" => array(
			"user_id" => "1",
			"user_name" => "1",
			"email" 	=> "1",

			)
		);




//********* お知らせ **********************************
	public  static $newsCheckData = Array(
	   "dbstring" =>  array(
			"title" 	=> "お知らせタイトル",
			"detail" 	=> "内容",
			"news_date" => "投稿日設定",
			"display_flg" => "表示/非表示",
	),
	"string" =>  array(
			"title" 	=> "お知らせタイトル",
			"detail" 	=> "内容",
			"news_date" => "投稿日設定",
	),
     "type" => array(
			"title" 	=> "text",
			"detail" 	=> "text",
 		 ),
    "max" => array(
	 ),
    "min" => array(
	 ),
 	"chk" => array(
	 ),
	"nchk" => array(
			"title" 	=> "1",
			"detail" 	=> "1",
			"news_date" => "1",
	 )
	);




}

	/*
	 *
	 * チェック配列の数字意味
	   ========================================================================================
	   string  --　項目名
	　 chk     --  チェックするタイプ (0:なし 1:半角英数 2:数値のみ 3:e-mail 5:郵便番号/電話番号等(数字と"-")
	　　　　　　　 6:全角カナ //7:全角かな  8:半角カナ 11:数値と/ 12:数値と小数点)
		       14:数値と-と:
	   nchk    --  必須項目フラグ(1:必須　0:任意）　　
	   ========================================================================================
	*/

?>
