<?php
require_once(SITE_PATH."libs/Smarty.class.php");

class MySmarty extends Smarty {
	public function __construct() {
		parent::__construct();
		$this->template_dir=SITE_PATH."view/template/";
		$this->compile_dir=SITE_PATH."view/template_c/";
		$this->cache_dir=SITE_PATH."view/cache/";
		$this->caching=false;
		$this->default_modifiers = array('escape:"html"');
	}
}
?>
