<?php

// エラー出力する場合
//ini_set( 'display_errors', 1 );
ini_set("display_errors", "OFF");

  //----------------------------------------
  // Site PATH
  //----------------------------------------
    define("SITE_PATH", "/var/www/aport/");

  //----------------------------------------
  // Site URL
  //----------------------------------------
	define("ROOT_URL", "https://aport-asahi.com/");
	define("ROOT_URL2", "https://aport-asahi.com");
	define("FACEBOOK_REDIRECT", ROOT_URL."mypage/facebookRedirct/");

	$param = ereg_replace("/?$", "", $_SERVER["REQUEST_URI"]);
	$params = array();
	if ("" != $param) {
		// パラメーターを / で分割
		$params = explode("/", $param);
	}
    define("CURRENT_URL", $params[2]);

  //----------------------------------------
  // IMAGE PATH
  //----------------------------------------
  $C_IMG_URL = "https://aport-asahi.com/images/";
  $C_IMG_UPLOAD = "https://aport-asahi.com/image-upload/";
	define("C_IMG_URL", "https://aport-asahi.com/images/");
	define("C_IMG_UPLOAD", "https://aport-asahi.com/image-upload/");
	define("C_IMG_AD", C_IMG_UPLOAD."banner/");
	define("C_IMG_SLIDE", C_IMG_UPLOAD."slide/");
	define("C_IMG_PROJECT", C_IMG_UPLOAD."project/");
	define("C_IMG_PROJECT_CONTENT", C_IMG_UPLOAD."project/content/");
	define("C_IMG_ACTRESS", C_IMG_UPLOAD."actress/");
	define("C_IMG_TMP", C_IMG_UPLOAD."tmp/");

	define("C_ASSETS", "https://aport-asahi.com/ja/assets/");

	define("DOCUMENT_ROOT", "/var/www/aport/httpdocs/");

	define("DIR_IMG_TMP", DOCUMENT_ROOT . "image-upload/tmp/");
	define("DIR_IMG_PROJECT", DOCUMENT_ROOT . "image-upload/project/");
	define("DIR_IMG_PROJECT_CONTENT", DOCUMENT_ROOT . "image-upload/project/content/");
	define("DIR_IMG_ACTRESS", DOCUMENT_ROOT . "image-upload/actress/");

  //----------------------------------------
  // E-MAIL SETUP
  //----------------------------------------
  $C_SEND_EMAIL    = "info@a-port-mail.com";
  $C_SEND_EMAIL_NAME    = "A-port運営事務局";
  $C_ADMIN_EMAIL    = "info@a-port-mail.com";
  $C_INQUIRY_EMAIL  = "info@a-port-mail.com";

//  $C_SEND_EMAIL    = "test@apice-tec.co.jp";
//  $C_ADMIN_EMAIL    = "fujisawa@apice-tec.co.jp";
//  $C_INQUIRY_EMAIL  = "test@apice-tec.co.jp";

  //----------------------------------------
  // CREDIX SETUP
  //----------------------------------------
define( 'ROOT_BASE',$_SERVER['DOCUMENT_ROOT']);
define( 'PGCARD_SAMPLE_BASE', dirname(__FILE__));
define( 'PGCARD_SAMPLE_ENCODING' , 'UTF-8');
  //テスト用
//	define("CDREDIX_CLIENTIP_JA", "1019000495");
//	define("CDREDIX_CLIENTIP_OTHER", "1019000496");

//define("PGCARD_SITE_ID", "tsite00018218");
//define("PGCARD_SITE_PASS", "w2f21vvr");
//define("PGCARD_SHOP_ID", "tshop00019557");
//define("PGCARD_SHOP_PASS", "tkvgwc2f");



define("CDREDIX_CLIENTIP_JA", "1011003684");
define("CDREDIX_CLIENTIP_OTHER", "1011003685");
//テスト用
//define("PGCARD_SITE_ID", "tsite00018218");
//define("PGCARD_SITE_PASS", "w2f21vvr");
//define("PGCARD_SHOP_ID", "tshop00019557");
//define("PGCARD_SHOP_PASS", "tkvgwc2f");
//本番用
define("PGCARD_SITE_ID", "mst2000005032");
define("PGCARD_SITE_PASS", "m8ff4h2x");
define("PGCARD_SHOP_ID", "9100522515177");
define("PGCARD_SHOP_PASS", "x7e45a8t");


	//戻りページの表示文字
	define("CDREDIX_SUCCESS_STR_JA", "トップページへ");
	define("CDREDIX_SUCCESS_STR_OTHER", "go to HOME");

	define("CDREDIX_FAILURE_STR_JA", "プロジェクトを支援するへ戻る");
	define("CDREDIX_FAILURE_STR_OTHER", "go to Back this Project");

  //----------------------------------------
  // PAGE SETUP
  //----------------------------------------
  $url_home = "https://aport-asahi.com/";
  $pagelink_index = $url_home.$lang."/index.php";	//トップページ

  $pagelink_entry_facebook = "reg-member-facebook.php";	//Facebookで新規登録
  $pagelink_login_facebook = "login-facebook.php";	//Facebookでログイン

  $pagelink_entry_twitter = $url_home."reg-member-twitter.php";	//Twitterで新規登録
  $pagelink_login_twitter = $url_home."login-twitter.php";	//Twitterでログイン

  $pagelink_head = "sf-head.php";
  $pagelink_header = "sf-header.php";
  $pagelink_footer = "sf-footer.php";
  $pagelink_ranking = "sf-ranking.php";
  $pagelink_left = "sf-left.php";
  $pagelink_top="index.php";
  $pagelink_login = $url_home.$lang."/login.php";	//ログイン
  $pagelink_logout = $url_home.$lang."/logout.php";	//ログアウト
  $pagelink_reissue = $url_home.$lang."/reissue.php";	//パスワード忘れ

  $pagelink_mypage = $url_home.$lang."/mypage.php";	//マイページ
  $pagelink_mypage_profile_edit = $url_home.$lang."/mypage-profile-edit.php";	//マイページ編集
  $pagelink_mypage_mail_edit = $url_home.$lang."/mypage-mail-edit.php";	//メール編集
  $pagelink_mypage_pwd_edit = $url_home.$lang."/mypage-pwd-edit.php";	//パスワード編集
  $pagelink_mypage_set_new_pwd = $url_home.$lang."/mypage-set-new-pwd.php";	//パスワード
  $pagelink_mypage_address_edit = $url_home.$lang."/mypage-address-edit.php";	//配送先情報編集
  $pagelink_mypage_mail_subscriptions = $url_home.$lang."/mypage-mail-subscriptions.php";	//メール通知設定

  $pagelink_mypage_contribution = $url_home.$lang."/mypage-contribution.php";	//応援したプロジェクト
  $pagelink_mypage_myprojects = $url_home.$lang."/mypage-myprojects.php";	//投稿したプロジェクト
  $pagelink_mypage_contribution_comment = $url_home.$lang."/mypage-contribution-comment.php";	//応援コメント
  $pagelink_mypage_mailbox = $url_home.$lang."/mypage-mailbox.php";	//メール
  $pagelink_mypage_sendmailbox = $url_home.$lang."/mypage-sendmailbox.php";	//メール
  $pagelink_mypage_mail_detail = $url_home.$lang."/mypage-mail-detail.php";	//メール
  $pagelink_mypage_sendmsg = $url_home.$lang."/mypage-sendmsg.php";	//メール
  $pagelink_project_widget = $url_home.$lang."/project-widget.php";	//

  $pagelink_error = $url_home.$lang."/error.php";	//エラー

  $pagelink_book_support = $url_home.$lang."/book-support.php";	//支援情報入力
  $pagelink_book_invest = $url_home.$lang."/book-invest.php";	//支援情報確認
  $pagelink_book_invest_complete = $url_home.$lang."/book-invest-complete.php";	//支援登録完了

  $pagelink_collection_ticket = $url_home.$lang."/collection-ticket.php";	//応援リターンの選択
  $pagelink_collection_info_input = $url_home.$lang."/collection-info-input.php";	//決済情報の入力
  $pagelink_collection_info_confirm = $url_home.$lang."/collection-info-confirm.php";	//応援内容の確認
  $pagelink_collection_info_complete = $url_home.$lang."/collection-info-complete.php";	//完了（応援コメントを寄せよう）

  $pagelink_drafting = $url_home.$lang."/drafting.php";
  $pagelink_drafting1 = $url_home.$lang."/drafting1.php";
  $pagelink_drafting2 = $url_home.$lang."/drafting2.php";
  $pagelink_drafting3 = $url_home.$lang."/drafting3.php";
  $pagelink_drafting4 = $url_home.$lang."/drafting4.php";
  $pagelink_drafting5 = $url_home.$lang."/drafting5.php";
  $pagelink_drafting6 = $url_home.$lang."/drafting6.php";

  $pagelink_publish = $url_home.$lang."/publish.php";
  $pagelink_publish_add = $url_home.$lang."/publish-add.php";
  $pagelink_publish_update = $url_home.$lang."/publish-update.php";
  $pagelink_publish_update_add = $url_home.$lang."/publish-update-add.php";
  $pagelink_publish_mail = $url_home.$lang."/publish-mail.php";
  $pagelink_publish_mail_detail = $url_home.$lang."/publish-mail-detail.php";

  $pagelink_reg_project = $url_home.$lang."/reg-project.php";	//プロジェクト登録
  $pagelink_reg_project_confirm = $url_home.$lang."/reg-project-confirm.php";	//プロジェクト登録確認
  $pagelink_reg_project_complete = $url_home.$lang."/reg-project-complete.php";//プロジェクト登録完了

  $pagelink_upt_project = $url_home.$lang."/upt-project.php";
  $pagelink_upt_project_complete = $url_home.$lang."/upt-project-complete.php";

  $pagelink_register = $url_home.$lang."/reg-member-entry.php";	//新規会員登録
  $pagelink_register_complete = $url_home.$lang."/reg-member-entry-complete.php";	//仮登録完了
  $pagelink_reg_member_complete = $url_home.$lang."/reg-member-complete.php";	//認証完了
  $pagelink_reg_member_error = $url_home.$lang."/reg-member-error.php";	//認証エラー


  $pagelink_upt_member = $url_home.$lang."/upt-member.php";
  $pagelink_upt_member_complete = $url_home.$lang."/upt-member-complete.php";

  $pagelink_upt_member_project = $url_home.$lang."/upt-member-project.php";


  $pagelink_upt_invest = $url_home.$lang."/upt-invest.php";
  $pagelink_upt_invest_complete = $url_home.$lang."/upt-invest-complete.php";

  $pagelink_inquiry = $url_home.$lang."/inquiry.php";	//問い合わせ
  $pagelink_inquiry_confirm = $url_home.$lang."/inquiry-confirm.php";
  $pagelink_inquiry_complete = $url_home.$lang."/inquiry-complete.php";	//問い合わせ完了

  $pagelink_list_project = $url_home.$lang."/list-project.php";	//プロジェクト一覧
  $pagelink_detail_project = $url_home."projects/";	//プロジェクト詳細
  $pagelink_detail_update = $url_home."updates/";	//プロジェクト詳細
  $pagelink_detail_supporter = $url_home."supporters/";	//プロジェクト詳細
  $pagelink_detail_project_sns = $url_home.$lang."/project_";	//プロジェクト詳細(いいね・ツイート用URL)

  $pagelink_column = $url_home.$lang."/column.php";	//コラム一覧
  $pagelink_column_detail = $url_home.$lang."/column-detail.php";	//コラム詳細

  $pagelink_list_actress = $url_home.$lang."/list-actress.php";	//起案者一覧
  $pagelink_list_ranking = $url_home.$lang."/list-ranking.php";//ランキング

  $pagelink_socialfund = $url_home.$lang."/whats-socialfund.php";

  $pagelink_faq = $url_home.$lang."/faq.php";
  $pagelink_about = $url_home.$lang."/about.php";	//サイト説明○○とは
  $pagelink_guide = $url_home.$lang."/guide.php";	//ご利用ガイド

  $pagelink_privacypolicy = $url_home.$lang."/privacypolicy.php";	//プライバシーポリシー
  $pagelink_corporation = $url_home.$lang."/corporation.php";	//運営会社
  $pagelink_usage = $url_home.$lang."/usage.php";	//会員規約
  $pagelink_usage_support = $url_home.$lang."/usage-support.php";	//支援者向け利用規約
  $pagelink_usage_project = $url_home.$lang."/usage-project.php";	//プロジェクト参加者向け利用規約
  $pagelink_tokusyo = $url_home.$lang."/tokusyo.php";	//特商法に基づく表記

  $pagelink_adsales = $url_home.$lang."/adsales.php";


  $pagelink_news = $url_home.$lang."/news.php";
  $pagelink_news_detail = $url_home.$lang."/news-detail.php";

  $pagelink_list_contact = $url_home.$lang."/list-contact.php";
  $pagelink_detail_contact = $url_home.$lang."/detail-contact.php";
  $pagelink_send_contact = $url_home.$lang."/send-contact.php";
  $pagelink_del_contact = $url_home.$lang."/del-contact.php";

  $pagelink_new_project = $url_home.$lang."/new-project.php";
  $pagelink_report_project = $url_home.$lang."/report-project.php";
  $pagelink_support_project = $url_home.$lang."/support-project.php";
  $pagelink_list_supporter = $url_home.$lang."/list-supporter.php";
  $pagelink_list_member = $url_home.$lang."/list-member.php";

  $pagelink_start = $url_home.$lang."/start.php";

  //----------------------------------------
  // DATABASE SETUP
  //----------------------------------------
/*
  $db_host = "localhost";
  $db_user = "cf_demo_user";
  $db_pass = "46fGjxmpThcMeWYu";
  $db_name = "cf_demo";
*/

  $db_host = "aport.cid07kswul69.ap-northeast-1.rds.amazonaws.com";
  $db_name = "aport";
  $db_user = "www";
  $db_pass = "arp2015";

	// DB設定
define("DSN", "mysql:dbname=aport;host=aport.cid07kswul69.ap-northeast-1.rds.amazonaws.com");
define("DB_USER", "www");
define("DB_PASS", "arp2015");

	//Mysql
  function sql_connect($db_host, $db_user, $db_pass, $db_name)
  {
    $result = mysql_connect($db_host, $db_user, $db_pass) or die(mysql_error());
    mysql_select_db($db_name) or die(mysql_error());
    return $result;
  }


  // modelのディレクトリパス
	define("MODEL_PATH", realpath(dirname(__FILE__) . "/../models"));


 //----------------------------------------
  // MULTI LANGUAGE SETUP
  //----------------------------------------

  // データベース多言語対応
	define("DB_MULTI_LANG", "0");	//1・・・多言語対応　0・・・ja固定

 	//データベース用言語コード
 	if(DB_MULTI_LANG){
 		$db_lang=$lang;
 	}else{
 		$db_lang="ja";
 	}

 //----------------------------------------
  // FACEBOOK
  //----------------------------------------

//アプリの情報を定数にに格納
//テスト用
//	define("FACEBOOK_APPID", "1478632652421323");
//	define("FACEBOOK_SECRET", "21699a1605e5d72bcd5b5ab68f24f3d6");
	define("FACEBOOK_APPID", "1641109546171318");
	define("FACEBOOK_SECRET", "f894678ef85b3b44a1d9ec6cfece5938");

//本番用
//	define("FACEBOOK_APPID", "937322679630366");
//	define("FACEBOOK_SECRET", "3f25134f61f1184a7b2bba4e0a702720");


 //----------------------------------------
  // Twitter
  //----------------------------------------

//テスト用
//	define('CONSUMER_KEY', 'pOsbxdCYurXmT5AKHaeC8Lf3j');
//	define('CONSUMER_SECRET', 'LFdZ1mAHobHpO7FDXRwHXQwJkP4CLDckPMjfBVuAl4d9ztQQp7');

//本番用
	define('CONSUMER_KEY', '7G1XD2dZa4GanvTf5DMvknLwd');
	define('CONSUMER_SECRET', 'N79P1QEp5MflDbbnIZwVLcE2bDO69WdE6UXlPDD0Y87Aet3OyU');

  //----------------------------------------
  // Mail SMTP用
  //----------------------------------------
/*
    $smtp_param = array(
        'host'=>'smtp.aport-asahi.com',
        'port'=> 587 ,
        'from'=>$C_SEND_EMAIL,
        'protocol'=>'SMTP_AUTH',
        'user'=>'test@aport-asahi.com',
        'pass' => 'FrkUXHJr',
    );
*/
 //----------------------------------------
  // CONST
  //----------------------------------------

  // 会員のログイン情報保持セッション名
	define("MEMBER_SESSION_NAME", "member_session");

	//logファイルの保存フォルダ
	$today=date("Ymd",time());
	define("LOG_F",DOCUMENT_ROOT."logs/".$today.".txt");

// 会員の仮登録→本登録までの猶予(時間)
	define("MEMBER_REGIST_APPLY_LIMIT_HOUR", 24);

  $C_IMG_SIZE_MAX = 2000000;
  $C_START_MONTH  = "2011-03";
  $C_UPLOAD_IMAGE = "../image-upload/";
  $C_LIST_PROJECT_PAGE_ROW = 5;
  $C_LIST_PROJECT_PAGE_SCALE = 3;
  $C_DETAIL_PROJECT_PAGE_ROW = 9;
  $C_DETAIL_PROJECT_PAGE_SCALE = 3;
  $C_DETAIL_COMMENT_PAGE_ROW = 5;
  $C_DETAIL_COMMENT_PAGE_SCALE = 3;
  $C_DETAIL_MESSAGE_PAGE_ROW = 10;
  $C_DETAIL_MESSAGE_PAGE_SCALE = 3;
  $C_RANK_PROJECT_PAGE_ROW = 10;
  $C_RANK_PROJECT_PAGE_SCALE = 3;
  $C_LIST_NEWS_PAGE_ROW = 10;
  $C_LIST_NEWS_PAGE_SCALE = 3;
  $C_PROJECT_INITIAL = "PJ";
  $C_MEMBER_INITIAL = "MB";
  $C_MEMBER_NO_IMAGE = "../images/member_no_image.jpg";
  $C_PROJECT_NO_IMAGE = "../images/project_no_image.jpg";
  $C_LICENSE_NO_IMAGE = "../images/license_no_image.gif";
  $C_INVEST_AMOUNT_MIN  = "5";

  ?>
