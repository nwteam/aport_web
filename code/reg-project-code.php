<?php

  header('Content-Type: text/html; charset=utf-8');
 require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	$commonDao = new CommonDao();

		//ログインユーザの取得
	$current_user = getMemberSession();

	$smarty->assign("current_user", $current_user);

	if($_POST){	//送信ボタンクリック時
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;

		//---------------- 入力チェック ---------------------------
		//基本事項
		$baseData=CommonChkArray::$actressRegistCheckData;
		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet=check($input_data,$baseData,$err_msg_str);
		//-------------- ここまで -----------------------------------
		//規約
		if(! $input_data[rulesParticipant]){
			$errRet[rulesParticipant]=$err_msg_str[16];
		}

		if(count($errRet)==0){

			$_SESSION['name']=$input_data[name];
			$_SESSION['email']=$input_data[email];
			$_SESSION['fb_url']=$input_data[fb_url];
			$_SESSION['project_name']=$input_data[project_name];
			$_SESSION['category_no']=$input_data[category_no];
			$_SESSION['project_text']=$input_data[project_text];
			$_SESSION['wish_price']=$input_data[wish_price];
			$_SESSION['comment_text']=$input_data[comment_text];

			header("Location:  {$pagelink_reg_project_confirm}");
		}else{
			$errRet[top]=$err_msg_str[99];
		}

	}else{
		$_SESSION[TMP_FUP]="";

	}

	//都道府県リスト
	$smarty->assign("array_area", $array_area);
	$smarty->assign("array_category", $array_category);


	$smarty->assign("err_msg", $errRet);
	$smarty->assign("input_data", $input_data);


?>