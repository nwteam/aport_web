<?php

  header('Content-Type: text/html; charset=utf-8');
 require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
 require '../../libs/Bcrypt.class.php';

  session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();
	$smarty->assign("current_user", $current_user);

	if(isset($_POST[sendForm])){	//送信ボタンクリック時
		$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
		//print_r_with_pre($input_data);
		//---------------- 入力チェック ---------------------------
		//基本事項
		$baseData=CommonChkArray::$memberRegistCheckData;
		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet=check($input_data,$baseData,$err_msg_str);
		//-------------- ここまで -----------------------------------

		//ニックネーム重複チェック
		if(!isset($errRet[nickname])){
			$query  = "select * from sf_member where nickname='%s' and del_flg='0' ";
			$query = sprintf(
				$query,
				mysql_real_escape_string($input_data[nickname])
			);
		//	print $query."<br>";
			$ret = mysql_query("set names utf8");
			$ret = mysql_query($query, $connect);
			$tmp   = mysql_fetch_array($ret);
			if(is_array($tmp)){
				$errRet[nickname]=$err_msg_str[19];
			}
		}

		//メールアドレスチェック
		if(!isset($errRet[email])){
			//重複チェック
			$query  = "select * from sf_member where email='%s' and del_flg='0' ";
			$query = sprintf(
				$query,
				mysql_real_escape_string($input_data[email])
			);
			//print $query."<br>";
			$ret = mysql_query("set names utf8");
			$ret = mysql_query($query, $connect);
			$tmp   = mysql_fetch_array($ret);
			if(is_array($tmp)){
				$errRet[email]=$err_msg_str[20];
			}elseif($input_data[email]!=$input_data[email2]){	//確認と一致
				$errRet[email]=$err_msg_str[15];
			}
		}

		//パスワードチェック
		if(!isset($errRet[password])){
			if($input_data[password]!=$input_data[password2]){	//確認と一致
				$errRet[password]=$err_msg_str[18];
			}
		}
		
		//生年月日チェック
		//選択
		//if($input_data[birth_year]=="" || $input_data[birth_month]=="" ||$input_data[birth_day]=="" ){
		//	$errRet[birthdate]=$err_msg_str[0];
		//}
		//年齢
		//else{
		//	$age = (int) (((int)(date('Ymd'))-(int)($input_data[birth_year].sprintf("%02d",$input_data[birth_month]).sprintf("%02d",$input_data[birth_day])))/10000);
		//	if($age<18){
		//		$errRet[birthdate2]=$err_msg_str[21];
		//	}
		//}
		//規約
		if(! $input_data[rulesParticipant]){
			$errRet[rulesParticipant]=$err_msg_str[17];
		}

		if(count($errRet)==0){
			//登録処理
			  date_default_timezone_set("Asia/Tokyo");

			foreach($baseData[dbstring] as $key=>$val){
				$data[$key]=$input_data[$key];
			}
			//ニックネームを表示名に使う（あなたの名前がニックネームで表示されます）
			if($input_data[ml_flg]=="1"){
				$data[ml_flg]="1";
				$data[member_name]=$data[nickname];
			}else{
				$data[ml_flg]="0";
				$data[member_name]=$data[name_1].$data[name_2];
			}
			//パスワード
			//$data[password]=to_hash($input_data[password]);
			$data[password]=Bcrypt::hash($input_data[password]);
			//認証情報
			$param=substr((md5(date("YmdD His"))), 0, 20);
			$data[param]=$param;
			//期限
			$data[regist_limit]=date("Y-m-d H:i:s",strtotime("+".MEMBER_REGIST_APPLY_LIMIT_HOUR." hours"));
			$data[create_date]=date("Y-m-d H:i:s");
			//言語
			$data[org_lang]=$lang;
			$sql=$commonDao->MakeInsertSQL("sf_member",$data);
			$result = mysql_query("set names utf8");
			$result = mysql_query($sql, $connect);
			$user_no = mysql_insert_id();

			if( $result === true ){

				//メール送信
				$regist_url=$pagelink_reg_member_complete."?param=".$user_no."_".$param;
				$smarty->assign("regist_url", $regist_url);
				$smarty->assign("input_data", $input_data);

				$mail_subject=getMailSubject(3);

				$subject = "[" . $str_site_title . "]".$mail_subject;
				$mailBody = $smarty->fetch($lang."/mail/m-reg-member-entry-complete.tpl");
				//$mailfrom="From:" .mb_encode_mimeheader(MAIL_FROM_NAME) ."<".MAIL_FROM.">";
				send_mail($input_data[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

//$headers = "MIME-Version: 1.0" . "\r\n";
//$headers .= "Content-type:text/html;charset=utf-8" . "\r\n";
//$headers .= 'From: <webmaster@nestwinds.com>' . "\r\n";
//$headers .= 'Cc: fivbak@gmail.com' . "\r\n";
//mail($to,$subject,$mailBody,$headers);

				mysql_close($connect);

				//完了ページへ遷移
				header("Location:  {$pagelink_register_complete}");

			}else{
				$errRet[top]=$err_msg_str[90];
		    }
			mysql_close($connect);

		}else{
			$errRet[top]=$err_msg_str[99];
		}

	}elseif($_GET[type]=="f"){
		$redirect_url=ROOT_URL."ja/".$pagelink_entry_facebook."?lang={$lang}";
		//echo $redirect_url;die;
		//Facebookで新規登録
		$facebookConfig[appId]=FACEBOOK_APPID;
		$facebookConfig[secret]=FACEBOOK_SECRET;
		$facebookConfig[redirect]=$redirect_url;
		$facebook = new Facebook($facebookConfig);
		$authURL = $facebook->getLoginUrl (array ('scope' => 'email'
                                                  , 'redirect_uri' => $facebookConfig[redirect]));

 		header ("Location: " . $authURL);
		exit;
	}elseif($_GET[type]=="t"){
		$redirect_url=$pagelink_register."?type=tr";
		//ツイッターで新規登録
		// request token取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

		$token = $tw->getRequestToken($redirect_url);
		if(! isset($token['oauth_token'])){
		    echo "error: getRequestToken\n";
		    exit;
		}

		// callback.php で使うので session に突っ込む
		$_SESSION['tw_oauth_token']        = $token['oauth_token'];
		$_SESSION['tw_oauth_token_secret'] = $token['oauth_token_secret'];

		// 認証用URL取得してredirect
		$authURL = $tw->getAuthorizeURL($_SESSION['tw_oauth_token']);
		header("Location: " . $authURL);

		exit;
	}elseif($_GET[type]=="tr"){
		// getToken.php でセットした oauth_token と一致するかチェック
		if ($_SESSION['tw_oauth_token'] !== $_REQUEST['oauth_token']) {
		    unset($_SESSION);
			header("Location:  {$pagelink_error}?msgFlg=99");
	  	  	exit;
		 }

		// access token 取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET,
		    $_SESSION['tw_oauth_token'], $_SESSION['tw_oauth_token_secret']);
		$access_token = $tw->getAccessToken($_REQUEST['oauth_verifier']);

		//print_r_with_pre($access_token);

		// Twitter の user_id + screen_name(表示名)
		$user_id     = $access_token['user_id'];
		$screen_name = $access_token['screen_name'];

		//登録済みか確認
		//DB接続
	 	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
		//IDチェック
  		$query  = " select * from sf_member where twitter_id = '{$user_id}' ";
		$ret = mysql_query("set names utf8");
		$ret = mysql_query($query, $connect);
		$data   = mysql_fetch_array($ret);
		if($data[user_no]){
			mysql_close($connect);
			header("Location:  {$pagelink_error}?msgFlg=1");
	  	  	exit;
		}

		$input_data[nickname]=$screen_name;
		$input_data[twitter_id]=$user_id;
		$input_data[rulesParticipant]=1;

		unset($_SESSION['tw_oauth_token']);
		unset($_SESSION['tw_oauth_token_secret']);


	}

	//年月日プルダウン
	$yearArr=makeYearList("1940",0,1);
	$monthArr=makeMonthList(1);
	$dayArr=makeDayList(1);

	$smarty->assign("yearArr", $yearArr);
	$smarty->assign("monthArr", $monthArr);
	$smarty->assign("dayArr", $dayArr);

	$smarty->assign("err_msg", $errRet);
	$smarty->assign("input_data", $input_data);

?>