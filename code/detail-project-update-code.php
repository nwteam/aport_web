<?php
header('Content-Type: text/html; charset=utf-8');
require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);

$current_user = getMemberSession();

$smarty->assign("current_user", $current_user);

//DB接続
$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
$commonDao = new CommonDao();

//$m_hope_url = trim(str_replace('/updates/','',$_SERVER['REQUEST_URI']));
$m_hope_url = trim($_GET['hope_url']);
//$m_project_no = trim($_GET['p_no']);
$p_no_arr=getProjectNoByHopeURL($connect,$m_hope_url);
$m_project_no = $p_no_arr[project_no];

if (!isset($m_project_no) || $m_project_no == "") {
	echo("<script>document.location.href='".$pagelink_index."';</script>");
	exit;
}

//プロジェクト情報の取得
$query_project  = " select a.* , c.*,";
$query_project  .= "  a.status as project_status , ";
$query_project .= "        b.category_name as category_name , ";
$query_project .= "        c.profile_img as profile_img , ";
$query_project .= "        a.cover_img as cover_img , ";
$query_project .= "        a.public_title as title , ";
$query_project .= "        c.full_name as full_name , ";
$query_project .= "        d.* ";
$query_project .= "   from sf_project a ";
$query_project .= "  inner join sf_project_detail d ";
$query_project .= "     on a.no = d.project_no ";
$query_project .= "    and d.lang = 'ja'  ";
$query_project .= "  inner join sf_category b ";
$query_project .= "     on a.category_no = b.no ";
$query_project .= "    and b.del_flg = '0' ";
$query_project .= "  inner join sf_member c ";
$query_project .= "     on a.user_no = c.user_no ";
$query_project .= "    and c.status = '1' ";
$query_project .= "  where 1 ";
$query_project .= "    and a.no = '%s'";
$query_project .= "    and a.del_flg = '0'";
$query_project = sprintf(
	$query_project,
	mysql_real_escape_string($m_project_no)
);
if(isset($_GET[test_view])){
	$test_view_flg=1;
	$smarty->assign("test_view_flg", $test_view_flg);
}else{
	$query_project .= "    and a.status > '0'";
}
//print $query_project."<br>";die;

$result = mysql_query("set names utf8");
$result = mysql_query($query_project, $connect);
$data   = mysql_fetch_array($result);
//print_r_with_pre($data);

  if(!is_array($data)){
		header("Location:  {$pagelink_error}");
  	  	exit;
  }
  if ($data) {
    $m_no                 = $data['no'];
    $m_wish_price         = $data['wish_price'];
    $m_invest_limit       = $data['invest_limit'];
    $m_project_start_date = $data['project_start_date'];
    $m_project_record_date = $data['project_record_date'];
    $m_now_summary        = $data['now_summary'];
    $m_now_supporter      = $data['now_supporter'];
    $m_status             = $data['status'];

    $m_member_image1      = $data['member_image1'];

    //プロジェクト詳細のデコード
    $data['project_text']=htmlspecialchars_decode($data['project_text']);



	//達成率

	if($data['wish_price']>0){
	$data['now_summary']=$data['now_summary']+$data['admin_summary'];
			$data['percent'] = round(($data['now_summary'] / $data['wish_price']) * 100);
	}else{
		if($data['now_summary']>0){
			$data['percent']=100;
		}else{
			$data['percent']=0;
		}
	}
	  	//メーター
	$data['meter']=round(230*($data['percent']/100));
  	if($data['meter']>215){
		$data['meter']=215;

	}
  	$data['meter_left']=$data['meter']-15;
	if($data['meter_left']<0){
		$data['meter_left']=0;
	}

  	//残り時間
  	if($data[status]==1){
  		$date1=strtotime($data['invest_limit']);
	    $now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
		    $data['diff_in_days'] = "-";
		    $data[in_invest_flg]=0;

		}else{
		    $total_secs = ($date1 - $date2);
		    $data['diff_in_days'] = floor($total_secs / 86400);
		    $data[in_invest_flg]=1;
		}
  	}else{
		$data['diff_in_days'] = "-";
		$data[in_invest_flg]=0;
  	}


  }
  $smarty->assign("item", $data);

  //起案者さんの情報の取得
	$query  = " select a.* , d.* from sf_actress as a
				inner join sf_actress_detail as d on a.actress_no=d.actress_no and d.lang='ja'
				where a.actress_no = '%s' ";
	$query = sprintf(
		$query,
		mysql_real_escape_string($data[project_owner])
	);
	//print $query."<br>";
	$ret = mysql_query("set names utf8");
	  $ret = mysql_query($query, $connect);
  	  $actress   = mysql_fetch_array($ret);
	//print_r_with_pre($actress);
  	  $smarty->assign("actress", $actress);

  //支援コースの取得
	$query  = " select *  from sf_prj_present
					where project_no = '%s' and lang='ja'
					order by min";
	$query = sprintf(
		$query,
		mysql_real_escape_string($data[no])
	);
//	print $query."<br>";
	$ret = mysql_query("set names utf8");
	  $ret = mysql_query($query, $connect);
	  $present_list=array();
	  while($p_data = mysql_fetch_array($ret)){
	  	//支援者数の取得
		$query  = " select count(no) as cnt  from sf_invest
					where project_no = '%s' and present_no='%s' and status>0 and status<=91";
		$query = sprintf(
			$query,
			mysql_real_escape_string($data[no]),
			mysql_real_escape_string($p_data[present_no])
		);
	  	$result = mysql_query("set names utf8");
	  	$result = mysql_query($query, $connect);
	  	$data_cnt   = mysql_fetch_array($result);

	  	$p_data[invest_cnt]=$data_cnt[cnt];
	  	$present_list[]=$p_data;

	  }
  	  $smarty->assign("present_list", $present_list);
  //いいね数の取得
  		$like_cnt = 0;
  	  	foreach($array_lang as $lang_cd => $value){
	  		$url_fb=$url_home.$lang_cd."/project_".$data[no].".php";
	  		$url_en=urlencode($url);
	        $json = file_get_contents('http://graph.facebook.com/' . $url_fb ,true);
	        $array = json_decode($json, true);
	       // print_r_with_pre($array);

			if(!isset($array['shares'])){
				$like_cnt += 0;
			}else{
				$like_cnt += $array['shares'];
			}
		}
 //ツイート数の取得
 		$twitter_cnt=0;
 	  	foreach($array_lang as $lang_cd => $value){
			$url_tw=$url_home.$lang_cd."/detail-project.php?p_no=".$data[no];
			$json = @file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url_tw . '');
			$array = json_decode($json,true);
	        //print_r_with_pre($array);

			$twitter_cnt += $array['count'];

 	  	}
	  	$share_cnt=$like_cnt+$twitter_cnt+$data[admin_share];

 	  	$smarty->assign("share_cnt", $share_cnt);


		//print_r_with_pre($array);


 //


  // COMMENT
  $query_comment  = " select a.* , ";
  $query_comment .= "        c.user_no , ";
  $query_comment .= "        c.profile_img, ";
  $query_comment .= "        c.nickname as member_nickname, ";
  $query_comment .= "        c.add_1 ";
  $query_comment .= "   from sf_comment a ";
  $query_comment .= "  inner join sf_member c ";
  $query_comment .= "     on a.member_id = c.user_no ";
  $query_comment .= "    and c.status = '1' ";
  $query_comment .= "  where 1 ";
  $query_comment .= "    and a.project_no = '%s'";
  $query_comment .= "    and a.del_flg = '0'";
  $query_comment .= "  order by a.no desc ";
	$query_comment = sprintf(
		$query_comment,
		mysql_real_escape_string($m_project_no)
	);
	$result_comment_total = mysql_query("set names utf8");
  $result_comment_total = mysql_query($query_comment, $connect);

  $comment_list_total=array();
  while($data_total = mysql_fetch_array($result_comment_total)){
	  $comment_list_total[]=$data_total;
  }
	$total_count = count($comment_list_total);


  // update_msg
  $query_update  = " select a.* ";
  $query_update .= "   from sf_update_msg a ";
  $query_update .= "  where 1 ";
  $query_update .= "    and a.project_no = %d";
  $query_update .= "    and a.del_flg = '0'";
  $query_update .= "  order by a.update_id desc ";
	$query_update = sprintf($query_update,$m_project_no);

	$result_update_total = mysql_query("set names utf8");
  $result_update_total = mysql_query($query_update, $connect);

  $update_list_total=array();
  while($data_total = mysql_fetch_array($result_update_total)){
	  $update_list_total[]=$data_total;
  }
	$total_count_update = count($update_list_total);

  if($_GET[page] && $_GET[page] > 0){
    $page = $_GET[page];
  }else{
    $page = 1;
  }
	$nowpage = isset($_GET['page']) ? intval($_GET['page']) : 0;
	if($_GET['page']==1){
		$limit_start=0;
	}else{
		$limit_start=$nowpage;
	}

  $page_row = 20;
  $page_scale = $C_DETAIL_PROJECT_PAGE_SCALE;

  $query = $query_update." limit ".$limit_start.", ".$page_row;

  $result_update = mysql_query("set names utf8");
  $result_update = mysql_query($query, $connect);

  $update_list=array();
  while($data = mysql_fetch_array($result_update)){
	  $update_list[]=$data;
  }
	$update_cnt=count($update_list);
	for($i=0;$i<$update_cnt;$i++){
		$update_list[$i][update_contents]=htmlspecialchars_decode($update_list[$i][update_contents]);
	}
	//$total_count = count($update_list);

  $total_page  = ceil($total_count_update / $page_row);
	
	if( $nowpage <= 1 ) {
		$nowpage = 1;
	} elseif( $total_page && $nowpage >= $total_page ) {
		$nowpage = $total_page;	
	}

//print_r_with_pre($update_list);die;
$smarty->assign("update_list",$update_list);
//会員情報取得
if ($current_user['user_no']!=""){
	$member_info=getMemberInfo($connect,$current_user['user_no']);
}
mysql_close($connect);

//都道府県リスト
$smarty->assign("array_area", $array_area);
//会員情報
$smarty->assign("member_info", $member_info);
//ページ
$smarty->assign("total_page", $total_page);
$smarty->assign("nowpage", $nowpage);
$smarty->assign("total_count", $total_count);
$smarty->assign("total_count_update", $total_count_update);
$smarty->assign("update_cnt", $update_cnt);
?>