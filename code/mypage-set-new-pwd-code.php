<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	require '../../libs/Bcrypt.class.php';
	session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();

	//ログインチェック
	if(!$current_user){
		header("Location:  {$pagelink_index}");
		exit;
	}

	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	//会員情報をinput_dataに格納
	$member_info=getMemberInfo($connect,$current_user['user_no']);
//print_r_with_pre($input_data);
	$input_data['profile_img']=$member_info['profile_img'];

	if(isset($_POST[new_pwd])){//パスワード情報更新
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;

		//---------------- 入力チェック ---------------------------
		//基本事項
		$baseData=CommonChkArray::$newPwdEditCheckData;
		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet=check($input_data,$baseData,$err_msg_str);
		//-------------- ここまで -----------------------------------
		//パスワードチェック
		if(!isset($errRet[new_password]) && $input_data[new_password]!=""){
			if($input_data[new_password]!=$input_data[password2]){	//確認と一致
				$errRet[new_password]=$err_msg_str[18];
			}
		}

		if(count($errRet)==0){
			//登録処理
			date_default_timezone_set("Asia/Tokyo");

			foreach($baseData[dbstring] as $key=>$val){
				$data[$key]=$input_data[$key];
			}
			//パスワード
			if($input_data[new_password]!=""){
				//$data[password]=to_hash($input_data[new_password]);
				$data[password]=Bcrypt::hash($input_data[new_password]);
				$input_data[password]=$input_data[new_password];
			}else{
				$data[password]=$old_password;
			}

			$where[user_no]=$current_user[user_no];
			$sql=$commonDao->MakeUpdateSQL("sf_member",$data,$where);
			$result = mysql_query("set names utf8");
			$result = mysql_query($sql, $connect);

			if( $result === true ){
				$smarty->assign("finish_msg", CommonMessageArray::$finish_msg[5]);

				//メール送信
				$smarty->assign("C_INQUIRY_EMAIL", $C_INQUIRY_EMAIL);

				$mail_subject=getMailSubject(8);
				$subject = $mail_subject;
				$mailBody = $smarty->fetch($lang."/mail/m-set-new-pwd.tpl");
				send_mail($current_user['email'], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

				header("Location:  {$pagelink_mypage_profile_edit}");
				exit;
			}else{
				$errRet[top]=$err_msg_str[90];
		    }
		}else{
			$errRet[top]=$err_msg_str[99];
		}
	}
	else{
		//会員情報取得
		$query  = "select * from sf_member where user_no='%s' ";
		$query = sprintf(
			$query,
			mysql_real_escape_string($current_user[user_no])
		);
		//print $query."<br>";
		$ret = mysql_query("set names utf8");
		$ret = mysql_query($query, $connect);
		$input_data   = mysql_fetch_array($ret);
		$input_data[org_nickname]   = $input_data[nickname] ;
		$input_data[org_email]   = $input_data[email] ;
		$input_data[tab_no]   = 0;
	}

	//サポートしたプロジェクトを取得
  //件数取得
  $query_total  = " select count(e.project_no) as cnt  ";
  $query_total .= "   from sf_project a ";
  $query_total .= "  inner join sf_project_detail d ";
  $query_total .= "     on a.no = d.project_no ";
  $query_total .= "    and d.lang = 'ja'  ";
  $query_total .= "  inner join sf_actress c ";
  $query_total .= "     on a.project_owner = c.actress_no ";
  $query_total .= "    and c.del_flg = '0' ";
  $query_total .= "  inner join sf_invest e ";
  $query_total .= "     on a.no = e.project_no ";
  $query_total .= "    and e.status > '0' ";
  $query_total .= "    and e.status <= '91' ";
  $query_total .= "    and e.member_id = '%s' ";
  $query_total .= " where 1 = 1 ";
  $query_total .= "   and a.del_flg = '0' ";
  $query_total .= "    and a.status != '0'";
  $query_total .= " group by e.project_no";
	$query_total = sprintf(
		$query_total,
		mysql_real_escape_string($current_user[user_no])
	);
  $result_total = mysql_query($query_total, $connect);
  $data_total = mysql_fetch_array($result_total);
  $total_count = $data_total[cnt];
	//print "cnt=".$total_count."<br>";
  if($_GET[page] && $_GET[page] > 0){
    $page = $_GET[page];
  }else{
    $page = 1;
  }

  $page_row = $C_DETAIL_PROJECT_PAGE_ROW;
  //$page_row=3;
  $page_scale = $C_DETAIL_PROJECT_PAGE_SCALE;

  $total_page  = ceil($total_count / $page_row);
  $from_record = ($page - 1) * $page_row;
  $paging_str = "";
  $paging_before = "";
  $paging_next = "";

  $start_page = ( (ceil( $page / $page_scale ) - 1) * $page_scale ) + 1;
  $end_page = $start_page + $page_scale - 1;
  if ($end_page >= $total_page) $end_page = $total_page;

  if ($page > 1){
    $paging_before .= "<li><a href='".$pagelink_list_project."?page=".($page - 1)."'>&lt;</a></li>";
  } else {
    $paging_before .= "<li><a href='javascript:void(0)'>&lt;</a></li>";
  }

  if ($total_page > 0) {
    for ($i=$start_page;$i<=$end_page;$i++) {
      if ($page != $i){
          $paging_str .= "<li><a href='".$pagelink_list_project."?page=".$i."'>$i</a></li>";
       }else{
          $paging_str .= "<li class='active'><a href='javascript:void(0)'>".$i."</a></li>";
      }
    }
  }

  if ($total_page > $page){
    $paging_next .= "<li><a href='".$pagelink_list_project."?page=".($page + 1)."'>&gt;</a></li>";
  } else {
    $paging_next .= "<li><a href='javascript:void(0)'>&gt;</a></li>";
  }

  //ナビ
	$smarty->assign("total_count",$total_count);
	$smarty->assign("paging_before",$paging_before);
	$smarty->assign("paging_str",$paging_str);
	$smarty->assign("paging_next",$paging_next);

  $query_project  = " select distinct a.* , ";
//  $query_project .= "        b.category_name as category_name , ";
  $query_project .= "        c.actress_no as member_no , ";
  $query_project .= "        c.public_name as member_nickname , ";
  $query_project .= "        c.fresh_flg as fresh_flg , ";
  $query_project .= "        d.* ";
  $query_project .= "   from sf_project a ";
  $query_project .= "  inner join sf_project_detail d ";
  $query_project .= "     on a.no = d.project_no ";
  $query_project .= "    and d.lang = 'ja'  ";
//  $query_project .= "  inner join sf_category b ";
//  $query_project .= "     on a.category_no = b.no ";
//  $query_project .= "    and b.del_flg = '0' ";
  $query_project .= "  inner join sf_actress c ";
  $query_project .= "     on a.project_owner = c.actress_no ";
  $query_project .= "    and c.del_flg = '0' ";
  $query_project .= "  inner join sf_invest e ";
  $query_project .= "     on a.no = e.project_no ";
  $query_project .= "    and e.status > '0' ";
  $query_project .= "    and e.status <= '91' ";
  $query_project .= "    and e.member_id = '%s' ";
  $query_project .= "  where 1 = 1 ";
  $query_project .= "    and a.status != '0'";
  $query_project .= "    and a.del_flg = '0'";
  $query_project .= "  order by e.create_date desc";
	$query_project = sprintf(
		$query_project,
		mysql_real_escape_string($current_user[user_no])
	);
//  $query = $query_project." limit ".$from_record.", ".$page_row;
	$query = $query_project;
  //print $query."<br>";
  $result = mysql_query("set names utf8");
  $result = mysql_query($query, $connect);

  $project_data_list=array();
  while($project_data = mysql_fetch_array($result)){
	//達成率
  	$project_data['percent'] = round(($project_data['now_summary'] / $project_data['wish_price']) * 100) > 100 ? 100 : round(($project_data['now_summary'] / $project_data['wish_price']) * 100);
	//メーター
	$project_data['meter']=round(230*($project_data['percent']/100));
  	if($project_data['meter']>215){
		$project_data['meter']=215;

	}
  	$project_data['meter_left']=$project_data['meter']-15;
	if($project_data['meter_left']<0){
		$project_data['meter_left']=0;
	}

  	//残り時間
    if($project_data[status]==1){
  		$date1=strtotime($project_data['invest_limit']);
	    $now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
		    $project_data['diff_in_days'] = "-";
		    $project_data[in_invest_flg]=0;

		}else{
		    $total_secs = ($date1 - $date2);
		    $project_data['diff_in_days'] = floor($total_secs / 86400);
		    $project_data[in_invest_flg]=1;
		}
  	}else{
		$project_data['diff_in_days'] = "-";
		$project_data[in_invest_flg]=0;
  	}

	  //説明
	  $project_data['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($project_data['project_text'],ENT_QUOTES));

	  //ステータス
	$project_data['status_class']="project";
 	  if($project_data['status']==0){
			$project_data['status_str']=$array_project_status[$project_data['status']][1];
 	 }
	  if($project_data['status']==1){
			$project_data['status_class']="project";
 	 }
	  if($project_data['status']==3){
			$project_data['status_class']="project successful";
 	 }
	  if($project_data['status']==9){
			$project_data['status_class']="project fail";
 	 }
 	 $project_data_list[]=$project_data;
  }
  //print_r_with_pre($project_data_list);
  $smarty->assign("project_data_list", $project_data_list);

  //print_r_with_pre($project_data_list);

  // COMMENT
  $query_comment  = " select a.* , p.* ,p.no as p_no";
  $query_comment .= "   from sf_comment a ";
  $query_comment .= "  inner join sf_project p ";
  $query_comment .= "     on a.project_no = p.no ";
  $query_comment .= "  where 1 ";
  $query_comment .= "    and a.member_id = '%s'";
  $query_comment .= "    and a.del_flg = '0'";
  $query_comment .= "  order by a.no desc ";
	$query_comment = sprintf(
		$query_comment,
		mysql_real_escape_string($current_user[user_no])
	);
	$result_comment_total = mysql_query("set names utf8");
  $result_comment_total = mysql_query($query_comment, $connect);

  $comment_list_total=array();
  while($data_total = mysql_fetch_array($result_comment_total)){
	  $comment_list_total[]=$data_total;
  }
	$total_count = count($comment_list_total);

  if($_GET[page] && $_GET[page] > 0){
    $page = $_GET[page];
  }else{
    $page = 1;
  }
	$nowpage = isset($_GET['page']) ? intval($_GET['page']) : 0;
	if($_GET['page']==1){
		$limit_start=0;
	}else{
		$limit_start=$nowpage;
	}

  $page_row = 20;
  $page_scale = $C_DETAIL_PROJECT_PAGE_SCALE;

  $query = $query_comment." limit ".$limit_start.", ".$page_row;

  $result_comment = mysql_query("set names utf8");
  $result_comment = mysql_query($query, $connect);

  $comment_list=array();
  while($data = mysql_fetch_array($result_comment)){
	  $comment_list[]=$data;
  }
	//$total_count = count($comment_list);

  $total_page  = ceil($total_count / $page_row);
	
	if( $nowpage <= 1 ) {
		$nowpage = 1;
	} elseif( $total_page && $nowpage >= $total_page ) {
		$nowpage = $total_page;	
	}

//print_r_with_pre($comment_list);
$smarty->assign("comment_list",$comment_list);

	//会員情報をinput_dataに格納
	$member_info=getMemberInfo($connect,$current_user['user_no']);
	$input_data['profile_img']=$member_info['profile_img'];

	//年月日プルダウン
	$yearArr=makeYearList("1940",0,1);
	$monthArr=makeMonthList(1);
	$dayArr=makeDayList(1);

	$smarty->assign("yearArr", $yearArr);
	$smarty->assign("monthArr", $monthArr);
	$smarty->assign("dayArr", $dayArr);

	//ログイン情報
	$smarty->assign("current_user", $current_user);
	//都道府県リスト
	$smarty->assign("array_area", $array_area);
	//エラー情報
	$smarty->assign("err_msg", $errRet);
	//入力情報
	$smarty->assign("input_data", $input_data);

	mysql_close($connect);

?>