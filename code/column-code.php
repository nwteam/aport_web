<?php
  header('Content-Type: text/html; charset=utf-8');

	$current_user = getMemberSession();

	$smarty->assign("current_user", $current_user);

	//DB接続
 	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	//検索条件

	$m_page_list       = get_check(trim(preg_change($_POST['hid_page_list'])));
	$m_category        = get_check(trim(preg_change($_POST['cmb_category'])));

	$smarty->assign("m_page_list",$m_page_list);
	$smarty->assign("m_category",$m_category);
	$smarty->assign("array_category",$array_category_smarty);	//カテゴリー選択配列

	if(isset($_GET['c_no'])){
		$m_category = get_check(preg_change(trim($_GET['c_no'])));
	}
	if(isset($_GET['id'])){
		$m_id = get_check(preg_change(trim($_GET['id'])));
	}
	if(isset($_GET['tag'])){
		$m_tag = get_check(preg_change(trim($_GET['tag'])));
	}


  // コラム
  $query_project  = " select a.* , ";
  $query_project .= "        b.cat_name as category_name ";
  $query_project .= "   from sf_column a ";
  $query_project .= "  inner join sf_column_category b ";
  $query_project .= "     on a.column_category_no = b.cat_no ";
  $query_project .= "    and b.del_flg = '0' ";
  $query_project .= " where 1 ";
  $query_project .= "   and a.del_flg = '0' ";

  $query_total  = " select count(*) as cnt  ";
  $query_total .= "   from sf_column a ";
  $query_total .= " where 1  ";
  $query_total .= "   and a.del_flg = '0' ";

  // カテゴリー
  if (isset($m_category) && $m_category != "") {
    $query_project .= " and a.column_category_no = '".$m_category."'";
    $query_total .= " and a.column_category_no = '".$m_category."'";
  }
  // ID
  if (isset($m_id) && $m_id != "") {
    $query_project .= " and a.id = '".$m_id."'";
    $query_total .= " and a.id = '".$m_id."'";
  }
  // TAG
  if (isset($m_tag) && $m_tag != "") {
    $query_project .= " and FIND_IN_SET(".$m_tag.",tag_list)";
    $query_total .= " and FIND_IN_SET(".$m_tag.",tag_list)";
  }

  $result_total = mysql_query($query_total, $connect);
  $data_total = mysql_fetch_array($result_total);
  $total_count = $data_total[cnt];

  if($_GET[page] && $_GET[page] > 0){
    $page = get_check(preg_change($_GET[page]));
  }else{
    $page = 1;
  }

  $page_row = 4;
  $page_scale = $C_DETAIL_PROJECT_PAGE_SCALE;

  $total_page  = ceil($total_count / $page_row);

	$nowpage = isset($_GET['page']) ? intval($_GET['page']) : 1;
	if( $nowpage <= 1 ) {
		$nowpage = 1;
	} elseif( $total_page && $nowpage >= $total_page ) {
		$nowpage = $total_page;	
	}
	
	$firstrow = $page_row * ($nowpage-1);

  $from_record = ($page - 1) * $page_row;
  $paging_str = "";
  $paging_before = "";
  $paging_next = "";

  $start_page = ( (ceil( $page / $page_scale ) - 1) * $page_scale ) + 1;
  $end_page = $start_page + $page_scale - 1;
  if ($end_page >= $total_page) $end_page = $total_page;

  if ($page > 1){
    $paging_before .= "<li><a href='".$pagelink_list_project."?page=".($page - 1)."'>&lt;</a></li>";
  } else {
    $paging_before .= "<li><a href='javascript:void(0)'>&lt;</a></li>";
  }

  if ($total_page > 0) {
    for ($i=$start_page;$i<=$end_page;$i++) {
      if ($page != $i){
          $paging_str .= "<li><a href='".$pagelink_list_project."?page=".$i."'>$i</a></li>";
       }else{
          $paging_str .= "<li class='page active'><a href='column.php?page=1'>".$i."</a></li>";
      }
    }
  }

  if ($total_page > $page){
    $paging_next .= "<li><a href='".$pagelink_list_project."?page=".($page + 1)."'>&gt;</a></li>";
  } else {
    $paging_next .= "<li><a href='javascript:void(0)'>&gt;</a></li>";
  }

  //ナビ
	$smarty->assign("total_count",$total_count);
	$smarty->assign("paging_before",$paging_before);
	$smarty->assign("paging_str",$paging_str);
	$smarty->assign("paging_next",$paging_next);

  if($agent==""){
	$query = $query_project." limit ".$firstrow.", ".$page_row;

  }else{
  	$query = $query_project;
  }
  //print $query."<br>";
  $result = mysql_query("set names utf8");
  $result = mysql_query($query, $connect);
  $result_list=array();

  $result_recent = mysql_query("set names utf8");
  $result_recent = mysql_query($query_project_recent, $connect);
  $result_recent=array();

  while($data = mysql_fetch_array($result)){
     //category_class
	if($data['category_name']=='HOWTO'){
		$data['category_class']="category-howto";
	}elseif($data['category_name']=='プロジェクト'){
		$data['category_class']="category-project";
	}elseif($data['category_name']=='お知らせ'){
		$data['category_class']="category-info";
	}else{
		$data['category_class']="category-info";
	}
	//コラム内容
	$data['contents']=str_replace("<br />", " ", htmlspecialchars_decode($data['contents'],ENT_QUOTES));

	//コラム内容-Limit
	$c_len=mb_strlen($data['contents'],'utf-8');

	//コラム内容-Limit
	if($c_len<=125){
		$data['contents_limit']=$data['contents'];
	}else{
		$data['contents_limit']=mb_substr($data['contents'],0,125,'utf-8')."...";
	}

	$result_list[]=$data;
  }

  $smarty->assign("result_list", $result_list);
  $data_cnt=count($result_list);
  $smarty->assign("data_cnt",$data_cnt);

//Recent Column Limit 4
// コラム(RECENT ENTRIES)
$query_column_recent  = " select a.* , ";
$query_column_recent .= "        b.cat_name as category_name ";
$query_column_recent .= "   from sf_column a ";
$query_column_recent .= "  inner join sf_column_category b ";
$query_column_recent .= "     on a.column_category_no = b.cat_no ";
$query_column_recent .= "    and b.del_flg = '0' ";
$query_column_recent .= " where 1 ";
$query_column_recent .= "   and a.del_flg = '0' ";
$query_column_recent .= "   order by a.update_time desc ";
$query_column_recent .= "   Limit 4 ";

$column_recent_list = array();
$column_recent = mysql_query($query_column_recent);
while($row = mysql_fetch_assoc($column_recent)){
	$column_recent_list[] = $row;
}
$smarty->assign("column_recent_list", $column_recent_list);

//分类
$category = mysql_query("SELECT * FROM sf_column_category where del_flg = '0'");
$categorylist = array();
while($row = mysql_fetch_assoc($category)){
	$categorylist[] = $row;
}
$smarty->assign("categorylist", $categorylist);

//TAGS
$tag = mysql_query("SELECT * FROM sf_column_tag where del_flg = '0'");
$taglist = array();
while($row = mysql_fetch_assoc($tag)){
	$taglist[] = $row;
}
$smarty->assign("taglist", $taglist);

//分页代码
$smarty->assign("total_page", $total_page);
$smarty->assign("nowpage", $nowpage);

mysql_close($connect);


?>