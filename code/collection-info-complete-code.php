<?php
header('Content-Type: text/html; charset=utf-8');
require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);

$current_user = getMemberSession();

$smarty->assign("current_user", $current_user);

//DB接続
$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
$commonDao = new CommonDao();

$m_project_no = $_SESSION["project_no"];
$payment_method = $_SESSION["payment_method"];
/*
$receipt_no=$_GET['rep_no'];
$payment_term=$_GET['pt'];
$commtent_time=$_GET['ct'];
*/
$receipt_no=get_check(preg_change($_GET['rep_no']));
$payment_term=get_check(preg_change($_GET['pt']));
$commtent_time=preg_change($_GET['ct']);

$commtent_time = urldecode($commtent_time);

//決済情報(金額)
$invest_amount = $_SESSION["invest_amount"];
$amount_plus = $_SESSION["amount_plus"];
$total_amount = $invest_amount + $amount_plus;
if($payment_method=="Credit"){
	//決済情報(credit)
	$card_no = $_SESSION["card_no"];
	$expdate_month = $_SESSION["expdate_month"];
	$expdate_year = $_SESSION["expdate_year"];
	$hld_name = $_SESSION["hld_name"];
	$security_code = $_SESSION["security_code"];
}elseif($payment_method=="cvs"){
	//決済情報(cvs)
	$convenience = $_SESSION["convenience"];
	$ks_name_1 = $_SESSION["ks_name_1"];
	$ks_name_2 = $_SESSION["ks_name_2"];
	$customername=$ks_name_1.$ks_name_2;
	$ks_name_kana_1 = $_SESSION["ks_name_kana_1"];
	$ks_name_kana_2 = $_SESSION["ks_name_kana_2"];
	$customerkana=$ks_name_kana_1.$ks_name_kana_2;
	$ks_tel = $_SESSION["ks_tel"];
}elseif($payment_method=="payeasy"){
	//決済情報(payeasy)
	$ks_name_1 = $_SESSION["ks_name_1"];
	$ks_name_2 = $_SESSION["ks_name_2"];
	$customername=$ks_name_1.$ks_name_2;
	$ks_name_kana_1 = $_SESSION["ks_name_kana_1"];
	$ks_name_kana_2 = $_SESSION["ks_name_kana_2"];
	$customerkana=$ks_name_kana_1.$ks_name_kana_2;
	$ks_tel = $_SESSION["ks_tel"];
}else{
	//決済情報(credit)
	$card_no = "";
	$expdate_month = "";
	$expdate_year = "";
	$hld_name = "";
	$security_code = "";
	//決済情報(cvs,payeasy)
	$convenience = "";
	$ks_name_1 = "";
	$ks_name_2 = "";
	$customername="";
	$ks_name_kana_1 = "";
	$ks_name_kana_2 = "";
	$customerkana="";
	$ks_tel = "";
}

if (!isset($m_project_no) || $m_project_no == "") {
	echo("<script>document.location.href='".$pagelink_index."';</script>");
	exit;
}

//プロジェクト情報の取得
$query_project  = " select a.* , ";
$query_project  .= "  a.status as project_status , ";
$query_project .= "        b.category_name as category_name , ";
$query_project .= "        c.profile_img as profile_img , ";
$query_project .= "        a.cover_img as cover_img , ";
$query_project .= "        a.public_title as title , ";
$query_project .= "        c.full_name as full_name , ";
$query_project .= "        d.* ";
$query_project .= "   from sf_project a ";
$query_project .= "  inner join sf_project_detail d ";
$query_project .= "     on a.no = d.project_no ";
$query_project .= "    and d.lang = 'ja'  ";
$query_project .= "  inner join sf_category b ";
$query_project .= "     on a.category_no = b.no ";
$query_project .= "    and b.del_flg = '0' ";
$query_project .= "  inner join sf_member c ";
$query_project .= "     on a.user_no = c.user_no ";
$query_project .= "    and c.status = '1' ";
$query_project .= "  where 1 ";
$query_project .= "    and a.no = '%s'";
$query_project .= "    and a.del_flg = '0'";
$query_project .= "    and a.status > '0'";
$query_project = sprintf(
$query_project,
mysql_real_escape_string($m_project_no)
);
//print $query_project."<br>";die;

$result = mysql_query("set names utf8");
$result = mysql_query($query_project, $connect);
$data   = mysql_fetch_array($result);
//print_r_with_pre($data);

  if(!is_array($data)){
		header("Location:  {$pagelink_error}");
  	  	exit;
  }
  $smarty->assign("item", $data);
	$c_invest_limit=substr(str_replace("-","",$data[invest_limit]),0,8);
	if (substr($payment_term,0,8)>$c_invest_limit){
		$payment_term=$c_invest_limit."235959";
	}
  //起案者さんの情報の取得
	$query  = " select a.* , d.* from sf_actress as a
				inner join sf_actress_detail as d on a.actress_no=d.actress_no and d.lang='ja'
				where a.actress_no = '%s' ";
	$query = sprintf(
	$query,
	mysql_real_escape_string($data[project_owner])
	);
	//print $query."<br>";
	$ret = mysql_query("set names utf8");
	$ret = mysql_query($query, $connect);
	$actress   = mysql_fetch_array($ret);
	//print_r_with_pre($actress);
	$smarty->assign("actress", $actress);

//print_r_with_pre($comment_list);
$smarty->assign("comment_list",$comment_list);
//会員情報取得
if ($current_user['user_no']!=""){
	$member_info=getMemberInfo($connect,$current_user['user_no']);
}

mysql_close($connect);

if(isset($_POST[sbm_update])){	//送信ボタンクリック時
		$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

		$supporter_comment_dt=trim(preg_change($_POST['supporter_comment']));
		$supporter_comment_dt=htmlspecialchars(addslashes($supporter_comment_dt));

		//登録処理
		date_default_timezone_set("Asia/Tokyo");
		//$data[regist_limit]=date("Y-m-d H:i:s",strtotime("+".MEMBER_REGIST_APPLY_LIMIT_HOUR." hours"));
		//$insert_data[project_no]=$_SESSION["project_no"];
		//$insert_data[member_id]=$current_user['user_no'];
		//$insert_data[comment]=$supporter_comment_dt;
		//$insert_data[create_date]=date("Y-m-d H:i:s");

		$data_c[comment]=$supporter_comment_dt;

		$where_c[project_no]=$_SESSION["project_no"];
		$where_c[member_id]=$current_user[user_no];
		$where_c[create_date]=$commtent_time;
		$sql=$commonDao->MakeUpdateSQL("sf_comment",$data_c,$where_c);
		//$sql=$commonDao->MakeInsertSQL("sf_comment",$insert_data);

		$result = mysql_query("set names utf8");
		$result = mysql_query($sql, $connect);
		//$user_no = mysql_insert_id();
		
		if( $result === true ){
			$m_hope_urL_arr=getHopeURLByProjectNo($connect,$m_project_no);
			$m_hope_urL=$m_hope_urL_arr[hope_url];
			//完了ページへ遷移
			header("Location:  {$pagelink_detail_project}{$m_hope_urL}");
		}else{
			$errRet[top]=$err_msg_str[90];
		}
		mysql_close($connect);

}

//都道府県リスト
$smarty->assign("array_area", $array_area);
$smarty->assign("array_cvs", $array_cvs);
$smarty->assign("array_week", $array_week);
//会員情報
$smarty->assign("member_info", $member_info);
//支払方法
$smarty->assign("payment_method", $payment_method);
//決済情報(金額)
$smarty->assign("total_amount", $total_amount);
//決済情報(credit)
$smarty->assign("card_no", $card_no);
$smarty->assign("expdate_month", $expdate_month);
$smarty->assign("expdate_year", $expdate_year);
$smarty->assign("hld_name", $hld_name);
$smarty->assign("security_code", $security_code);
//決済情報(cvs,payeasy)
$smarty->assign("convenience", $convenience);
$smarty->assign("ks_name_1", $ks_name_1);
$smarty->assign("ks_name_2", $ks_name_2);
$smarty->assign("ks_name_kana_1", $ks_name_kana_1);
$smarty->assign("ks_name_kana_2", $ks_name_kana_2);
$smarty->assign("ks_tel", $ks_tel);
$smarty->assign("receipt_no", $receipt_no);
$smarty->assign("payment_term", $payment_term);
//ページ
$smarty->assign("total_page", $total_page);
$smarty->assign("nowpage", $nowpage);
$smarty->assign("total_count", $total_count);
$smarty->assign("m_project_no", $m_project_no);

?>