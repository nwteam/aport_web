<?php

  header('Content-Type: text/html; charset=utf-8');

	$current_user = getMemberSession();

	$smarty->assign("current_user", $current_user);

	//DB接続
 	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

 	//開始日の取得
 	$type=$_GET['type'];
 	if($type=="w"){	//週間ランキング
 		$now = time();
		// 週の始め（月曜日）
		$start_date=date('Y-m-d', strtotime('-1 Sunday', $now));
 	}elseif($type=="m"){
 		$start_date = date("Y-m-1");
 	}else{
 		$type="y";
 		$start_date=date("Y-1-1");
 	}
	$smarty->assign("type",$type);


  $query_project  = " select a.* , ";
  $query_project .= "        c.actress_no as member_no , ";
  $query_project .= "        c.public_name as member_nickname , ";
  $query_project .= "        c.fresh_flg as fresh_flg , ";
  $query_project .= "        d.* ";
  $query_project .= "   from sf_project a ";
  $query_project .= "  inner join sf_project_detail d ";
  $query_project .= "     on a.no = d.project_no ";
  $query_project .= "    and d.lang = '{$db_lang}'  ";
  $query_project .= "  inner join sf_actress c ";
  $query_project .= "     on a.project_owner = c.actress_no ";
  $query_project .= "    and c.del_flg = '0' ";
  $query_project .= " where 1 = 1 ";
  $query_project .= "   and a.del_flg = '0' ";

  $query_total  = " select count(*) as cnt  ";
  $query_total .= "   from sf_project a ";
  $query_total .= "  inner join sf_project_detail d ";
  $query_total .= "     on a.no = d.project_no ";
  $query_total .= "    and d.lang = '{$db_lang}'  ";
  $query_total .= "  inner join sf_actress c ";
  $query_total .= "     on a.project_owner = c.actress_no ";
  $query_total .= "    and c.del_flg = '0' ";
  $query_total .= " where 1 = 1 ";
  $query_total .= "   and a.del_flg = '0' ";


  // 募集中
   	$query_project .= " and ((a.status = '1' and a.invest_limit >= str_to_date(now(),'%Y-%m-%d')) ";
    $query_total .= " and ((a.status = '1' and a.invest_limit >= str_to_date(now(),'%Y-%m-%d')) ";


  // 成立
    $query_project .= " or (a.status = '3' and a.project_success_date >= '{$start_date}')) ";
    $query_total .= " or (a.status = '3'  and a.project_success_date >= '{$start_date}')) ";


   // 支援金額順
    $query_project .= " order by a.now_summary+a.admin_summary desc ,a.now_summary desc,a.now_supporter desc,a.now_like+a.now_twitter+a.admin_share desc,a.project_record_date desc";

    $result_total = mysql_query($query_total, $connect);
  $data_total = mysql_fetch_array($result_total);
  $total_count = $data_total[cnt];


//  if($agent==""){
//	$query = $query_project." limit ".$from_record.", ".$page_row;
//  }else{
  	$query = $query_project;
//  }
 // print $query."<br>";
  $result = mysql_query("set names utf8");
  $result = mysql_query($query, $connect);
  $result_list=array();

  //表示する順位の指定
  if($_GET[page] && $_GET[page] > 0){
    $page = $_GET[page];
  }else{
    $page = 1;
  }

  $page_row = 10;
  //$page_row=3;
  $page_scale = $C_DETAIL_PROJECT_PAGE_SCALE;
  $from_record = ($page - 1) * $page_row;

  $rank_num=0;
  $pre_summary=-1;
  while($data = mysql_fetch_array($result)){
	//画像の取得
	  $query  = " select * from general_images where image_type = 'PROFILE_IMG' and parent_no = {$data[member_no]} ";
  	  //print $query."<br>";
	  $ret = mysql_query("set names utf8");
	  $ret = mysql_query($query, $connect);
  	  $image   = mysql_fetch_array($ret);
	  $data[image]=$image[file_name];
  	//youtube
	  $query  = " select * from general_images where image_type = 'PROJECT_URL' and parent_no = {$data[no]} ";
	  //print $query."<br>";
	  $ret = mysql_query("set names utf8");
	  $ret = mysql_query($query, $connect);
  	  $image   = mysql_fetch_array($ret);
	$m_youtube_id=getParamdata($data['url'],"v");
	$data['youtube_id']=$m_youtube_id;

	//達成率
	$data['now_summary']=$data['now_summary']+$data['admin_summary'];
  	$data['percent'] = round(($data['now_summary'] / $data['wish_price']) * 100) > 100 ? 100 : round(($data['now_summary'] / $data['wish_price']) * 100);
	//メーター
	$data['meter']=round(490*($data['percent']/100));
  	if($data['meter']>475){
		$data['meter']=475;

	}
  	$data['meter_left']=$data['meter']-15;
	if($data['meter_left']<0){
		$data['meter_left']=0;
	}
  	//残り時間
    if($data[status]==1){
  		$date1=strtotime($data['invest_limit']);
	    $now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
		    $data['diff_in_days'] = "-";
		    $data[in_invest_flg]=0;

		}else{
		    $total_secs = ($date1 - $date2);
		    $data['diff_in_days'] = floor($total_secs / 86400);
		    $data[in_invest_flg]=1;
		}
  	}else{
		$data['diff_in_days'] = "-";
		$data[in_invest_flg]=0;
  	}
     //説明
	  $data['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($data['project_text'],ENT_QUOTES));
	  //ステータス
	  if($data['status']==0){
			$data['status_str']=$array_project_status[$data['status']][1];
 	 }
	  if($data['status']==1){
			$data['status_class']="rank";
 	 }
	  if($data['status']==3){
			$data['status_class']="rank successful";
 	 }

 	 //ランキング
 	 $rank_num +=1;

	 if($pre_summary==$data[now_summary]){
	 	$data[rank]=$pre_rank;
	 }else{
	 	$data[rank]=$rank_num;
	 }

	 $pre_summary=$data[now_summary];
 	 $pre_rank=$data[rank];

 	 if($agent==""){
 	 	if($data[rank]>=($from_record+1) && $data[rank]<=($from_record+10)){
	 		$result_list[]=$data;
  	 	}

 	 }else{
	 	$result_list[]=$data;
 	 }
  }
  $smarty->assign("result_list", $result_list);
  $data_cnt=count($result_list);
  $smarty->assign("data_cnt",$data_cnt);

  //Pager
  $total_page  = ceil($pre_rank / $page_row);

  $paging_str = "";
  $paging_before = "";
  $paging_next = "";

  $start_page = ( (ceil( $page / $page_scale ) - 1) * $page_scale ) + 1;
  $end_page = $start_page + $page_scale - 1;
  if ($end_page >= $total_page) $end_page = $total_page;

  if ($page > 1){
    $paging_before .= "<li><a href='".$pagelink_list_ranking."?type=".$type."&page=".($page - 1)."'>&lt;</a></li>";
  } else {
    $paging_before .= "<li><a href='javascript:void(0)'>&lt;</a></li>";
  }

  if ($total_page > 0) {
    for ($i=$start_page;$i<=$end_page;$i++) {
      if ($page != $i){
          $paging_str .= "<li><a href='".$pagelink_list_ranking."?type=".$type."&page=".$i."'>$i</a></li>";
       }else{
          $paging_str .= "<li class='active'><a href='javascript:void(0)'>".$i."</a></li>";
      }
    }
  }

  if ($total_page > $page){
    $paging_next .= "<li><a href='".$pagelink_list_ranking."?type=".$type."&page=".($page + 1)."'>&gt;</a></li>";
  } else {
    $paging_next .= "<li><a href='javascript:void(0)'>&gt;</a></li>";
  }

  //ナビ
	$smarty->assign("total_count",$total_count);
	$smarty->assign("paging_before",$paging_before);
	$smarty->assign("paging_str",$paging_str);
	$smarty->assign("paging_next",$paging_next);
	$smarty->assign("from_record",$from_record);

	mysql_close($connect);

  ?>