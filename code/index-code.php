<?php
header('Content-Type: text/html; charset=utf-8');

$current_user = getMemberSession();

//DB接続
$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

// 新着プロジェクト
//表示数
if($agent==""){
	$limit_str="  limit 14" ;
}

$query_project  = " select a.* , ";
$query_project .= "        b.category_name as category_name , ";
$query_project .= "        c.actress_no as member_no , ";
$query_project .= "        c.public_name as member_nickname , ";
$query_project .= "        c.fresh_flg as fresh_flg , ";
$query_project .= "        d.* , ms.view_flg ";
$query_project .= "   from sf_project a ";
$query_project .= "  inner join sf_project_detail d ";
$query_project .= "     on a.no = d.project_no ";
$query_project .= "    and d.lang = '{$db_lang}'  ";
$query_project .= "  inner join sf_category b ";
$query_project .= "     on a.category_no = b.no ";
$query_project .= "    and b.del_flg = '0' ";
$query_project .= "  inner join sf_actress c ";
$query_project .= "     on a.project_owner = c.actress_no ";
$query_project .= "    and c.del_flg = '0' ";
$query_project .= "  inner join mst_slide ms ";
$query_project .= "     on a.no = ms.link ";
$query_project .= "  where 1 ";
$query_project .= "    and (a.status = '1' or a.status = '3')";
$query_project .= "    and a.chk_status = '3' ";
$query_project .= "    and UNIX_TIMESTAMP(a.project_record_date) <= ".strtotime(date("Y-m-d",time())."23:59:59");
$query_project .= "    and a.del_flg = '0'";
$query_project .= "    and current_date <= a.invest_limit";
//$query_project .= "  order by a.project_record_date desc, a.no desc ";
$query_project .= "  order by ms.v_order asc ";
$query_project .= $limit_str;

//print $query_project."<br>";
$result = mysql_query("set names utf8");
$result = mysql_query($query_project, $connect);

$new_data_list=array();
while($new_data = mysql_fetch_array($result)){
	//達成率
	if($new_data['wish_price']>0){
			$new_data['now_summary']=$new_data['now_summary']+$new_data['admin_summary'];
			$new_data['percent'] = round(($new_data['now_summary'] / $new_data['wish_price']) * 100);
	}else{
		if($new_data['now_summary']>0){
			$new_data['percent']=100;
		}else{
			$new_data['percent']=0;
		}
	}

	//残り時間
	$date1=strtotime($new_data['invest_limit']);
	$now_date = date("Y-m-d");
	$date2=strtotime($now_date);
	$total_secs = ($date1 - $date2);
	$new_data['diff_in_days'] = floor($total_secs / 86400);

	//説明
	//$new_data['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($new_data['project_text'],ENT_QUOTES));
	$new_data_list[]=$new_data;
}
//print_r_with_pre($new_data_list);
$smarty->assign("new_data_list", $new_data_list);
//件数
$new_data_cnt=count($new_data_list);
$smarty->assign("new_data_cnt",new_data_cnt);

//あとひと押しのプロジェクト
$query_project  = " select a.* , ";
$query_project .= "        b.category_name as category_name , ";
$query_project .= "        c.actress_no as member_no , ";
$query_project .= "        c.public_name as member_nickname , ";
$query_project .= "        d.* ";
$query_project .= "   from sf_project a ";
$query_project .= "  inner join sf_project_detail d ";
$query_project .= "     on a.no = d.project_no ";
$query_project .= "    and d.lang = '{$db_lang}'  ";
$query_project .= "  inner join sf_category b ";
$query_project .= "     on a.category_no = b.no ";
$query_project .= "    and b.del_flg = '0' ";
$query_project .= "  inner join sf_actress c ";
$query_project .= "     on a.project_owner = c.actress_no ";
$query_project .= "    and c.del_flg = '0' ";
$query_project .= "  where 1 ";
$query_project .= "    and a.status = '1' ";
$query_project .= "    and a.del_flg = '0'";
$query_project .= "    and current_date <= a.invest_limit";
$query_project .= "  order by invest_limit asc";
$query_project .= "  limit 4 ";

$result = mysql_query("set names utf8");
$result = mysql_query($query_project, $connect);

$comp_data_list=array();
while($comp_data = mysql_fetch_array($result)){
	//達成率
	if($comp_data['wish_price']>0){
			$comp_data['now_summary']=$comp_data['now_summary']+$comp_data['admin_summary'];
		$comp_data['percent'] = round(($comp_data['now_summary'] / $comp_data['wish_price']) * 100) > 100 ? 100 : round(($comp_data['now_summary'] / $comp_data['wish_price']) * 100);
	}else{
		if($comp_data['now_summary']>0){
			$comp_data['percent']=100;
		}else{
			$comp_data['percent']=0;
		}
	}
	//残り時間
	if (isset($comp_data['project_record_date']) && $comp_data['project_record_date'] != "") {

		$l_start_attr = explode(" ", $comp_data['project_record_date']);
		$l_start_attr_date = explode("-", $l_start_attr[0]);
		$l_start_attr_time = explode(":", $l_start_attr[1]);

		$m_p_date = mktime($l_start_attr_time[0],$l_start_attr_time[1],$l_start_attr_time[2],
									$l_start_attr_date[1],$l_start_attr_date[2],$l_start_attr_date[0]);

		$date1 = strtotime("+".$comp_data['invest_limit']." days", $m_p_date);
		$date2 = mktime();
		$now_date = date("Y-m-d h:m:s");
		$total_secs = ($date1 - $date2);
		$comp_data['diff_in_days'] = floor($total_secs / 86400);
	}
	//説明
	//$comp_data['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($comp_data['project_text'],ENT_QUOTES));
	$comp_data_list[]=$comp_data;
}
// print_r_with_pre($comp_data_list);

$comp_data_cnt=count($comp_data_list);
if($comp_data_cnt>3 && $comp_data_cnt<6){
	$comp_data_cnt=3;
}
$smarty->assign("comp_data_list", $comp_data_list);
$smarty->assign("comp_data_cnt",$comp_data_cnt);

$smarty->assign("current_user", $current_user);
  ?>