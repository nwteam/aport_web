<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();

	//ログインチェック
	if(!$current_user){
		header("Location:  {$pagelink_index}");
		exit;
	}

	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	//会員情報取得
	$member_info=getMemberInfo($connect,$current_user['user_no']);
	$input_data['profile_img']=$member_info['profile_img'];
	$input_data['member_name']=$member_info['member_name'];
	$input_data['add_1']=$member_info['add_1'];
	$input_data['hp_url']=$member_info['hp_url'];
	$input_data['profile']=$member_info['profile'];

  // mail_msg
  $query  = " select a.*,m.member_name,m.email ";
  $query .= "   from sf_message a ";
  $query .= "  inner join sf_member m ";
  $query .= "     on a.from_user_id = m.user_no ";
  $query .= "    and m.del_flg = '0'";
  $query .= "  where 1 ";
  $query .= "    and a.to_user_id = %d";
  $query .= "    and a.del_flg = '0'";
  $query .= "  order by a.no desc ";
	$query = sprintf($query,$current_user['user_no']);

  $result = mysql_query("set names utf8");
  $result = mysql_query($query, $connect);

  $mail_list=array();
  while($data = mysql_fetch_array($result)){
		if($data[to_user_id]==$current_user['user_no']){}else{header("Location:  {$pagelink_error}");exit;}
	  $mail_list[]=$data;
  }

	//print_r_with_pre($mail_list);

	mysql_close($connect);

	//ログイン情報
	$smarty->assign("current_user", $current_user);
	//都道府県リスト
	$smarty->assign("array_area", $array_area);
	//エラー情報
	$smarty->assign("err_msg", $errRet);
	//入力情報
	$smarty->assign("input_data", $input_data);
	//COMMENT情報
	$smarty->assign("mail_list",$mail_list);
	//Project情報
	$smarty->assign("project_info",$project_info);
	//起案者情報
	$smarty->assign("member_info",$member_info);
?>