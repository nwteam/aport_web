<?php
header('Content-Type: text/html; charset=utf-8');
require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
$commonDao = new CommonDao();

$current_user = getMemberSession();

$project_no = $_SESSION["project_no"];
$present_no = $_SESSION["present_no"];
$payment_method = $_SESSION["payment_method"];

	if($payment_method=="Credit"&&$_SESSION["card_no"]!=""){
		$input_data[card_no] = $_SESSION["card_no"];
		$input_data[expdate_month] = $_SESSION["expdate_month"];
		$input_data[expdate_year] = $_SESSION["expdate_year"];
		$input_data[hld_name] = $_SESSION["hld_name"];
		$input_data[security_code] = $_SESSION["security_code"];
	}
	if($payment_method=="cvs"&&$_SESSION["ks_name_1"]!=""){
		$input_data[convenience] = $_SESSION["convenience"];
		$input_data[ks_name_1] = $_SESSION["ks_name_1"];
		$input_data[ks_name_2] = $_SESSION["ks_name_2"];
		$input_data[ks_name_kana_1] = $_SESSION["ks_name_kana_1"];
		$input_data[ks_name_kana_2] = $_SESSION["ks_name_kana_2"];
		$input_data[ks_tel] = $_SESSION["ks_tel"];
	}
	if($payment_method=="payeasy"&&$_SESSION["ks_name_1"]!=""){
		$input_data[ks_name_1] = $_SESSION["ks_name_1"];
		$input_data[ks_name_2] = $_SESSION["ks_name_2"];
		$input_data[ks_name_kana_1] = $_SESSION["ks_name_kana_1"];
		$input_data[ks_name_kana_2] = $_SESSION["ks_name_kana_2"];
		$input_data[ks_tel] = $_SESSION["ks_tel"];
	}

//ログインチェック
if(!$current_user){
	header("Location:  {$pagelink_login}");
	exit;
}

//DB接続
$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

//プロジェクト情報の取得
if($project_no){
	$project_info=getProjectInfo($connect,$project_no);
	if(!is_array($project_info)){
		header("Location:  {$pagelink_error}");
		exit;
	}
}else{
		header("Location:  {$pagelink_error}");
		exit;
}

//起案者情報の取得
$drafter_info=getDrafterInfo($connect,$project_info[project_owner]);
if(!is_array($drafter_info)){
	header("Location:  {$pagelink_error}");
	exit;
}

if(isset($_POST[sbm_send])){	//送信ボタンクリック時
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
	//$input_data[ks_tel]=GetAlabNum($input_data[ks_tel]);

	//---------------- 入力チェック ---------------------------
	if($payment_method=="Credit"){
		//基本事項
		$baseData=CommonChkArray::$cardCheckData;

		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet1=check($input_data,$baseData,$err_msg_str);

		$_SESSION["card_no"]=$input_data[card_no];
		$_SESSION["expdate_month"]=$input_data[expdate_month];
		$_SESSION["expdate_year"]=$input_data[expdate_year];
		$_SESSION["hld_name"]=$input_data[hld_name];
		$_SESSION["security_code"]=$input_data[security_code];
	}
	elseif($payment_method=="cvs"){
		//基本事項
		$baseData=CommonChkArray::$cvsInfoCheckData;

		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet1=check($input_data,$baseData,$err_msg_str);

		$_SESSION["convenience"]=$input_data[convenience];
		$_SESSION["ks_name_1"]=$input_data[ks_name_1];
		$_SESSION["ks_name_2"]=$input_data[ks_name_2];
		$_SESSION["ks_name_kana_1"]=$input_data[ks_name_kana_1];
		$_SESSION["ks_name_kana_2"]=$input_data[ks_name_kana_2];
		$_SESSION["ks_tel"]=$input_data[ks_tel];
	}
	elseif($payment_method=="payeasy"){
		//基本事項
		$baseData=CommonChkArray::$payeasyInfoCheckData;

		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet1=check($input_data,$baseData,$err_msg_str);

		$_SESSION["ks_name_1"]=$input_data[ks_name_1];
		$_SESSION["ks_name_2"]=$input_data[ks_name_2];
		$_SESSION["ks_name_kana_1"]=$input_data[ks_name_kana_1];
		$_SESSION["ks_name_kana_2"]=$input_data[ks_name_kana_2];
		$_SESSION["ks_tel"]=$input_data[ks_tel];
	}
	else{
		//基本事項
		$baseData=CommonChkArray::$bankInfoCheckData;

		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet1=check($input_data,$baseData,$err_msg_str);

		$_SESSION["ks_name_1"]=$input_data[ks_name_1];
		$_SESSION["ks_name_2"]=$input_data[ks_name_2];
		$_SESSION["ks_name_kana_1"]=$input_data[ks_name_kana_1];
		$_SESSION["ks_name_kana_2"]=$input_data[ks_name_kana_2];
		$_SESSION["ks_tel"]=$input_data[ks_tel];
	}
//print_r_with_pre($errRet1);
	//-------------- ここまで -----------------------------------
	if(count($errRet1)==0){
		//確認ページへ遷移
		header("Location:  {$pagelink_collection_info_confirm}");
	}else{
		$errRet1[top]=$err_msg_str[99];
	}
}elseif(isset($_POST[back])){
	header("Location:  {$pagelink_collection_ticket}?no={$project_no}&present_no={$present_no}&pm={$payment_method}");
}
else{
}

//DB Cut
mysql_close($connect);

//Credit　Clientipのセット
if($lang=="ja"){
	$clientip=CDREDIX_CLIENTIP_JA;
}else{
	$clientip=CDREDIX_CLIENTIP_OTHER;
}

//ログイン情報
$smarty->assign("current_user", $current_user);
//Clientip情報
$smarty->assign("clientip", $clientip);
//都道府県リスト
$smarty->assign("array_area", $array_area);
//コンビニ
$smarty->assign("array_cvs", $array_cvs);
//有効月リスト
$smarty->assign("array_expdate_m", $array_expdate_m);
//有効年リスト
$smarty->assign("array_expdate_y", $array_expdate_y);
//エラー情報
$smarty->assign("err_msg1", $errRet1);
//入力情報
$smarty->assign("input_data", $input_data);
//プロジェクト情報
$smarty->assign("project_info", $project_info);
//支援情報
$smarty->assign("present_info", $present_info);
//選択した支援コース
if($present_no){
	$smarty->assign("selected_present_no", $present_no);
}
//起案者情報取得
$smarty->assign("drafter_info", $drafter_info);
//支払方法
$smarty->assign("payment_method", $payment_method);
?>