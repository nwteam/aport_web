<?php

  header('Content-Type: text/html; charset=utf-8');

 require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
 require '../../libs/Bcrypt.class.php';

  session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();
	$smarty->assign("current_user", $current_user);

	if($_POST){
		$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
		$err_msg_str=CommonMessageArray::$input_err_msg;

		if($input_data[email]==""){
			$errRet[email]=$err_msg_str[0];
		}
		else{
			//会員情報取得
			$password=to_hash($input_data[password]);
			$query  = "select * from sf_member where email='%s' ";
			$query = sprintf(
				$query,
				mysql_real_escape_string($input_data[email])
			);
			//print $query."<br>";
			$ret = mysql_query("set names utf8");
			$ret = mysql_query($query, $connect);
			$user_data   = mysql_fetch_array($ret);

			if(!is_array($user_data)){
				$errRet[email]=$err_msg_str[30];
			}
			else{
				//パスワード作成
				$pass=substr((md5(date("YmdD His"))), 0, 8);
				//$hash_pass=to_hash($pass);
				$hash_pass=Bcrypt::hash($pass);

				$data[password]=$hash_pass;
				$where[user_no]=$user_data[user_no];

				$sql=$commonDao->MakeUpdateSQL("sf_member",$data,$where);
				$result = mysql_query("set names utf8");
				$result = mysql_query($sql, $connect);

				if( $result === true ){
					//メール送信
					$smarty->assign("user_data", $user_data);
					$smarty->assign("pass", $pass);

					$mail_subject=getMailSubject(5);

					$subject = "[" . $str_site_title . "]".$mail_subject;
					$mailBody = $smarty->fetch($lang."/mail/m-reissue.tpl");
					//$mailfrom="From:" .mb_encode_mimeheader(MAIL_FROM_NAME) ."<".MAIL_FROM.">";
					send_mail($user_data[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

					$smarty->assign("finish_flg", "1");
				}else{
					print "Error<br>";
				}
			}

		}
		mysql_close($connect);


	}
	$smarty->assign("err_msg", $errRet);
	$smarty->assign("input_data", $input_data);

?>