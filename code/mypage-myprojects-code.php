<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();

	//ログインチェック
if(!$current_user){header("Location: {$pagelink_login}");exit;}

$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

//プロジェクト情報の取得(入稿中)
$result=getMyProjectListInfo($connect,$current_user['user_no']);
$project_data_list=array();
while($project_list_info = mysql_fetch_array($result)){
	//達成率
	if ($project_list_info['wish_price']=="0"){
		$project_list_info['percent'] = 0;
	}else{
		$project_list_info['percent'] = round(($project_list_info['now_summary'] / $project_list_info['wish_price']) * 100) > 100 ? 100 : round(($project_list_info['now_summary'] / $project_list_info['wish_price']) * 100);
	}
	//残り時間
	if($project_list_info[status]==1){
		$date1=strtotime($project_list_info['invest_limit']);
		$now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
			$project_list_info['diff_in_days'] = "-";
			$project_list_info[in_invest_flg]=0;
		}else{
			$total_secs = ($date1 - $date2);
			$project_list_info['diff_in_days'] = floor($total_secs / 86400);
			$project_list_info[in_invest_flg]=1;
		}
	}else{
		$project_list_info['diff_in_days'] = "-";
		$project_list_info[in_invest_flg]=0;
	}
	//説明
	$project_list_info['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($project_list_info['project_text'],ENT_QUOTES));
	$project_data_list[]=$project_list_info;
}
//プロジェクト情報の取得(審査待)
$result_Checking=getMyCheckingProjectListInfo($connect,$current_user['user_no']);
$project_data_list_Checking=array();
while($project_list_info_Checking = mysql_fetch_array($result_Checking)){
	//達成率
	if ($project_list_info_Checking['wish_price']=="0"){
		$project_list_info_Checking['percent'] = 0;
	}else{
		$project_list_info_Checking['percent'] = round(($project_list_info_Checking['now_summary'] / $project_list_info_Checking['wish_price']) * 100) > 100 ? 100 : round(($project_list_info_Checking['now_summary'] / $project_list_info_Checking['wish_price']) * 100);
	}
	//残り時間
	if($project_list_info_Checking[status]==1){
		$date1=strtotime($project_list_info_Checking['invest_limit']);
		$now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
			$project_list_info_Checking['diff_in_days'] = "-";
			$project_list_info_Checking[in_invest_flg]=0;
		}else{
			$total_secs = ($date1 - $date2);
			$project_list_info_Checking['diff_in_days'] = floor($total_secs / 86400);
			$project_list_info_Checking[in_invest_flg]=1;
		}
	}else{
		$project_list_info_Checking['diff_in_days'] = "-";
		$project_list_info_Checking[in_invest_flg]=0;
	}
	//説明
	$project_list_info_Checking['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($project_list_info_Checking['project_text'],ENT_QUOTES));
	$project_data_list_Checking[]=$project_list_info_Checking;
}
//プロジェクト情報の取得(修正依頼)
$result_Change=getMyChangeProjectListInfo($connect,$current_user['user_no']);
$project_data_list_Change=array();
while($project_list_info_Change = mysql_fetch_array($result_Change)){
	//達成率
	if ($project_list_info_Change['wish_price']=="0"){
		$project_list_info_Change['percent'] = 0;
	}else{
		$project_list_info_Change['percent'] = round(($project_list_info_Change['now_summary'] / $project_list_info_Change['wish_price']) * 100) > 100 ? 100 : round(($project_list_info_Change['now_summary'] / $project_list_info_Change['wish_price']) * 100);
	}
	//残り時間
	if($project_list_info_Change[status]==1){
		$date1=strtotime($project_list_info_Change['invest_limit']);
		$now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
			$project_list_info_Change['diff_in_days'] = "-";
			$project_list_info_Change[in_invest_flg]=0;
		}else{
			$total_secs = ($date1 - $date2);
			$project_list_info_Change['diff_in_days'] = floor($total_secs / 86400);
			$project_list_info_Change[in_invest_flg]=1;
		}
	}else{
		$project_list_info_Change['diff_in_days'] = "-";
		$project_list_info_Change[in_invest_flg]=0;
	}
	//説明
	$project_list_info_Change['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($project_list_info_Change['project_text'],ENT_QUOTES));
	$project_data_list_Change[]=$project_list_info_Change;
}
//プロジェクト情報の取得(審査完了)
$result_Checked=getMyCheckedProjectListInfo($connect,$current_user['user_no']);
$project_data_list_Checked=array();
while($project_list_info_Checked = mysql_fetch_array($result_Checked)){
	//達成率
	if ($project_list_info_Checked['wish_price']=="0"){
		$project_list_info_Checked['percent'] = 0;
	}else{
		$project_list_info_Checked['percent'] = round(($project_list_info_Checked['now_summary'] / $project_list_info_Checked['wish_price']) * 100) > 100 ? 100 : round(($project_list_info_Checked['now_summary'] / $project_list_info_Checked['wish_price']) * 100);
	}
	//残り時間
	if($project_list_info_Checked[status]==1){
		$date1=strtotime($project_list_info_Checked['invest_limit']);
		$now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
			$project_list_info_Checked['diff_in_days'] = "-";
			$project_list_info_Checked[in_invest_flg]=0;
		}else{
			$total_secs = ($date1 - $date2);
			$project_list_info_Checked['diff_in_days'] = floor($total_secs / 86400);
			$project_list_info_Checked[in_invest_flg]=1;
		}
	}else{
		$project_list_info_Checked['diff_in_days'] = "-";
		$project_list_info_Checked[in_invest_flg]=0;
	}
	//説明
	$project_list_info_Checked['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($project_list_info_Checked['project_text'],ENT_QUOTES));
	$project_data_list_Checked[]=$project_list_info_Checked;
}
//print_r_with_pre($project_data_list);die;

//会員情報をinput_dataに格納
$member_info=getMemberInfo($connect,$current_user['user_no']);
$input_data['profile_img']=$member_info['profile_img'];
$input_data['member_name']=$member_info['member_name'];
$input_data['add_1']=$member_info['add_1'];
$input_data['hp_url']=$member_info['hp_url'];
$input_data['profile']=$member_info['profile'];

mysql_close($connect);

//Credit　Clientipのセット
if($lang=="ja"){
	$clientip=CDREDIX_CLIENTIP_JA;
}else{
	$clientip=CDREDIX_CLIENTIP_OTHER;
}

//Clientip情報
$smarty->assign("clientip", $clientip);
//ログイン情報
$smarty->assign("current_user", $current_user);
$smarty->assign("share_cnt", $share_cnt);
// カテゴリー
$smarty->assign("array_category", $array_category);
//プロジェクト情報
$smarty->assign("project_data_list", $project_data_list);
$smarty->assign("project_data_list_Checking", $project_data_list_Checking);
$smarty->assign("project_data_list_Change", $project_data_list_Change);
$smarty->assign("project_data_list_Checked", $project_data_list_Checked);
//都道府県リスト
$smarty->assign("array_area", $array_area);
//エラー情報
$smarty->assign("err_msg", $errRet);
//入力情報
$smarty->assign("input_data", $input_data);

?>