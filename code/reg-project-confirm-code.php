<?php

  header('Content-Type: text/html; charset=utf-8');
 require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	$commonDao = new CommonDao();

		//ログインユーザの取得
	$current_user = getMemberSession();

	$smarty->assign("current_user", $current_user);

	if($_POST){	//送信ボタンクリック時
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;

		//print_r_with_pre($_FILES);
		//---------------- 入力チェック ---------------------------
		//基本事項
		$baseData=CommonChkArray::$actressRegistCheckData;
//		$err_msg_str=CommonMessageArray::$input_err_msg;
//		$errRet=check($input_data,$baseData,$err_msg_str);
		//-------------- ここまで -----------------------------------

		//規約
//		if(! $input_data[rulesParticipant]){
//			$errRet[rulesParticipant]=$err_msg_str[16];
//		}

		if(count($errRet)==0){
			//登録処理
			  $connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
			  date_default_timezone_set("Asia/Tokyo");

		    //トランザクションをはじめる準備
			    $Query = "set autocommit = 0";
			    mysql_query( $Query, $connect );

			    //トランザクション開始
			    $Query = "begin";
			    mysql_query( $Query, $connect );

			//起案者基本情報
			$data[public_name]=$input_data[name];
			$data[email]=$input_data[email];
			$data[entry_type]=1;
			$data[create_date]=date('Y-m-d H:i:s');

			$sql=$commonDao->MakeInsertSQL("sf_actress",$data);

			$result = mysql_query("set names utf8");
			$result = mysql_query($sql, $connect);
			$actress_no = mysql_insert_id();
			if( $result === true ){
				//起案者詳細
				$data=array();
				$data[actress_no]=$actress_no;
				$data[lang]='ja';
				$data[create_date]=date('Y-m-d H:i:s');
				foreach($baseData[dbstring] as $key=>$val){
					$data[$key]=$input_data[$key];
				}
				$sql=$commonDao->MakeInsertSQL("sf_actress_detail",$data);
				$result = mysql_query("set names utf8");
				$result = mysql_query($sql, $connect);
				if( $result === true ){
						//コミット
				        $Query = "commit";
				        mysql_query( $Query, $connect );

						//メール送信
						$smarty->assign("input_data", $input_data);

						$mail_subject=getMailSubject(1);

						$subject = "[" . $str_site_title . "]　".$mail_subject;
						$mailBody = $smarty->fetch($lang."/mail/m-reg-project-complete.tpl");
						//$mailfrom="From:" .mb_encode_mimeheader(MAIL_FROM_NAME) ."<".MAIL_FROM.">";
						send_mail($input_data[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
						send_mail("info@a-port-mail.com", $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

						mysql_close($connect);

						//完了ページへ遷移
						header("Location:  {$pagelink_reg_project_complete}");

			    }else{

			        //ロールバック
			        $Query = "rollback";
			        mysql_query( $Query, $connect );

			    }
			}else{

		        //ロールバック
		        $Query = "rollback";
		        mysql_query( $Query, $connect );

		    }
			mysql_close($connect);

		}else{
			$errRet[top]=$err_msg_str[99];
		}

	}else{
		$_SESSION[TMP_FUP]="";

	}

	$input_data[name]=$_SESSION['name'];
	$input_data[email]=$_SESSION['email'];
	$input_data[fb_url]=$_SESSION['fb_url'];
	$input_data[project_name]=$_SESSION['project_name'];
	$input_data[category_no]=$_SESSION['category_no'];
	$input_data[project_text]=$_SESSION['project_text'];
	$input_data[wish_price]=$_SESSION['wish_price'];
	$input_data[comment_text]=$_SESSION['comment_text'];

	$smarty->assign("err_msg", $errRet);
	$smarty->assign("input_data", $input_data);

// カテゴリー
$smarty->assign("array_category", $array_category);

?>