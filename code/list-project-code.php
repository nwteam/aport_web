<?php

  header('Content-Type: text/html; charset=utf-8');

	$current_user = getMemberSession();

	$smarty->assign("current_user", $current_user);

	//DB接続
 	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	//検索条件
	if($_GET['st']==3){
		$m_status_2=1;	//成立したプロジェクトのみ表示
		$cm_view_flg=1;	//commeng_soon非表示

	}else{
		$m_status_0=1;	//募集中のプロジェクトのみ表示
		$cm_view_flg=1;	//commeng_soon表示
	}
  $m_page_list       = get_check(trim(preg_change($_GET['hid_page_list'])));
  $m_project_name    = get_check(trim(preg_change($_POST['txt_project_name'])));
  $m_area            = get_check(trim(preg_change($_POST['txt_area'])));
  $m_category        = get_check(trim(preg_change($_POST['cmb_category'])));
  $m_type            = get_check(trim(preg_change($_POST['cmb_type'])));
  $m_wish_price_from = get_check(trim(preg_change($_POST['txt_wish_price_from'])));
  $m_wish_price_to   = get_check(trim(preg_change($_POST['txt_wish_price_to'])));
  $m_kubn_1          = get_check(trim(preg_change($_POST['chk_kubn_1'])));
  $m_kubn_2          = get_check(trim(preg_change($_POST['chk_kubn_2'])));
/*
  $m_status_0        = trim($_POST['chk_status_0']);	//募集中
  $m_status_1        = trim($_POST['chk_status_1']);	//不成立
  $m_status_2        = trim($_POST['chk_status_2']);	//成立
  $m_status_3        = trim($_POST['chk_status_3']);	//未承認
  $m_member_no       = trim($_POST['hid_member_no']);
 */
	$smarty->assign("m_page_list",$m_page_list);
	$smarty->assign("m_project_name",$m_project_name);
	$smarty->assign("m_area",$m_area);
	$smarty->assign("m_category",$m_category);
	$smarty->assign("m_type",$m_type);
	$smarty->assign("m_wish_price_from",$m_wish_price_from);
	$smarty->assign("m_wish_price_to",$m_wish_price_to);
	$smarty->assign("m_kubn_1",$m_kubn_1);
	$smarty->assign("m_kubn_2",$m_kubn_2);
	$smarty->assign("m_status_0",$m_status_0);
	$smarty->assign("m_status_1",$m_status_1);
	$smarty->assign("m_status_2",$m_status_2);
	$smarty->assign("m_status_3",$m_status_3);
	$smarty->assign("m_member_no",$m_member_no);
	$smarty->assign("array_category",$array_category_smarty);	//カテゴリー選択配列

  if(isset($_GET['c_no'])){
  	$m_category        = get_check(trim($_GET['c_no']));

  }

  $m_project_name    = cleanup($m_project_name);
  $m_area            = cleanup($m_area);
  $m_wish_price_from = cleanup($m_wish_price_from);
  $m_wish_price_to   = cleanup($m_wish_price_to);

  // プロジェクト
//  $connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
/*
  // ADMIN
  $query_admin  = " select count(*) as cnt ";
  $query_admin .= "   from wp_usermeta ";
  $query_admin .= "  where 1 = 1 ";
  $query_admin .= "    and user_id = '".$user_id."' ";
  $query_admin .= "    and meta_key = 'wp_capabilities' ";
  $query_admin .= "    and meta_value like '%administrator%' ";

  $result_admin = mysql_query($query_admin, $connect);
  $data_admin = mysql_fetch_array($result_admin);
  $admin_count = $data_admin[cnt];

  if ($admin_count > 0) {
    $is_admin = true;
  } else {
    $is_admin = false;
  }
*/

  $query_project  = " select a.* , ";
  $query_project .= "        b.category_name as category_name , ";
  $query_project .= "        c.actress_no as member_no , ";
  $query_project .= "        c.public_name as member_nickname , ";
  $query_project .= "        c.fresh_flg as fresh_flg , ";
  $query_project .= "        d.* ";
  $query_project .= "   from sf_project a ";
  $query_project .= "  left join sf_project_detail d ";
  $query_project .= "     on a.no = d.project_no ";
  $query_project .= "    and d.lang = '{$db_lang}'  ";
  $query_project .= "  inner join sf_category b ";
  $query_project .= "     on a.category_no = b.no ";
  $query_project .= "    and b.del_flg = '0' ";
  $query_project .= "  inner join sf_actress c ";
  $query_project .= "     on a.project_owner = c.actress_no ";
  $query_project .= "    and c.del_flg = '0' ";
  $query_project .= " where 1 ";
  $query_project .= "   and a.del_flg = '0' ";
	$query_project .= "    and (a.status = '1' or a.status = '3')";
	$query_project .= "    and a.chk_status = '3' ";
	$query_project .= "    and UNIX_TIMESTAMP(a.project_record_date) <= ".strtotime(date("Y-m-d",time())."23:59:59");

  $query_total  = " select count(*) as cnt  ";
  $query_total .= "   from sf_project a ";
  $query_total .= "  left join sf_project_detail d ";
  $query_total .= "     on a.no = d.project_no ";
  $query_total .= "    and d.lang = '{$db_lang}'  ";
  $query_total .= "  inner join sf_actress c ";
  $query_total .= "     on a.project_owner = c.actress_no ";
  $query_total .= "    and c.del_flg = '0' ";
  $query_total .= " where 1 ";
  $query_total .= "   and a.del_flg = '0' ";
	$query_total .= "    and (a.status = '1' or a.status = '3')";
	$query_total .= "    and a.chk_status = '3' ";
	$query_total .= "    and UNIX_TIMESTAMP(a.project_record_date) <= ".strtotime(date("Y-m-d",time())."23:59:59");

  // 掲載プロジェクト
  if (isset($m_member_no) && $m_member_no != "") {
    $query_project .= " and a.project_owner = '".$m_member_no."'";
    $query_total .= " and a.project_owner = '".$m_member_no."'";
  }

  // キーワード
  if (isset($m_project_name) && $m_project_name != "") {
    $query_project .= " and (d.project_name like '%".$m_project_name."%' ";
    $query_project .= " or a.category_no in (select no from sf_category where category_name like '%".$m_project_name."%' and del_flg ='0') ";
    $query_project .= " or d.project_text like '%".$m_project_name."%') ";
    $query_total .= " and (d.project_name like '%".$m_project_name."%' ";
    $query_total .= " or a.category_no in (select no from sf_category where category_name like '%".$m_project_name."%' and del_flg ='0') ";
    $query_total .= " or d.project_text like '%".$m_project_name."%') ";
	$cm_view_flg=0;	//commeng_soon非表示
    }

  // 地域
  if (isset($m_area) && $m_area != "") {
    $query_project .= " and a.area like '%".$m_area."%'";
    $query_total .= " and a.area like '%".$m_area."%'";
  }

  // カテゴリー
  if (isset($m_category) && $m_category != "") {
    $query_project .= " and a.category_no = '".$m_category."'";
    $query_total .= " and a.category_no = '".$m_category."'";
  }

  // 掲載区分
  //if (isset($m_type) && $m_type != "") {
  //  $query_project .= " and a.type = '".$m_type."'";
  //  $query_total .= " and a.type = '".$m_type."'";
  //}

  // 目標サポート金額(FROM)
  if (isset($m_wish_price_from) && $m_wish_price_from != "") {
    $query_project .= " and a.wish_price >= ".$m_wish_price_from."";
    $query_total .= " and a.wish_price >= ".$m_wish_price_from."";
  }

  // 目標サポート金額(TO)
  if (isset($m_wish_price_to) && $m_wish_price_to != "") {
    $query_project .= " and a.wish_price <= ".$m_wish_price_to."";
    $query_total .= " and a.wish_price <= ".$m_wish_price_to."";
  }

  // 区分
  if ((isset($m_kubn_1) && $m_kubn_1 != "") && !(isset($m_kubn_2) && $m_kubn_2 != "")) {
  	// サポートプロジェクト
    $query_project .= " and a.no in (select project_no from sf_invest where member_id = '".$user_id."' and status>0 and status<=91 and del_flg = '0') ";
    $query_total .= " and a.no in (select project_no from sf_invest where member_id = '".$user_id."' and status>0 and status<=91 and del_flg = '0') ";
  } else if (!(isset($m_kubn_1) && $m_kubn_1 != "") && (isset($m_kubn_2) && $m_kubn_2 != "")) {
  	// 掲載プロジェクト
    $query_project .= " and a.no in (select no from $tbl_project where project_owner = '".$user_id."' and del_flg = '0') ";
    $query_total .= " and a.no in (select no from $tbl_project where project_owner = '".$user_id."' and del_flg = '0') ";
  } else if ((isset($m_kubn_1) && $m_kubn_1 != "") && (isset($m_kubn_2) && $m_kubn_2 != "")) {
    $query_project .= " and (a.no in (select project_no from sf_invest where member_id = '".$user_id."' and status>0 and status<=91 and del_flg = '0') ";
    $query_project .= "  or  a.no in (select no from $tbl_project where project_owner = '".$user_id."' and del_flg = '0')) ";
    $query_total .= " and (a.no in (select project_no from sf_invest where member_id = '".$user_id."' and status>0 and status<=91 and del_flg = '0') ";
    $query_total .= "  or  a.no in (select no from $tbl_project where project_owner = '".$user_id."' and del_flg = '0')) ";
  }
/*
  // ステータス
  if (isset($m_status_0) && $m_status_0 != "") {	//募集中
  	$arr_0 = "1";
  } else {
  	$arr_0 = "0";
  }
  if (isset($m_status_1) && $m_status_1 != "") {	//不成立
  	$arr_1 = "1";
  } else {
  	$arr_1 = "0";
  }
  if (isset($m_status_2) && $m_status_2 != "") {	//成立
  	$arr_2 = "1";
  } else {
  	$arr_2 = "0";
  }
  if (isset($m_status_3) && $m_status_3 != "") {	//未承認
  	$arr_3 = "1";
  } else {
  	$arr_3 = "0";
  }


  $arr_status = array();
  if (isset($m_status_1) && $m_status_1 != "") {
  	$arr_status[] = "4";
  }
  if (isset($m_status_2) && $m_status_2 != "") {
  	$arr_status[] = "3";
  }
  if (isset($m_status_3) && $m_status_3 != "") {
  	$arr_status[] = "0";
  }
  if (count($arr_status) > 0) {
    $sql_tmp = implode(" , ", $arr_status);
    $query_project .= " and a.status in ( " . $sql_tmp . " ) ";
    $query_total .= " and a.status in ( " . $sql_tmp . " ) ";
  }


  if ($arr_0 == "1" || $arr_1 == "1" || $arr_2 == "1" || $arr_3 == "1") {
    $query_project .= " and (1 = 2 ";
    $query_total .= " and (1 = 2 ";
  } else {
    $query_project .= " and (1 = 1 ";
    $query_total .= " and (1 = 1 ";
  }

  // 募集中
  if (isset($m_status_0) && $m_status_0 != "") {
    $query_project .= " or (a.status = '1' and a.invest_limit >= str_to_date(now(),'%Y-%m-%d')) ";
    $query_total .= " or (a.status = '1' and a.invest_limit >= str_to_date(now(),'%Y-%m-%d')) ";
  }

  // 不成立
  if (isset($m_status_1) && $m_status_1 != "") {
  	$query_project .= " or (ifnull(a.now_summary,0) < a.wish_price and a.invest_limit < str_to_date(now(),'%Y-%m-%d') ) ";
  	$query_total .= " or (ifnull(a.now_summary,0) < a.wish_price and a.invest_limit < str_to_date(now(),'%Y-%m-%d')  )";
  //  $query_project .= " or (ifnull(a.now_summary,0) < a.wish_price) ";
  //  $query_total .= " or (ifnull(a.now_summary,0) < a.wish_price) ";
  }

  // 成立
  if (isset($m_status_2) && $m_status_2 != "") {
    $query_project .= " or (a.status = '3') ";
    $query_total .= " or (a.status = '3' ) ";
  	//    $query_project .= " or (ifnull(a.now_summary,0) >= a.wish_price and a.invest_limit < str_to_date(now(),'%Y-%m-%d')) ";
//    $query_total .= " or (ifnull(a.now_summary,0) >= a.wish_price and a.invest_limit < str_to_date(now(),'%Y-%m-%d')) ";
  }

  // 未承認
  if (isset($m_status_3) && $m_status_3 != "") {
    if ($is_admin) {
      $query_project .= " or a.status = '0' ) ";
      $query_total .= " or a.status = '0' ) ";
    } else {
      $query_project .= " or ( a.status = '0' and a.project_owner = '".$user_id."' ) ) ";
      $query_total .= " or ( a.status = '0' and a.project_owner = '".$user_id."' ) ) ";
    }
  } else {
    $query_project .= " ) and a.status != '0'  ";
    $query_total .= " ) and a.status != '0'  ";
  }
*/
  // 新着順
  if ($m_page_list=="1") {
  //$query_project .= " order by a.no desc ";
    $query_project .= " order by a.no desc ";
  // 達成率順
  } else if ($m_page_list=="2") {
//    $query_project .= " order by (a.now_summary/a.wish_price*100) desc ";
    $query_project .= " and a.invest_limit>now() and a.status<>3 and a.now_summary<a.wish_price";
    $query_total .= " and a.invest_limit>now() and a.status<>3 and a.now_summary<a.wish_price";
    $query_project .= " order by a.invest_limit asc ";
//echo $query_project;
  // 寄付金額順
  } else if ($m_page_list=="3") {
//    $query_project .= " order by a.wish_price desc ";
    $query_project .= " order by a.now_summary desc ";
  // サポーター数準
  } else if ($m_page_list=="4") {
    $query_project .= " order by a.now_supporter desc ";
  } else {
    $query_project .= " order by a.project_record_date desc, a.no desc ";
  }
//echo $query_total;
  $result_total = mysql_query($query_total, $connect);
  $data_total = mysql_fetch_array($result_total);
  $total_count = $data_total[cnt];

  if($_GET[page] && $_GET[page] > 0){
    $page = $_GET[page];
  }else{
    $page = 1;
  }

  $page_row = $C_DETAIL_PROJECT_PAGE_ROW;
  //$page_row=3;
  $page_scale = $C_DETAIL_PROJECT_PAGE_SCALE;

  $total_page  = ceil($total_count / $page_row);

	$nowpage = isset($_GET['page']) ? intval($_GET['page']) : 1;
	if( $nowpage <= 1 ) {
		$nowpage = 1;
	} elseif( $total_page && $nowpage >= $total_page ) {
		$nowpage = $total_page;	
	}
	
	$firstrow = $page_row * ($nowpage-1);

  $from_record = ($page - 1) * $page_row;
  $paging_str = "";
  $paging_before = "";
  $paging_next = "";

  $start_page = ( (ceil( $page / $page_scale ) - 1) * $page_scale ) + 1;
  $end_page = $start_page + $page_scale - 1;
  if ($end_page >= $total_page) $end_page = $total_page;

  if ($page > 1){
    $paging_before .= "<li><a href='".$pagelink_list_project."?page=".($page - 1)."'>&lt;</a></li>";
  } else {
    $paging_before .= "<li><a href='javascript:void(0)'>&lt;</a></li>";
  }

  if ($total_page > 0) {
    for ($i=$start_page;$i<=$end_page;$i++) {
      if ($page != $i){
          $paging_str .= "<li><a href='".$pagelink_list_project."?page=".$i."'>$i</a></li>";
       }else{
          $paging_str .= "<li class='active'><a href='javascript:void(0)'>".$i."</a></li>";
      }
    }
  }

  if ($total_page > $page){
    $paging_next .= "<li><a href='".$pagelink_list_project."?page=".($page + 1)."'>&gt;</a></li>";
  } else {
    $paging_next .= "<li><a href='javascript:void(0)'>&gt;</a></li>";
  }

  //ナビ
	$smarty->assign("total_count",$total_count);
	$smarty->assign("paging_before",$paging_before);
	$smarty->assign("paging_str",$paging_str);
	$smarty->assign("paging_next",$paging_next);

  if($agent==""){
	$query = $query_project." limit ".$from_record.", ".$page_row;
  }else{
  	$query = $query_project;
  }
  //print $query."<br>";
  $result = mysql_query("set names utf8");
  $result = mysql_query($query, $connect);
  $result_list=array();
  while($data = mysql_fetch_array($result)){
	//画像の取得
	  $query  = " select * from general_images where image_type = 'PROFILE_IMG' and parent_no = {$data[member_no]} ";
  	  //print $query."<br>";
	  $ret = mysql_query("set names utf8");
	  $ret = mysql_query($query, $connect);
  	  $image   = mysql_fetch_array($ret);
	  $data[image]=$image[file_name];
  	//youtube
	  $query  = " select * from general_images where image_type = 'PROJECT_URL' and parent_no = {$data[no]} ";
	  //print $query."<br>";
	  $ret = mysql_query("set names utf8");
	  $ret = mysql_query($query, $connect);
  	  $image   = mysql_fetch_array($ret);
	$m_youtube_id=getParamdata($data['url'],"v");
	$data['youtube_id']=$m_youtube_id;

	//達成率
	if($data['wish_price']>0){
		$data['now_summary']=$data['now_summary']+$data['admin_summary'];
			$data['percent'] = round(($data['now_summary'] / $data['wish_price']) * 100);
	}else{
		if($data['now_summary']>0){
			$data['percent']=100;
		}else{
			$data['percent']=0;
		}
	}
  	//残り時間
    if($data[status]==1){
  		$date1=strtotime($data['invest_limit']);
	    $now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
		    $data['diff_in_days'] = "-";
		    $data[in_invest_flg]=0;

		}else{
		    $total_secs = ($date1 - $date2);
		    $data['diff_in_days'] = floor($total_secs / 86400);
		    $data[in_invest_flg]=1;
		}
  	}else{
		$data['diff_in_days'] = "-";
		$data[in_invest_flg]=0;
  	}
     //説明
	  $data['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($data['project_text'],ENT_QUOTES));
	  //ステータス
	  if($data['status']==0){
			$data['status_str']=$array_project_status[$data['status']][1];
 	 }
	  if($data['status']==1){
			$data['status_class']="project";
 	 }
	  if($data['status']==3){
			$data['status_class']="project successful";
 	 }


	  $result_list[]=$data;
  }
  $smarty->assign("result_list", $result_list);
  $data_cnt=count($result_list);
  $smarty->assign("data_cnt",$data_cnt);

  //CommingSoon件数
  $comming_cnt=0;
  if($cm_view_flg){
  	  if($data_cnt){

		  $amari=($data_cnt % 3);
		  if($amari>0){
		  	  $comming_cnt=3-$amari;
		  }
	  }else{
  		$comming_cnt=3;
	  }
	}
  $smarty->assign("comming_cnt",$comming_cnt);

//分类
$category = mysql_query("SELECT * FROM `sf_category` where del_flg = '0'");
$categorylist = array();
while($row = mysql_fetch_assoc($category)){
	$categorylist[] = $row;
}
$smarty->assign("categorylist", $categorylist);
//分页代码
$smarty->assign("total_page", $total_page);
$smarty->assign("nowpage", $nowpage);

mysql_close($connect);

?>