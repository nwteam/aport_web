<?php
header('Content-Type: text/html; charset=utf-8');
require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
$commonDao = new CommonDao();

$current_user = getMemberSession();

$project_no = trim($_REQUEST['no']);
$present_no = trim($_REQUEST['present_no']);

//ログインチェック
if(!$current_user){
	header("Location:  {$pagelink_login}");
	exit;
}

$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

if($project_no){
	//プロジェクト情報の取得
	$project_info=getProjectInfo($connect,$project_no);
	if(!is_array($project_info)){
		header("Location:  {$pagelink_error}");
		exit;
	}
	if($project_info['user_no']==$current_user['user_no']){
		header("Location:  {$pagelink_index}");
		exit;
	}
	//該当プロジェクトの支援コース情報の取得
	$ret=getProjectPresentInfo($connect,$project_no);
	$present_info=array();
	while($project_present_info = mysql_fetch_array($ret)){
		//支援者数の取得
		$invest_cnt=getPresentCntInfo($connect,$project_no,$project_present_info[present_no]);

		$project_present_info[invest_cnt]=$invest_cnt[cnt];
		$present_info[]=$project_present_info;
	}
}else{
		header("Location:  {$pagelink_error}");
		exit;
}
//起案者情報の取得
$drafter_info=getDrafterInfo($connect,$project_info[project_owner]);
if(!is_array($drafter_info)){
	header("Location:  {$pagelink_error}");
	exit;
}

//画像の取得
//$image=getImage($connect,'PROFILE_IMG',$project_info[project_owner]);
//$drafter_info[image]=C_IMG_ACTRESS.$image[file_name];

if(isset($_POST[sbm_send])){	//送信ボタンクリック時
	//$_SESSION["input_data"]=$_POST;
	//$input_data=$_SESSION["input_data"];
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
	//---------------- 入力チェック ---------------------------
	//基本事項
	$baseData=CommonChkArray::$investCheckData;

	$err_msg_str=CommonMessageArray::$input_err_msg;
	$errRet1=check($input_data,$baseData,$err_msg_str);
	if(count($errRet1)==0){
		//選択した支援情報の取得
		if($present_no){
			$selected_present_info=getPresentInfo($connect,$present_no);
			if(!is_array($selected_present_info)){
				header("Location:  {$pagelink_error}");
				exit;
			}
		}else{
				header("Location:  {$pagelink_error}");
				exit;
		}
		$invest_total=$selected_present_info[min]+$input_data[amount_plus];
		if($invest_total<1000 && $input_data[payment_method]=="cvs"){
			$errRet1[payment_method]=$err_msg_str[84];//ご支援金額が1000円未満の場合、コンビニが選択できません。
		}
		if($invest_total<1000 && $input_data[payment_method]=="payeasy"){
			$errRet1[payment_method]=$err_msg_str[88];//ご支援金額（追加を含めて計算）が1000円に満たない場合は、Pay-easy決済はできません。
		}
		//コンビニ　299,999円
		if($invest_total>299999 && $input_data[payment_method]=="cvs"){
			$errRet1[payment_method1]=$err_msg_str[40];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method2]=$err_msg_str[41];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method3]=$err_msg_str[42];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method4]=$err_msg_str[43];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method5]=$err_msg_str[44];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method6]=$err_msg_str[45];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method7]=$err_msg_str[46];//ご指定された決済方法の限度額を超えております。
		}

		//ペイジー　999,999円
		if($invest_total>999999 && $input_data[payment_method]=="payeasy"){
			$errRet1[payment_method1]=$err_msg_str[40];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method2]=$err_msg_str[41];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method3]=$err_msg_str[42];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method4]=$err_msg_str[43];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method5]=$err_msg_str[44];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method6]=$err_msg_str[45];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method7]=$err_msg_str[46];//ご指定された決済方法の限度額を超えております。
		}
		//クレジット　9,999,999円
		if($invest_total>9999999 && $input_data[payment_method]=="Credit"){
			$errRet1[payment_method1]=$err_msg_str[40];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method2]=$err_msg_str[41];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method3]=$err_msg_str[42];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method4]=$err_msg_str[43];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method5]=$err_msg_str[44];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method6]=$err_msg_str[45];//ご指定された決済方法の限度額を超えております。
			$errRet1[payment_method7]=$err_msg_str[46];//ご指定された決済方法の限度額を超えております。
		}
	}else{
		$errRet1[top]=$err_msg_str[99];
	}
	//-------------- ここまで -----------------------------------
	if(count($errRet1)==0){
		$_SESSION["project_no"]=$project_no;
		$_SESSION["present_no"]=$present_no;
		$_SESSION["payment_method"]=$input_data[payment_method];
		$_SESSION["invest_amount"]=$selected_present_info[min];
		$_SESSION["amount_plus"]=$input_data[amount_plus];
		//確認ページへ遷移
		header("Location:  {$pagelink_collection_info_input}");
	}else{
		$errRet1[top]=$err_msg_str[99];
	}
}
mysql_close($connect);

//Credit　Clientipのセット
if($lang=="ja"){
	$clientip=CDREDIX_CLIENTIP_JA;
}else{
	$clientip=CDREDIX_CLIENTIP_OTHER;
}

//ログイン情報
$smarty->assign("current_user", $current_user);
//Clientip情報
$smarty->assign("clientip", $clientip);
//都道府県リスト
$smarty->assign("array_area", $array_area);
//エラー情報
$smarty->assign("err_msg1", $errRet1);
//入力情報
$smarty->assign("input_data", $input_data);
//プロジェクト情報
$smarty->assign("project_info", $project_info);
//支援情報
$smarty->assign("present_info", $present_info);
//選択した支援コース
if($present_no){
	$smarty->assign("selected_present_no", $present_no);
}
//起案者情報取得
$smarty->assign("drafter_info", $drafter_info);
?>