<?php

  header('Content-Type: text/html; charset=utf-8');

  session_cache_limiter('no-cache, must-revalidate');

 require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	$commonDao = new CommonDao();

	$current_user = getMemberSession();
	$smarty->assign("current_user", $current_user);

		if(!$_GET[param] || $_GET[param]==""){
			//エラーページへ遷移
			header("Location:  {$pagelink_reg_member_error}?errno=1");
			exit;
		}
		$param=explode("_",makeGetRequest($_GET[param]));

		if(!$param){
			//エラーページへ遷移
			header("Location:  {$pagelink_reg_member_error}?errno=1");
			exit;
		}

		$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

		//会員情報取得
		$query  = "select * from sf_member where user_no='%s' and param='%s' ";
		$query = sprintf(
			$query,
			mysql_real_escape_string($param[0]),
			mysql_real_escape_string($param[1])
		);
		//print $query."<br>";
		$ret = mysql_query("set names utf8");
		$ret = mysql_query($query, $connect);
		$db_data   = mysql_fetch_array($ret);
		if(!is_array($db_data)){
			//エラーページへ遷移
			header("Location:  {$pagelink_reg_member_error}?errno=1");
			exit;
		}

		if($db_data[status]>=1){//本登録になっている
			//エラーページへ遷移
			header("Location:  {$pagelink_reg_member_error}?errno=2");
			exit;
		}

		//登録期限チェック
		if(date("Y-m-d H:i:s")> $db_data[regist_limit]){

			//消す
			$query  = "delete from sf_member where user_no='%s' ";
			$query = sprintf(
				$query,
				mysql_real_escape_string($param[0])
			);
			$ret = mysql_query("set names utf8");
			$ret = mysql_query($query, $connect);

			//エラーページへ遷移
			header("Location:  {$pagelink_reg_member_error}?errno=3");
			exit;
		}

		//ステータス更新
			$data[status]=1;
			$where[user_no]=$db_data[user_no];
			
			$sql=$commonDao->MakeUpdateSQL("sf_member",$data,$where);
			$result = mysql_query("set names utf8");
			$result = mysql_query($sql, $connect);
		
			if( $result === true ){

				//メール送信
				$smarty->assign("db_data", $db_data);

				$mail_subject=getMailSubject(4);

				$subject = "[" . $str_site_title . "]".$mail_subject;
				$mailBody = $smarty->fetch($lang."/mail/m-reg-member-complete.tpl");
				//$mailfrom="From:" .mb_encode_mimeheader(MAIL_FROM_NAME) ."<".MAIL_FROM.">";
				send_mail($db_data[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

				mysql_close($connect);

			}else{
				mysql_close($connect);
				//エラーページへ遷移
				header("Location:  {$pagelink_reg_member_error}?errno=4");
				exit;
		    }
		

?>