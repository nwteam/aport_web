<?php

  header('Content-Type: text/html; charset=utf-8');

 require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
 require '../../libs/bcrypt.php';
 require '../../libs/Bcrypt.class.php';

  session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();
	$smarty->assign("current_user", $current_user);

	//ログイン済みの場合はマイページへ
	if($current_user[user_no]){
		header("Location:  {$pagelink_mypage}");
	}
	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
	$err_msg_str=CommonMessageArray::$input_err_msg;

	if(isset($_POST[sbm_login])){
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;

		if($input_data[email]=="" || $input_data[password]==""){
			$errRet[top]=$err_msg_str[22];
		}
		else{

			//会員情報取得
			$password=to_hash($input_data[password]);
			//echo $password;die;
			//$query  = "select * from sf_member where email='{$input_data[email]}' and password='{$password}' and del_flg=0";
			/*
			$query  = "select * from sf_member where email='{$input_data[email]}' and del_flg=0";
			$ret = mysql_query("set names utf8");
			$ret = mysql_query($query, $connect);
			$user_data   = mysql_fetch_array($ret);
			*/
			$tmp=$commonDao->get_data_tbl("sf_member",array("email","del_flg"),array($input_data[email],0));
			$user_data=$tmp[0];
			$ret=Bcrypt::check($input_data[password],$user_data[password]);

			if($ret!="1"){
				$errRet[top]=$err_msg_str[23];
			}
			else{
				if($user_data[status]==0){
					$errRet[top]=$err_msg_str[24];
				}
				else if($user_data[status]==9){
					$errRet[top]=$err_msg_str[25];
				}
				else{
					//ログイン成功
					setMemberSession($user_data);
					session_regenerate_id();
					mysql_close($connect);

					header("location:{$pagelink_index}");
					exit;
				}

			}
		}
	}

	elseif($_GET[type]=="f"){
		$redirect_url=ROOT_URL."ja/".$pagelink_login_facebook."?lang={$lang}";
		//Facebookでログイン
		$facebookConfig[appId]=FACEBOOK_APPID;
		$facebookConfig[secret]=FACEBOOK_SECRET;
		$facebookConfig[redirect]=$redirect_url;
		$facebook = new Facebook($facebookConfig);
		$authURL = $facebook->getLoginUrl (array ('scope' => 'email'
                                                  , 'redirect_uri' => $facebookConfig[redirect]));

 		header ("Location: " . $authURL);
		exit;
	}elseif($_GET[type]=="f0"){
		$errRet=getMsgSession();
		deleteMsgSession();
	}elseif($_GET[type]=="t"){
		//ツイッターでログイン
		$redirect_url=$pagelink_login."?type=tr";
		//ツイッターでログイン
		// request token取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

		$token = $tw->getRequestToken($redirect_url);
		if(! isset($token['oauth_token'])){
		    echo "error: getRequestToken\n";
		    exit;
		}

		// callback.php で使うので session に突っ込む
		$_SESSION['tw_oauth_token']        = $token['oauth_token'];
		$_SESSION['tw_oauth_token_secret'] = $token['oauth_token_secret'];

		// 認証用URL取得してredirect
		$authURL = $tw->getAuthorizeURL($_SESSION['tw_oauth_token'],true);
		header("Location: " . $authURL);

	}elseif($_GET[type]=="tr"){
		// getToken.php でセットした oauth_token と一致するかチェック
		if ($_SESSION['tw_oauth_token'] !== $_REQUEST['oauth_token']) {
		    unset($_SESSION);
			header("Location:  {$pagelink_error}?msgFlg=99");
	  	  	exit;
		 }

		// access token 取得
		$tw = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET,
		    $_SESSION['tw_oauth_token'], $_SESSION['tw_oauth_token_secret']);
		$access_token = $tw->getAccessToken($_REQUEST['oauth_verifier']);

		//print_r_with_pre($access_token);

		// Twitter の user_id + screen_name(表示名)
		$user_id     = $access_token['user_id'];
		$screen_name = $access_token['screen_name'];

		//登録済みか確認
		//DB接続
	 	//$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
		//IDチェック
  		$query  = " select * from sf_member where twitter_id = '{$user_id}' ";
		$ret = mysql_query("set names utf8");
		$ret = mysql_query($query, $connect);
		$user_data   = mysql_fetch_array($ret);
		if($user_data[user_no]){
			if($user_data[status]==1){
				//ログインOK
				setMemberSession($user_data);
				mysql_close($connect);
				unset($_SESSION['tw_oauth_token']);
				unset($_SESSION['tw_oauth_token_secret']);
				header("location:{$pagelink_index}");
				exit;
			}else{
				$errRet[top]=$err_msg_str[24];

			}
		}else{
			$errRet[top]=$err_msg_str[27];
		}

		unset($_SESSION['tw_oauth_token']);
		unset($_SESSION['tw_oauth_token_secret']);
	}

	//print_r_with_pre($errRet);
	mysql_close($connect);


	$smarty->assign("err_msg", $errRet);
	$smarty->assign("input_data", $input_data);

?>