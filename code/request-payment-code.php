<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);

	$commonDao = new CommonDao();
	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	$input_data=$_POST;

	//登録処理
	date_default_timezone_set("Asia/Tokyo");
	$PayType=$_POST[PayType];//3:コンビニ 4:Pay-easy
	$ShopID=$_POST[ShopID];
	$ShopPass=$_POST[ShopPass];
	$AccessID=$_POST[AccessID];
	$AccessPass=$_POST[AccessPass];
	$OrderID=$_POST[OrderID];
	$Status=$_POST[Status];//UNPROCESSED:未決済 REQSUCCESS:要求成功 PAYSUCCESS:決済完了 PAYFAIL:決済失敗 EXPIRED:期限切れ
	$Amount=$_POST[Amount];
	$Currency=$_POST[Currency];
	$TranID=$_POST[TranID];
	$TranDate=$_POST[TranDate];
	$ErrCode=$_POST[ErrCode];
	$ErrInfo=$_POST[ErrInfo];
	$CvsCode=$_POST[CvsCode];
	$CvsConfNo=$_POST[CvsConfNo];
	$CvsReceiptNo=$_POST[CvsReceiptNo];
	$PaymentTerm=$_POST[PaymentTerm];
	$FinishDate=$_POST[FinishDate];
	$ReceiptDate=$_POST[ReceiptDate];

	//オーダー情報の取得
	if($OrderID){
		$order_info=getOrderInfo($connect,$OrderID);

		$project_no=$order_info[project_no];
		$present_no=$order_info[present_no];
		$member_id=$order_info[member_id];
	}
	//支援者情報の取得
	if($member_id){
		$supporter_info=getMemberInfo($connect,$member_id);
	}
	//プロジェクト情報の取得
	if($project_no){
		$project_info=getProjectInfo($connect,$project_no);
		$now_summary=$project_info[now_summary];
		$now_supporter=$project_info[now_supporter];
		$user_no=$project_info[user_no];

	}
	//選択した支援情報の取得
	if($present_no){
		$selected_present_info=getPresentInfo($connect,$present_no);

	}
	//起案者情報の取得
	if($user_no){
		$drafter_info=getMemberInfo($connect,$user_no);

	}
	$data_invest=array();
	if($Status=="PAYSUCCESS"){
		$data_invest[status]=1;
	}else{
		$data_invest[status]=0;
	}

	$data_invest[paytype]=$_POST[PayType];
	$where_invest[order_id]=$_POST[OrderID];
	$sql_invest=$commonDao->MakeUpdateSQL("sf_invest",$data_invest,$where_invest);

	$result_invest = mysql_query("set names utf8");
	$result_invest = mysql_query($sql_invest, $connect);

	//
	$data_project=array();
	if($Status=="PAYSUCCESS"){
		$data_project[now_summary]=$now_summary+$total_amount;
		$data_project[now_supporter]=$now_supporter+1;
		$where_project[no]=$project_no;
		$sql_project=$commonDao->MakeUpdateSQL("sf_project",$data_project,$where_project);

		$result_project = mysql_query("set names utf8");
		$result_project = mysql_query($sql_project, $connect);

		//支援者コメント
		$data_c[del_flg]="0";

		$where_c[project_no]=$project_no;
		$where_c[member_id]=$member_id;
		$where_c[order_id]=$OrderID;
		$sql_c=$commonDao->MakeUpdateSQL("sf_comment",$data_c,$where_c);

		$result_c = mysql_query("set names utf8");
		$result_c = mysql_query($sql_c, $connect);

		//メール送信
		$smarty->assign("C_INQUIRY_EMAIL", $C_INQUIRY_EMAIL);
		$smarty->assign("d_data", $drafter_info);
		$smarty->assign("m_data", $supporter_info);
		$smarty->assign("p_data", $project_info);
		$smarty->assign("total_amount", $Amount);
		//支援者へ
		//$mail_subject=getMailSubject(10);
		$subject = "【A-port】銀行振込の入金を確認いたしました。";
		$mailBody = $smarty->fetch($lang."/mail/m-invest-project-charge-ok.tpl");
		send_mail($supporter_info[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

		//起案者へ
		//$mail_subject=getMailSubject(10);
		$subject = "【A-port】「".$project_info[public_title]."」の支援者が増えました";
		$mailBody = $smarty->fetch($lang."/mail/m-project-new-invest.tpl");
		send_mail($drafter_info['email'], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
	}
	//
	if ($selected_present_info[invest_limit]!="9999"){
		$data_present=array();
		if($Status=="EXPIRED"||$Status=="CANCEL"){
			$data_present[invest_limit]=$selected_present_info[invest_limit]+1;
			$where_present[project_no]=$project_no;
			$where_present[present_no]=$present_no;
			$sql_present=$commonDao->MakeUpdateSQL("sf_prj_present",$data_present,$where_present);
			$result_present = mysql_query("set names utf8");
			$result_present = mysql_query($sql_present, $connect);
		}
	}

//	if( $result === true ){
		//0：受信OK1：受信失敗
//		echo 0;
//	}else{
//		echo 1;
//	}

	mysql_close($connect);

	//0：受信OK1：受信失敗
	echo 0;
?>