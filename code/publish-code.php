<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	require '../../libs/Bcrypt.class.php';
	session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();

	//ログインチェック
	if(!$current_user){
		header("Location:  {$pagelink_index}");
		exit;
	}

	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	$project_no = $_GET['p_no'];
	if($project_no!=""){
		if(!is_numeric($project_no)){
			header("Location:  {$pagelink_error}");
			exit;
		}
	}else{
			header("Location:  {$pagelink_error}");
			exit;
	}
	//会員情報取得
	$member_info=getMemberInfo($connect,$current_user['user_no']);
	//Project情報取得
	$project_info=getMyProjectInfo($connect,$project_no,$current_user['user_no']);
	$project_no = $project_info['no'];
	if($project_no!=""){
		if(!is_numeric($project_no)){
			header("Location:  {$pagelink_error}");
			exit;
		}
	}else{
			header("Location:  {$pagelink_error}");
			exit;
	}
	//print_r_with_pre($project_info);
	if(isset($_POST[sbm_update])){
	}
	else{

	}

  // COMMENT
  $query_comment  = " select a.* ,v.*, p.* ,p.no as p_no,a.comment as u_comment,m.*,ps.title as ps_title,p.user_no as p_user_no, ";
  $query_comment .= " (select count(1) from sf_message ms where 1 and ms.from_user_id=a.member_id and ms.status='0' and ms.project_no=a.project_no) as unread_cnt ";
  $query_comment .= "   from sf_comment a ";
  $query_comment .= "  inner join sf_project p ";
  $query_comment .= "     on a.project_no = p.no ";
  $query_comment .= "  inner join sf_invest v ";
  $query_comment .= "     on a.order_id = v.order_id ";
  $query_comment .= "  inner join sf_member m ";
  $query_comment .= "     on a.member_id = m.user_no ";
  $query_comment .= "  inner join sf_prj_present ps ";
  $query_comment .= "     on v.present_no = ps.present_no ";
  $query_comment .= "  where 1 ";
  $query_comment .= "    and p.no = %d";
  $query_comment .= "    and a.order_id <> '' ";
  $query_comment .= "    and a.del_flg = '0'";
  $query_comment .= "  order by a.no desc ";
	$query_comment = sprintf($query_comment,$project_no);

  $result_comment = mysql_query("set names utf8");
  $result_comment = mysql_query($query_comment, $connect);

  $comment_list=array();
  while($data = mysql_fetch_array($result_comment)){
		if($data[p_user_no]!=$current_user['user_no']){
				header("Location:  {$pagelink_error}");
				exit;
		}
	  $comment_list[]=$data;
  }

	//print_r_with_pre($comment_list);

	mysql_close($connect);

	//ログイン情報
	$smarty->assign("current_user", $current_user);
	//都道府県リスト
	$smarty->assign("array_area", $array_area);
	//エラー情報
	$smarty->assign("err_msg", $errRet);
	//入力情報
	$smarty->assign("input_data", $input_data);
	//COMMENT情報
	$smarty->assign("comment_list",$comment_list);
	//Project情報
	$smarty->assign("project_info",$project_info);
	//起案者情報
	$smarty->assign("member_info",$member_info);
?>