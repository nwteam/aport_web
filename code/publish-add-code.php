<?php

	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	require_once sprintf("%s/dao/ProjectDao.class.php", MODEL_PATH);
	session_cache_limiter('no-cache, must-revalidate');

	$commonDao = new CommonDao();
	$projectDao = new ProjectDao();

	$current_user = getMemberSession();

	//ログインチェック
	if(!$current_user){header("Location:  {$pagelink_index}");exit;}

	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	$project_no = $_GET['p_no'];
	if($project_no!=""){if(!is_numeric($project_no)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}

	$update_id = $_GET['m_id'];
	if($update_id!=""){if(!is_numeric($update_id)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}
	$input_data['to_user_id']=$update_id;

	$msg_id = $_GET['ms_id'];
	if($msg_id!=""){if(!is_numeric($msg_id)){header("Location:  {$pagelink_error}");exit;}}
	$input_data['msg_id']=$msg_id;

	//送信元会員情報取得
	$member_info=getMemberInfo($connect,$current_user['user_no']);
	//宛先会員情報取得
	$supporter_info=getMemberInfo($connect,$update_id);

	//Project情報取得
	$project_info=getMyProjectInfo($connect,$project_no,$current_user['user_no']);
	$project_no = $project_info['no'];
	if($project_no!=""){if(!is_numeric($project_no)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}

	//print_r_with_pre($project_info);
	if(isset($_POST[sbm_update])){
		$post=arr_preg_change($_POST);
		$input_data=$post;

		$input_data['message_body']=html_tag_chg($input_data['message_body']);

		//会員情報
		$member_info=getMemberInfo($connect,$current_user['user_no']);

		//---------------- 入力チェック ---------------------------
		//基本事項
		$baseData=CommonChkArray::$mailContentCheckData;

		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet=check($input_data,$baseData,$err_msg_str);

		//-------------- ここまで -----------------------------------
		if(count($errRet)==0){
			//print_r_with_pre($input_data);die;
			/*
			foreach($baseData[dbstring] as $key=>$val){
				$dkey[]=$key;
				$dval[]=$input_data[$key];
			}
			*/

			if ($input_data['to_user_id']=="all"){
				// COMMENT
				$query_comment  = " select a.* ,v.*, p.* ,p.no as p_no,a.comment as u_comment,m.*,ps.title as ps_title,p.user_no as p_user_no, ";
				$query_comment .= " (select count(1) from sf_message ms where 1 and ms.from_user_id=a.member_id and ms.status='0' and ms.project_no=a.project_no) as unread_cnt ";
				$query_comment .= "   from sf_comment a ";
				$query_comment .= "  inner join sf_project p ";
				$query_comment .= "     on a.project_no = p.no ";
				$query_comment .= "  inner join sf_invest v ";
				$query_comment .= "     on a.order_id = v.order_id ";
				$query_comment .= "  inner join sf_member m ";
				$query_comment .= "     on a.member_id = m.user_no ";
				$query_comment .= "  inner join sf_prj_present ps ";
				$query_comment .= "     on v.present_no = ps.present_no ";
				$query_comment .= "  where 1 ";
				$query_comment .= "    and p.no = %d";
				$query_comment .= "    and a.order_id <> '' ";
				$query_comment .= "    and a.del_flg = '0'";
				$query_comment .= "  order by a.no desc ";
				$query_comment = sprintf($query_comment,$project_no);

				$result_comment = mysql_query("set names utf8");
				$result_comment = mysql_query($query_comment, $connect);

				$comment_list=array();
				while($data = mysql_fetch_array($result_comment)){
					if($data[p_user_no]!=$current_user['user_no']){
							header("Location:  {$pagelink_error}");
							exit;
					}
					$comment_list[]=$data;
				}
				//print_r_with_pre($comment_list);die;
				foreach($comment_list as $key=>$val) {
					$update_id = $val['user_no'];
					if($update_id!=""){if(!is_numeric($update_id)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}
					//宛先会員情報取得
					$supporter_info=getMemberInfo($connect,$update_id);
					$ikey=array();
					$ival=array();
					$ikey[]="project_no";
					$ival[]=$project_no;
					$ikey[]="from_user_id";
					$ival[]=$current_user['user_no'];
					$ikey[]="to_user_id";
					$ival[]=$update_id;
					$ikey[]="status";
					$ival[]="0";//未処理
					$ikey[]="message_title";
					$ival[]=$input_data['message_title'];
					$ikey[]="message_body";
					$ival[]=$input_data['message_body'];
					$ikey[]="create_date";
					$ival[]=date("Y-m-d H:i:s");

					$ret=$commonDao->InsertItemData("sf_message",$ikey,$ival);
					if($ret){
						$dkey[]="status";
						$dval[]="1";
						$ret=$projectDao->upItemData8($dkey,$dval,"from_user_id",$update_id,$input_data);
						if($ret){
							//メール送信
							$smarty->assign("m_data", $member_info);
							$smarty->assign("s_data", $supporter_info);
							$smarty->assign("p_data", $project_info);
							$message_body=trim($input_data['message_body']);
							$message_body=htmlspecialchars(addslashes($message_body));
							$smarty->assign("message_body", $message_body);
							//支援者へ
							$subject = "【A-port】「".$project_info['public_title']."」よりメールが送られました";
							$mailBody = $smarty->fetch($lang."/mail/m-publish-add.tpl");
							send_mail($supporter_info['email'], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
						}
						else{
							//アップデータ情報の保存エラーです
							$errRet[top]=$err_msg_str[80];
						}
					}else{
							//アップデータ情報の保存エラーです
							$errRet[top]=$err_msg_str[80];
					}
				}
			}else{
				$msg_id = $input_data['msg_id'];
				$update_id = $input_data['to_user_id'];
				if($msg_id!=""){if(!is_numeric($msg_id)){header("Location:  {$pagelink_error}");exit;}}
				if($update_id!=""){if(!is_numeric($update_id)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}
				//宛先会員情報取得
				$supporter_info=getMemberInfo($connect,$update_id);

				$ikey[]="project_no";
				$ival[]=$project_no;
				$ikey[]="from_user_id";
				$ival[]=$current_user['user_no'];
				$ikey[]="to_user_id";
				$ival[]=$update_id;
				$ikey[]="status";
				$ival[]="0";//未処理
				$ikey[]="message_title";
				$ival[]=$input_data['message_title'];
				$ikey[]="message_body";
				$ival[]=$input_data['message_body'];
				$ikey[]="create_date";
				$ival[]=date("Y-m-d H:i:s");

				$ret=$commonDao->InsertItemData("sf_message",$ikey,$ival);
				if($ret){
					if($msg_id!=""){
						$dkey[]="status";
						$dval[]="1";
						$ret=$projectDao->upItemData8($dkey,$dval,"no",$msg_id,$input_data);
						if($ret){
							//メール送信
							$smarty->assign("m_data", $member_info);
							$smarty->assign("s_data", $supporter_info);
							$smarty->assign("p_data", $project_info);
							$message_body=trim($input_data['message_body']);
							$message_body=htmlspecialchars(addslashes($message_body));
							$smarty->assign("message_body", $message_body);
							//支援者へ
							//$mail_subject=getMailSubject(10);
							$subject = "【A-port】「".$project_info['public_title']."」よりメールが送られました";
							$mailBody = $smarty->fetch($lang."/mail/m-publish-add.tpl");
							send_mail($supporter_info['email'], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
						}
						else{
							//アップデータ情報の保存エラーです
							$errRet[top]=$err_msg_str[80];
						}
					}
				}else{
						//アップデータ情報の保存エラーです
						$errRet[top]=$err_msg_str[80];
				}
			}
			if($ret){
			}
			else{
				//アップデータ情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}else{
			$errRet[top]=$err_msg_str[99];
		}
		
		if(count($errRet)==0){
			//確認ページへ遷移
			header("Location:  {$pagelink_publish_mail}?p_no={$project_no}");
		}else{
			$errRet[top]=$err_msg_str[99];
		}
	}
	elseif(isset($_POST[sbm_back])){
			header("Location:  {$pagelink_publish_mail}?p_no={$project_no}");
	}
	else{

	}

	mysql_close($connect);

	//ログイン情報
	$smarty->assign("current_user", $current_user);
	//都道府県リスト
	$smarty->assign("array_area", $array_area);
	//エラー情報
	$smarty->assign("err_msg", $errRet);
	//入力情報
	$smarty->assign("input_data", $input_data);
	//Project情報
	$smarty->assign("project_info",$project_info);
	//起案者情報
	$smarty->assign("member_info",$member_info);
	//宛先情報
	$smarty->assign("supporter_info",$supporter_info);
?>