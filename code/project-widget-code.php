<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();

	//ログインチェック
if(!$current_user){header("Location: {$pagelink_login}");exit;}

$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

$m_hope_url = trim($_GET['hope_url']);
$p_no_arr=getProjectNoByHopeURL($connect,$m_hope_url);
$m_project_no = $p_no_arr[project_no];

//プロジェクト情報の取得
$result_Checked=getProjectInfoWithOutFetch($connect,$m_project_no);
$project_data_list=array();
while($project_list_info_Checked = mysql_fetch_array($result_Checked)){
	//達成率
	if ($project_list_info_Checked['wish_price']=="0"){
		$project_list_info_Checked['percent'] = 0;
	}else{
		$project_list_info_Checked['percent'] = round(($project_list_info_Checked['now_summary'] / $project_list_info_Checked['wish_price']) * 100) > 100 ? 100 : round(($project_list_info_Checked['now_summary'] / $project_list_info_Checked['wish_price']) * 100);
	}
	//残り時間
	if($project_list_info_Checked[status]==1){
		$date1=strtotime($project_list_info_Checked['invest_limit']);
		$now_date = date("Y-m-d");
		$date2=strtotime($now_date);
		if($date2>$date1){
			$project_list_info_Checked['diff_in_days'] = "-";
			$project_list_info_Checked[in_invest_flg]=0;
		}else{
			$total_secs = ($date1 - $date2);
			$project_list_info_Checked['diff_in_days'] = floor($total_secs / 86400);
			$project_list_info_Checked[in_invest_flg]=1;
		}
	}else{
		$project_list_info_Checked['diff_in_days'] = "-";
		$project_list_info_Checked[in_invest_flg]=0;
	}
	//説明
	$project_list_info_Checked['project_text']=str_replace("<br />", " ", htmlspecialchars_decode($project_list_info_Checked['project_text'],ENT_QUOTES));
	$project_data_list[]=$project_list_info_Checked;
}
//print_r_with_pre($project_data_list);die;

//会員情報をinput_dataに格納
$member_info=getMemberInfo($connect,$current_user['user_no']);
$input_data['profile_img']=$member_info['profile_img'];
$input_data['member_name']=$member_info['member_name'];
$input_data['add_1']=$member_info['add_1'];
$input_data['hp_url']=$member_info['hp_url'];
$input_data['profile']=$member_info['profile'];

mysql_close($connect);

//Credit　Clientipのセット
if($lang=="ja"){
	$clientip=CDREDIX_CLIENTIP_JA;
}else{
	$clientip=CDREDIX_CLIENTIP_OTHER;
}

//Clientip情報
$smarty->assign("clientip", $clientip);
//ログイン情報
$smarty->assign("current_user", $current_user);
$smarty->assign("share_cnt", $share_cnt);
// カテゴリー
$smarty->assign("array_category", $array_category);
//プロジェクト情報
$smarty->assign("project_data_list", $project_data_list);
//都道府県リスト
$smarty->assign("array_area", $array_area);
//エラー情報
$smarty->assign("err_msg", $errRet);
//入力情報
$smarty->assign("input_data", $input_data);

?>