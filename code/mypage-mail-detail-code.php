<?php

	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	require_once sprintf("%s/dao/ProjectDao.class.php", MODEL_PATH);
	session_cache_limiter('no-cache, must-revalidate');

	$commonDao = new CommonDao();
	$projectDao = new ProjectDao();

	$current_user = getMemberSession();

	//ログインチェック
	if(!$current_user){header("Location:  {$pagelink_index}");exit;}

	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	$project_no = $_GET['p_no'];
	if($project_no!=""){if(!is_numeric($project_no)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}

	$update_id = $_GET['m_id'];
	if($update_id!=""){if(!is_numeric($update_id)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}
	$input_data['to_user_id']=$update_id;
	//宛先会員情報取得
	$member_info=getMemberInfo($connect,$current_user['user_no']);
	$input_data['profile_img']=$member_info['profile_img'];
	$input_data['member_name']=$member_info['member_name'];
	$input_data['add_1']=$member_info['add_1'];
	$input_data['hp_url']=$member_info['hp_url'];
	$input_data['profile']=$member_info['profile'];
	//送信元会員情報取得
	$maker_info=getMemberInfo($connect,$update_id);

	//Project情報取得
	$project_info=getMyProjectInfo($connect,$project_no,$update_id);
	$project_no = $project_info['no'];
	$hope_url = $project_info['hope_url'];
	if($project_no!=""){if(!is_numeric($project_no)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}

	//print_r_with_pre($project_info);
	if(isset($_POST[sbm_update])){
		$post=arr_preg_change($_POST);
		$input_data=$post;

		$message_body=html_tag_chg($input_data['message_body']);

		//会員情報
		$member_info=getMemberInfo($connect,$current_user['user_no']);
		$input_data['profile_img']=$member_info['profile_img'];
		$input_data['member_name']=$member_info['member_name'];
		$input_data['add_1']=$member_info['add_1'];
		$input_data['hp_url']=$member_info['hp_url'];
		$input_data['profile']=$member_info['profile'];

		//---------------- 入力チェック ---------------------------
		//基本事項
		$baseData=CommonChkArray::$questionMailContentCheckData;

		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet=check($input_data,$baseData,$err_msg_str);

		//-------------- ここまで -----------------------------------
		if(count($errRet)==0){
			//print_r_with_pre($input_data);die;

			$update_id = $input_data['to_user_id'];
			if($update_id!=""){if(!is_numeric($update_id)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}
			//宛先会員情報取得
			$maker_info=getMemberInfo($connect,$update_id);
			//Project情報取得
			$project_info=getMyProjectInfo($connect,$project_no,$update_id);
			$project_no = $project_info['no'];
			$project_name = $project_info['public_title'];
			if($project_no!=""){if(!is_numeric($project_no)){header("Location:  {$pagelink_error}");exit;}}else{header("Location:  {$pagelink_error}");exit;}

			$ikey[]="project_no";
			$ival[]=$project_no;
			$ikey[]="from_user_id";
			$ival[]=$current_user['user_no'];
			$ikey[]="to_user_id";
			$ival[]=$update_id;
			$ikey[]="status";
			$ival[]="0";//未処理
			$ikey[]="message_title";
			$ival[]="「".$project_name."」"."の質問・意見メール";
			$ikey[]="message_body";
			$ival[]=$message_body;
			$ikey[]="create_date";
			$ival[]=date("Y-m-d H:i:s");

			$ret=$commonDao->InsertItemData("sf_message",$ikey,$ival);
			if($ret){
			}
			else{
				//アップデータ情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}else{
			$errRet[top]=$err_msg_str[99];
		}
		
		if(count($errRet)==0){
			//メール送信
			$smarty->assign("m_data", $member_info);
			$smarty->assign("p_data", $project_info);
			$message_body=trim($message_body);
			$message_body=htmlspecialchars(addslashes($message_body));
			$smarty->assign("message_body", $message_body);
			//起案者へ
			//$mail_subject=getMailSubject(10);
			$subject = "【A-port】「".$project_info['public_title']."」よりメールが送られました";
			$mailBody = $smarty->fetch($lang."/mail/m-mypage-sendmsg.tpl");
			send_mail($maker_info['email'], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
			//確認ページへ遷移
			header("Location:  {$pagelink_mypage_sendmailbox}");
		}else{
			$errRet[top]=$err_msg_str[99];
		}
	}
	elseif(isset($_POST[sbm_back])){
			header("Location:  {$pagelink_detail_project}{$hope_url}");
	}
	else{

	}

	mysql_close($connect);

	//ログイン情報
	$smarty->assign("current_user", $current_user);
	//都道府県リスト
	$smarty->assign("array_area", $array_area);
	//エラー情報
	$smarty->assign("err_msg", $errRet);
	//入力情報
	$smarty->assign("input_data", $input_data);
	//Project情報
	$smarty->assign("project_info",$project_info);
	//起案者情報
	$smarty->assign("member_info",$member_info);
	//宛先情報
	$smarty->assign("maker_info",$maker_info);

?>