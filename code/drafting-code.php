<?php
header('Content-Type: text/html; charset=utf-8');
require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
require_once sprintf("%s/dao/ProjectDao.class.php", MODEL_PATH);

$commonDao = new CommonDao();
$projectDao = new ProjectDao();

$current_user = getMemberSession();
$nav_tab = trim($_REQUEST['nav_tab']);

//ログインチェック
if(!$current_user){header("Location: {$pagelink_login}");exit;}

$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

if(($_POST[sbm_update]=="")&&($_POST[preview]=="")){
	//セッションクリア
	$_SESSION[TMP_FUP11]="";
	$_SESSION[TMP_FUP12]="";
	$_SESSION[TMP_FUP13]="";
	for($p=1;$p<31;$p++){
		$_SESSION[TMP_FUP][$p]="";
	}
}

$project_no = $_GET['p_no'];
//$preview_flg = trim($_GET['prv']);
if($project_no!=""){
	if(!is_numeric($project_no)){
		header("Location:  {$pagelink_error}");
		exit;
	}
}
if($input_data['no']!=""){
	if(!is_numeric($input_data['no'])){
		header("Location:  {$pagelink_error}");
		exit;
	}
}
if (!isset($project_no) || $project_no == "") {
	echo("<script>document.location.href='".$pagelink_index."';</script>");
	exit;
}

//プロジェクト情報の取得
$project_info=getMyProjectInfo($connect,$project_no,$current_user['user_no']);
//echo $project_info[chk_status];
//echo $project_info[status];
if(!is_array($project_info)){
	header("Location:  {$pagelink_error}");
	exit;
}elseif($project_info[chk_status]=="3"&&$project_info[status]=="1"){//審査完了,掲載中
	header("Location:  {$pagelink_detail_project}{$project_info[hope_url]}");
	exit;
}elseif($project_info[chk_status]=="3"&&$project_info[status]=="3"){//審査完了,達成
	header("Location:  {$pagelink_detail_project}{$project_info[hope_url]}");
	exit;
}elseif($project_info[chk_status]=="3"&&$project_info[status]=="9"){//未達成,審査完了
	header("Location:  {$pagelink_detail_project}{$project_info[hope_url]}");
	exit;
}elseif($project_info[chk_status]=="1"){//審査待
	header("Location:  {$pagelink_mypage_myprojects}");
	exit;
}elseif($project_info[chk_status]=="3"){//審査完了
	header("Location:  {$pagelink_mypage_myprojects}");
	exit;
}else{
	$project_info=arr_preg_change($project_info);
}

if ($project_info) {
	//プロジェクト詳細のデコード
	$project_info['project_text']=htmlspecialchars_decode($project_info['project_text']);

	//お礼メッセージのデコード
	$project_info['thanks_msg']=htmlspecialchars_decode($project_info['thanks_msg']);
}

//プロジェクト情報をinput_dataに格納
$input_data=$project_info;
//該当プロジェクトの支援コース情報の取得
$ret=getProjectPresentInfo($connect,$project_no);
$present_info=array();
$k=1;
while($project_present_info = mysql_fetch_array($ret)){
	//支援者数の取得
	$invest_cnt=getPresentCntInfo($connect,$project_no,$project_present_info[present_no]);

	$project_present_info[invest_cnt]=$invest_cnt[cnt];
	//支援コース情報
	$present_info[$k]=preg_change($project_present_info);
	//print_r_with_pre($project_present_info);
	//print_r_with_pre($present_info);
	//支援コース情報をinput_dataに格納
	$input_data['return_min'][$k]=preg_change($project_present_info['min']);
	$input_data['max_count'][$k]=preg_change($project_present_info['invest_limit']);
	$input_data['return_title'][$k]=preg_change($project_present_info['title']);
	//リターン内容のデコード
	$input_data['return_text'][$k]=preg_change(htmlspecialchars_decode($project_present_info['text']));
	$input_data['return_img'][$k]=preg_change($project_present_info['return_img']);
	$input_data['return_post_flg'][$k]=preg_change($project_present_info['return_post_flg']);
	$input_data['return_year'][$k]=preg_change($project_present_info['return_year']);
	$input_data['return_month'][$k]=preg_change($project_present_info['return_month']);

	$k++;
}
//print_r_with_pre($input_data);
//$present_info['text']=htmlspecialchars_decode($present_info['text']);
//会員情報をinput_dataに格納
$member_info=arr_preg_change(getMemberInfo($connect,$current_user['user_no']));
$input_data['profile_img']=$member_info['profile_img'];
$input_data['hp_url']=$member_info['hp_url'];
$input_data['add_1']=$member_info['add_1'];
$input_data['full_name']=$member_info['full_name'];
$input_data['introduce']=htmlspecialchars_decode($member_info['profile']);
$input_data['bank_name']=$member_info['bank_name'];
//$input_data['bank_code']=$member_info['bank_code'];
$input_data['branch_bank']=$member_info['branch_bank'];
$input_data['branch_bank_code']=$member_info['branch_bank_code'];
$input_data['card_type']=$member_info['card_type'];
$input_data['card_number']=$member_info['card_number'];
$input_data['card_owner']=$member_info['card_owner'];

if($input_data['movie_type']=="1"){
 $ext = explode("youtu.be/",$input_data['movie_url']);
 $youtube_id=$ext[1];
 if($youtube_id==""){
	$ext = explode("youtube.com/embed/",$input_data['movie_url']);
	$youtube_id=$ext[1];
 }
 if($youtube_id==""){
	 $ext = explode("watch?v=",$input_data['movie_url']);
	 $youtube_id=$ext[1];
 }
}
if($input_data['movie_type']=="2"){
 $ext = explode("vimeo.com/",$input_data['movie_url']);
 $vimemo_id=$ext[1];
 if($vimemo_id==""){
	$ext = explode("vimeo.com/video/",$input_data['movie_url']);
	$vimemo_id=$ext[1];
 }
}

//print_r_with_pre($present_info);die;
//print_r_with_pre($member_info);

if(isset($_POST[sbm_update])){	//送信ボタンクリック時
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
	//print_r_with_pre($_POST);die;
	//$project_no=$_POST['no'];

	$input_data[user_no]=intval($current_user['user_no']);
//	$input_data['no']=intval($input_data['no']);
if(!is_numeric($input_data['no'])){
	header("Location:  {$pagelink_error}");
	exit;
}
	if(count($errRet)==0){
		foreach($baseData[dbstring] as $key=>$val){
			$dkey[]=$key;
			$dval[]=$input_data[$key];
		}

		//print_r_with_pre($input_data);die;
		if($input_data['no']){//修正
			$dkey[]="status";
			$dval[]=0;//掲載前
			$dkey[]="chk_status";
			$dval[]=1;//審査待
			$dkey[]="user_no";
			$dval[]=$current_user['user_no'];//ユーザーNO

			$ret=$projectDao->upItemData6($dkey,$dval,"no",$input_data['no'],$input_data);
			if($ret){

				//$member_info=getMemberInfo($connect,$current_user['user_no']);
				//メール送信
				$smarty->assign("C_INQUIRY_EMAIL", $C_INQUIRY_EMAIL);
				$smarty->assign("m_data", $member_info);
				$smarty->assign("p_data", $project_info);
				//起案者へ
				$mail_subject=getMailSubject(10);
				$subject = $mail_subject;
				$mailBody = $smarty->fetch($lang."/mail/m-project-check.tpl");
				//send_mail($C_ADMIN_EMAIL, $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
				send_mail($m_data['email'], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
				header("Location:  {$pagelink_mypage_myprojects}");
				//A-port事務局へ
				$mail_subject=getMailSubject(11);
				$subject = $mail_subject;
				$mailBody = $smarty->fetch($lang."/mail/m-project-wait-for-check.tpl");
				send_mail($C_ADMIN_EMAIL, $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
				//send_mail("fivbak@gmail.com", $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
				header("Location:  {$pagelink_mypage_myprojects}#wait_for_check");
			}
			else{
				//プロジェクト基本情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}
	}else{
		$errRet[top]=$err_msg_str[99];
	}
}
elseif(isset($_POST[d1])){
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
	//print_r_with_pre($_POST);die;
	//print_r_with_pre($input_data);die;
	$input_data['wish_price']=GetAlabNum($_POST['wish_price']);
//	$input_data['no']=intval($input_data['no']);
if(!is_numeric($input_data['no'])){
	header("Location:  {$pagelink_error}");
	exit;
}
	//---------------- 入力チェック ---------------------------
	//基本事項
	$baseData=CommonChkArray::$drafting1CheckData;

	$err_msg_str=CommonMessageArray::$input_err_msg;
	$errRet=check($input_data,$baseData,$err_msg_str);

	//期間設定チェック
	$startdate=$input_data['project_record_date'];
	$enddate=$input_data['invest_limit'];
	$check_date=floor((strtotime($enddate)-strtotime($startdate))/86400);
	if ($input_data['project_type']=="1"){
		if($check_date>80){
				$errRet[project_record_date]=$err_msg_str[81];//"最大80日間に設定してください。"
		}
	}else{
		if($check_date>120){
				$errRet[project_record_date]=$err_msg_str[82];//"最大120日間に設定してください。"
		}
	}
	//動画のURLチェック
	if ($input_data['movie_type']!="0"){
		if($input_data['movie_url']==""){
				$errRet[movie_url]=$err_msg_str[83];//"動画のURLを記入してください。"
		}
	}
	//希望URL重複チェック
	$hope_url_cnt=getHopeUrlCnt($connect,$input_data['hope_url'],$project_no);
	if ($hope_url_cnt[cnt]!="0"){
		$errRet[hope_url]=$err_msg_str[85];//"ご希望のURLが既に存在しました。"
	}
	//サムネイルファイルアップ
	if($_FILES["up_file1"]["name"]!=""){if(!in_array($_FILES["up_file1"]["type"],$array_type)){$errRet[up_file1]=$err_msg_str[87];}}
	//list($width,$height) = getimagesize($_FILES["up_file1"]["name"]);
	//echo list(2);
	if(count($errRet)==0){
		if(is_uploaded_file($_FILES["up_file1"]["tmp_name"])){
				$temp_up_fname = $i.date("His",time())."sn_".$_FILES["up_file1"]["name"];//
				$temp_up_fname=SBC_DBC($temp_up_fname,0);
				$temp_up_fname=html_tag_chg($temp_up_fname);
				$_SESSION[TMP_FUP11]=$temp_up_fname;
				//最初は仮フォルダに入れておく
				$ret_crop_1=make_crop($_FILES["up_file1"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname,608,435,2);
				//copy($_FILES["up_file1"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);
				$input_data[new_cover_img]=$temp_up_fname; 
		}elseif($_SESSION[TMP_FUP11]!=""){
			$input_data[new_cover_img]=$_SESSION[TMP_FUP11];
		}
	}

	//お礼画像
	if($_FILES["up_file2"]["name"]!=""){if(!in_array($_FILES["up_file2"]["type"],$array_type)){$errRet[up_file2]=$err_msg_str[87];}}
	if(count($errRet)==0){
		if(is_uploaded_file($_FILES["up_file2"]["tmp_name"])){
				$temp_up_fname = $i.date("His",time())."ti_".$_FILES["up_file2"]["name"];//
				$temp_up_fname=SBC_DBC($temp_up_fname,0);
				$temp_up_fname=html_tag_chg($temp_up_fname);
				$_SESSION[TMP_FUP12]=$temp_up_fname;
				//最初は仮フォルダに入れておく
				$ret_crop_2=make_crop($_FILES["up_file2"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname,175,175,2);
				//copy($ret_crop_2,DIR_IMG_TMP.$temp_up_fname);
				//copy($_FILES["up_file2"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);
				$input_data[new_thanks_img]=$temp_up_fname;
		}elseif($_SESSION[TMP_FUP12]!=""){
			$input_data[new_thanks_img]=$_SESSION[TMP_FUP12];
		}
	}
	if($input_data[movie_url]!=""){
		if(!isUrl($input_data[movie_url])){
			$errRet[movie_url]="正確なURLで入力してください。例：https://youtu.be/xxxxxx";
		}
	}
	if($input_data[hope_url]!=""){
		if(!isUrl(ROOT_URL."projects/".$input_data[hope_url])){
			$errRet[hope_url]="正確なURLで入力してください。";
		}
	}
	//-------------- ここまで -----------------------------------
	if(count($errRet)==0){
		$input_data[user_no]=$current_user['user_no'];
		foreach($baseData[dbstring] as $key=>$val){
			$dkey[]=$key;
			$dval[]=$input_data[$key];
		}
		if($input_data['no']){//修正
			$dkey[]="user_no";
			$dval[]=$current_user['user_no'];//ユーザーNO

			if($_SESSION[TMP_FUP11]){
				$file_name1=$input_data[no]."_sn_".$_SESSION[TMP_FUP11];
				$dkey[]="cover_img";
				$dval[]=$file_name1;
			}
			if($_SESSION[TMP_FUP12]){
				$file_name2=$input_data[no]."_ti_".$_SESSION[TMP_FUP12];
				$dkey[]="thanks_image";
				$dval[]=$file_name2;
			}
			//print_r_with_pre($input_data);die;

			$ret=$projectDao->upItemData1($dkey,$dval,"no",$input_data['no'],$input_data);

			if($ret){
				//====== 画像 正式アップ & 画像リサイズ作業 ===================
				//サムネイル
				if($_SESSION[TMP_FUP11]){
					//copy(DIR_IMG_TMP.$_SESSION[TMP_FUP11],DIR_IMG_PROJECT.$file_name1);
					//$photoArr=CommonArray::$photosize_array;
					$moto=DIR_IMG_TMP.$_SESSION[TMP_FUP11];
					$newDir=DIR_IMG_PROJECT.$file_name1;
					//画像をリサイズして正式アップ
					resize_image2($moto,$newDir,580,419);
					//古い画像を削除
					if($input_data[cover_img]){
						unlink(DIR_IMG_PROJECT.$input_data[cover_img]);
					}
				}
				//仮画像削除（他の動画タイプの仮画像があるかもしれないので全部削除)
				if($_SESSION[TMP_FUP11]){
					unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP11]);
				}
				//お礼画像
				if($_SESSION[TMP_FUP12]){
					copy(DIR_IMG_TMP.$_SESSION[TMP_FUP12],DIR_IMG_PROJECT.$file_name2);
					//$photoArr2=CommonArray::$photosize_array;
					//$moto2=DIR_IMG_TMP.$_SESSION[TMP_FUP12];
					//$newDir2=DIR_IMG_PROJECT.$file_name2;
					////画像をリサイズして正式アップ
					//resize_image2($moto2,$newDir2,$photoArr2[profile_img][w],$photoArr[profile_img][h]);
					//古い画像を削除
					if($input_data[thanks_image]){
						unlink(DIR_IMG_PROJECT.$input_data[thanks_image]);
					}
				}
				//仮画像削除（他の動画タイプの仮画像があるかもしれないので全部削除)
				if($_SESSION[TMP_FUP12]){
					unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP12]);
				}
				//セッションクリア
				$_SESSION[TMP_FUP11]="";
				$_SESSION[TMP_FUP12]="";
			}
			else{
				//プロジェクト基本情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}
	}else{
		$errRet[top]=$err_msg_str[99];
	}
	
	if(count($errRet)==0){
		//print_r_with_pre($input_data);die;
		//確認ページへ遷移
		header("Location:  {$pagelink_drafting2}?p_no={$project_no}");
	}else{
		$errRet[top]=$err_msg_str[99];
	}
}
elseif(isset($_POST[d2])){
		//$post=arr_preg_change($_POST);
		//$input_data=$post;
	
	$input_data=$_POST;
//	$input_data['no']=intval($input_data['no']);
if(!is_numeric($input_data['no'])){
	header("Location:  {$pagelink_error}");
	exit;
}
	//print_r_with_pre($_POST);die;
	//print_r_with_pre($input_data);die;
	//会員情報をinput_dataに格納
	$member_info=arr_preg_change(getMemberInfo($connect,$current_user['user_no']));

	//該当プロジェクトの支援コース情報の取得
	$ret=getProjectPresentInfo($connect,$project_no);
	$present_info=array();
	$k=1;
	while($project_present_info = mysql_fetch_array($ret)){
		//支援コース情報
		$input_data['return_img'][$k]=$project_present_info['return_img'];
		$k++;
	}
	for($k=1;$k<31;$k++){
		if($input_data['return_min'][$k]!=''){$input_data['return_min'][$k]=GetAlabNum(preg_change($_POST['return_min'][$k]));}
		$input_data['return_title'][$k]=get_check(preg_change($_POST['return_title'][$k]));
		$input_data['return_text'][$k]=get_check(preg_change($_POST['return_text'][$k]));
		$input_data['return_post_flg'][$k]=get_check(preg_change($_POST['return_post_flg'][$k]));
		$input_data['return_year'][$k]=get_check(preg_change($_POST['return_year'][$k]));
		$input_data['return_month'][$k]=get_check(preg_change($_POST['return_month'][$k]));
		if($input_data['return_min'][$k]!=''){$input_data['max_count'][$k]=GetAlabNum(preg_change($_POST['max_count'][$k]));}
	}
//print_r_with_pre($input_data);die;
	//---------------- 入力チェック ---------------------------
	//基本事項
	$baseData=CommonChkArray::$drafting2CheckData;

	$err_msg_str=CommonMessageArray::$input_err_msg;
	$errRet=check($input_data,$baseData,$err_msg_str);

	for($k=1;$k<31;$k++){
		$upload_file='upload_file'.$k;
		if($_FILES[$upload_file]["name"]!=""){if(!in_array($_FILES[$upload_file]["type"],$array_type)){$errRet[upload_file][$k]=$err_msg_str[87];}}
		if(count($errRet)==0){
			if(is_uploaded_file($_FILES[$upload_file]["tmp_name"])){
					$temp_up_fname = $k.date("His",time())."ri_".$_FILES[$upload_file]["name"];//
					$temp_up_fname=SBC_DBC($temp_up_fname,0);
					$temp_up_fname=html_tag_chg($temp_up_fname);
					$_SESSION[TMP_FUP][$k]=$temp_up_fname;
					//最初は仮フォルダに入れておく
					$ret_crop=make_crop($_FILES[$upload_file]['tmp_name'],DIR_IMG_TMP.$temp_up_fname,280,165,2);
					//copy($_FILES[$upload_file]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);
					$input_data['return_img'][$k]=$temp_up_fname;
			}elseif($_SESSION[TMP_FUP][$k]!=""){
				$input_data['return_img'][$k]=$_SESSION[TMP_FUP][$k];
			}
		}
		if($input_data['return_min'][$k]<500&&$input_data['return_min'][$k]!=''){$errRet[return_min][$k]="500円以上で入力してください。";}
		if($input_data['max_count'][$k]>9999){$errRet[max_count][$k]="9999以下で入力してください。";}
	}
	//-------------- ここまで -----------------------------------
	if(count($errRet)==0){
		$input_data[user_no]=$current_user['user_no'];
		foreach($baseData[dbstring] as $key=>$val){
			$dkey[]=$key;
			$dval[]=$input_data[$key];
		}
		if($input_data['no']){//修正
			$dkey[]="user_no";
			$dval[]=$current_user['user_no'];//ユーザーNO

			//該当プロジェクトの支援コース情報の取得
			$ret=getProjectPresentInfo($connect,$project_no);
			$present_info=array();
			$k=1;
			while($project_present_info = mysql_fetch_array($ret)){
				//支援コース情報
				$input_data['present_num'][$k]=$project_present_info['present_no'];
				$input_data['return_img'][$k]=$project_present_info['return_img'];
				$k++;
			}
			//リターン画像
			for($r=1;$r<31;$r++){
				if($_SESSION[TMP_FUP][$r]){
					$file_name[$r]=$input_data[no]."_ri_".$_SESSION[TMP_FUP][$r];
					$input_data['new_return_img'][$r]=$file_name[$r];
				}else{
					$input_data['new_return_img'][$r]=$input_data['return_img'][$r];
				}
			}
			$ret=$projectDao->upItemData2($dkey,$dval,"no",$input_data['no'],$input_data);

			if($ret){
				//====== 画像 正式アップ & 画像リサイズ作業 ===================
				//リターン画像
				for($r=1;$r<31;$r++){
					if($_SESSION[TMP_FUP][$r]){
						copy(DIR_IMG_TMP.$_SESSION[TMP_FUP][$r],DIR_IMG_PROJECT.$file_name[$r]);
						//古い画像を削除
						if($input_data['return_img'][$r]){
							unlink(DIR_IMG_PROJECT.$input_data['return_img'][$r]);
						}
					}
					//仮画像削除（他の動画タイプの仮画像があるかもしれないので全部削除)
					if($_SESSION[TMP_FUP][$r]){
						unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP][$r]);
					}
					//セッションクリア
					$_SESSION[TMP_FUP][$r]="";
				}
			}
			else{
				//プロジェクト基本情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}
	}else{
		$errRet[top]=$err_msg_str[99];
	}
	

	if(count($errRet)==0){
		//$input_data['no']=$project_info['no'];
		//print_r_with_pre($input_data);die;
		//確認ページへ遷移
		header("Location:  {$pagelink_drafting3}?p_no={$project_no}");
	}else{
		$errRet[top]=$err_msg_str[99];
	}
}
elseif(isset($_POST[d3])){
	$post=arr_preg_change($_POST);
	$input_data=$post;
//	$input_data['no']=intval($input_data['no']);
if(!is_numeric($input_data['no'])){
	header("Location:  {$pagelink_error}");
	exit;
}
	$input_data['project_text']=html_tag_chg($input_data['project_text']);
	//$input_data=$_POST;

	//会員情報をinput_dataに格納
	$member_info=getMemberInfo($connect,$current_user['user_no']);

	//---------------- 入力チェック ---------------------------
	//基本事項
	$baseData=CommonChkArray::$drafting3CheckData;

	$err_msg_str=CommonMessageArray::$input_err_msg;
	$errRet=check($input_data,$baseData,$err_msg_str);

	//-------------- ここまで -----------------------------------
	if(count($errRet)==0){
		$input_data[user_no]=$current_user['user_no'];
		foreach($baseData[dbstring] as $key=>$val){
			$dkey[]=$key;
			$dval[]=$input_data[$key];
		}
		if($input_data['no']){//修正
			$dkey[]="user_no";
			$dval[]=$current_user['user_no'];//ユーザーNO
//print_r_with_pre($input_data);die;
			$ret=$projectDao->upItemData3($dkey,$dval,"no",$input_data['no'],$input_data);
			if($ret){
			}
			else{
				//プロジェクト基本情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}
	}else{
		$errRet[top]=$err_msg_str[99];
	}
	
	if(count($errRet)==0){
		//$input_data['no']=$project_info['no'];
		//print_r_with_pre($input_data);die;
		//確認ページへ遷移
		header("Location:  {$pagelink_drafting4}?p_no={$project_no}");
	}else{
		$errRet[top]=$err_msg_str[99];
	}
}
elseif(isset($_POST[d4])){
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
//	$input_data['no']=intval($input_data['no']);
if(!is_numeric($input_data['no'])){
	header("Location:  {$pagelink_error}");
	exit;
}
//print_r_with_pre($input_data);
	//---------------- 入力チェック ---------------------------
	//基本事項
	$baseData=CommonChkArray::$drafting4CheckData;
	$err_msg_str=CommonMessageArray::$input_err_msg;
	$errRet=check($input_data,$baseData,$err_msg_str);

	if($input_data[blog_url_1]!=""){
		if(!isUrl($input_data[blog_url_1])){
			$errRet[blog_url_1]="正確なURLで入力してください。";
		}
	}
	if($input_data[blog_url_2]!=""){
		if(!isUrl($input_data[blog_url_2])){
			$errRet[blog_url_2]="正確なURLで入力してください。";
		}
	}
	if($input_data[blog_url_3]!=""){
		if(!isUrl($input_data[blog_url_3])){
			$errRet[blog_url_3]="正確なURLで入力してください。";
		}
	}
	//-------------- ここまで -----------------------------------
	if(count($errRet)==0){
		$input_data[user_no]=$current_user['user_no'];
		foreach($baseData[dbstring] as $key=>$val){
			$dkey[]=$key;
			$dval[]=$input_data[$key];
		}
		if($input_data['no']){//修正
			$dkey[]="user_no";
			$dval[]=$current_user['user_no'];//ユーザーNO
//print_r_with_pre($input_data);die;
			$ret=$projectDao->upItemData4($dkey,$dval,"no",$input_data['no'],$input_data);
			if($ret){
			}
			else{
				//プロジェクト基本情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}
	}else{
		$errRet[top]=$err_msg_str[99];
	}
	if(count($errRet)==0){
		//$input_data['no']=$project_info['no'];
		//print_r_with_pre($input_data);die;
		//確認ページへ遷移
		header("Location:  {$pagelink_drafting5}?p_no={$project_no}");
	}else{
		$errRet[top]=$err_msg_str[99];
	}
}
elseif(isset($_POST[d5])){
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
//	$input_data['no']=intval($input_data['no']);
if(!is_numeric($input_data['no'])){
	header("Location:  {$pagelink_error}");
	exit;
}
	//print_r_with_pre($_POST);die;
	//print_r_with_pre($input_data);die;
	//$input_data['card_number']=$_POST['card_number'];
	//$input_data['branch_bank_code']=$_POST['branch_bank_code'];
	//会員情報をinput_dataに格納
	$member_info=getMemberInfo($connect,$current_user['user_no']);
	$input_data['profile_img']=$member_info['profile_img'];
	$input_data['hp_url']=$member_info['hp_url'];
	if($input_data['bank_name_txt']!=""){
		$input_data['bank_name']=$input_data['bank_name_txt'];
	}

//print_r_with_pre($input_data);
	//---------------- 入力チェック ---------------------------
	//基本事項
	$baseData=CommonChkArray::$drafting5CheckData;

	$err_msg_str=CommonMessageArray::$input_err_msg;
	$errRet=check($input_data,$baseData,$err_msg_str);
		//プロフィール画像
	if($_FILES["up_file3"]["name"]!=""){if(!in_array($_FILES["up_file3"]["type"],$array_type)){$errRet[profile_img]=$err_msg_str[87];}}
	if(count($errRet)==0){
		if(is_uploaded_file($_FILES["up_file3"]["tmp_name"])){
				$temp_up_fname = $i.date("His",time())."pi_".$_FILES["up_file3"]["name"];//
				$temp_up_fname=SBC_DBC($temp_up_fname,0);
				$temp_up_fname=html_tag_chg($temp_up_fname);
				$_SESSION[TMP_FUP13]=$temp_up_fname;
				//最初は仮フォルダに入れておく
				$ret_crop=make_crop($_FILES["up_file3"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname,200,200,2);
				//copy($_FILES["up_file3"]['tmp_name'],DIR_IMG_TMP.$temp_up_fname);
				$input_data[profile_img]=$temp_up_fname;
		}elseif($_SESSION[TMP_FUP13]!=""){
			$input_data[profile_img]=$_SESSION[TMP_FUP13];
		}
	}
	//-------------- ここまで -----------------------------------
	if(count($errRet)==0){
		$input_data[user_no]=$current_user['user_no'];
		foreach($baseData[dbstring] as $key=>$val){
			$dkey[]=$key;
			$dval[]=$input_data[$key];
		}
		if($input_data['no']){//修正
			$dkey[]="user_no";
			$dval[]=$current_user['user_no'];//ユーザーNO

			if($_SESSION[TMP_FUP13]){
				$file_name3=$input_data[no]."_pi_".$_SESSION[TMP_FUP13];
				//$dkey[]="profile_img";
				//$dval[]=$file_name;
				$input_data[new_profile_img]=$file_name3;
			}else{
				//会員情報をinput_dataに格納
				$member_info=getMemberInfo($connect,$current_user['user_no']);
				$input_data['profile_img']=$member_info['profile_img'];
				$input_data[new_profile_img]=$input_data['profile_img'];
			}
			//print_r_with_pre($input_data);die;

			$ret=$projectDao->upItemData5($dkey,$dval,"no",$input_data['no'],$input_data);

			if($ret){
				//====== 画像 正式アップ & 画像リサイズ作業 ===================
				//プロフィール画像
				if($_SESSION[TMP_FUP13]){
					copy(DIR_IMG_TMP.$_SESSION[TMP_FUP13],DIR_IMG_ACTRESS.$file_name3);
					//$photoArr3=CommonArray::$photosize_array;
					//$moto3=DIR_IMG_TMP.$_SESSION[TMP_FUP13];
					//$newDir3=DIR_IMG_PROJECT.$file_name3;
					////画像をリサイズして正式アップ
					//resize_image2($moto2,$newDir2,$photoArr2[profile_img][w],$photoArr[profile_img][h]);
					//古い画像を削除
					if($input_data[profile_img]){
						unlink(DIR_IMG_ACTRESS.$input_data[profile_img]);
					}
				}
				//仮画像削除（他の動画タイプの仮画像があるかもしれないので全部削除)
				if($_SESSION[TMP_FUP13]){
					unlink(DIR_IMG_TMP.$_SESSION[TMP_FUP13]);
				}
				//セッションクリア
				$_SESSION[TMP_FUP13]="";
			}
			else{
				//プロジェクト基本情報の保存エラーです
				$errRet[top]=$err_msg_str[80];
			}
		}
	}else{
		$errRet[top]=$err_msg_str[99];
	}
	if(count($errRet)==0){
		//確認ページへ遷移
		header("Location:  {$pagelink_drafting6}?p_no={$project_no}");
	}else{
		$errRet[top]=$err_msg_str[99];
	}
}
else{
}

mysql_close($connect);

//Credit　Clientipのセット
if($lang=="ja"){
	$clientip=CDREDIX_CLIENTIP_JA;
}else{
	$clientip=CDREDIX_CLIENTIP_OTHER;
}

//Clientip情報
$smarty->assign("clientip", $clientip);
//ログイン情報
$smarty->assign("current_user", $current_user);
$smarty->assign("present_list", $present_list);
$smarty->assign("share_cnt", $share_cnt);
// カテゴリー
$smarty->assign("array_category", $array_category);
// リターンのお届け予定年
$smarty->assign("array_post_year", $array_post_year);
// リターンのお届け予定月
$smarty->assign("array_post_month", $array_post_month);
//プロジェクト情報
$smarty->assign("project_info", $project_info);
//支援情報
//print_r_with_pre($present_info);
$smarty->assign("present_info", $present_info);
//都道府県リスト
$smarty->assign("array_area", $array_area);
//エラー情報
$smarty->assign("err_msg", $errRet);
//入力情報
$smarty->assign("input_data", $input_data);
$smarty->assign("youtube_id", $youtube_id);
$smarty->assign("vimemo_id", $vimemo_id);
$smarty->assign("preview_flg", $preview_flg);

?>