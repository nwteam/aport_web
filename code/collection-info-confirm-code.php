<?php
header('Content-Type: text/html; charset=utf-8');
require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
$commonDao = new CommonDao();

$current_user = getMemberSession();

$project_no = $_SESSION["project_no"];
$present_no = $_SESSION["present_no"];
$payment_method = $_SESSION["payment_method"];

//決済情報(金額)
$invest_amount = $_SESSION["invest_amount"];
$amount_plus = $_SESSION["amount_plus"];
$total_amount = $invest_amount + $amount_plus;
if($payment_method=="Credit"){
	//決済情報(credit)
	$card_no = $_SESSION["card_no"];
	$expdate_month = $_SESSION["expdate_month"];
	$expdate_year = $_SESSION["expdate_year"];
	$hld_name = $_SESSION["hld_name"];
	$security_code = $_SESSION["security_code"];
}elseif($payment_method=="cvs"){
	//決済情報(cvs)
	$convenience = $_SESSION["convenience"];
	$ks_name_1 = $_SESSION["ks_name_1"];
	$ks_name_2 = $_SESSION["ks_name_2"];
	$customername=$ks_name_1.$ks_name_2;
	$ks_name_kana_1 = $_SESSION["ks_name_kana_1"];
	$ks_name_kana_2 = $_SESSION["ks_name_kana_2"];
	$customerkana=$ks_name_kana_1.$ks_name_kana_2;
	$ks_tel = $_SESSION["ks_tel"];
}elseif($payment_method=="payeasy"){
	//決済情報(payeasy)
	$ks_name_1 = $_SESSION["ks_name_1"];
	$ks_name_2 = $_SESSION["ks_name_2"];
	$customername=$ks_name_1.$ks_name_2;
	$ks_name_kana_1 = $_SESSION["ks_name_kana_1"];
	$ks_name_kana_2 = $_SESSION["ks_name_kana_2"];
	$customerkana=$ks_name_kana_1.$ks_name_kana_2;
	$ks_tel = $_SESSION["ks_tel"];
}else{
	//決済情報(credit)
	$card_no = "";
	$expdate_month = "";
	$expdate_year = "";
	$hld_name = "";
	$security_code = "";
	//決済情報(cvs,payeasy)
	$convenience = "";
	$ks_name_1 = "";
	$ks_name_2 = "";
	$customername="";
	$ks_name_kana_1 = "";
	$ks_name_kana_2 = "";
	$customerkana="";
	$ks_tel = "";
}

//ログインチェック
if(!$current_user){
	header("Location:  {$pagelink_login}");
	exit;
}

//DB接続
$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

//プロジェクト情報の取得
if($project_no){
	$project_info=getProjectInfo($connect,$project_no);
	if(!is_array($project_info)){
		header("Location:  {$pagelink_error}");
		exit;
	}
	$now_summary=$project_info[now_summary];
	$now_supporter=$project_info[now_supporter];

}else{
		header("Location:  {$pagelink_error}");
		exit;
}
//選択した支援情報の取得
if($present_no){
	$selected_present_info=getPresentInfo($connect,$present_no);
	if(!is_array($selected_present_info)){
		header("Location:  {$pagelink_error}");
		exit;
	}
}else{
		header("Location:  {$pagelink_error}");
		exit;
}
//起案者情報の取得
$drafter_info=getDrafterInfo($connect,$project_info[project_owner]);
if(!is_array($drafter_info)){
	header("Location:  {$pagelink_error}");
	exit;
}
//会員情報の取得
if ($current_user['user_no']!=""){
	$member_info=getMemberInfo($connect,$current_user[user_no]);
}
$mailaddress=$member_info[email];
if(!is_array($member_info)){
	header("Location:  {$pagelink_error}");
	exit;
}

if(isset($_POST[sbm_send])){	//送信ボタンクリック時
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;

	//---------------- 入力チェック ---------------------------
	//基本事項
	//$baseData=CommonChkArray::$investCheckData;

	$err_msg_str=CommonMessageArray::$input_err_msg;
	//$errRet1=check($input_data,$baseData,$err_msg_str);

	//お届け先チェック
	if ($input_data['post_user_name']==""&&$selected_present_info['return_post_flg']=="1"){$errRet1[post_address]=$err_msg_str[86];}
	if ($input_data['post_zip_code']==""&&$selected_present_info['return_post_flg']=="1"){$errRet1[post_address]=$err_msg_str[86];}
	if ($input_data['post_add_1']==""&&$selected_present_info['return_post_flg']=="1"){$errRet1[post_address]=$err_msg_str[86];}
	if ($input_data['post_add_2']==""&&$selected_present_info['return_post_flg']=="1"){$errRet1[post_address]=$err_msg_str[86];}
	//if ($input_data['post_add_3']==""&&$selected_present_info['return_post_flg']=="1"){$errRet1[post_address]=$err_msg_str[86];}
	if ($input_data['post_tel']==""&&$selected_present_info['return_post_flg']=="1"){$errRet1[post_address]=$err_msg_str[86];}

	//-------------- 入力チェック ここまで -----------------------------------

	//-------------- 決済処理 ここまで -----------------------------------
	if(count($errRet1)==0){
			//---------------- 決済処理 ---------------------------
			$sql = "UPDATE sf_order_id SET id = LAST_INSERT_ID(id+1)";
			$result = mysql_query("set names utf8");
			$result = mysql_query($sql,$connect);
			if(!$result){
				mysql_close($connect);
				header("Location:  {$pagelink_error}");
				exit;
			}

			$sql = "select last_insert_id() as id";
			$result = mysql_query("set names utf8");
			$result = mysql_query($sql,$connect);
			if(!$result){
				mysql_close($connect);
				header("Location:  {$pagelink_error}");
				exit;
			}

			$row=mysql_fetch_assoc($result);
			$orderId = "S" . str_pad( $row['id'], 19,"0",STR_PAD_LEFT);

			if($payment_method=="Credit"){
				$payMethod=1;//1：一括 2：分割 3：ボーナス一括 4：ボーナス分割 5：リボ
				$paytimes=1;
				$expire=substr($expdate_year, 2, 2).$expdate_month;
				$itemCd = str_pad($present_no, 7, "0", STR_PAD_LEFT);
//echo $itemCd;die;
				$ks_ret=exePayCredit($itemCd,$total_amount,$orderId,"",$card_no,$expire,$security_code,$payMethod,$paytimes);
		//print_r_with_pre($ks_ret);die;
			}
			elseif($payment_method=="cvs"){
				$ks_ret=exePaycvs($convenience,$total_amount,$orderId,$customername,$customerkana,$ks_tel,$mailaddress);
				$receipt_no=$ks_ret->getReceiptNo();//受付番号
				$payment_term=$ks_ret->getPaymentTerm();//支払期限
				$conf_no=$ks_ret->getConfNo();//確認番号
			}
			elseif($payment_method=="payeasy"){
				$ks_ret=exePayPayeasy($total_amount,$orderId,$customername,$customerkana,$ks_tel,$mailaddress);
				$receipt_no=$ks_ret->getEncryptReceiptNo();//暗号化決済番号
				$payment_term=$ks_ret->getPaymentTerm();//支払期限
				$conf_no=$ks_ret->getConfNo();//確認番号
				$bk_code=$ks_ret->getBkCode();//収納機関番号
				$cust_id=$ks_ret->getCustId();//お客様番号
		//print_r_with_pre($ks_ret);
			}
			else{
				header("Location:  {$pagelink_error}");
				exit;
			}
			if($ks_ret!='1'&&$ks_ret!='2'&&$ks_ret!='3'){
				//登録処理
				date_default_timezone_set("Asia/Tokyo");
				//トランザクションをはじめる準備
				$Query = "set autocommit = 0";
				mysql_query( $Query, $connect );

				//トランザクション開始
				$Query = "begin";
				mysql_query( $Query, $connect );

				//支援情報
					$data=array();
					$data[project_no]=$project_no;
					$data[member_id]=$current_user[user_no];
					$data[order_id]=$orderId;
					$data[invest_amount]=$total_amount;
					$data[present_no]=$present_no;
					$data[payment_method]=$payment_method;
					$data[create_date]=date('Y-m-d H:i:s');
					if ($payment_method=="Credit") {
						$data[status]="1";
					}else{
						$data[status]="0";
					}

					$sql=$commonDao->MakeInsertSQL("sf_invest",$data);
					$result = mysql_query("set names utf8");
					$result = mysql_query($sql, $connect);
					//$invest_no = mysql_insert_id();

				//プロジェクト情報更新
				if( $result === true){
						if($payment_method=="Credit"){
							$data=array();
							$data[now_summary]=$now_summary+$total_amount;
							$data[now_supporter]=$now_supporter+1;

							$where1[no]=$project_no;

							$sql=$commonDao->MakeUpdateSQL("sf_project",$data,$where1);

							$result = mysql_query("set names utf8");
							$result = mysql_query($sql, $connect);

							if ($selected_present_info[invest_limit]!="9999"){
								$data_p=array();
								$data_p[invest_limit]=$selected_present_info[invest_limit]-1;

								$where_p[project_no]=$project_no;
								$where_p[present_no]=$present_no;

								$sql_p=$commonDao->MakeUpdateSQL("sf_prj_present",$data_p,$where_p);

								$result_p = mysql_query("set names utf8");
								$result_p = mysql_query($sql_p, $connect);
							}
						}
					//会員情報更新
						if( $result === true && $payment_method!="Credit"){
							$data=array();
							$data[ks_name_1]=$_SESSION["ks_name_1"];
							$data[ks_name_2]=$_SESSION["ks_name_2"];
							$data[ks_name_kana_1]=$_SESSION["ks_name_kana_1"];
							$data[ks_name_kana_2]=$_SESSION["ks_name_kana_2"];
							$data[ks_tel]=$_SESSION["ks_tel"];

							$where2[user_no]=$current_user[user_no];

							$sql=$commonDao->MakeUpdateSQL("sf_member",$data,$where2);

							$result = mysql_query("set names utf8");
							$result = mysql_query($sql, $connect);

							if ($selected_present_info[invest_limit]!="9999"){
								$data_p=array();
								$data_p[invest_limit]=$selected_present_info[invest_limit]-1;

								$where_p[project_no]=$project_no;
								$where_p[present_no]=$present_no;

								$sql_p=$commonDao->MakeUpdateSQL("sf_prj_present",$data_p,$where_p);

								$result_p = mysql_query("set names utf8");
								$result_p = mysql_query($sql_p, $connect);
							}
							
							if( $result === true ){
								//支援者コメント
								$insert_data[project_no]=$_SESSION["project_no"];
								$insert_data[member_id]=$current_user['user_no'];
								$insert_data[order_id]=$orderId;
								$insert_data[comment]="";
								$insert_data[create_date]=date("Y-m-d H:i:s");
								$insert_data[del_flg]="2";

								$sql=$commonDao->MakeInsertSQL("sf_comment",$insert_data);

								$result = mysql_query("set names utf8");
								$result = mysql_query($sql, $connect);
								//コミット
								$Query = "commit";
								mysql_query( $Query, $connect );
								mysql_close($connect);

								//メール送信(payeasy,コンビニ)
								$smarty->assign("d_data", $drafter_info);
								$smarty->assign("m_data", $member_info);
								$smarty->assign("p_data", $project_info);
								$smarty->assign("C_INQUIRY_EMAIL", $C_INQUIRY_EMAIL);
								$smarty->assign("total_amount", $total_amount);
								$smarty->assign("pre_data", $selected_present_info);
								$c_invest_limit=substr(str_replace("-","",$project_info[invest_limit]),0,8);
								if (substr($payment_term,0,8)>$c_invest_limit){
									$payment_term=$c_invest_limit."235959";
								}
								$smarty->assign("payment_term", $payment_term);

								if($payment_method=="payeasy"){
									$smarty->assign("conf_no", $conf_no);//確認番号
									$smarty->assign("bk_code", $bk_code);//収納機関番号
									$smarty->assign("cust_id", substr_replace($cust_id,'****',-4));//お客様番号
									$mailBody = $smarty->fetch($lang."/mail/m-invest-project-payeasy.tpl");
								}
								if($payment_method=="cvs"){
									$smarty->assign("conf_no", $conf_no);//確認番号
									$smarty->assign("receipt_no", $receipt_no);//お客様番号
									$smarty->assign("cvs_name", $array_cvs[$convenience]);
									$mailBody = $smarty->fetch($lang."/mail/m-invest-project-cvs.tpl");
								}
								//$mail_subject=getMailSubject(6);
								$subject = "【A-port】".date_format_php($payment_term,'%m月%d日')."までの支払い手続きのお願い";
								send_mail($member_info[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);

								//完了ページへ遷移
								$comment_time=urlencode($insert_data[create_date]);
								header("Location:  {$pagelink_collection_info_complete}?rep_no={$receipt_no}&pt={$payment_term}&ct={$comment_time}");
								exit;
							}else{
								//ロールバック
								$Query = "rollback";
								mysql_query( $Query, $connect );
								//print "エラーです3";
							}
						}elseif( $result === true && $payment_method=="Credit"){
							//支援者コメント
							$insert_data[project_no]=$_SESSION["project_no"];
							$insert_data[member_id]=$current_user['user_no'];
							$insert_data[order_id]=$orderId;
							$insert_data[comment]="";
							$insert_data[create_date]=date("Y-m-d H:i:s");

							$sql=$commonDao->MakeInsertSQL("sf_comment",$insert_data);

							$result = mysql_query("set names utf8");
							$result = mysql_query($sql, $connect);

							//コミット
								$Query = "commit";
								mysql_query( $Query, $connect );
								mysql_close($connect);

								//メール送信(カード)
								$smarty->assign("d_data", $drafter_info);
								$smarty->assign("m_data", $member_info);
								$smarty->assign("p_data", $project_info);
								$smarty->assign("a_area", $array_area);
								$smarty->assign("C_INQUIRY_EMAIL", $C_INQUIRY_EMAIL);
								$smarty->assign("total_amount", $total_amount);
								$smarty->assign("pre_data", $selected_present_info);
								//支援者へ
								$mail_subject=getMailSubject(6);
								$subject = "【A-port】「".$project_info[public_title]."」".$mail_subject;
								$mailBody = $smarty->fetch($lang."/mail/m-invest-project-complete.tpl");
								send_mail($member_info[email], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
								//起案者へ
								//$mail_subject=getMailSubject(10);
								$subject = "【A-port】「".$project_info[public_title]."」の支援者が増えました";
								$mailBody = $smarty->fetch($lang."/mail/m-project-new-invest.tpl");
								send_mail($drafter_info['email'], $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody,"",$C_ADMIN_EMAIL);
								//完了ページへ遷移
								$comment_time=urlencode($insert_data[create_date]);
								header("Location:  {$pagelink_collection_info_complete}?ct={$comment_time}");
								exit;
						}
						else{
							//ロールバック
							$Query = "rollback";
							mysql_query( $Query, $connect );
							//print "エラーです2";
						}
				}else{
							//ロールバック
							$Query = "rollback";
							mysql_query( $Query, $connect );
							//print "エラーです1";
				}
		}//決済処理終了
		else{
			//$errRet1[top]=$err_msg_str[99];
			if($ks_ret=='1'){
				$errRet1[charge_err_msg]="通信エラーです、決済を完了する事が出来ませんでした。";
			}
			if($ks_ret=='2'){
				$errRet1[charge_err_msg]="決済情報に誤りがあるために、決済を完了する事が出来ませんでした。";
			}
			if($ks_ret=='3'){
				$errRet1[charge_err_msg]="決済実行エラーです、決済を完了する事が出来ませんでした。";
			}
		}
	}else{
		$errRet1[top]=$err_msg_str[99];
	}
}elseif(isset($_POST[back])){
	$_SESSION["project_no"] = $project_no;
	$_SESSION["present_no"] = $present_no;
	$_SESSION["payment_method"] = $payment_method;

	//決済情報(金額)
	$_SESSION["invest_amount"] = $invest_amount;
	$_SESSION["amount_plus"] = $amount_plus;

	if($payment_method=="Credit"){
		//決済情報(credit)
		$_SESSION["card_no"] = $card_no;
		$_SESSION["expdate_month"] = $expdate_month;
		$_SESSION["expdate_year"] = $expdate_year;
		$_SESSION["hld_name"] = $hld_name;
		$_SESSION["security_code"] = $security_code;
		//決済情報(cvs,payeasy)
		$_SESSION["ks_name_1"] = "";
		$_SESSION["ks_name_2"] = "";
		$_SESSION["ks_name_kana_1"] = "";
		$_SESSION["ks_name_kana_2"] = "";
		$_SESSION["ks_tel"] = "";
	}elseif($payment_method=="cvs"){
		//決済情報(credit)
		$_SESSION["card_no"] = "";
		$_SESSION["expdate_month"] = "";
		$_SESSION["expdate_year"] = "";
		$_SESSION["hld_name"] = "";
		$_SESSION["security_code"] = "";
		//決済情報(cvs)
		$_SESSION["convenience"] = $convenience;
		$_SESSION["ks_name_1"] = $ks_name_1;
		$_SESSION["ks_name_2"] = $ks_name_2;
		$_SESSION["ks_name_kana_1"] = $ks_name_kana_1;
		$_SESSION["ks_name_kana_2"] = $ks_name_kana_2;
		$_SESSION["ks_tel"] = $ks_tel;
	}elseif($payment_method=="payeasy"){
		//決済情報(credit)
		$_SESSION["card_no"] = "";
		$_SESSION["expdate_month"] = "";
		$_SESSION["expdate_year"] = "";
		$_SESSION["hld_name"] = "";
		$_SESSION["security_code"] = "";
		//決済情報(payeasy)
		$_SESSION["convenience"] = "";
		$_SESSION["ks_name_1"] = $ks_name_1;
		$_SESSION["ks_name_2"] = $ks_name_2;
		$_SESSION["ks_name_kana_1"] = $ks_name_kana_1;
		$_SESSION["ks_name_kana_2"] = $ks_name_kana_2;
		$_SESSION["ks_tel"] = $ks_tel;
	}else{
		//決済情報(credit)
		$_SESSION["card_no"] = "";
		$_SESSION["expdate_month"] = "";
		$_SESSION["expdate_year"] = "";
		$_SESSION["hld_name"] = "";
		$_SESSION["security_code"] = "";
		//決済情報(cvs,payeasy)
		$_SESSION["convenience"] = "";
		$_SESSION["ks_name_1"] = "";
		$_SESSION["ks_name_2"] = "";
		$_SESSION["ks_name_kana_1"] = "";
		$_SESSION["ks_name_kana_2"] = "";
		$_SESSION["ks_tel"] = "";
	}
	header("Location:  {$pagelink_collection_info_input}");
}else{
}

mysql_close($connect);

//Credit　Clientipのセット
if($lang=="ja"){
	$clientip=CDREDIX_CLIENTIP_JA;
}else{
	$clientip=CDREDIX_CLIENTIP_OTHER;
}

//ログイン情報
$smarty->assign("current_user", $current_user);
//Clientip情報
$smarty->assign("clientip", $clientip);
//都道府県リスト
$smarty->assign("array_area", $array_area);
$smarty->assign("array_cvs", $array_cvs);
//エラー情報
$smarty->assign("err_msg1", $errRet1);
//入力情報
$smarty->assign("input_data", $input_data);
//プロジェクト情報
$smarty->assign("project_info", $project_info);
//選択した支援情報
$smarty->assign("selected_present_info", $selected_present_info);

//起案者情報取得
$smarty->assign("drafter_info", $drafter_info);
//会員情報取得
$smarty->assign("member_info", $member_info);

//支払方法
$smarty->assign("payment_method", $payment_method);
//決済情報(金額)
$smarty->assign("total_amount", $total_amount);
//決済情報(credit)
$smarty->assign("card_no", $card_no);
$smarty->assign("expdate_month", $expdate_month);
$smarty->assign("expdate_year", $expdate_year);
$smarty->assign("hld_name", $hld_name);
$smarty->assign("security_code", $security_code);
//決済情報(cvs,payeasy)
$smarty->assign("convenience", $convenience);
$smarty->assign("ks_name_1", $ks_name_1);
$smarty->assign("ks_name_2", $ks_name_2);
$smarty->assign("ks_name_kana_1", $ks_name_kana_1);
$smarty->assign("ks_name_kana_2", $ks_name_kana_2);
$smarty->assign("ks_tel", $ks_tel);

?>