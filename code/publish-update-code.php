<?php
	header('Content-Type: text/html; charset=utf-8');
	require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
	require '../../libs/Bcrypt.class.php';
	session_cache_limiter('no-cache, must-revalidate');
	$commonDao = new CommonDao();

	$current_user = getMemberSession();

	//ログインチェック
	if(!$current_user){
		header("Location:  {$pagelink_index}");
		exit;
	}

	$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);

	$project_no = $_GET['p_no'];
	if($project_no!=""){
		if(!is_numeric($project_no)){
			header("Location:  {$pagelink_error}");
			exit;
		}
	}else{
			header("Location:  {$pagelink_error}");
			exit;
	}
	//会員情報取得
	$member_info=getMemberInfo($connect,$current_user['user_no']);
	//Project情報取得
	$project_info=getMyProjectInfo($connect,$project_no,$current_user['user_no']);
	$project_no = $project_info['no'];
	if($project_no!=""){
		if(!is_numeric($project_no)){
			header("Location:  {$pagelink_error}");
			exit;
		}
	}else{
			header("Location:  {$pagelink_error}");
			exit;
	}
	//print_r_with_pre($project_info);
	if(isset($_POST[sbm_update])){
	}
	else{

	}

  // update_msg
  $query_comment  = " select a.* ";
  $query_comment .= "   from sf_update_msg a ";
  $query_comment .= "  where 1 ";
  $query_comment .= "    and a.project_no = %d";
  $query_comment .= "    and a.del_flg = '0'";
  $query_comment .= "  order by a.update_id desc ";
	$query_comment = sprintf($query_comment,$project_no);

  $result_comment = mysql_query("set names utf8");
  $result_comment = mysql_query($query_comment, $connect);

  $update_list=array();
  while($data = mysql_fetch_array($result_comment)){
		if($data[user_no]!=$current_user['user_no']){
				header("Location:  {$pagelink_error}");
				exit;
		}
	  $update_list[]=$data;
  }
	$update_cnt=count($update_list);
	//for($i=0;$i<$update_cnt;$i++){
	//	$update_list[vol]=$update_cnt-$i;
	//}
	//echo $update_cnt;
	//print_r_with_pre($update_list);

	mysql_close($connect);

	//ログイン情報
	$smarty->assign("current_user", $current_user);
	//都道府県リスト
	$smarty->assign("array_area", $array_area);
	//エラー情報
	$smarty->assign("err_msg", $errRet);
	//入力情報
	$smarty->assign("input_data", $input_data);
	//COMMENT情報
	$smarty->assign("update_list",$update_list);
	//Project情報
	$smarty->assign("project_info",$project_info);
	//起案者情報
	$smarty->assign("member_info",$member_info);
	$smarty->assign("update_cnt",$update_cnt);
?>