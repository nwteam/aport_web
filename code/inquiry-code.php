<?php
header('Content-Type: text/html; charset=utf-8');
require_once sprintf("%s/dao/CommonDao.class.php", MODEL_PATH);
$current_user = getMemberSession();
$smarty->assign("current_user", $current_user);

//DB接続
$connect = sql_connect($db_host, $db_user, $db_pass, $db_name);
$commonDao = new CommonDao();

	if($_POST){	//送信ボタンクリック時
		//$_SESSION["input_data"]=$_POST;
		//$input_data=$_SESSION["input_data"];
		$post=post_check(arr_preg_change($_POST));
		$input_data=$post;
		//print_r_with_pre($input_data);

		//---------------- 入力チェック ---------------------------
		//基本事項
		$baseData=CommonChkArray::$inquiryCheckData;

		$err_msg_str=CommonMessageArray::$input_err_msg;
		$errRet=check($input_data,$baseData,$err_msg_str);

		//-------------- ここまで -----------------------------------
		//規約
		if(! $input_data[rulesParticipant]){
			$errRet[rulesParticipant]=$err_msg_str[28];
		}
		if(count($errRet)==0){

			////メール送信
			//$smarty->assign("input_data", $input_data);

			//$mail_subject=getMailSubject(2);

			//$subject = "[" . $str_site_title . "]　お問い合わせがありました。";
			//$mailBody = $smarty->fetch($lang."/mail/m-inquiry.tpl");
			//$mailfrom="From:" .mb_encode_mimeheader(MAIL_FROM_NAME) ."<".MAIL_FROM.">";
			//send_mail($C_INQUIRY_EMAIL, $C_SEND_EMAIL, $C_SEND_EMAIL_NAME, $subject, $mailBody);

			//登録処理

			date_default_timezone_set("Asia/Tokyo");
			$insert_data[name]=$input_data["name"];
			$insert_data[email]=$input_data["email"];
			$insert_data[message]=$input_data["message"];
			$insert_data[create_date]=date("Y-m-d H:i:s");

			$sql=$commonDao->MakeInsertSQL("sf_inquiry",$insert_data);
			echo $sql;
			$result = mysql_query("set names utf8");
			$result = mysql_query($sql, $connect);
			//$user_no = mysql_insert_id();

			if( $result === true ){
				//完了ページへ遷移
				header("Location:  $pagelink_inquiry_complete");
			}else{
				$errRet[top]=$err_msg_str[90];
			}
			mysql_close($connect);
		}

	}

	$smarty->assign("err_msg", $errRet);
 	$smarty->assign("input_data", $input_data);


?>