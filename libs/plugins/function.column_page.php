<?php
function smarty_function_column_page($params, $template)
{
	$countpage = $params['countpage'] ? $params['countpage'] : 1;
	$nowpage = $params['nowpage'] ? $params['nowpage'] : 1;

	if( $countpage <= 1 ) {
		return '<li class="page active"><a href="column.php?page=1">1</a></li>';
	}
	
	$string = '';
	if( $nowpage != 1 ) {
		$priepage = max(1, $nowpage-1);
		$string .= '<li class="prev"><a href="column.php?page='.$priepage.'">PREV</a></li><li class="first"><a href="column.php?page=1">FIRST</a></li>';
	}
	
	if( $countpage>5 && ($nowpage+4) >= $countpage ) {
		$i = $countpage-5;
		$string .= '<li class="page gap">…</li>';
	} else {
		$i = 1;	
	}

	$maxpage = min($i*5, $countpage);
	for(; $i<=$maxpage; $i++) {
		if($nowpage==$i) {
			$string .= '<li class="page active"><a href="column.php?page='.$i.'">'.$i.'</a></li>';
		} else {
			$string .= '<li class="page"><a href="column.php?page='.$i.'">'.$i.'</a></li>';
		}
	}
	
	if( $countpage>5 && ($nowpage+4) <= $countpage ) {
		$string .= '<li class="page gap">…</li>';
	}
	
	if( $nowpage != $countpage ) {
		$nextpage = min($countpage, $nowpage+1);
		$string .= '<li class="last"><a href="column.php?page='.$countpage.'">LAST</a></li><li class="next"><a href="column.php?page='.$nextpage.'">NEXT</a></li>';
	}
	
	return $string;
}
