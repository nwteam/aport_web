<?php
function smarty_function_my_page($params, $template)
{
	$countpage = $params['countpage'] ? $params['countpage'] : 1;
	$nowpage = $params['nowpage'] ? $params['nowpage'] : 1;
	$m_page_list = $params['m_page_list'];

	if( $countpage <= 1 ) {
		return '<li class="page active"><a href="list-project.php?page=1&hid_page_list='.$m_page_list.'">1</a></li>';
	}
	
	$string = '';
	if( $nowpage != 1 ) {
		$priepage = max(1, $nowpage-1);
		$string .= '<li class="prev"><a href="list-project.php?page='.$priepage.'&hid_page_list='.$m_page_list.'">PREV</a></li><li class="first"><a href="list-project.php?page=1&hid_page_list='.$m_page_list.'">FIRST</a></li>';
	}
	
	if( $countpage>5 && ($nowpage+4) >= $countpage ) {
		$i = $countpage-5;
		$string .= '<li class="page gap">…</li>';
	} else {
		$i = 1;	
	}

	$maxpage = min($i*5, $countpage);
	for(; $i<=$maxpage; $i++) {
		if($nowpage==$i) {
			$string .= '<li class="page active"><a href="list-project.php?page='.$i.'&hid_page_list='.$m_page_list.'">'.$i.'</a></li>';
		} else {
			$string .= '<li class="page"><a href="list-project.php?page='.$i.'&hid_page_list='.$m_page_list.'">'.$i.'</a></li>';
		}
	}
	
	if( $countpage>5 && ($nowpage+4) <= $countpage ) {
		$string .= '<li class="page gap">…</li>';
	}
	
	if( $nowpage != $countpage ) {
		$nextpage = min($countpage, $nowpage+1);
		$string .= '<li class="last"><a href="list-project.php?page='.$countpage.'&hid_page_list='.$m_page_list.'">LAST</a></li><li class="next"><a href="list-project.php?page='.$nextpage.'&hid_page_list='.$m_page_list.'">NEXT</a></li>';
	}
	
	return $string;
}
