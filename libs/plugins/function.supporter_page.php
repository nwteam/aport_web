<?php
function smarty_function_supporter_page($params, $template)
{
	$countpage = $params['countpage'] ? $params['countpage'] : 1;
	$nowpage = $params['nowpage'] ? $params['nowpage'] : 1;
	$hope_url = $params['hope_url'];

	if( $countpage <= 1 ) {
		return '<li class="page active"><a href="1">1</a></li>';
	}
	
	$string = '';
	if( $nowpage > 2) {
		$priepage = max(1, $nowpage-1);
		$string .= '<li class="prev"><a href="'.$priepage.'">PREV</a></li><li class="first"><a href="1">FIRST</a></li>';
	}elseif ( $nowpage == 2 ) {
		$string .= '<li class="prev"><a href="1">PREV</a></li><li class="first"><a href="1">FIRST</a></li>';
	}
	
	if( $countpage>5 && ($nowpage+4) >= $countpage ) {
		$i = $countpage-5;
		$string .= '<li class="page gap">…</li>';
	} else {
		$i = 1;	
	}

	$maxpage = min($i*5, $countpage);
	for(; $i<=$maxpage; $i++) {
		if($nowpage==$i) {
			$string .= '<li class="page active"><a href="'.$i.'">'.$i.'</a></li>';
		} else {
			$string .= '<li class="page"><a href="'.$i.'">'.$i.'</a></li>';
		}
	}
	
	if( $countpage>5 && ($nowpage+4) <= $countpage ) {
		$string .= '<li class="page gap">…</li>';
	}
	
	if( $nowpage != $countpage ) {
		$nextpage = min($countpage, $nowpage+1);
		$string .= '<li class="last"><a href="'.$countpage.'">LAST</a></li><li class="next"><a href="'.$nextpage.'">NEXT</a></li>';
	}
	
	return $string;
}
